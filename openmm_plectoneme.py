#!/usr/bin/python
# -*- coding: utf-8 -*-
"""@package docstring
The module 'openmm_plectoneme_functions.py' is a collection
of functions used to define a Kremer-Grest polymer with
bending and twisting rigidities.
This module goes with a python script 'openmm_plectoneme.py'
that is an example on how to create and run your model.
The following code (has been developped by Pascal Carrivain)
is distributed under MIT licence.
"""
import data_analysis as da
import getopt
import json
import numpy as np
import openmm_plectoneme_functions as opf
import os
import simtk.openmm as mm
import simtk.unit as unit
import sys
import time


def main(argv):
    seed = 1
    resolution = 10.5
    xmass = 1.0
    helix = 10.5
    dLk = 0.0
    N = 5714
    bp_per_nm3 = 0.0001
    platform_name = "CPU1"
    PBC = False
    mode = "run"
    pstyle = "wca"
    rerun = 0
    initial_conformation = "circular"
    bond = "fene"
    thermostat = "langevin"
    linear = False
    precision = "double"
    start = 0
    every = 100000
    MDS = 20 * every
    print_every = every
    bequilibration = False
    init_with_gaussian = False
    init_with_rigid = False
    init_with_wlc = False
    no_bending = False
    no_twist = False
    use_double_twist = True
    bblender = False
    btwist = False
    binternal_distances = False
    write_data = "/scratch"
    bstack = False
    try:
        opts, args = getopt.getopt(
            argv,
            "hn:r:s:R:i:g:m:b:t:c:l:",
            [
                "dLk=",
                "chain_length=",
                "resolution=",
                "xmass=",
                "helix=",
                "seed=",
                "rerun=",
                "initial_conformation=",
                "platform_name=",
                "bp_per_nm3=",
                "mode=",
                "bond=",
                "thermostat=",
                "pair_style=",
                "linear=",
                "start=",
                "every=",
                "print_every=",
                "iterations=",
                "equilibration",
                "init_with_gaussian",
                "init_with_rigid",
                "init_with_wlc",
                "no_bending",
                "no_twist",
                "precision=",
                "write_data=",
                "save_for_blender",
                "test_twist_implementation",
                "test_internal_distances",
                "get_platforms",
                "get_local_tw",
                "stack_conformations",
            ],
        )
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-h":
            print("python3.7 openmm_plectoneme.py --dLk=<linking number deficit>")
            print("                               -n <chain length, number of bonds>")
            print("                               -r <resolution>")
            print("                               --xmass=<mass multiplier>")
            print(
                "                               --helix=<bp per DNA turn (dafault 10.5)>"
            )
            print("                               -s <seed>")
            print("                               -R <rerun>")
            print("                               -i <initial conformation>")
            print("                               -g <platform name>")
            print("                               --bp_per_nm3=<bp per nm^3> (WIP)")
            print("                               -m <mode> (WIP)")
            print("                               -b <bond type(fene,harmonic,rigid)>")
            print(
                "                               -t <thermostat(gjf,global,langevin,mnve,verlet)>"
            )
            print("                               -c <pairwise style(wca,no)>")
            print("                               -l <linear(no,yes)>")
            print(
                "                               --start <restart simulation from MD step>"
            )
            print(
                "                               --iterations <run simulation for this many steps>"
            )
            print(
                "                               --equilibration <run simulation over tau>"
            )
            print(
                "                               --every <save conformation every 'every' steps>"
            )
            print(
                "                               --print_every <print log every 'every' steps>"
            )
            print("                               --precision=<single,mixed,double>")
            print(
                "                             --init_with_gaussian return gaussian chain (initial conformation)"
            )
            print(
                "                             --init_with_rigid return chain with rigid bonds (initial conformation)"
            )
            print(
                "                             --init_with_wlc run MC with bending and twisting energies"
            )
            print("                             --no_bending disable bending forces")
            print("                             --no_twist disable twist forces")
            print(
                "                               --save_for_blender save conformation file to load in Blender"
            )
            print(
                "                               --test_twist_implementation compute square twist average"
            )
            print(
                "                               --test_internal_distances compute internal distances"
            )
            print(
                "                               --write_data=<where to write the data>"
            )
            print(
                "                               --stack_conformations=<stack conformations in one file>"
            )
            print("get the available platforms and devices (write json file), exit:")
            print("python3.7 openmm_plectoneme.py --get_platforms")
            print("get local twist, exit:")
            print("python3.7 openmm_plectoneme.py --get_local_tw")
            print("example:")
            print(
                "1: run circular molecule made of 5714 bonds 10.5 bp each, with FENE bond and WCA potential, with supercoiled density of about -0.06, with seed equal to 1, on GeForce RTX 2060 SUPER, OpenCL implementation and double precision. Start from MD step 0. Save every 100000 steps. Run for 1000000 iterations with Langevin thermostat."
            )
            print(
                "python3.7 openmm_plectoneme.py --dLk=-6 -n 5714 -r 10.5 -s 1 -R 0 -i circular -g GeForce_RTX_2060_SUPER_OpenCL -m run -b fene -c wca -t langevin -l no --precision=double --iterations=1000000 --every=100000 --start=0"
            )
            print(
                "2: run circular molecule made of 10000 bonds 10.5 bp each, with FENE bond and WCA potential, with supercoiled density of about 0.0, with seed equal to 1, on GeForce GTX 1070, OpenCL implementation and double precision. Start from MD step 0. Save every 100000 steps. Run for 1000000 iterations with global thermostat. Test twist implementation."
            )
            print(
                "python3.7 openmm_plectoneme.py --dLk=0 -n 10000 -r 10.5 -s 1 -R 0 -i circular -g GeForce_GTX_1070_OpenCL -m run -b fene -c wca -t global -l no --precision=double --iterations=1000000 --every=100000 --start=0 --test_twist_implementation"
            )
            print(
                "3: run linear molecule made of 1024 bonds 10.5 bp each, with FENE bond, with supercoiled density of about 0.0, with seed equal to 1, on Tesla K80, OpenCL implementation and double precision. Start from MD step 0. Save every 100000 steps. Run for 1000000 iterations with global thermostat. Test twist implementation."
            )
            print(
                "python3.7 openmm_plectoneme.py --dLk=0 -n 1024 -r 10.5 -s 1 -R 0 -i circular -g Tesla_K80_OpenCL -m run -b fene -c no -t global -l yes --precision=double --iterations=1000000 --every=100000 --start=0 --test_twist_implementation"
            )
            print(
                "4: run linear molecule made of 128 bonds 10.5 bp each, with FENE bond, no WCA potential except for two consecutive beads, with supercoiled density of about 0.0, with seed equal to 1, on GeForce GTX 1070, OpenCL implementation and double precision. Start from MD step 0. Save every 100000 steps. Run for 1000000 iterations with Verlet integrator. Test twist implementation."
            )
            print(
                "python3.7 openmm_plectoneme.py --dLk=0 -n 128 -r 10.5 -s 1 -R 0 -i circular -g GeForce_RTX_2060_SUPER_OpenCL -m run -b fene -c no -t gjf -l no --precision=double --iterations=1000000 --every=1000000 --start=0 --test_twist_implementation"
            )
            print(
                "python3.7 openmm_plectoneme.py --dLk=0 -n 512 -r 10.5 -s 1 -R 0 -i circular -g GeForce_GTX_1660_Ti_OpenCL -m run -b fene -c no -t gjf -l yes --precision=double --iterations=1000000 --every=1000000 --start=0 --test_internal_distances"
            )
            print("documentation:")
            print("pydoc3.7 -w openmm_plectoneme_functions.py")
            sys.exit()
        elif opt == "--get_platforms":
            opf.get_platforms()
            sys.exit()
        elif opt == "--get_local_tw":
            opf.overtwist_to_chain_no_closure_tw(5714, 10.5, helix, False)
            sys.exit()
        elif opt == "--dLk":
            dLk = float(arg)
        elif opt == "-n" or opt == "--chain_length":
            N = int(arg)
        elif opt == "-r" or opt == "--resolution":
            resolution = float(arg)
        elif opt == "--xmass":
            xmass = float(arg)
        elif opt == "-s" or opt == "--seed":
            seed = int(arg)
        elif opt == "-R" or opt == "--rerun":
            rerun = int(arg)
        elif opt == "-i" or opt == "--initial_conformation":
            initial_conformation = arg
        elif opt == "-g" or opt == "--platform_name":
            platform_name = str(arg)
        elif opt == "--bp_per_nm3":
            bp_per_nm3 = float(arg)
        elif opt == "-m" or opt == "--mode":
            mode = str(arg)
            mode = "run"
        elif opt == "-b" or opt == "--bond":
            bond = str(arg)
        elif opt == "-t" or opt == "--thermostat":
            thermostat = str(arg)
        elif opt == "-c" or opt == "--pair_style":
            pstyle = str(arg)
        elif opt == "-l" or opt == "--linear":
            linear = bool(arg == "yes")
        elif opt == "--start":
            start = int(arg)
        elif opt == "--iterations":
            MDS = int(arg)
        elif opt == "--equilibration":
            bequilibration = True
        elif opt == "--init_with_gaussian":
            init_with_gaussian = True
        elif opt == "--init_with_rigid":
            init_with_rigid = True
        elif opt == "--init_with_wlc":
            init_with_wlc = True
        elif opt == "--no_bending":
            no_bending = True
        elif opt == "--no_twist":
            no_twist = True
        elif opt == "--every":
            every = int(arg)
        elif opt == "--print_every":
            print_every = int(arg)
        elif opt == "--precision":
            precision = arg
        elif opt == "--write_data":
            write_data = arg
        elif opt == "--analysis":
            analysis = True
        elif opt == "--save_for_blender":
            bblender = True
        elif opt == "--test_twist_implementation":
            btwist = True
        elif opt == "--test_internal_distances":
            binternal_distances = True
        elif opt == "--stack_conformations":
            bstack = True
        else:
            pass

    # no pair style for gaussian chain
    if bond == "harmonic":
        pair_style = "no"
    # seed
    np.random.seed(seed)
    # number of beads from the number of bonds
    B = N + int(linear)
    # Kuhn length in bp
    kbp = 300.0
    # Temperature in Kelvin
    Temp = 300.0
    # mass in amu
    mass = xmass * resolution * 700.0
    # bond size in nm
    sigma = 100.0 * resolution / kbp
    # box size in nm
    L = 2.0 * np.sqrt(
        N * (resolution / kbp) * 100.0 * 100.0 / (12.0 - 6.0 * int(linear))
    )
    # number of particles per bead
    if not no_twist:
        twist1 = opf.particles_per_bead(2)
        if use_double_twist:
            twist2 = opf.particles_per_bead(3)
    # bending strength
    Kb = opf.bending_rigidity(kbp, resolution, Temp)
    # twisting strength (harmonic strength)
    Kt = opf.twisting_rigidity(86.0, sigma, Temp)
    # pseudo bending strength
    pKb = opf.get_pKb(Temp, Kb, Kt)
    # positions and frames u,v,t
    positions, u, v, t, success = opf.puvt_chain(
        N, sigma, L, linear, initial_conformation
    )
    if linear and pstyle == "no" and init_with_gaussian:
        positions, u, v, t = opf.gaussian_chain(positions, u, v, t, sigma, Temp, L, -1)
    if linear and pstyle == "no" and init_with_rigid:
        positions, u, v, t = opf.chain_with_rigid_bonds(
            positions, u, v, t, sigma, Temp, L, -1
        )
    if linear and init_with_wlc:
        positions, u, v, t, acceptation_ratio = opf.wlc_conformation(
            positions, u, v, t, sigma, 50.0, 86.0, Kb, Kt, Temp, L, 3 * N * N, -1
        )
        print("acceptation ratio={0:f}, <t.t>={1:f}".format(acceptation_ratio,np.mean(np.array([np.dot(t[i], t[i + 1]) for i in range(N - 1)]))))
    # twist between two consecutives frames -> overtwist
    overtwist = dLk / (N * resolution / helix)
    if dLk != 0.0:
        tw, tw_closure, Lk_error = opf.overtwist_to_local_twist(
            N, resolution, helix, linear, overtwist, 10000000
        )
        print("tw=", tw, "tw_closure=", tw_closure, "Lk error=", Lk_error)
        u, v = opf.twist_each_uv(u, v, t, np.array([tw] * (N - 1) + [0.0]), linear)
    # system creation (com motion removed every 10 steps)
    system, masses = opf.create_system(L, B, mass, 10 if thermostat == "verlet" else -1)
    # pseudo-u beads
    vsigma = sigma / 0.965 if bond == "fene" else sigma
    new_positions = opf.create_pseudo_u_beads(positions, u, vsigma)

    # creation of the system, integrator, platform and context
    # rigid bond ?
    if bond == "rigid":
        opf.rigid_bond(system, N, sigma, linear)
    elif bond == "harmonic":
        # Harmonic bond k = 3 * kB * T / sigma^2
        system.addForce(opf.harmonic(N, Temp, sigma, linear, PBC))
    else:
        # Kremer-Grest model by default (add WCA if 'pstyle' is 'no')
        system.addForce(
            opf.fene(
                N,
                Temp,
                sigma / 0.965,
                linear,
                PBC,
                bool(pstyle == "no"),
            )
        )
    # pseudo-vector
    if not no_twist:
        # FENE pseudo-u vector
        system.addForce(opf.virtual_fene_wca(B, Temp, vsigma, PBC))
        # Harmonic pseudo-u vector
        # system.addForce(opf.virtual_harmonic(B, 3.0 * opf.get_kBT(Temp) / sigma**2, sigma, PBC))
    # wca
    if pstyle == "wca":
        system.addForce(opf.wca(N, Temp, sigma, linear, PBC))
    # bending
    if not no_bending:
        system.addForce(opf.tt_bending(N, Kb, linear, PBC))
    # add pseudo-bending and twist
    if not no_twist:
        system.addForce(opf.ut_bending(N, pKb, linear, PBC))
        system.addForce(opf.uU_bending(N, pKb, linear, PBC))
        # system.addForce(opf.uUt_bending(N, pKb, linear, PBC))
        # twist
        if use_double_twist:
            system.addForce(opf.double_twist(N, Kt, [0.0] * (N - int(linear)), linear, PBC))
        else:
            system.addForce(opf.twist(N, Kt, [0.0] * (N - int(linear)), linear, PBC, False))
    # coupling frequency to the thermostat and time-step
    friction = opf.get_bead_friction(
        0.5 * sigma, 5.4 * 0.001 if thermostat == "global" else 0.83 * 0.001
    )
    tau = mass / friction
    kbond = 3.0 * opf.get_kBT(Temp) / sigma ** 2
    if bond == "harmonic":
        dt = np.minimum(np.sqrt(0.01 * mass / kbond), 0.01 * tau if thermostat == "langevin" or True else 0.1 * kbond / friction)
    elif bond == "fene":
        dt = np.minimum(0.01 * sigma * np.sqrt(mass / opf.get_kBT(Temp)), 0.01 * tau)
    else:
        tau = opf.minimal_time_scale([mass], [sigma], [opf.get_kBT(Temp)])
        dt = 0.01 * tau
    if not no_bending:
        dt = np.minimum(dt, 0.01 * sigma * np.sqrt(mass / Kb))
    if not no_twist:
        dt = np.minimum(dt, 0.01 * sigma * np.sqrt(mass / Kt))
        dt = np.minimum(dt, 0.01 * sigma * np.sqrt(mass / pKb))
    print("friction={0:f}".format(friction))
    print("kbond={0:f}".format(kbond))
    print("mass/friction=" + str(mass / friction))
    print("friction/kbond=" + str(friction / kbond))
    print("dt/tau=" + str(dt / tau))
    # creating the platform
    platform, properties = opf.create_platform(platform_name, precision)
    # creating the integrator
    integrator = opf.create_integrator(
        thermostat,
        Temp,
        dt,
        B,
        system.getNumConstraints(),
        mass,
        tau,
        seed,
        False,
        False,
    )
    # creating the context
    context = opf.create_context(
        system,
        integrator,
        platform,
        properties,
        new_positions,
        opf.v_gaussian(Temp, masses, -1),
    )
    # get info about the system you just created
    opf.get_info_system(context)
    # write info (log file)
    if not btwist:
        log_file = {}
        log_file["bond"] = bond
        log_file["pair_style"] = pstyle
        log_file["bonds"] = N
        log_file["mass_amu"] = mass
        log_file["xmass"] = xmass
        log_file["resolution_bp"] = resolution
        log_file["bond_length_nm"] = sigma
        log_file["bending_persistence_nm"] = 50
        log_file["twisting_persistence_nm"] = 86
        log_file["Kb_kBT"] = Kb
        log_file["pKb_kBT"] = pKb
        log_file["Kt_kBT"] = Kt
        log_file["kBT"] = opf.get_kBT(Temp)
        log_file["kbond"] = kbond
        log_file["friction"] = opf.get_bead_friction(0.5 * sigma)
        log_file["Temp"] = Temp
        log_file["dt"] = dt
        log_file["tau"] = tau
        log_file["particles_per_bead"] = int(opf.particles_per_bead.nparticles_per_bead)
        log_file["use_bending"] = not no_bending
        log_file["use_twist"] = not no_twist
        log_file["dLk"] = dLk
        log_file["overtwist"] = overtwist
        log_file["thermostat"] = thermostat
        log_file["initial_conformation"] = initial_conformation
        log_file["linear"] = "yes" if linear else "no"
        log_file["platform"] = platform_name
        log_file["precision"] = precision
        log_file["Wr_t0"] = da.chain_writhe(positions)
        log_file["Tw_t0"] = np.sum(da.chain_twist(u, v, t, linear)) / (2.0 * np.pi)
        log_file["use_double_twist"] = "yes" if use_double_twist else "no"
        log_file["step0"] = 0
        log_file["step1"] = start + MDS
        log_file["every"] = every
        log_file["last_number_of_MDS"] = MDS
        with open(write_data + "/info.json", "w") as out_file:
            json.dump(log_file, out_file, indent=1)

    # do we have an OpenMM checkpoint to start from ?
    opf.read_write_checkpoint(
        context, write_data + "/restart/restart.s" + str(start) + ".chk", "read"
    )
    # test variables
    mean_std_phi2 = np.zeros((MDS // every + 1, 2))
    Rs2 = np.zeros(B, dtype=np.double)
    # equilibration run
    if bequilibration:
        opf.print_variables(context, linear, -1)
        context.getIntegrator().step(int(tau / dt))
    # run
    # remove center-of-mass motion
    if False and thermostat == "global":
        context.setVelocities(
            opf.from_np_to_mm_positions(opf.remove_com_motion(context))
        )
    # variables to store stack of positions and velocities
    if bstack:
        spositions = np.zeros((B * opf.particles_per_bead.nparticles_per_bead * (MDS // every + 1), 3), dtype=np.double)
        svelocities = np.zeros((B * opf.particles_per_bead.nparticles_per_bead * (MDS // every + 1), 3), dtype=np.double)
        istack = np.arange(0, B * opf.particles_per_bead.nparticles_per_bead, 1)
    for i in range(start, start + MDS + 1, every):
        # get the positions (in nanometer) for further analysis
        time0 = time.time()
        # vpositions, vvelocities = opf.get_vpositions_and_velocities_from_context(context)
        # vpositions = opf.from_OpenMM_Vec3_to_Numpy_array(context.getState(getPositions=True, enforcePeriodicBox=PBC).getPositions(), 1.0 / unit.nanometer)
        # vvelocities = opf.from_OpenMM_Vec3_to_Numpy_array(context.getState(getVelocities=True).getVelocities(), 1.0 / (unit.nanometer / unit.picosecond))
        vpositions = context.getState(getPositions=True, enforcePeriodicBox=PBC).getPositions(asNumpy=True)
        vvelocities = context.getState(getVelocities=True, enforcePeriodicBox=PBC).getVelocities(asNumpy=True)
        # print(context.getState(getPositions=True, enforcePeriodicBox=PBC).getPositions())
        # print(context.getState(getVelocities=True, enforcePeriodicBox=PBC).getVelocities())
        # sys.exit()
        time0 -= time.time()
        if (i % print_every) == 0:
            opf.print_variables(context, linear, i)
        if not btwist and not binternal_distances:
            if bstack:
                # stack conformations
                time01 = time.time()
                spositions[istack, :3] = np.copy(vpositions[:, :3])
                svelocities[istack, :3] = np.copy(vvelocities[:, :3])
                np.add(istack, B * opf.particles_per_bead.nparticles_per_bead, out=istack)
                time01 -= time.time()
            else:
                # write the conformation
                if xmass == 1.0:
                    opf.write_draw_conformation(
                        vpositions,
                        name_write=write_data + "/out/vxyz." + str(i) + ".out",
                        mode="write",
                    )
                else:
                    opf.write_draw_conformation(
                        vpositions,
                        vvelocities,
                        name_write=write_data + "/out/vxyz." + str(i) + ".out",
                        mode="write_add_velocities",
                    )
        else:
            # twist
            if not no_twist and use_double_twist:
                us, vs, Us, Vs, ts = opf.get_double_uvt_from_positions(vpositions)
                tws = np.multiply(
                    0.5,
                    np.add(
                        da.chain_twist(us, vs, ts, linear),
                        da.chain_twist(Us, Vs, ts, linear),
                    ),
                )
                phi2 = np.square(tws)
            else:
                us, vs, ts = opf.get_uvt_from_positions(vpositions)
                phi2 = np.square(da.chain_twist(us, vs, ts, linear))
            mean_std_phi2[i // every, :2] = np.mean(phi2), np.std(phi2)
            # internal distances
            Rs2 = np.add(
                Rs2, da.square_internal_distances(vpositions, 0, B - 1, linear)
            )
        del vpositions, vvelocities
        # stack conformation
        if (bblender or seed == 1) and not btwist and xmass == 1.0:
            opf.from_context_to_blender(
                context,
                False,
                np.full((B,), sigma),
                write_data + "/visualisation_with_blender/output",
                "yes",
            )
        # step the context
        time1 = time.time()
        if i < (start + MDS):
            context.getIntegrator().step(every)
        time1 -= time.time()
        # print(-time0, -time01, -time1)
        # print("")
    # write a checkpoint to restart the simulation
    if not btwist and not binternal_distances:
        opf.read_write_checkpoint(
            context,
            write_data + "/restart/restart.s" + str(start + MDS) + ".chk",
            "write",
        )
    else:
        print(
            "<square twist>="
            + str(mean_std_phi2[MDS // every, 0])
            + "+/-"
            + str(mean_std_phi2[MDS // every, 1] / np.sqrt(N))
            + "/"
            + str(sigma / 86.0)
        )
        Rs2_simulation = np.multiply(1.0 / (MDS // every + 1), Rs2)[1 : (B // 2)]
        Rs2_theory = da.Rs2_wlc(
            50.0,
            N * 100.0 * resolution / kbp,
            100.0 * resolution / kbp,
            np.arange(0, B, 1),
        )[1 : (B // 2)]
        print(
            "sqrt(<(1-Rs2/wlc)^2>)="
            + str(
                np.sqrt(
                    np.mean(
                        np.square(
                            np.divide(
                                np.subtract(Rs2_simulation, Rs2_theory), Rs2_theory
                            )
                        )
                    )
                )
            )
        )
    # write stacked conformations
    if bstack:
        if xmass == 1.0:
            opf.write_draw_conformation(
                spositions, name_write=write_data + "/out/vxyz.out", mode="write",
            )
        else:
            opf.write_draw_conformation(
                spositions,
                svelocities,
                name_write=write_data + "/out/vxyz.out",
                mode="write_add_velocities",
            )


if __name__ == "__main__":
    main(sys.argv[1:])
