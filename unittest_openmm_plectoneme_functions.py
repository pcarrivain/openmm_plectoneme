"""
The module 'unittest_openmm_plectoneme_functions.py'
is a collection of test cases used to test the
module 'openmm_plectoneme_functions.py'.
The following code (has been developped by
Pascal Carrivain) is distributed under MIT licence.
"""
#!/usr/bin/python
# -*- coding: utf-8 -*-
import data_analysis as da
import numpy as np
import openmm_plectoneme_functions as opf
import random
import simtk.openmm as mm
import time
import unittest


class openmm_plectoneme_functionsTest(unittest.TestCase):
    """Test case for the 'openmm_plectoneme_functions' module."""

    def test_from_i_to_bead(self):
        """Test of the function 'from_i_to_bead'."""
        self.assertEqual(opf.from_i_to_bead(0, 100), 0)
        self.assertEqual(opf.from_i_to_bead(99, 100), 99)
        self.assertEqual(opf.from_i_to_bead(100, 100), 0)
        self.assertEqual(opf.from_i_to_bead(-1, 100), 99)

    def test_create_system(self):
        """Test of the function 'create_system'."""
        L = 100.0
        B = 101
        mass = 1.0
        sigma = 1.5
        system, masses = opf.create_system(L, B, mass, 10)
        self.assertTrue(system.getNumParticles() == (opf.get_particles_per_bead() * B))
        self.assertTrue(len(masses) == (opf.get_particles_per_bead() * B))

    def test_create_virtual_sites(self):
        """Test of the function 'create_virtual_sites'."""
        N = 100
        sigma = 1.0
        positions = np.array([[0.0, float(i) * sigma, 0.0] for i in range(N)])
        u = [mm.Vec3(1.0, 0.0, 0.0) for i in range(N)]
        virtual_sites, new_positions = opf.create_virtual_sites(positions, u, sigma)
        # lengths of 'create_virtual_sites' returns
        self.assertEqual(len(new_positions), opf.get_particles_per_bead() * N)
        self.assertEqual(len(virtual_sites), N * int(opf.get_particles_per_bead() >= 2))
        for i in range(N):
            # does it correctly copy the first N positions ?
            dx = new_positions[i][0] - positions[i][0]
            self.assertTrue(abs(dx / sigma) < 0.001)
            dy = new_positions[i][1] - positions[i][1]
            self.assertTrue(abs(dy / sigma) < 0.001)
            dz = new_positions[i][2] - positions[i][2]
            self.assertTrue(abs(dz / sigma) < 0.001)
            if opf.get_particles_per_bead() >= 3:
                # does the distance bewteen virtual site and particle equal sigma ?
                dx = new_positions[2 * N + i][0] - new_positions[i][0]
                dy = new_positions[2 * N + i][1] - new_positions[i][1]
                dz = new_positions[2 * N + i][2] - new_positions[i][2]
                self.assertTrue(
                    abs((dx * dx + dy * dy + dz * dz - sigma * sigma) / (sigma * sigma))
                    < 0.001
                )

    def test_link_virtual_sites(self):
        """Test of the function 'link_virtual_sites'."""
        linear = True
        L = 100.0
        B = 101
        N = B - int(linear)
        m = 1.0
        sigma = 1.5
        system, masses = opf.create_system(L, B, m, 10)
        positions, u, v, t = opf.puvt_chain(B - 1, sigma, L, linear)
        virtual_sites, new_positions = opf.create_virtual_sites(positions, u, sigma)
        opf.link_virtual_sites(system, virtual_sites)
        # is virtual site ?
        for v in range(B):
            self.assertTrue(not system.isVirtualSite(v))
        if opf.get_particles_per_bead() >= 2:
            for v in range(B, 2 * B):
                self.assertTrue(system.isVirtualSite(v))
        if opf.get_particles_per_bead() >= 3:
            for v in range(2 * B, 3 * B):
                self.assertTrue(not system.isVirtualSite(v))

    def test_normalize(self):
        """Test of the function 'normalize'."""
        # norm of a zero vector
        a = np.array([0.0, 0.0, 0.0])
        a = opf.normalize(a)
        self.assertTrue(abs(a[0] * a[0] + a[1] * a[1] + a[2] * a[2] - 0.0) < 0.000001)
        # norm of the following vector
        a = np.array([1.0, -3.0, 9.0])
        a = opf.normalize(a)
        self.assertTrue(abs(a[0] * a[0] + a[1] * a[1] + a[2] * a[2] - 1.0) < 0.000001)

    def test_rotation(self):
        """Test of the function 'rotation'."""
        X = np.array([1.0, 0.0, 0.0])
        Y = np.array([0.0, 1.0, 0.0])
        Z = np.array([0.0, 0.0, 1.0])
        rX = opf.rotation(Z, 0.5 * np.pi, X)
        self.assertTrue(abs(rX[1] - Y[1]) < 0.000001)
        rY = opf.rotation(Z, -0.5 * np.pi, Y)
        self.assertTrue(abs(rY[0] - X[0]) < 0.000001)

    def test_twist_each_uv(self):
        """Test of the function 'twist_each_uv'."""
        seed = int(np.fmod(time.time(), 10007))
        np.random.seed(seed)
        print("test_twist_each_uv, seed={0:d}".format(seed))
        # one frame per bead
        # ring: N bonds, N beads
        # linear: N bonds, N+1 beads
        N = 8
        sigma = 1.0
        L = N * sigma
        Pi = np.pi
        twoPi = 2.0 * Pi
        # ring, linear
        for b in [False, True]:
            # two consecutive frames are twisted about tw0
            for j, tw0 in enumerate(
                np.multiply(
                    0.1 * Pi, np.subtract(1.0, np.multiply(2.0, np.random.random(20)))
                )
            ):
                positions, u, v, t = opf.puvt_chain(N, sigma, L, b)
                old_tws = da.chain_twist(u, v, t, b)
                if b:
                    # linear
                    u, v = opf.twist_each_uv(u, v, t, [tw0] * (N - 1), b)
                    tws = da.chain_twist(u, v, t, b)
                    for i in range(N - 1):
                        self.assertTrue(abs(tws[i] / tw0 - 1.0) < 1e-4)
                else:
                    # ring
                    u, v = opf.twist_each_uv(u, v, t, [tw0] * N, b)
                    tws = da.chain_twist(u, v, t, b)
                    for i in range(1, N):
                        # print(b, j, i, "old=", old_tws[i], "new=", tws[i], "tw=", tw0, "err=", abs(tws[i] / tw0 - 1.0), "/", old_tws[N - 1], tws[N - 1])
                        self.assertTrue(abs(tws[i] / tw0 - 1.0) < 1e-4)
                    # because we consider a ring chain,
                    # twisting the frame N-1 with respect to the frame N-2
                    # it changes the twist between frame N-1 and frame 0
                    tw_closure = twoPi * (
                        np.rint((N - 1) * tw0 / twoPi) - (N - 1) * tw0 / twoPi
                    )
                    self.assertTrue(abs(tws[0] / tw_closure - 1.0) < 1e-4)

    def test_wlc_conformation(self):
        """Test of the function 'wlc_conformation'."""
        N = 32
        kbp = 300.0
        lk, lt = 100.0, 86.0
        lp = 0.5 * lk
        lb = lk * 10.5 / kbp
        Temp = 300.0
        Kb = opf.bending_rigidity(kbp, 10.5, Temp)
        Kt = 0.0  # opf.twisting_rigidity(lt,lb,Temp)
        L = N * lb
        # linear chain
        ps = np.array([[0.0, 0.0, i * lb] for i in range(N + 1)])
        us = np.array([[1.0, 0.0, 0.0] for i in range(N + 1)])
        vs = np.array([[0.0, 1.0, 0.0] for i in range(N + 1)])
        ts = np.array([[0.0, 0.0, 1.0] for i in range(N + 1)])
        # wlc conformation
        # N^3 Monte-Carlo moves
        ps, us, vs, ts = opf.wlc_conformation(
            ps, us, vs, ts, lb, lp, lt, Kb, Kt, Temp, L, 2 * N * N * N, 111
        )
        P, D = ps.shape
        self.assertTrue(P == (N + 1) and D == 3)
        P, D = us.shape
        self.assertTrue(P == (N + 1) and D == 3)
        P, D = vs.shape
        self.assertTrue(P == (N + 1) and D == 3)
        P, D = ts.shape
        self.assertTrue(P == (N + 1) and D == 3)
        for i in range(N):
            self.assertTrue(abs(np.dot(us[i], vs[i])) < 1e-4)
            self.assertTrue(abs(np.dot(us[i], ts[i])) < 1e-4)
            self.assertTrue(abs(np.dot(vs[i], ts[i])) < 1e-4)
        # check bending cosine average
        cosb = np.array([np.dot(ts[i], ts[i + 1]) for i in range(N - 1)])
        avg = np.mean(cosb)
        stde = np.std(cosb) / np.sqrt(N - 1)
        self.assertTrue(abs(avg - (lk - lb) / (lk + lb)) < (2.0 * stde))
        # check tw^2 average
        costw = np.array(
            [
                min(
                    1.0,
                    max(
                        -1.0,
                        (np.dot(us[i], us[i + 1]) + np.dot(vs[i], vs[i + 1]))
                        / (1.0 + np.dot(ts[i], ts[i + 1])),
                    ),
                )
                for i in range(N - 1)
            ]
        )
        tw2 = np.array([np.arccos(costw[i]) ** 2 for i in range(N - 1)])
        avg, stde = np.mean(tw2), np.std(tw2) / np.sqrt(N - 1)
        if Kt > 0.0:
            self.assertTrue(abs(avg - lb / lt) < (2.0 * stde))
        else:
            self.assertTrue(abs(avg - np.pi ** 2 / 3.0) < (2.0 * stde))
        # check internal distances against theory (linear chain)
        N = 16
        L = N * lb
        ps = np.array([[0.0, 0.0, i * lb] for i in range(N + 1)])
        us = np.array([[1.0, 0.0, 0.0] for i in range(N + 1)])
        vs = np.array([[0.0, 1.0, 0.0] for i in range(N + 1)])
        ts = np.array([[0.0, 0.0, 1.0] for i in range(N + 1)])
        # 1000 samples
        S = 1000
        P, D = ps.shape
        Rs2 = np.zeros((S, P), dtype=np.double)
        for s in range(S):
            # N^3 Monte-Carlo moves for the first sample
            # then N^2 moves
            ps, us, vs, ts = opf.wlc_conformation(
                ps,
                us,
                vs,
                ts,
                lb,
                lp,
                lt,
                Kb,
                Kt,
                Temp,
                L,
                (1 + (N - 1) * int(s == 0)) * N * N,
                s + 1,
            )
            Rs2[s, :] = da.square_internal_distances(ps, 0, P - 1, True)
        Rs2_sim = np.mean(Rs2, axis=0)
        std_Rs2 = np.std(Rs2, axis=0, dtype=np.float64)
        # check simulation v. theory over half of the chain
        Rs2_wlc = da.Rs2_wlc(lp, L, lb, np.arange(0, P, 1))
        self.assertTrue(Rs2_sim[0] == 0.0)
        self.assertTrue(abs(Rs2_sim[1] - lb * lb) < (1e-6 * lb * lb))
        for i in range(2, 3 * P // 4):
            # print(i,Rs2_sim[i],Rs2_wlc[i],std_Rs2[i])
            self.assertTrue(abs(Rs2_sim[i] - Rs2_wlc[i]) < (1.5 * std_Rs2[i]))

    def test_puvt_chain(self):
        """Test of the function 'puvt_chain'."""
        N = 100
        sigma = 1.5
        L = 200.0
        H = int(0.5 * N)
        # linear
        positions, u, v, t = opf.puvt_chain(N, sigma, L, True)
        dx = positions[N][0] - positions[0][0]
        dy = positions[N][1] - positions[0][1]
        dz = positions[N][2] - positions[0][2]
        self.assertTrue(np.sqrt(dx * dx + dy * dy + dz * dz) > (0.5 * sigma))
        dx = positions[H][0] - positions[H + 1][0]
        dy = positions[H][1] - positions[H + 1][1]
        dz = positions[H][2] - positions[H + 1][2]
        self.assertTrue(
            abs((dx * dx + dy * dy + dz * dz - sigma * sigma) / (sigma * sigma)) < 0.001
        )
        for i in range(N):
            self.assertTrue(np.dot(u[i], v[i]) < 0.00000001)
            self.assertTrue(np.dot(u[i], t[i]) < 0.00000001)
            self.assertTrue(np.dot(v[i], t[i]) < 0.00000001)
        # ring
        positions, u, v, t = opf.puvt_chain(N, sigma, L, False)
        dx = positions[N - 1][0] - positions[0][0]
        dy = positions[N - 1][1] - positions[0][1]
        dz = positions[N - 1][2] - positions[0][2]
        self.assertTrue(
            abs((dx * dx + dy * dy + dz * dz - sigma * sigma) / (sigma * sigma)) < 0.001
        )
        dx = positions[0][0] - positions[1][0]
        dy = positions[0][1] - positions[1][1]
        dz = positions[0][2] - positions[1][2]
        self.assertTrue(
            abs((dx * dx + dy * dy + dz * dz - sigma * sigma) / (sigma * sigma)) < 0.001
        )
        for i in range(N):
            self.assertTrue(np.dot(u[i], v[i]) < 0.00000001)
            self.assertTrue(np.dot(u[i], t[i]) < 0.00000001)
            self.assertTrue(np.dot(v[i], t[i]) < 0.00000001)

    def test_puvt_single_molecule(self):
        """Test of the function 'puvt_single_molecule'."""
        abs_tol = 1e-6
        rel_tol = 1e-4
        N = 1000
        B = N + 1
        sigma = 2.7
        positions, u, v, t = opf.puvt_single_molecule(N, sigma)
        for i in range(N):
            dx = positions[i][0]
            dy = positions[i][1]
            dz = positions[i][2]
            self.assertTrue(
                abs(dx - 0.5 * B * sigma) <= (abs_tol + rel_tol * abs(0.5 * B * sigma))
            )
            self.assertTrue(
                abs(dy - 0.5 * B * sigma) <= (abs_tol + rel_tol * abs(0.5 * B * sigma))
            )
            self.assertTrue(abs(dz - i * sigma) <= (abs_tol + rel_tol * abs(i * sigma)))

    def test_bending_rigidity(self):
        """Test of the function 'bending_rigidity'."""
        self.assertTrue(opf.bending_rigidity(100.0, 100.0, 300.0) < 0.00000001)

    def test_change_overtwist(self):
        """Test of the function 'change_overtwist'."""
        seed = int(np.fmod(time.time(), 10007))
        np.random.seed(seed)
        print("test_change_overtwist, seed={0:d}".format(seed))
        lbond, resolution = 1.0, 10.5
        Pi = np.pi
        twoPi = 2.0 * Pi
        for b in [False, True]:
            for N in [512, 1024, 2048, 4096, 8192]:
                Lk0 = N * resolution / 10.5
                for tw in np.multiply(
                    0.01 * Pi, np.subtract(1.0, np.multiply(2.0, np.random.random(20)))
                ):
                    # chain with Wr = Tw = 0
                    positions, u, v, t = opf.puvt_chain(
                        N, lbond, N * lbond, b, "circular"
                    )
                    # twist the frames
                    if b:
                        u, v = opf.twist_each_uv(u, v, t, [tw] * (N - 1), b)
                        tw_closure = 0.0
                    else:
                        u, v = opf.twist_each_uv(u, v, t, [tw] * (N - 1) + [0.0], b)
                        tw_closure = twoPi * (
                            np.rint(tw * (N - 1) / twoPi) - tw * (N - 1) / twoPi
                        )
                    # get the Tw
                    Tw0 = (
                        np.sum(da.chain_twist(u, v, t, b, 1e-12), dtype=np.double)
                        / twoPi
                    )
                    print(tw * (N - 1), tw_closure, Tw0)
                    print(b, N, tw, "diff=", (tw * (N - 1) + tw_closure) / twoPi - Tw0)
                    # add virtual and pseudo beads
                    vpositions = np.concatenate(
                        (
                            positions[:N, :],
                            np.concatenate(
                                (
                                    positions[:N, :],
                                    np.add(positions[:N, :], np.multiply(lbond, u)),
                                ),
                                axis=0,
                            ),
                        ),
                        axis=0,
                    )
                    # change Tw
                    # overtwist = (Lk - Lk0) / Lk0
                    new_Tw = 1.01 * Tw0
                    new_positions = opf.change_overtwist(
                        vpositions, lbond, Lk0, Tw0 / Lk0, new_Tw / Lk0, b
                    )
                    u, v, t = opf.get_uvt_from_positions(new_positions, b)
                    # check new Tw
                    Tw1 = (
                        np.sum(da.chain_twist(u, v, t, b, 1e-12), dtype=np.double)
                        / twoPi
                    )
                    print(
                        b,
                        N,
                        "from=",
                        Tw0,
                        "to=",
                        new_Tw,
                        "observed=",
                        Tw1,
                        "expected=",
                        new_Tw,
                    )
                    print(np.absolute(new_Tw - Tw1), np.absolute(1e-3 * new_Tw))
                    self.assertTrue(
                        np.absolute(new_Tw - Tw1) < np.absolute(1e-3 * new_Tw)
                    )

    def test_overtwist_to_local_twist(self):
        """Test of the function 'overtwist_to_local_twist'."""
        seed = int(np.fmod(time.time(), 10007))
        np.random.seed(seed)
        print("test_overtwist_to_local_twist, seed={0:d}".format(seed))
        resolution = 10.5
        Pi = np.pi
        for N in [1024, 2048, 4096, 8192]:
            Tw, Tw_closure, Lk_error = opf.overtwist_to_local_twist(
                N, resolution, 10.5, True, 0.0, 1000000
            )
            self.assertTrue(Tw == 0.0)
            # loop over random overtwist
            for o in np.multiply(
                0.4, np.subtract(1.0, np.multiply(2.0, np.random.random(20)))
            ):
                for b in [True, False]:
                    # linear (no iterative procedure)
                    # ring (iterative procedure)
                    Tw, Tw_closure, Lk_error = opf.overtwist_to_local_twist(
                        N, resolution, 10.5, b, o, 10000000
                    )
                    self.assertTrue(
                        abs(
                            (Tw * (N - 1) + Tw_closure)
                            / (2.0 * Pi * o * N * resolution / 10.5)
                            - 1.0
                        )
                        < 1e-4
                    )

    def test_overtwist_to_chain(self):
        """Test of the function 'overtwist_to_chain'."""
        seed = int(np.fmod(time.time(), 10007))
        np.random.seed(seed)
        print("test_overtwist_to_local_twist, seed={0:d}".format(seed))
        N, resolution = 8192, 10.5
        Pi = np.pi
        twoPi = 2.0 * Pi
        # loop over random overtwist
        for b in [False, True]:
            tw, tw_closure, Lk_error = opf.overtwist_to_chain(
                N, resolution, 10.5, b, 0.0
            )
            self.assertTrue(tw == 0.0)
            continue
            for o in np.multiply(
                0.3, np.subtract(1.0, np.multiply(2.0, np.random.random(20)))
            ):
                tw, tw_closure, Lk_error = opf.overtwist_to_chain(
                    N, resolution, 10.5, b, o
                )
                # check ring a*(N-1)+b=2*Pi*Lk
                #       linear tw*(N-1)=2*Pi*Lk
                Tw = (N - 1) * tw + tw_closure
                print(
                    "test:",
                    b,
                    o,
                    Lk_error,
                    abs(Tw / (twoPi * o * N * resolution / 10.5) - 1.0),
                )
                print(tw, (twoPi * o * N * resolution / 10.5) / (N - 1), "\n")
                self.assertTrue(
                    abs(Tw / (twoPi * o * N * resolution / 10.5) - 1.0) < 1e-2
                )
                if not b:
                    # positions and frames u, v, t
                    positions, u, v, t = opf.puvt_chain(N, 1.0, N * 1.0, b, "circular")
                    u, v = opf.twist_each_uv(
                        u, v, t, [tw - tw_closure] + [tw] * (N - 2) + [tw_closure], b
                    )
                    Tws = da.chain_twist(u, v, t, b, 1e-12)
                    self.assertTrue(
                        abs(np.sum(Tws) / (twoPi * o * N * resolution / 10.5) - 1.0)
                        < 1e-2
                    )

    def test_fene(self):
        """Test of the function 'fene'."""
        N = 101
        sigma = 1.75
        Temp = 300.0
        for linear in [False, True]:
            for PBC in [False, True]:
                fene = opf.fene(N, Temp, sigma, linear, PBC)
                self.assertTrue(fene.getNumBonds() == N)
                self.assertTrue(fene.getNumGlobalParameters() == 2)

    def test_rigid_bond(self):
        """Test of the function 'rigid_bond'."""
        L = 100.0
        B = 101
        mass = 1.0
        sigma = 1.5
        # linear
        system1, masses = opf.create_system(L, B, mass, 10)
        opf.rigid_bond(system1, B - 1, sigma, True)
        self.assertTrue(system1.getNumConstraints() == (B - 1))
        # ring
        system2, masses = opf.create_system(L, B, mass, 10)
        opf.rigid_bond(system2, B, sigma, False)
        self.assertTrue(system2.getNumConstraints() == B)

    def test_virtual_harmonic(self):
        """Test of the function 'virtual_harmonic'."""
        Temp = 300.0
        B = 201
        sigma = 1.6
        for PBC in [False, True]:
            vharmonic = opf.virtual_harmonic(B, 8.0 * opf.get_kBT(Temp), sigma, PBC)
            self.assertTrue(
                vharmonic.getNumBonds() == (B * int(opf.get_particles_per_bead() >= 3))
            )
            self.assertTrue(vharmonic.getNumGlobalParameters() == 2)

    def test_wca(self):
        """Test of the function 'wca'."""
        N = 100
        Temp = 300.0
        sigma = 1.4
        for linear in [False, True]:
            for PBC in [False, True]:
                B = N + int(linear)
                wca = opf.wca(N, Temp, sigma, linear, PBC)
                self.assertTrue(
                    wca.getNumParticles() == (opf.get_particles_per_bead() * B)
                )
                if PBC:
                    self.assertTrue(
                        wca.getNonbondedMethod() == mm.NonbondedForce.CutoffPeriodic
                    )
                else:
                    self.assertTrue(
                        wca.getNonbondedMethod() == mm.NonbondedForce.CutoffNonPeriodic
                    )

    def test_wca_virtual_sites(self):
        """Test of the function 'wca_virtual_sites'."""
        N = 100
        Temp = 300.0
        sigma = 2.3
        for linear in [False, True]:
            B = N + int(linear)
            for PBC in [False, True]:
                vwca = opf.wca_virtual_sites(N, Temp, sigma, linear, PBC)
                self.assertTrue(vwca.getNumGlobalParameters() == 3)
                self.assertTrue(
                    vwca.getNumBonds() == (B * int(opf.get_particles_per_bead() >= 3))
                )

    def test_clamp_bead(self):
        """Test of the function 'clamp_bead'."""
        Temp = 300.0
        k_clamp = 1.5 * opf.get_kBT(Temp)
        clamp = opf.clamp_bead(3, k_clamp, 0.1, -0.2, 0.05)
        self.assertTrue(type(clamp) == mm.CustomExternalForce)
        self.assertTrue(clamp.getNumGlobalParameters() == 0)
        self.assertTrue(clamp.getNumPerParticleParameters() == 4)
        self.assertTrue(clamp.getNumParticles() == 1)

    def test_external_force(self):
        """Test of the function 'external_force'."""
        axe = "y"
        external_force = opf.external_force(10, axe, -0.1)
        self.assertTrue(type(external_force) == mm.CustomExternalForce)
        self.assertTrue(external_force.getNumGlobalParameters() == 0)
        self.assertTrue(external_force.getNumPerParticleParameters() == 1)
        self.assertTrue(external_force.getNumParticles() == 1)
        self.assertTrue(external_force.getEnergyFunction() == "f_external*" + axe)

    def test_plane_repulsion(self):
        """Test of the function 'plane_repulsion'."""
        normal = "x"
        plane = opf.plane_repulsion(20, normal, 2.0, opf.get_kBT(300.0))
        self.assertTrue(type(plane) == mm.CustomExternalForce)
        self.assertTrue(plane.getNumGlobalParameters() == 0)
        self.assertTrue(plane.getNumPerParticleParameters() == 4)
        self.assertTrue(plane.getNumParticles() == 1)
        self.assertTrue(
            plane.getEnergyFunction()
            == "4*epsilon_plane*((sigma_plane/("
            + normal
            + "-p0))^12-(sigma_plane/("
            + normal
            + "-p0))^6+0.25)*step(cutoff_plane-("
            + normal
            + "1-p0))"
        )

    def test_tt_bending(self):
        """Test of the function 'tt_bending'."""
        N = 100
        Temp = 300.0
        sigma = 1.7
        k_bp = 100.0
        resolution = 10.0
        Kb = opf.bending_rigidity(k_bp, resolution, Temp)
        for linear in [False, True]:
            B = N + int(linear)
            for PBC in [False, True]:
                tt_bending = opf.tt_bending(N, Kb, linear, PBC)
                self.assertTrue(tt_bending.getNumGlobalParameters() == 2)
                self.assertTrue(tt_bending.getNumAngles() == (N - int(linear)))

    def test_ut_bending(self):
        """Test of the function 'ut_bending'."""
        N = 137
        Temp = 300.0
        sigma = 1.55
        k_bp = 130.0
        resolution = 20.0
        Kb = opf.bending_rigidity(k_bp, resolution, Temp)
        for linear in [False, True]:
            B = N + int(linear)
            for PBC in [False, True]:
                ut_bending = opf.ut_bending(N, 256.0 * opf.get_kBT(Temp), linear, PBC)
                self.assertTrue(ut_bending.getNumGlobalParameters() == 2)
                self.assertTrue(
                    ut_bending.getNumAngles()
                    == (N * int(opf.get_particles_per_bead() >= 3))
                )

    def test_twist(self):
        """Test of the function 'twist'."""
        N = 335
        Temp = 300.0
        sigma = 7.8
        k_bp = 130.0
        resolution = 26.0
        Tw = 0.01 * np.pi
        Kt = opf.twisting_rigidity(95.0, sigma, Temp)
        for linear in [False, True]:
            B = N + int(linear)
            for PBC in [False, True]:
                twist = opf.twist(N, Kt, [Tw] * (N - int(linear)), linear, PBC)
                self.assertTrue(twist.getNumGlobalParameters() == 2)
                self.assertTrue(
                    twist.getNumBonds()
                    == ((N - int(linear)) * int(opf.get_particles_per_bead() >= 3))
                )

    # def test_get_platform(self):
    #     """Test of the function 'get_platform'."""

    def test_create_platform(self):
        """Test of the function 'create_platform'."""
        platform, properties = opf.create_platform("CPU4", "single")
        self.assertTrue(properties["CpuThreads"] == "4")

    def test_create_integrator(self):
        """Test of the function 'create_integrator'."""
        # variables
        Temp = 300.0
        B = 1020
        C = 0
        m = 1.0
        seed = 1
        sigma = 6.5
        dt = sigma * np.sqrt(m / opf.get_kBT(Temp))
        tau = 100.0 * dt
        # integrators
        integrator = opf.create_integrator(
            "gjf", Temp, dt, B, C, m, tau, seed, False, False
        )
        self.assertTrue(type(integrator) == mm.CustomIntegrator)
        integrator = opf.create_integrator(
            "global", Temp, dt, B, C, m, tau, seed, False, False
        )
        self.assertTrue(type(integrator) == mm.CustomIntegrator)
        integrator = opf.create_integrator(
            "langevin", Temp, dt, B, C, m, tau, seed, False, False
        )
        self.assertTrue(type(integrator) == mm.LangevinIntegrator)
        integrator = opf.create_integrator(
            "mnve", Temp, dt, B, C, m, tau, seed, False, False
        )
        self.assertTrue(type(integrator) == mm.CustomIntegrator)
        integrator = opf.create_integrator(
            "verlet", Temp, dt, B, C, m, tau, seed, True, True
        )
        self.assertTrue(type(integrator) == mm.CustomIntegrator)
        integrator = opf.create_integrator(
            "verlet", Temp, dt, B, C, m, tau, seed, False, False
        )
        self.assertTrue(type(integrator) == mm.VerletIntegrator)
        integrator = opf.create_integrator(
            "void", Temp, dt, B, C, m, tau, seed, False, False
        )
        self.assertTrue(type(integrator) == mm.LangevinIntegrator)

    def test_create_context(self):
        """Test of the function 'create_context'."""
        # variables
        L = 100.0
        B = 1021
        C = 0
        m = 1.1
        sigma = 2.3
        Temp = 300.0
        dt = 0.001 * sigma * np.sqrt(m / opf.get_kBT(Temp))
        tau = 100.0 * dt
        seed = 3
        linear = True
        N = B - int(linear)
        # context
        system, masses = opf.create_system(L, B, m, 10)
        integrator = opf.create_integrator(
            "verlet", Temp, dt, B, C, m, tau, seed, False, False
        )
        platform, properties = opf.create_platform("CPU2", "double")
        positions, u, v, t = opf.puvt_chain(N, sigma, L, linear)
        virtual_sites, positions = opf.create_virtual_sites(positions, u, sigma)
        opf.link_virtual_sites(system, virtual_sites)
        # print(system.getNumParticles(),len(positions),N,B)
        velocities = opf.v_gaussian(Temp, masses, seed)
        context = opf.create_context(
            system, integrator, platform, properties, positions, velocities
        )
        Positions = opf.get_vpositions_from_context(context, False)
        Velocities = opf.get_velocities_from_context(context)
        # check if we get the correct positions and velocities
        for i in range(opf.get_particles_per_bead() * B):
            dp = np.subtract(positions[i], Positions[i])
            self.assertTrue(np.sqrt(np.dot(dp, dp)) < 0.000001)
            dv = np.subtract(velocities[i], Velocities[i])
            self.assertTrue(np.sqrt(np.dot(dv, dv)) < 0.01)

    def test_read_write_checkpoint(self):
        """Test of the function 'read_write_checkpoint'."""
        # variables
        L = 100.0
        B = 1021
        C = 0
        m = 1.1
        sigma = 2.3
        Temp = 300.0
        dt = 0.001 * sigma * np.sqrt(m / opf.get_kBT(Temp))
        tau = 100.0 * dt
        seed = 3
        linear = True
        N = B - int(linear)
        # context
        system, masses = opf.create_system(L, B, m, 10)
        integrator = opf.create_integrator(
            "verlet", Temp, dt, B, C, m, tau, seed, False, False
        )
        platform, properties = opf.create_platform("CPU2", "double")
        positions, u, v, t = opf.puvt_chain(N, sigma, L, linear)
        virtual_sites, positions = opf.create_virtual_sites(positions, u, sigma)
        opf.link_virtual_sites(system, virtual_sites)
        velocities = opf.v_gaussian(Temp, masses, seed)
        context = opf.create_context(
            system, integrator, platform, properties, positions, velocities
        )
        # step the context
        context.getIntegrator().step(10)
        positions = opf.get_vpositions_from_context(context, False)
        velocities = opf.get_velocities_from_context(context)
        # save it
        opf.read_write_checkpoint(context, "test/test_restart.chk", "write")
        # read what has just been saved
        opf.read_write_checkpoint(context, "test/test_restart.chk", "read")
        Positions = opf.get_vpositions_from_context(context, False)
        Velocities = opf.get_velocities_from_context(context)
        # check if we get the correct positions and velocities
        for i in range(opf.get_particles_per_bead() * B):
            dp = np.subtract(positions[i], Positions[i])
            self.assertTrue(np.sqrt(np.dot(dp, dp)) < 0.000001)
            dv = np.subtract(velocities[i], Velocities[i])
            self.assertTrue(np.sqrt(np.dot(dv, dv)) < 0.01)

    def test_get_positions_from_context(self):
        """Test of the function 'get_positions_from_context'."""
        # variables
        L = 120.0
        B = 721
        C = 0
        m = 3.1
        sigma = 1.4
        Temp = 300.0
        dt = 0.001 * sigma * np.sqrt(m / opf.get_kBT(Temp))
        tau = 10.0 * dt
        seed = 2
        linear = True
        N = B - int(linear)
        # context
        system, masses = opf.create_system(L, B, m, 10)
        integrator = opf.create_integrator(
            "verlet", Temp, dt, B, C, m, tau, seed, False, False
        )
        platform, properties = opf.create_platform("CPU2", "double")
        positions, u, v, t = opf.puvt_chain(N, sigma, L, linear)
        virtual_sites, positions = opf.create_virtual_sites(positions, u, sigma)
        opf.link_virtual_sites(system, virtual_sites)
        velocities = opf.v_gaussian(Temp, masses, seed)
        context = opf.create_context(
            system, integrator, platform, properties, positions, velocities
        )
        # does not return the virtual sites
        Positions = opf.get_positions_from_context(context, False)
        self.assertTrue(len(Positions) == B)
        # return the positions in unit of nanometer
        for i in range(B):
            dx = positions[i][0] - Positions[i][0]
            dy = positions[i][1] - Positions[i][1]
            dz = positions[i][2] - Positions[i][2]
            self.assertTrue(np.sqrt(dx * dx + dy * dy + dz * dz) < 0.000001)

    def test_get_vpositions_from_context(self):
        """Test of the function 'get_vpositions_from_context'."""
        # variables
        L = 120.0
        B = 673
        C = 0
        m = 3.1
        sigma = 1.4
        Temp = 300.0
        dt = 0.001 * sigma * np.sqrt(m / opf.get_kBT(Temp))
        tau = 10.0 * dt
        seed = 2
        linear = True
        N = B - int(linear)
        # context
        system, masses = opf.create_system(L, B, m, 10)
        integrator = opf.create_integrator(
            "verlet", Temp, dt, B, C, m, tau, seed, False, False
        )
        platform, properties = opf.create_platform("CPU2", "double")
        positions, u, v, t = opf.puvt_chain(N, sigma, L, linear)
        virtual_sites, positions = opf.create_virtual_sites(positions, u, sigma)
        opf.link_virtual_sites(system, virtual_sites)
        velocities = opf.v_gaussian(Temp, masses, seed)
        context = opf.create_context(
            system, integrator, platform, properties, positions, velocities
        )
        # does return the virtual sites
        Positions = opf.get_vpositions_from_context(context, False)
        self.assertTrue(len(Positions) == (opf.get_particles_per_bead() * B))
        # return the positions in unit of nanometer
        for i in range(opf.get_particles_per_bead() * B):
            dx = positions[i][0] - Positions[i][0]
            dy = positions[i][1] - Positions[i][1]
            dz = positions[i][2] - Positions[i][2]
            self.assertTrue(np.sqrt(dx * dx + dy * dy + dz * dz) < 0.000001)

    def test_write_draw_conformation(self):
        """Test of the function 'write_draw_conformation'."""
        # variables
        L = 341.0
        B = 673
        C = 0
        m = 3.1
        sigma = 1.4
        Temp = 300.0
        dt = 0.001 * sigma * np.sqrt(m / opf.get_kBT(Temp))
        tau = 10.0 * dt
        seed = 2
        linear = True
        N = B - int(linear)
        # context
        system, masses = opf.create_system(L, B, m, 10)
        integrator = opf.create_integrator(
            "verlet", Temp, dt, B, C, m, tau, seed, False, False
        )
        platform, properties = opf.create_platform("CPU2", "double")
        positions, u, v, t = opf.puvt_chain(N, sigma, L, linear)
        virtual_sites, positions = opf.create_virtual_sites(positions, u, sigma)
        opf.link_virtual_sites(system, virtual_sites)
        velocities = opf.v_gaussian(Temp, masses, seed)
        context = opf.create_context(
            system, integrator, platform, properties, positions, velocities
        )
        # write the conformation
        positions = opf.get_positions_from_context(context, False)
        opf.write_draw_conformation(positions, name_write="test/test.xyz", mode="write")
        # read the conformation we just wrote
        Positions = []
        with open("test/test.xyz", "r") as in_file:
            lines = in_file.readlines()
            for l in lines:
                sl = l.split()
                Positions.append((float(sl[0]), float(sl[1]), float(sl[2])))
        self.assertTrue(len(Positions) == B)
        # did the function writes the correct positions
        for i in range(B):
            dx = positions[i][0] - Positions[i][0]
            dy = positions[i][1] - Positions[i][1]
            dz = positions[i][2] - Positions[i][2]
            self.assertTrue(np.sqrt(dx * dx + dy * dy + dz * dz) < 0.000001)

    def test_get_uvt_from_positions(self):
        """Test of the function 'get_uvt_from_positions'."""
        abs_tol = 1e-4
        rel_tol = 1e-6
        # variables
        L = 120.0
        N = 452
        C = 0
        mass = 0.1
        sigma = 2.7
        Temp = 300.0
        dt = 0.001 * sigma * np.sqrt(mass / opf.get_kBT(Temp))
        tau = 100.0 * dt
        seed = 2
        for linear in [False, True]:
            # number of bonds
            B = N - int(linear)
            # context
            system, masses = opf.create_system(L, N, mass, 10)
            integrator = opf.create_integrator(
                "verlet", Temp, dt, N, C, mass, tau, seed, False, False
            )
            platform, properties = opf.create_platform("CPU1", "double")
            positions, u, v, t = opf.puvt_chain(B, sigma, L, linear)
            virtual_sites, positions = opf.create_virtual_sites(positions, u, sigma)
            opf.link_virtual_sites(system, virtual_sites)
            velocities = opf.v_gaussian(Temp, masses, seed)
            context = opf.create_context(
                system, integrator, platform, properties, positions, velocities
            )
            U, V, T = opf.get_uvt_from_positions(
                opf.get_vpositions_from_context(context), linear
            )
            # check if the frame U,V,T we get is the one u,v,t we created
            for i in range(B):
                ui = u[i]
                Ui = U[i]
                vi = v[i]
                Vi = V[i]
                ti = t[i]
                Ti = T[i]
                if opf.get_particles_per_bead() >= 3:
                    for j in range(3):
                        self.assertTrue(
                            abs(Ui[j] - ui[j]) <= (abs_tol + rel_tol * abs(ui[j]))
                        )
                        self.assertTrue(
                            abs(Vi[j] - vi[j]) <= (abs_tol + rel_tol * abs(vi[j]))
                        )
                        self.assertTrue(
                            abs(Ti[j] - ti[j]) <= (abs_tol + rel_tol * abs(ti[j]))
                        )
                else:
                    self.assertTrue(abs(1.0 - np.dot(ui, ui)) <= abs_tol)
                    self.assertTrue(abs(1.0 - np.dot(vi, vi)) <= abs_tol)
                    self.assertTrue(abs(1.0 - np.dot(ti, ti)) <= abs_tol)
                    self.assertTrue(abs(1.0 - np.dot(Ti, Ti)) <= abs_tol)
                    for j in range(3):
                        self.assertTrue(abs(Ui[j]) <= abs_tol)
                        self.assertTrue(abs(Vi[j]) <= abs_tol)

    def test_add_torque_to_bond(self):
        """Test of the function 'add_torque_to_bond'."""
        linear = True
        PBC = False
        N = 1000
        B = N + int(linear)
        Temp = 300.0
        torque_to_bond = opf.add_torque_to_bond(
            0, N, -0.4 * opf.get_kBT(Temp), linear, PBC
        )
        # check
        # self.assertTrue(torque_to_bond.getNumPerParticleParameters()==1)
        # self.assertTrue(torque_to_bond.getNumGlobalParameters()==0)
        # self.assertTrue(torque_to_bond.getNumBonds()==1)
        # self.assertTrue(torque_to_bond.getNumParticlesPerBond()==4)
        self.assertTrue(torque_to_bond.getNumPerTorsionParameters() == 1)
        self.assertTrue(torque_to_bond.getNumGlobalParameters() == 0)
        self.assertTrue(torque_to_bond.getNumTorsions() == 1)

    def test_global_integrator(self):
        """Test of the function 'global_integrator'."""
        linear = False
        PBC = False
        resolution = 10.5
        overtwist = 0.0
        seed = 1
        # number of bonds
        N = 64
        # number of beads from the number of bonds
        B = N + int(linear)
        # Kuhn length in bp
        k_bp = 300.0
        # Temperature in Kelvin
        Temp = 300.0
        # mass in amu
        mass = resolution * 700.0
        # bond size in nm
        sigma = 100.0 * resolution / k_bp
        # box size in nm
        L = 2.0 * np.sqrt(
            N * (resolution / k_bp) * 100.0 * 100.0 / (12.0 - 6.0 * int(linear))
        )
        # bending strength
        Kb = opf.bending_rigidity(k_bp, resolution, Temp)
        # twisting strength (harmonic strength)
        Kt = opf.twisting_rigidity(86.0, sigma, Temp)
        # time scale
        tau = opf.minimal_time_scale([mass], [sigma], [Kb, Kt])
        # twist between two consecutives frames -> overtwist
        Tw, Tw_closure, Lk_error = opf.overtwist_to_chain(
            N, resolution, 10.5, linear, overtwist
        )
        # positions and frames u,v,t
        positions, u, v, t = opf.puvt_chain(N, sigma, L, linear, "circular")
        u, v = opf.twist_each_uv(u, v, t, [Tw] * (N - 1) + [0.0], linear)
        # system creation (com motion removed every 10 steps)
        system, masses = opf.create_system(L, B, mass, 10)
        # virtual sites
        virtual_sites, positions = opf.create_virtual_sites(positions, u, sigma)
        # link the virtual site to its bead
        opf.link_virtual_sites(system, virtual_sites)
        # Kremer-Grest model by default
        system.addForce(opf.fene(N, Temp, sigma, linear, PBC))
        # FENE pseudo-u vector
        system.addForce(opf.virtual_fene(B, Temp, sigma, PBC))
        # wca
        system.addForce(opf.wca(N, Temp, sigma, linear, PBC))
        system.addForce(opf.wca_virtual_sites(N, Temp, sigma, linear, PBC))
        # bending
        system.addForce(opf.tt_bending(N, Kb, linear, PBC))
        system.addForce(opf.ut_bending(N, 1024.0 * opf.get_kBT(Temp), linear, PBC))
        # twist
        system.addForce(opf.twist(N, Kt, [0.0] * (N - int(linear)), linear, PBC))
        # creating the platform
        platform, properties = opf.create_platform("CPU1", "double")
        # creating the integrator
        integrator = opf.create_integrator(
            "global",
            Temp,
            0.01 * tau,
            B,
            system.getNumConstraints(),
            mass,
            tau,
            seed,
            False,
            False,
        )
        # creating the context
        context = opf.create_context(
            system,
            integrator,
            platform,
            properties,
            positions,
            opf.v_gaussian(Temp, masses, seed),
        )
        # step the context
        nparticles = opf.get_particles_per_bead() * N
        for i in range(100):
            context.getIntegrator().step(1)
            mtot = context.getIntegrator().getGlobalVariableByName("mtot")
            self.assertTrue(abs(1.0 - mtot / (3 * sum(masses))) < 1e-6 or mtot == 0.0)
            self.assertTrue(
                len(context.getIntegrator().getPerDofVariableByName("am")) == nparticles
            )
            self.assertTrue(
                len(context.getIntegrator().getPerDofVariableByName("amx"))
                == nparticles
            )
            self.assertTrue(
                len(context.getIntegrator().getPerDofVariableByName("amy"))
                == nparticles
            )
            self.assertTrue(
                len(context.getIntegrator().getPerDofVariableByName("amz"))
                == nparticles
            )
            self.assertTrue(
                len(context.getIntegrator().getPerDofVariableByName("tv")) == nparticles
            )
            self.assertTrue(
                context.getIntegrator().getPerDofVariableByName("dx_lim_global")[0][0]
                == 0.0
            )
            self.assertTrue(
                context.getIntegrator().getPerDofVariableByName("dx_lim_global")[0][1]
                == 0.0
            )
            self.assertTrue(
                context.getIntegrator().getPerDofVariableByName("dx_lim_global")[0][2]
                == 0.0
            )
            # test virtual sites velocities
            ps = context.getIntegrator().getPerDofVariableByName("tv")
            if opf.get_particles_per_bead() >= 2:
                for v in range(B, 2 * B):
                    self.assertTrue(ps[v][0] == 0.0)
                    self.assertTrue(ps[v][1] == 0.0)
                    self.assertTrue(ps[v][2] == 0.0)
                    self.assertTrue(
                        context.getIntegrator().getGlobalVariableByName("ke")
                        != 0.0 * opf.get_kBT(Temp)
                    )
            # test if com = 0,0,0
            if (i % 10) == 0:
                xcom, ycom, zcom = 0.0, 0.0, 0.0
                for p in ps:
                    xcom += p[0] / nparticles
                    ycom += p[1] / nparticles
                    ycom += p[2] / nparticles
                self.assertTrue(abs(xcom) < 1e-4)
                self.assertTrue(abs(ycom) < 1e-4)
                self.assertTrue(abs(zcom) < 1e-4)

    def test_correction_flying_ice_cube(self):
        """Test of the function 'correction_flying_ice_cube'."""
        linear = True
        PBC = False
        resolution = 10.5
        overtwist = 0.0
        seed = 1
        # number of bonds
        N = 32
        # number of beads from the number of bonds
        B = N + int(linear)
        # Kuhn length in bp
        k_bp = 300.0
        # Temperature in Kelvin
        Temp = 300.0
        # mass in amu
        mass = 1.0  # resolution*700.0
        # bond size in nm
        sigma = 100.0 * resolution / k_bp
        # box size in nm
        L = 2.0 * np.sqrt(
            N * (resolution / k_bp) * 100.0 * 100.0 / (12.0 - 6.0 * int(linear))
        )
        # bending strength
        Kb = opf.bending_rigidity(k_bp, resolution, Temp)
        # twisting strength (harmonic strength)
        Kt = opf.twisting_rigidity(86.0, sigma, Temp)
        # time scale
        tau = opf.minimal_time_scale([mass], [sigma], [Kb, Kt])
        # twist between two consecutives frames -> overtwist
        Tw, Tw_closure, Lk_error = opf.overtwist_to_chain(
            N, resolution, 10.5, linear, overtwist
        )
        # positions and frames u,v,t
        ps, u, v, t = opf.puvt_chain(N, sigma, L, linear, "linear")
        ps, u, v, t = opf.wlc_conformation(
            ps, u, v, t, sigma, 50.0, 86.0, Kb, Kt, Temp, L, 1000, seed
        )
        # system creation (com motion removed every 10 steps)
        system, masses = opf.create_system(L, B, mass, 10)
        # virtual sites
        virtual_sites, ps = opf.create_virtual_sites(ps, u, sigma)
        # link the virtual site to its bead
        opf.link_virtual_sites(system, virtual_sites)
        # creating the platform
        platform, properties = opf.create_platform("CPU1", "double")
        # creating the integrator
        integrator = opf.create_integrator(
            "global",
            Temp,
            0.0,
            B,
            system.getNumConstraints(),
            mass,
            tau,
            seed,
            False,
            False,
        )
        # creating the context
        context = opf.create_context(
            system,
            integrator,
            platform,
            properties,
            ps,
            opf.v_gaussian(Temp, masses, seed),
        )
        # "flying-ice-cube" correction
        context.setVelocities(
            opf.from_np_to_mm_positions(opf.correction_flying_ice_cube(context))
        )
        masses = opf.get_masses_from_context(context)
        com, vcom = opf.get_com_from_context(context)
        ps = np.subtract(opf.get_vpositions_from_context(context), com)
        velocities = np.subtract(opf.get_velocities_from_context(context), vcom)
        # inertia (symmetric)
        I3 = np.zeros((3, 3), dtype=np.double)
        for i, p in enumerate(ps):
            I3 = np.add(I3, np.multiply(masses[i], np.outer(p, p)))
        # check com motion
        for i in range(3):
            self.assertTrue(abs(vcom[i]) < 1e-5)
        # check angular momentum
        am = np.zeros(3, dtype=np.double)
        count = 0
        for p, v in zip(ps, velocities):
            am = np.add(am, np.multiply(masses[count], np.cross(p, v)))
            count += 1
        for i in range(3):
            self.assertTrue(abs(am[i]) < 1e-5)

    def test_mnve_integrator(self):
        """Test of the function 'nve_integrator'."""
        linear, PBC = False, False
        resolution = 10.5
        overtwist = 0.0
        seed = 1
        # number of bonds
        N = 64
        # number of beads from the number of bonds
        B = N + int(linear)
        # Kuhn length in bp
        k_bp = 300.0
        # Temperature in Kelvin
        Temp = 300.0
        # mass in amu
        mass = resolution * 700.0
        # bond size in nm
        sigma = 100.0 * resolution / k_bp
        # box size in nm
        L = 2.0 * np.sqrt(
            N * (resolution / k_bp) * 100.0 * 100.0 / (12.0 - 6.0 * int(linear))
        )
        # bending strength
        Kb = opf.bending_rigidity(k_bp, resolution, Temp)
        # twisting strength (harmonic strength)
        Kt = opf.twisting_rigidity(86.0, sigma, Temp)
        # time scale
        tau = opf.minimal_time_scale([mass], [sigma], [Kb, Kt])
        # twist between two consecutives frames -> overtwist
        Tw, Tw_closure, Lk_error = opf.overtwist_to_chain(
            N, resolution, 10.5, linear, overtwist
        )
        # positions and frames u,v,t
        positions, u, v, t = opf.puvt_chain(N, sigma, L, linear, "circular")
        u, v = opf.twist_each_uv(u, v, t, [Tw] * (N - 1) + [0.0], linear)
        # system creation (com motion removed every 10 steps)
        system, masses = opf.create_system(L, B, mass, 10)
        # virtual sites
        virtual_sites, positions = opf.create_virtual_sites(positions, u, sigma)
        # link the virtual site to its bead
        opf.link_virtual_sites(system, virtual_sites)
        # Kremer-Grest model by default
        system.addForce(opf.fene(N, Temp, sigma, linear, PBC))
        # FENE pseudo-u vector
        system.addForce(opf.virtual_fene(B, Temp, sigma, PBC))
        # wca
        system.addForce(opf.wca(N, Temp, sigma, linear, PBC))
        system.addForce(opf.wca_virtual_sites(N, Temp, sigma, linear, PBC))
        # bending
        system.addForce(opf.tt_bending(N, Kb, linear, PBC))
        system.addForce(opf.ut_bending(N, 1024.0 * opf.get_kBT(Temp), linear, PBC))
        # twist
        system.addForce(opf.twist(N, Kt, [0.0] * (N - int(linear)), linear, PBC))
        # creating the platform
        platform, properties = opf.create_platform("CPU1", "double")
        # creating the integrator
        integrator = opf.create_integrator(
            "mnve",
            Temp,
            0.01 * tau,
            B,
            system.getNumConstraints(),
            mass,
            tau,
            seed,
            False,
            False,
        )
        # creating the context
        context = opf.create_context(
            system,
            integrator,
            platform,
            properties,
            positions,
            opf.v_gaussian(Temp, masses, seed),
        )
        # step the context
        for i in range(11000):
            context.getIntegrator().step(1)
            if context.getIntegrator().getGlobalVariableByName("gjf_count") == 101.0:
                self.assertTrue(
                    context.getIntegrator().getGlobalVariableByName("verlet_count")
                    < 10000.0
                )
            if (
                context.getIntegrator().getGlobalVariableByName("verlet_count")
                == 10001.0
            ):
                self.assertTrue(
                    context.getIntegrator().getGlobalVariableByName("gjf_count") < 100.0
                )


if __name__ == "__main__":
    unittest.main()
