#!/usr/bin/python
# -*- coding: utf-8 -*-
"""@package docstring
The module 'openmm_plectoneme_functions.py' is a collection of functions used to define
a Kremer-Grest polymer with bending and twisting rigidities.
This module goes with a python script 'openmm_plectoneme.py' that is an example on how to
create and run your model.
The following code (has been developped by Pascal Carrivain) is distributed under MIT licence.
"""
import data_analysis as da
import getopt
import json
import numpy as np
import openmm_plectoneme_functions as opf
import simtk.openmm as mm
import sys


def main(argv):
    seed = 1
    resolution = 10.5
    helix = 10.5
    dLk = 0.0
    N = 5714
    bp_per_nm3 = 0.0001
    platform_name = "CPU1"
    PBC = False
    mode = "run"
    pstyle = "wca"
    rerun = 0
    initial_conformation = "circular"
    bond = "fene"
    thermostat = "langevin"
    linear = False
    precision = "double"
    start = 0
    every = 100000
    MDS = 200 * every
    bblender = False
    btwist = False
    binternal_distances = False
    write_data = "/scratch"
    try:
        opts, args = getopt.getopt(
            argv,
            "hn:r:s:R:i:g:m:b:t:c:l:",
            [
                "dLk=",
                "chain_length=",
                "resolution=",
                "helix=",
                "seed=",
                "rerun=",
                "initial_conformation=",
                "platform_name=",
                "bp_per_nm3=",
                "mode=",
                "bond=",
                "thermostat=",
                "pair_style=",
                "linear=",
                "start=",
                "every=",
                "iterations=",
                "precision=",
                "write_data=",
                "save_for_blender",
                "test_twist_implementation",
                "test_internal_distances",
                "get_platforms",
                "get_local_tw",
            ],
        )
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-h":
            print("python3.7 openmm_plectoneme.py --dLk=<linking number deficit>")
            print("                               -n <chain length, number of bonds>")
            print("                               -r <resolution>")
            print(
                "                               --helix=<bp per DNA turn (dafault 10.5)>"
            )
            print("                               -s <seed>")
            print("                               -R <rerun>")
            print("                               -i <initial conformation>")
            print("                               -g <platform name>")
            print("                               --bp_per_nm3=<bp per nm^3> (WIP)")
            print("                               -m <mode> (WIP)")
            print("                               -b <bond type(fene,rigid)>")
            print(
                "                               -t <thermostat(gjf,global,langevin,mnve,verlet)>"
            )
            print("                               -c <pairwise style(wca,no)>")
            print("                               -l <linear(no,yes)>")
            print(
                "                               --start <restart simulation from MD step>"
            )
            print(
                "                               --iterations <run simulation for this many steps>"
            )
            print(
                "                               --every <save conformation every 'every' steps>"
            )
            print("                               --precision=<single,mixed,double>")
            print(
                "                               --save_for_blender save conformation file to load in Blender"
            )
            print(
                "                               --test_twist_implementation compute square twist average"
            )
            print(
                "                               --test_internal_distances compute internal distances"
            )
            print(
                "                               --write_data=<where to write the data>"
            )
            print("get the available platforms and devices (write json file), exit:")
            print("python3.7 openmm_plectoneme.py --get_platforms")
            print("get local twist, exit:")
            print("python3.7 openmm_plectoneme.py --get_local_tw")
            print("example:")
            print(
                "1: run circular molecule made of 5714 bonds 10.5 bp each, with FENE bond and WCA potential, with supercoiled density of about -0.06, with seed equal to 1, on GeForce RTX 2060 SUPER, OpenCL implementation and double precision. Start from MD step 0. Save every 100000 steps. Run for 1000000 iterations with Langevin thermostat."
            )
            print(
                "python3.7 openmm_plectoneme.py -o -0.06 -n 5714 -r 10.5 -s 1 -R 0 -i circular -g GeForce_RTX_2060_SUPER_OpenCL -m run -b fene -c wca -t langevin -l no --precision=double --iterations=1000000 --every=100000 --start=0"
            )
            print(
                "2: run circular molecule made of 10000 bonds 10.5 bp each, with FENE bond and WCA potential, with supercoiled density of about 0.0, with seed equal to 1, on GeForce GTX 1070, OpenCL implementation and double precision. Start from MD step 0. Save every 100000 steps. Run for 1000000 iterations with global thermostat. Test twist implementation."
            )
            print(
                "python3.7 openmm_plectoneme.py -o 0.0 -n 10000 -r 10.5 -s 1 -R 0 -i circular -g GeForce_GTX_1070_OpenCL -m run -b fene -c wca -t global -l no --precision=double --iterations=1000000 --every=100000 --start=0 --test_twist_implementation"
            )
            print(
                "3: run linear molecule made of 1024 bonds 10.5 bp each, with FENE bond, with supercoiled density of about 0.0, with seed equal to 1, on Tesla K80, OpenCL implementation and double precision. Start from MD step 0. Save every 100000 steps. Run for 1000000 iterations with global thermostat. Test twist implementation."
            )
            print(
                "python3.7 openmm_plectoneme.py -o 0.0 -n 1024 -r 10.5 -s 1 -R 0 -i circular -g Tesla_K80_OpenCL -m run -b fene -c no -t global -l yes --precision=double --iterations=1000000 --every=100000 --start=0 --test_twist_implementation"
            )
            print(
                "4: run linear molecule made of 128 bonds 10.5 bp each, with FENE bond, no WCA potential except for two consecutive beads, with supercoiled density of about 0.0, with seed equal to 1, on GeForce GTX 1070, OpenCL implementation and double precision. Start from MD step 0. Save every 100000 steps. Run for 1000000 iterations with Verlet integrator. Test twist implementation."
            )
            print(
                "python3.7 openmm_plectoneme.py -o 0.0 -n 128 -r 10.5 -s 1 -R 0 -i circular -g GeForce_RTX_2060_SUPER_OpenCL -m run -b fene -c no -t gjf -l no --precision=double --iterations=1000000 --every=1000000 --start=0 --test_twist_implementation"
            )
            print(
                "python3.7 openmm_plectoneme.py -o 0.0 -n 512 -r 10.5 -s 1 -R 0 -i circular -g GeForce_GTX_1660_Ti_OpenCL -m run -b fene -c no -t gjf -l yes --precision=double --iterations=1000000 --every=1000000 --start=0 --test_internal_distances"
            )
            print("documentation:")
            print("pydoc3.7 -w openmm_plectoneme_functions.py")
            sys.exit()
        elif opt == "--get_platforms":
            opf.get_platforms()
            sys.exit()
        elif opt == "--get_local_tw":
            opf.overtwist_to_chain_no_closure_tw(5714, 10.5, helix, False)
            sys.exit()
        elif opt == "--dLk":
            dLk = float(arg)
        elif opt == "-n" or opt == "--chain_length":
            N = int(arg)
        elif opt == "-r" or opt == "--resolution":
            resolution = float(arg)
        elif opt == "-s" or opt == "--seed":
            seed = int(arg)
        elif opt == "-R" or opt == "--rerun":
            rerun = int(arg)
        elif opt == "-i" or opt == "--initial_conformation":
            initial_conformation = arg
        elif opt == "-g" or opt == "--platform_name":
            platform_name = str(arg)
        elif opt == "--bp_per_nm3":
            bp_per_nm3 = float(arg)
        elif opt == "-m" or opt == "--mode":
            mode = str(arg)
            mode = "run"
        elif opt == "-b" or opt == "--bond":
            bond = str(arg)
        elif opt == "-t" or opt == "--thermostat":
            thermostat = str(arg)
        elif opt == "-c" or opt == "--pair_style":
            pstyle = str(arg)
        elif opt == "-l" or opt == "--linear":
            linear = bool(arg == "yes")
        elif opt == "--start":
            start = int(arg)
        elif opt == "--iterations":
            MDS = int(arg)
        elif opt == "--every":
            every = int(arg)
        elif opt == "--precision":
            precision = arg
        elif opt == "--write_data":
            write_data = arg
        elif opt == "--analysis":
            analysis = True
        elif opt == "--save_for_blender":
            bblender = True
        elif opt == "--test_twist_implementation":
            btwist = True
        elif opt == "--test_internal_distances":
            binternal_distances = True
        else:
            pass

    np.random.seed(seed)
    # number of beads from the number of bonds
    B = N + int(linear)
    # Kuhn length in bp
    k_bp = 300.0
    # Temperature in Kelvin
    Temp = 300.0
    # mass in amu
    mass = resolution * 700.0
    # bond size in nm
    sigma = (100.0 * resolution / k_bp) / (
        1.0 * int(bond == "rigid") + 0.965 * int(bond == "fene")
    )
    # box size in nm
    L = 2.0 * np.sqrt(
        N * (resolution / k_bp) * 100.0 * 100.0 / (12.0 - 6.0 * int(linear))
    )
    # bending strength
    Kb = opf.bending_rigidity(k_bp, resolution, Temp)
    # twisting strength (harmonic strength)
    Kt = opf.twisting_rigidity(86.0, sigma, Temp)
    # pseudo bending strength
    pKb = opf.get_pKb(Temp, Kb, Kt)
    # twist between two consecutives frames -> overtwist
    overtwist = dLk / (N * resolution / helix)
    tw, tw_closure, Lk_error = opf.overtwist_to_local_twist(
        N, resolution, helix, linear, overtwist, 10000000
    )
    print("tw=", tw, "tw_closure=", tw_closure, "Lk error=", Lk_error)
    # positions and frames u,v,t
    positions, u, v, t, success = opf.puvt_chain(
        N, sigma, L, linear, initial_conformation
    )
    if linear and pstyle == "no":
        positions, u, v, t = opf.wlc_conformation(
            positions, u, v, t, sigma, 50.0, 86.0, Kb, Kt, Temp, L, 2 * N * N, -1
        )
    if tw != 0.0:
        u, v = opf.twist_each_uv(u, v, t, np.array([tw] * (N - 1) + [0.0]), linear)
    # system creation (com motion removed every 10 steps)
    system, masses = opf.create_system(L, B, mass, 10 if thermostat == "verlet" else -1)
    # virtual sites
    use_virtual = False
    vsigma = 1.0 * sigma
    virtual_sites, positions = opf.create_virtual_sites(positions, u, vsigma)
    # link the virtual site to its bead
    if use_virtual:
        opf.link_virtual_sites(system, virtual_sites)

    # creation of the system, integrator, platform and context
    # rigid bond ?
    if bond == "rigid":
        opf.rigid_bond(system, N, sigma, linear)
    else:
        # Kremer-Grest model by default (add WCA if 'pstyle' is 'no')
        system.addForce(opf.fene(N, Temp, sigma, linear, PBC, bool(pstyle == "no")))
    # FENE pseudo-u vector
    system.addForce(opf.virtual_fene_wca(B, Temp, vsigma, PBC, use_virtual))
    # Harmonic pseudo-u vector
    # system.addForce(opf.virtual_harmonic(B, 3.0 * opf.get_kBT(Temp) / sigma**2, sigma, PBC, use_virtual))
    # wca
    if pstyle == "wca":
        system.addForce(opf.wca(N, Temp, sigma, linear, PBC))
    # bending
    system.addForce(opf.tt_bending(N, Kb, linear, PBC))
    system.addForce(opf.ut_bending(N, pKb, linear, PBC, use_virtual))
    system.addForce(opf.uU_bending(N, pKb, linear, PBC))
    # twist
    system.addForce(opf.twist(N, Kt, [0.0] * (N - int(linear)), linear, PBC, False))
    # time scale
    tau = opf.minimal_time_scale(
        [mass] * 4, [sigma] * 4, [opf.get_kBT(Temp), Kb, Kt, pKb]
    )
    tau = opf.minimal_time_scale([mass], [sigma], [opf.get_kBT(Temp)])
    # creating the platform
    platform, properties = opf.create_platform(platform_name, precision)
    # creating the integrator
    xtau = 1.0
    dt = 0.001 * tau if thermostat in ["mnve", "verlet"] else 0.01 * tau
    if (np.sqrt(pKb / mass) * dt) > 2:
        print(np.sqrt(pKb / mass) * dt)
        sys.exit()
    integrator = opf.create_integrator(
        thermostat,
        Temp,
        dt,
        B,
        system.getNumConstraints(),
        mass,
        xtau * tau,
        seed,
        False,
        False,
    )
    # creating the context
    context = opf.create_context(
        system,
        integrator,
        platform,
        properties,
        positions,
        opf.v_gaussian(Temp, masses, -1),
    )
    # get info about the system you just created
    opf.get_info_system(context)
    # write info (log file)
    if not btwist:
        log_file = {}
        log_file["bond"] = bond
        log_file["pair_style"] = pstyle
        log_file["bonds"] = N
        log_file["mass_amu"] = mass
        log_file["resolution_bp"] = resolution
        log_file["bond_length_nm"] = sigma
        log_file["bending_persistence_nm"] = 50
        log_file["twisting_persistence_nm"] = 86
        log_file["Kb_kBT"] = Kb
        log_file["pKb_kBT"] = pKb
        log_file["Kt_kBT"] = Kt
        log_file["Temp"] = Temp
        log_file["dt"] = dt
        log_file["tau"] = tau
        log_file["particles_per_bead"] = opf.get_particles_per_bead()
        log_file["dLk"] = dLk
        log_file["overtwist"] = overtwist
        log_file["thermostat"] = thermostat
        log_file["initial_conformation"] = initial_conformation
        log_file["linear"] = linear
        log_file["platform"] = platform_name
        log_file["precision"] = precision
        with open(write_data + "/info.json", "w") as out_file:
            json.dump(log_file, out_file, indent=1)

    # do we have an OpenMM checkpoint to start from ?
    opf.read_write_checkpoint(
        context, write_data + "/restart/restart.s" + str(start) + ".chk", "read"
    )
    # test variables
    mean_std_phi2 = np.zeros((MDS // every + 1, 2))
    Rs2 = np.zeros(B, dtype=np.double)
    # running the model
    for i in range(start, start + MDS + 1, every):
        # get the positions (in nanometer) for further analysis
        vpositions = opf.get_vpositions_from_context(context)
        opf.print_variables(context, linear, i)
        if not btwist and not binternal_distances:
            # write the conformation
            opf.write_draw_conformation(
                vpositions,
                name_write=write_data + "/out/vxyz." + str(i) + ".out",
                mode="write",
            )
        else:
            # twist
            u, v, t = opf.get_uvt_from_positions(vpositions, linear)
            phi2 = np.square(da.chain_twist(u, v, t, linear))
            mean_std_phi2[i // every, :2] = np.mean(phi2), np.std(phi2)
            # internal distances
            Rs2 = np.add(
                Rs2, da.square_internal_distances(vpositions, 0, B - 1, linear)
            )
        # stack conformation
        if bblender or seed == 1:
            opf.from_context_to_blender(
                context,
                False,
                np.full((B,), sigma),
                write_data + "/visualisation_with_blender/output",
                "yes",
            )
        # step the context
        if i < (start + MDS):
            context.getIntegrator().step(every)
            if thermostat == "global":
                context.setVelocities(
                    opf.from_np_to_mm_positions(opf.correction_flying_ice_cube(context))
                )
    # write a checkpoint to restart the simulation
    if not btwist and not binternal_distances:
        opf.read_write_checkpoint(
            context,
            write_data + "/restart/restart.s" + str(start + MDS) + ".chk",
            "write",
        )
    else:
        print(
            "<square twist>="
            + str(mean_std_phi2[MDS // every, 0])
            + "+/-"
            + str(mean_std_phi2[MDS // every, 1] / np.sqrt(N))
            + "/"
            + str(sigma / 86.0)
        )
        Rs2_simulation = np.multiply(1.0 / (MDS // every + 1), Rs2)[1 : (B // 2)]
        Rs2_theory = da.Rs2_wlc(
            50.0,
            N * 100.0 * resolution / k_bp,
            100.0 * resolution / k_bp,
            np.arange(0, B, 1),
        )[1 : (B // 2)]
        print(
            "sqrt(<(1-Rs2/wlc)^2>)="
            + str(
                np.sqrt(
                    np.mean(
                        np.square(
                            np.divide(
                                np.subtract(Rs2_simulation, Rs2_theory), Rs2_theory
                            )
                        )
                    )
                )
            )
        )


if __name__ == "__main__":
    main(sys.argv[1:])
