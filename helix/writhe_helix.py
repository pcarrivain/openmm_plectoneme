#!/usr/bin/python
# -*- coding: utf-8 -*-
"""@package docstring
The python script 'writhe_helix' computes the writhe of a helix with out/in closure.
The following code (has been developped by Pascal Carrivain) is distributed under MIT licence.
"""
import json
import numpy as np
import sys
import getopt
import simtk.openmm as mm
import openmm_plectoneme_functions as opf
import data_analysis as da

from mpl_toolkits import mplot3d
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.patches as patches

def main(argv):
    resolution=10.5
    overtwist=-0.06
    turns=128
    N=5714
    linear=False
    compute_Wr=False
    try:
        opts,args=getopt.getopt(argv,"ht:n:p:r:l:",["turns=","chain_length=","resolution=","linear=","tmpdir=","compute_Wr"])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print("python3.7 writhe_helix.py -n <chain length, number of bonds> -r <resolution> -l <linear(no)> --tmpdir=<where to save the data>")
            print("/scratch/pcarriva/anaconda3/bin/python3.7 writhe_helix.py --compute_Wr --turns=512 -n 11429 -r 10.5 -l no")
            sys.exit()
        elif opt=="-t" or opt=="--turns":
            turns=int(arg)
        elif opt=="-n" or opt=="--chain_length":
            N=int(arg)
        elif opt=="-r" or opt=="--resolution":
            resolution=float(arg)
        elif opt=="-l" or opt=="--linear":
            linear=bool(arg=="yes")
        elif opt=="--tmpdir":
            tmpdir=arg
        elif opt=="--compute_Wr":
            compute_Wr=True
        else:
            pass

    # number of beads from the number of bonds
    B=N+int(linear)
    # Kuhn length in bp
    k_bp=300.0
    # mass in amu
    mass=resolution*700.0
    # bond size in nm
    sigma=100.0*resolution/k_bp
    # box size in nm
    L=2.0*np.sqrt(N*(resolution/k_bp)*100.0*100.0/(12.0-6.0*int(linear)))
    # twist between two consecutives frames -> overtwist
    Tw,Tw_closure,Lk_error=opf.overtwist_to_chain(N,resolution,10.5,linear,overtwist)
    # read json
    in_out=["helix_out","helix_in","double_helix"]
    try:
        with open("writhe_helix.json","r") as in_file:
            Wrs_Tws=json.load(in_file)
        for h in in_out:
            if not h in Wrs_Tws.keys():
                Wrs_Tws[h]={}
    except IOError:
        Wrs_Tws={}
        for h in in_out:
            Wrs_Tws[h]={}
    # compute
    for h in in_out:
        if not str(turns) in Wrs_Tws[h].keys() or compute_Wr:
            print(h+str(turns))
            try:
                positions,u,v,t=opf.puvt_chain(N,sigma,L,linear,h+str(turns))
                P=len(positions)
                new_positions=np.zeros((P,3))
                for k in range(P):
                    new_positions[k,:3]=positions[k][0],positions[k][1],positions[k][2]
                Wr=da.chain_writhe(new_positions)
                Tw=np.sum(da.chain_twist(u,v,t,linear))/(2.0*np.pi)
                Lk=Tw+Wr
                print("Lk="+str(Lk)+"("+str(Tw)+"+"+str(Wr)+")")
                Wrs_Tws[h][str(turns)]={"N":N,"Wr":Wr,"Tw":Tw,"Lk":Lk,"overtwist":Lk/(N*resolution/10.5)}
            except:
                print("too much turns.")
    # json
    with open("writhe_helix.json","w") as out_file:
        json.dump(Wrs_Tws,out_file,indent=1)
    turns=np.sort(np.array([int(k) for k in Wrs_Tws[in_out[0]].keys()],dtype='int64'))
    # get writhe, twist and overtwist
    Wrs=np.zeros((np.prod(turns.shape),3))
    Tws=np.zeros((np.prod(turns.shape),3))
    overtwists=np.zeros((np.prod(turns.shape),3))
    max_turns=0
    for i,h in enumerate(in_out):
        for j,t in enumerate(turns):
            if str(t) in Wrs_Tws[h].keys():
                Wrs[j,i]=Wrs_Tws[h][str(t)]["Wr"]
                Wrs[j,i]=Wrs_Tws[h][str(t)]["Wr"]
                Tws[j,i]=Wrs_Tws[h][str(t)]["Tw"]
                Tws[j,i]=Wrs_Tws[h][str(t)]["Tw"]
                overtwists[j,i]=Wrs_Tws[h][str(t)]["overtwist"]
                overtwists[j,i]=Wrs_Tws[h][str(t)]["overtwist"]
                if Wrs_Tws[h][str(t)]["N"]==N:
                    max_turns=max(max_turns,t)
    # plot
    fig=plt.figure("Wrs")
    ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r'$helix~turns$',ylabel=r'$Wr$',title="",xlim=(0.0,max_turns),ylim=(-1000.0,1000.0))
    ax.plot(turns,Wrs[:,0],marker='o',label="helix out")
    ax.plot(turns,Wrs[:,1],marker='o',label="helix in")
    ax.plot(turns,Wrs[:,2],marker='o',label="double helix")
    ax.legend()
    fig.savefig("Wrs_N"+str(N)+".png",dpi=600)
    fig.clf()
    plt.close("Wrs")
    fig=plt.figure("Tws")
    ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r'$helix~turns$',ylabel=r'$Tw$',title="",xlim=(0.0,max_turns),ylim=(-1.0,1.0))
    ax.plot(turns,Tws[:,0],marker='o',label="helix out")
    ax.plot(turns,Tws[:,1],marker='o',label="helix in")
    ax.plot(turns,Tws[:,2],marker='o',label="double helix")
    ax.legend()
    fig.savefig("Tws_N"+str(N)+".png",dpi=600)
    fig.clf()
    plt.close("Tws")
    fig=plt.figure("overtwists")
    ax=fig.add_axes([0.15,0.15,0.7,0.7],xlabel=r'$helix~turns$',ylabel=r'$overtwist$',title="",xlim=(0.0,max_turns),ylim=(-0.2,0.2))
    ax.plot(turns,overtwists[:,0],marker='o',label="helix out")
    ax.plot(turns,overtwists[:,1],marker='o',label="helix in")
    ax.plot(turns,overtwists[:,2],marker='o',label="double helix")
    ax.plot(turns,np.add(overtwists[:,0],overtwists[:,1]),marker='o',label="helix out + helix in")
    ax.legend()
    fig.savefig("overtwists_N"+str(N)+".png",dpi=600)
    fig.clf()
    plt.close("overtwists")

if __name__=="__main__":
    main(sys.argv[1:])
