#!/usr/bin/python
# -*- coding: utf-8 -*-
"""@package docstring
The module 'openmm_plectoneme_functions.py' is a collection of functions used to define
a Kremer-Grest polymer with bending and twisting rigidities.
This module goes with a python script 'openmm_plectoneme.py' that is an example on how to
create and run your model.
The following code (has been developped by Pascal Carrivain) is distributed under MIT licence.
"""
import sys
import getopt
import numpy as np
import openmm_plectoneme_functions as opf


def main(argv):
    seed = 1
    resolution = 10.5
    overtwist = -0.06
    N = 5714
    platform_name = "CPU1"
    bp_per_nm3 = 0.0001
    PBC = False
    mode = "run"
    pair_style = "wca"
    replica_bending = 1
    replica_twist = 101
    rerun = 0
    bond = "fene"
    thermostat = "langevin"
    linear = False
    precision = "double"
    iterations = 1000000
    every = 100000
    scheduler = "local"
    queue = ""
    start = 0
    write_data = "/scratch"
    bdask = False
    try:
        opts, args = getopt.getopt(
            argv,
            "ho:n:r:s:R:g:m:b:t:c:l:",
            [
                "overtwist=",
                "chain_length=",
                "np=",
                "resolution=",
                "seed=",
                "rerun=",
                "platform_name=",
                "bp_per_nm3=",
                "mode=",
                "bond=",
                "thermostat=",
                "pair_style",
                "replica_bending=",
                "replica_twist=",
                "linear=",
                "start=",
                "precision=",
                "iterations=",
                "every=",
                "scheduler=",
                "queue=",
                "write_data=",
                "dask",
                "get_platforms",
            ],
        )
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-h":
            print("python3.7 openmm_plectoneme -o <overtwist>")
            print("                            -n <chain length>")
            print("                            -r <resolution>")
            print("                            -s <seed>")
            print("                            -R <rerun>")
            print("                            -g <platform name>")
            print("                            --bp_per_nm3=<bp per nm^3>")
            print("                            -m <mode>")
            print("                            -b <bond type(fene,rigid)>")
            print(
                "                            -t <thermostat(gjf,global,langevin,mnve,verlet)>"
            )
            print("                            -c <wca>")
            print(
                "                            --replica_bending=<number of replicas for the bending>"
            )
            print(
                "                            --replica_twist=<number of replicas for the twist>"
            )
            print("                            -l <linear(no)>")
            print("                            --precision=<single,mixed,double>")
            print(
                "                            --iterations=<number of steps (# of exchanges is iterations/every)"
            )
            print("                            --every=<replica exchange every>")
            print("                            --scheduler=<local,SGE,PBS,SLURM>")
            print(
                "                            --queue=<name of the queue, not required for scheduler='local'>"
            )
            print("                            --write_data=<where to save the data>")
            print(
                "/scratch/pcarriva/anaconda3/bin/python3.7 openmm_plectoneme.py -o -0.06 -n 800 -r 10.5 -s 1 -g OpenCL --bp_per_nm3=0.0001 -m run -b fene -t langevin -c wca --replica_bending=10 --replica_twist=10 -l no --precision=double"
            )
            print(
                "/scratch/pcarriva/anaconda3/bin/python3.7 openmm_plectoneme.py --get_platforms prints the available platforms and devices and exit."
            )
            sys.exit()
        elif opt == "--get_platforms":
            opf.get_platforms()
            sys.exit()
        elif opt == "-o" or opt == "--overtwist":
            overtwist = float(arg)
        elif opt == "-n" or opt == "--chain_length":
            N = int(arg)
        elif opt == "-r" or opt == "--resolution":
            resolution = float(arg)
        elif opt == "-s" or opt == "--seed":
            seed = int(arg)
        elif opt == "-R" or opt == "--rerun":
            rerun = int(arg)
        elif opt == "-g" or opt == "--platform_name":
            platform_name = str(arg)
        elif opt == "--bp_per_nm3":
            bp_per_nm3 = float(arg)
        elif opt == "-m" or opt == "--mode":
            mode = str(arg)
            mode = "run"
        elif opt == "-b" or opt == "--bond":
            bond = str(arg)
        elif opt == "-t" or opt == "--thermostat":
            thermostat = str(arg)
        elif opt == "-c" or opt == "--pair_style":
            pair_style = arg
            pair_style = "wca"
        elif opt == "--replica_bending":
            replica_bending = int(arg)
        elif opt == "--replica_twist":
            replica_twist = int(arg)
        elif opt == "-l" or opt == "--linear":
            linear = bool(arg == "yes")
        elif opt == "--start":
            start = int(arg)
        elif opt == "--precision":
            precision = arg
        elif opt == "--iterations":
            iterations = int(arg)
        elif opt == "--every":
            every = int(arg)
        elif opt == "--scheduler":
            scheduler = arg
        elif opt == "--queue":
            queue = arg
        elif opt == "--dask":
            bdask = True
        elif opt == "--write_data":
            write_data = arg

    # Kuhn length in bp
    k_bp = 300.0
    # Temperature in Kelvin
    Temp = 300.0
    # mass
    mass = resolution * 700.0
    # bond size in nm
    sigma = (100.0 * resolution / k_bp) / (
        1.0 * int(bond == "rigid") + 0.965 * int(bond == "fene")
    )
    # box size in nm
    L = 0.5 * N * sigma
    # time scale
    tau = sigma * np.sqrt(mass / opf.get_kBT(Temp))

    # creation of the replicas
    replica = 0
    Ks = np.zeros((replica_bending * replica_twist, 6))
    start, end = 0.75, 1.25
    for b in range(replica_bending):
        # bending strength
        if replica_bending > 1:
            Kb = opf.bending_rigidity(
                (start + b * (end - start) / (replica_bending - 1)) * k_bp,
                resolution,
                Temp,
            )
        else:
            Kb = opf.bending_rigidity(k_bp, resolution, Temp)
        for t in range(replica_twist):
            # twisting strength
            if replica_twist > 1:
                Kt = opf.twisting_rigidity(
                    (start + t * (end - start) / (replica_twist - 1)) * 85.0,
                    sigma,
                    Temp,
                )
            else:
                Kt = opf.twisting_rigidity(85.0, sigma, Temp)
            Ks[replica, :6] = Kb, Kt, overtwist, Temp, sigma, mass
            tau = min(tau, sigma * np.sqrt(mass / Kb))
            tau = min(tau, sigma * np.sqrt(mass / Kt))
            tau = min(tau, sigma * np.sqrt(mass / opf.get_pKb(Temp)))
            replica += 1
    # running the replica exchange
    opf.replica_exchange(
        Ks,
        resolution,
        0.01 * tau,
        tau,
        N,
        L,
        every,
        iterations // every,
        seed,
        linear,
        platform_name + "_" + precision,
        write_data,
        replica // 2,
        True,
        bdask,
        scheduler,
        queue,
    )


if __name__ == "__main__":
    main(sys.argv[1:])
