#!/usr/bin/python
# -*- coding: utf-8 -*-
"""@package docstring
The module 'openmm_plectoneme_functions.py' is a collection
of functions used to define a Kremer-Grest polymer with
bending and twisting rigidities.
This module goes with a python script 'openmm_plectoneme.py'
that is an example on how to create and run your model.
This module goes with a python script
'openmm_plectoneme_replica_exchange.py' that is an example
on how to run replica exchange in mechanical space parameters.
The following code (has been developped by Pascal Carrivain)
is distributed under MIT licence.
"""
try:
    import logging
    import dask
    from dask.distributed import Client, LocalCluster, wait
except ImportError:
    print("dask ImportError")
try:
    from dask_jobqueue import SLURMCluster
    from dask_jobqueue import PBSCluster
    from dask_jobqueue import SGECluster
except ImportError:
    print("dask_jobqueue ImportError")
try:
    from jobqueue_features.clusters import CustomSLURMCluster
    from jobqueue_features.decorators import on_cluster, mpi_task
    from jobqueue_features.mpi_wrapper import mpi_wrap
    from jobqueue_features.functions import set_default_cluster
except ImportError:
    print("no jobqueue_features")
import data_analysis as da
import json
import os
import simtk.openmm as mm
import simtk.unit as unit
import struct
import sys
import time

from mpl_toolkits import mplot3d
import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt

import numpy as np
from numpy import linalg as LA
import numba

print("numba version", numba.__version__)
from numba import prange, int32
from numba.experimental import jitclass
from itertools import combinations


# @jitclass([("value", int32)])
class particles_per_bead:  # object):
    nparticles_per_bead = 1

    def __init__(self, value):
        self.value = value
        particles_per_bead.nparticles_per_bead += 1

    def change(self, new_value: int):
        self.value = new_value

    # @property
    def get(self) -> int:
        return self.value

    def use_simple_twist(self) -> bool:
        return self.nparticles_per_bead == 2

    def use_double_twist(self) -> bool:
        return self.nparticles_per_bead == 3


@numba.jit(nopython=True)
def my_cross(u: np.ndarray, v: np.ndarray) -> np.ndarray:
    """return the cross product u x v.
    do not use numpy cross because it does not work (from my experience) with numba.
    Parameters
    ----------
    u : first vector (np.ndarray)
    v : second vector (np.ndarray)
    Returns
    -------
    cross product u x v (np.ndarray)
    """
    return np.array(
        [
            u[1] * v[2] - u[2] * v[1],
            u[2] * v[0] - u[0] * v[2],
            u[0] * v[1] - u[1] * v[0],
        ],
        dtype=numba.float64,
    )


@numba.jit
def normalize(u: np.ndarray) -> np.ndarray:
    """return the normalized numpy array.
    Parameters
    ----------
    u : vector to normalize (np.ndarray)
    Returns
    -------
    np.ndarray
    """
    norm_u = np.sqrt(np.sum(np.square(u)))
    return u if norm_u < 1e-9 else np.multiply(1.0 / norm_u, u)


def openmm_plectoneme_units():
    print("position is in nanometer.")
    print("mass is in amu.")
    print("time is in picosecond.")
    print("kBT is in kJ/mol.")
    return (
        unit.nanometer,
        unit.amu,
        unit.picosecond,
        unit.BOLTZMANN_CONSTANT_kB * unit.AVOGADRO_CONSTANT_NA * unit.kelvin,
    )


def minimal_time_scale(mass: list, sigmas: list, U: list) -> float:
    """return the minimal time-scale based on the mass and size of the particle.
    Parameters
    ----------
    mass   : mass (in amu) of each particle (list of float)
    sigmas : size (in nm) of each particle (list of float)
    U      : energy strength (in kBT) (list float)
    Returns
    -------
    minimal time-scale (float)
    Raises
    ------
    """
    time_scale = np.zeros(len(U), dtype=np.double)
    for i, u in enumerate(U):
        if u != 0.0:
            time_scale[i] = np.min(
                np.array([s * np.sqrt(mass[j] / u) for j, s in enumerate(sigmas)])
            )
    return np.min(time_scale[np.where(time_scale > 0.0)])


@numba.jit
def get_kBT(Temp: float) -> float:
    """return kB*Temp (length is in nm, mass in amu unit and time in picosecond).
    Parameters
    ----------
    Temp : temperature in Kelvin (float)
    Returns
    -------
    float
    Raises
    ------
    """
    nm = 1e-9
    ps = 1e-12
    kg = 1.66053907e-27
    kB = 1.381e-23
    return kB * Temp / (kg * (nm / ps) ** 2)


@numba.jit
def get_bead_friction(radius: float, viscosity: float = 0.83 * 0.001) -> float:
    """return Stokes friction 6*Pi*viscosity*radius.
    Parameters
    ----------
    radius    : bead radius in nm (float)
    viscosity : viscosity (default is water viscosity) in kg.m-1.s-1 (float)
    Returns
    -------
    float
    Raises
    ------
    """
    # in kg.nm-1.s-1
    viscosity /= 1e9
    # in kg.nm-1.ps-1
    viscosity /= 1e12
    # in amu.nm-1.ps-1
    viscosity /= 1.66053907e-27
    return 6.0 * np.pi * viscosity * radius


@numba.jit
def get_pKb(Temp: float, Kb: float = 0.0, Kt: float = 0.0) -> float:
    """return pseudo-bending strength in kBT
    Parameters
    ----------
    Temp : temperature in Kelvin (float)
    Kb   : bending strength (float)
    Kt   : twisting strength (float)
    Returns
    -------
    float
    Raises
    ------
    """
    # return max(2.0 * Kb, max(2.0 * Kt, 64.0 * get_kBT(Temp)))
    return max(Kb, Kt)


@numba.jit
def from_i_to_bead(i: int, N: int) -> int:
    """return the index 'i' projected to the ring chain.
    Parameters
    ----------
    i : index (int)
    N : the number of beads in the ring chain (int)
    Returns
    -------
    return the index 'i' (int) projected onto the ring chain.
    Raises
    ------
    """
    if i < 0:
        return N - (-i) % N
    elif i >= N:
        return i % N
    else:
        return i


def print_variables(context: mm.Context, linear: bool, iterations: int = 0):
    """print variables from the context.
    Parameters
    ----------
    context    : OpenMM context
    linear     : linear (True) or ring polymer (False)
    iterations : current iteration of the context
    Returns
    -------
    Raises
    ------
    if the bead to pseudo-u bead vector is close to be parallel with the tangent vector, exit.
    """
    C = context.getSystem().getNumConstraints()
    vpositions = get_vpositions_from_context(context)
    V, D = vpositions.shape
    P = particles_per_bead.nparticles_per_bead
    N = V // P
    KE = context.getState(getEnergy=True).getKineticEnergy()
    PE = context.getState(getEnergy=True).getPotentialEnergy()
    derivatives = context.getState(
        getParameterDerivatives=True
    ).getEnergyParameterDerivatives()
    Temp = 300.0 * unit.kelvin
    try:
        Temp = context.getIntegrator().getTemperature()
    except:
        pass
    try:
        Temp = context.getIntegrator().getGlobalVariableByName("Temp") * unit.kelvin
    except:
        pass
    try:
        dt = context.getIntegrator().getStepSize() / unit.picosecond
    except:
        pass
    print("----- " + str(iterations) + ":")
    print(str(N) + " particles(" + str(V) + ")")
    print("dt=" + str(dt))
    print(
        "sqrt(<bond^2>),min,max="
        + str(
            average_bond_size_chain(
                vpositions[:N, :3], np.array([N], dtype=int), -1.0, 0, False
            )
        )
    )
    print("temperature=" + str(Temp))
    print(
        "K/<.>="
        + str(
            get_kinetic_energy_from_context(context, 0, P * N)
            / get_kinetic_energy_average(
                Temp / unit.kelvin, P * N, C
            )
        )
    )
    print(
        "K^2/<.>="
        + str(
            get_kinetic_energy_from_context(context, 0, P * N)
            ** 2
            / get_square_kinetic_energy_average(
                Temp / unit.kelvin, P * N, C
            )
        )
    )
    print("PE=" + str(PE / (unit.kilojoule / unit.mole)))
    print("TE=" + str((KE + PE) / (unit.kilojoule / unit.mole)))
    print("com motion=", np.sum(get_velocities_from_context(context), axis=0))
    if "K_fene" in derivatives.keys():
        print("diff(fene,K_fene)=" + str(derivatives["K_fene"]))
    if "Kb_tt" in derivatives.keys():
        print("diff(bending,Kb_tt)=" + str(derivatives["Kb_tt"]))
    if "epsilon_vwca" in derivatives.keys():
        print("diff(wca pseudo-u,epsilon)=" + str(derivatives["epsilon_vwca"]))
    if "K_vfene" in derivatives.keys():
        print("diff(fene pseudo-u,K)=" + str(derivatives["K_vfene"]))
    if "K_vharmonic" in derivatives.keys():
        print("diff(harmonic pseudo-u,K)=" + str(derivatives["K_vharmonic"]))
    if "Kt" in derivatives.keys():
        print("diff(twist,Kt)=" + str(derivatives["Kt"]))
    if "Kb_ut" in derivatives.keys():
        print("diff(pseudo bending,Kb_ut)=" + str(derivatives["Kb_ut"]))
    if "Kb_uU" in derivatives.keys():
        print("diff(pseudo bending,Kb_uU)=" + str(derivatives["Kb_uU"]))
    if "Kb_uUt" in derivatives.keys():
        print("diff(bending uUt,Kb_uUt)=" + str(derivatives["Kb_uUt"]))
    if P >= 2:
        print("<pseudo-u bead distance>=", get_bead_to_pseudo_bead_distance(vpositions))
    # print correlations
    u, v, t = get_uvt_from_positions(vpositions)
    if P >= 2:
        pseudou = get_pseudo_u_from_positions(vpositions, 0)
    if P >= 3:
        pseudoU = get_pseudo_u_from_positions(vpositions, 1)
    M = N - int(linear)
    uv_bar = np.mean(np.array([np.dot(u[i], v[i]) for i in range(M)]))
    ut_bar = np.mean(np.array([np.dot(u[i], t[i]) for i in range(M)]))
    vt_bar = np.mean(np.array([np.dot(v[i], t[i]) for i in range(M)]))
    tt_bar = np.mean(
        np.array([np.dot(t[i], t[from_i_to_bead(i + 1, N)]) for i in range(M - 1)])
    )
    if P >= 2:
        sqtw_bar = np.mean(
            np.square(
                np.arccos(
                    np.array([(np.dot(u[i], u[from_i_to_bead(i + 1, N)]) + np.dot(v[i], v[from_i_to_bead(i + 1, N)])) / (1.0 + np.dot(t[i], t[from_i_to_bead(i + 1, N)])) for i in range(M)])
                )
            )
        )
    if P >= 2:
        pseudou_bar = np.mean(np.array([np.dot(pseudou[i], u[i]) for i in range(M)]))
        for i in range(M):
            if np.dot(pseudou[i], u[i]) < np.cos(0.25 * np.pi):
                print(
                    "warning pseudo(u).u",
                    i,
                    ":",
                    np.dot(pseudou[i], u[i]),
                    np.sum(
                        np.square(
                            np.subtract(
                                vpositions[i], vpositions[get_pseudo_u_from_i(i, N)]
                            )
                        )
                    ),
                )
                sys.exit()
    print("frame correlation along the chain:")
    print("<t.t>=" + str(tt_bar))
    if P == 2:
        print(
            "<u.v>="
            + str(uv_bar)
            + " <u.t>="
            + str(ut_bar)
            + " <v.t>="
            + str(vt_bar)
            + " <u'.u>="
            + str(pseudou_bar)
        )
    if P == 3:
        print(
            "<u.v>="
            + str(uv_bar)
            + " <u.t>="
            + str(ut_bar)
            + " <v.t>="
            + str(vt_bar)
            + " <u'.u>="
            + str(pseudou_bar)
            + " <u'.U'>="
            + str(np.mean(np.array([np.dot(pseudou[i], pseudoU[i]) for i in range(M)])))
        )
    if P >= 2:
        print("<sqtw>=" + str(sqtw_bar))


@numba.jit(nopython=True)
def square_distance_with_PBC(p1: list, p2: list, L: list, PBC: bool = True) -> float:
    """return the distance between two points with PBC.
    Parameters
    ----------
    p1  : point 1 (list of float)
    p2  : point 2 (list of float)
    L   : box size (list of float)
    PBC : periodic boundary conditions (bool)
    Returns
    -------
    square distance with PBC (float)
    """
    dx, dy, dz = p1[0] - p2[0], p1[1] - p2[1], p1[2] - p2[2]
    if PBC:
        dx -= L[0] * np.rint(dx / L[0])
        dy -= L[1] * np.rint(dy / L[1])
        dz -= L[2] * np.rint(dz / L[2])
    return dx * dx + dy * dy + dz * dz


@numba.jit(nopython=True, parallel=True)
def average_bond_size_chain(
    positions: np.ndarray, N_per_C: np.ndarray, L: float, Ci: int = 0, PBC: bool = True
) -> tuple:
    """return the average bond size, min and max of chain.
    Parameters
    ----------
    positions : positions (in nm) of the N beads return by 'get_positions_from_context' function
    N_per_C   : number of beads per chain (np.ndarray)
    L         : box size (float)
    Ci        : chain index (int)
    PBC       : periodic boundary conditions (bool)
    Returns
    -------
    square root of the average quadratic bond size (float), min (float) and max (float)
    Raises
    ------
    np.sqrt raises an error for average_square_bond_size<0
    """
    C = int(N_per_C.size)
    box = [L] * 3
    # start of each chain
    s1 = int(np.sum(np.array([N_per_C[c] for c in range(0, Ci)])))
    n1 = int(N_per_C[Ci])
    p1 = positions[np.arange(s1, s1 + n1)]
    # average, min and max
    amM = np.zeros(3, dtype=np.double)
    d2 = np.array(
        [square_distance_with_PBC(p1[n], p1[n + 1], box, PBC) for n in range(n1 - 1)]
    )
    for i in prange(3):
        if i == 0:
            amM[i] = np.sum(d2)
        elif i == 1:
            amM[i] = np.min(d2)
        elif i == 2:
            amM[i] = np.max(d2)
    return (
        np.sqrt(amM[0] / n1),
        np.sqrt(amM[1]),
        np.sqrt(amM[2]),
    )  # ,np.where(d2==amM[1]),np.where(d2==amM[2])


def get_positions_from_context(context: mm.Context, PBC: bool = False) -> np.ndarray:
    """return the positions (in nanometer) from the OpenMM context (do not consider pseudo-u beads).
    Parameters                                       
    ----------
    context : OpenMM context
    PBC     : enforce periodic box ? (bool)
    Returns
    -------
    A list of the positions of the particles (from the context) in nanometer.
    Raises
    ------
    """
    pbuffer = context.getState(getPositions=True, enforcePeriodicBox=PBC).getPositions()
    inv_nm = 1.0 / unit.nanometer
    N = len(pbuffer) // particles_per_bead.nparticles_per_bead
    return np.array(
        [
            [pbuffer[i][0] * inv_nm, pbuffer[i][1] * inv_nm, pbuffer[i][2] * inv_nm]
            for i in range(N)
        ]
    )


def get_vpositions_from_context(context: mm.Context, PBC: bool = False) -> np.ndarray:
    """return the positions (in nanometer) from the OpenMM context.
    Parameters                                       
    ----------
    context : OpenMM context
    PBC     : enforce periodic box ? (bool)
    Returns
    -------
    A list (numpy.ndarray) of the positions of the particles (from the context) in nanometer.
    Raises
    ------
    """
    inv_nm = 1.0 / unit.nanometer
    pbuffer = context.getState(getPositions=True, enforcePeriodicBox=PBC).getPositions()
    N = len(pbuffer)
    positions = np.zeros((N, 3), dtype=np.double)
    for i, p in enumerate(pbuffer):
        positions[i, :3] = p[0] * inv_nm, p[1] * inv_nm, p[2] * inv_nm
    return positions


def from_OpenMM_Vec3_to_Numpy_array(v: mm.Vec3, x) -> np.ndarray:
    """return Numpy array from OpenMM Vec3
    Parameters
    ----------
    v : OpenMM Vec3 (mm.Vec3)
    x : unit (mm.unit)
    Return
    ------
    np.ndarray
    """
    N = len(v)
    return np.array(
        [
            [v[i][0] * x, v[i][1] * x, v[i][2] * x]
            for i in range(N)
        ],
        dtype=np.double,
    )


def get_vpositions_and_velocities_from_context(
    context: mm.Context, PBC: bool = False
) -> np.ndarray:
    """return the positions (in nanometer) and velocities (in nanometer/ps) from the OpenMM context.
    Parameters                                       
    ----------
    context : OpenMM context
    PBC     : enforce periodic box ? (bool)
    Returns
    -------
    A tuple (numpy.ndarray, numpy.ndarray) of the positions (in nanometer) and velocities (in nm/ps) of the particles.
    Raises
    ------
    """
    inv_nm = 1.0 / unit.nanometer
    inv_nm_per_ps = 1.0 / (unit.nanometer / unit.picosecond)
    # state = context.getState(getPositions=True, getVelocities=True, enforcePeriodicBox=PBC)
    # pbuffer = state.getPositions()
    # vbuffer = state.getVelocities()
    # del state
    pbuffer = context.getState(getPositions=True, enforcePeriodicBox=PBC).getPositions()
    vbuffer = context.getState(getVelocities=True).getVelocities()
    N = len(pbuffer)
    return (
        np.array(
            [
                [pbuffer[i][0] * inv_nm, pbuffer[i][1] * inv_nm, pbuffer[i][2] * inv_nm]
                for i in range(N)
            ],
            dtype=np.double,
        ),
        np.array(
            [
                [
                    vbuffer[i][0] * inv_nm_per_ps,
                    vbuffer[i][1] * inv_nm_per_ps,
                    vbuffer[i][2] * inv_nm_per_ps,
                ]
                for i in range(N)
            ],
            dtype=np.double,
        ),
    )


def get_masses_from_context(context: mm.Context) -> np.ndarray:
    """return the masses (in amu) from the OpenMM context.
    Parameters
    ----------
    context : OpenMM context
    Returns
    -------
    np.ndarray
    Raises
    ------
    """
    nparticles = context.getSystem().getNumParticles()
    masses = np.array(
        [context.getSystem().getParticleMass(i) / unit.amu for i in range(nparticles)],
        dtype=np.double,
    )
    return masses


def get_velocities_from_context(context: mm.Context) -> np.ndarray:
    """return the velocities (in nanometer per picosecond) from the OpenMM context.
    Parameters
    ----------
    context : OpenMM context
    Returns
    -------
    np.ndarray
    Raises
    ------
    """
    N = context.getSystem().getNumParticles()
    vbuffer = context.getState(getVelocities=True).getVelocities()
    inv_nm_per_ps = 1.0 / (unit.nanometer / unit.picosecond)
    return np.array(
        [
            [
                vbuffer[i][0] * inv_nm_per_ps,
                vbuffer[i][1] * inv_nm_per_ps,
                vbuffer[i][2] * inv_nm_per_ps,
            ]
            for i in range(N)
        ],
        dtype=np.double,
    )


def get_kinetic_energy_from_context(context: mm.Context, start: int, end: int) -> float:
    """return the kinetic energy (in units of kBT) from the OpenMM context.
    Parameters
    ----------
    context : OpenMM context
    start   : keep particles from 'start' to 'end' [start,end[ (int)
    end     : keep particles from 'start' to 'end' [start,end[ (int)
    Returns
    -------
    float
    Raises
    ------
    """
    masses = get_masses_from_context(context)
    velocities = get_velocities_from_context(context)
    N, D = velocities.shape
    K = 0.5 * np.sum(
        np.array([masses[i] * np.sum(np.square(velocities[i, :D])) for i in range(N)])
    )
    return K


def from_context_to_blender(
    context: mm.Context,
    PBC: bool = False,
    sigmas: np.ndarray = np.array([]),
    file_name: str = "/scratch/visualisation_with_blender",
    append_to: str = "no",
):
    """write files for visualisation of the context with blender (do not consider pseudo-u beads).
    first file with '.bin' extension is the conformation.
    second file with no extension is the details like bond length ...
    Parameters                                       
    ----------
    context   : OpenMM context
    PBC       : enforce periodic box ? (bool)
    sigmas    : array of bond size (np.ndarray)
    file_name : name of the file to create (str)
    append_to : append to the file no/yes (str)
    Returns
    -------
    Raises
    ------
    """
    # get max box edge
    L = 0.0
    for c in range(3):
        for x in range(3):
            L = max(
                L,
                (((context.getSystem().getDefaultPeriodicBoxVectors())[c])[x])
                / unit.nanometer,
            )
    # get positions
    positions = np.subtract(
        get_positions_from_context(context, PBC), np.array([0.5 * L, 0.5 * L, 0.5 * L])
    )
    # write the file
    str_wb_ab = ["wb", "ab"][int(append_to == "yes")]
    with open(file_name + ".bin", str_wb_ab) as out_file:
        for i, p in enumerate(positions):
            bd = struct.pack("ddd", p[0], p[1], p[2])
            out_file.write(bd)
            # we do not need frame
            bd = struct.pack("ddd", 0.0, 0.0, 1.0)
            out_file.write(bd)
    # write description
    with open(file_name, "w") as out_file:
        for i, p in enumerate(positions):
            out_file.write(
                "1 "
                + str(0.5 * sigmas[i])
                + " "
                + str(0.5 * sigmas[i])
                + " "
                + str(i)
                + " 0 0\n"
            )


@numba.jit(nopython=True)
def wlc_conformation(
    ps: np.ndarray,
    us: np.ndarray,
    vs: np.ndarray,
    ts: np.ndarray,
    lb: float,
    lp: float,
    lt: float,
    Kb: float,
    Kt: float,
    Temp: float,
    L: float,
    mcs: int = 1000,
    seed: int = 1,
) -> tuple:
    """return equilibrated WLC conformation (Monte-Carlo with pivot moves).
    Parameters
    ----------
    ps   : positions of the beads (np.ndarray)
    us   : normals (np.ndarray)
    vs   : cross(t,u) (np.ndarray)
    ts   : tangents (np.ndarray)
    lb   : bond length in nm (float)
    lp   : persistence length in nm (float)
    lt   : twisting persistence length in nm (float)
    Kb   : bending rigidity in kBT (float)
           Eb = Kb * (1 - cos(bending))
    Kt   : twisting rigidity in kBT (float)
           Et = 0.5 * Kt * tw^2
    Temp : temperature in kelvin (float)
    L    : box edge (float)
    mcs  : number of Monte-Carlo steps (int)
    seed : seed for the numpy pRNGs (int)
           if seed < 0, do nothing
    Returns
    -------
    np.ndarray
    Raises
    ------
    """
    if seed > -1:
        np.random.seed(seed)
    kBT = get_kBT(Temp)
    N, D = ps.shape
    # initialisation
    old_u = np.zeros((N, D), np.double)
    old_v = np.zeros((N, D), np.double)
    old_t = np.zeros((N, D), np.double)
    lbs = np.zeros(N - 1, np.double)
    for i in range(N - 1):
        lbs[i] = np.sqrt(np.sum(np.square(np.subtract(ps[i + 1, :3], ps[i, :3]))))
    for i in range(N):
        old_u[i, :D] = us[i, :D]
        old_v[i, :D] = vs[i, :D]
        old_t[i, :D] = ts[i, :D]
    # bending and twisting energies
    old_E = Kb * np.sum(
        np.array([1.0 - np.dot(old_t[i], old_t[i + 1]) for i in range(N - 2)])
    )
    costw = np.array(
        [
            min(
                1.0,
                max(
                    -1.0,
                    (np.dot(old_u[i], old_u[i + 1]) + np.dot(old_v[i], old_v[i + 1]))
                    / (1.0 + np.dot(old_t[i], old_t[i + 1])),
                ),
            )
            for i in range(N - 2)
        ]
    )
    # old_E += 0.5 * Kt * np.sum(np.array([np.arccos(c) ** 2 for c in costw]))
    old_E += 0.5 * Kt * np.sum(np.square(np.arccos(costw)))
    # Metropolis
    nacceptations = 0
    angle0 = np.sqrt(lb / lp)
    new_u = np.zeros((1, 3), np.double)
    new_v = np.zeros((1, 3), np.double)
    new_t = np.zeros((1, 3), np.double)
    for i in range(mcs):
        # new conformation
        unif1 = 2.0 * np.pi * np.random.random()
        unif2 = np.arccos(2.0 * np.random.random() - 1.0)
        axe = np.array(
            [
                np.sin(unif2) * np.cos(unif1),
                np.sin(unif2) * np.sin(unif1),
                np.cos(unif2),
            ]
        )
        angle = angle0 * (1.0 - 2.0 * np.random.random())
        start = 1 + int((N - 2) * np.random.random())
        new_u[0, :3] = rotation(axe, angle, old_u[start, :3])
        new_v[0, :3] = rotation(axe, angle, old_v[start, :3])
        new_t[0, :3] = rotation(axe, angle, old_t[start, :3])
        old_costw = min(
            1.0,
            max(
                -1.0,
                (
                    np.dot(old_u[start - 1, :3], old_u[start, :3])
                    + np.dot(old_v[start - 1, :3], old_v[start, :3])
                )
                / (1.0 + np.dot(old_t[start - 1, :3], old_t[start, :3])),
            ),
        )
        new_costw = min(
            1.0,
            max(
                -1.0,
                (
                    np.dot(old_u[start - 1], new_u[0, :3])
                    + np.dot(old_v[start - 1], new_v[0, :3])
                )
                / (1.0 + np.dot(old_t[start - 1], new_t[0, :3])),
            ),
        )
        # new energy
        new_E = old_E
        new_E -= Kb * (1.0 - np.dot(old_t[start - 1], old_t[start]))
        new_E -= 0.5 * Kt * np.arccos(old_costw) ** 2
        new_E += Kb * (1.0 - np.dot(old_t[start - 1], new_t[0, :3]))
        new_E += 0.5 * Kt * np.arccos(new_costw) ** 2
        # acceptation ?
        acceptation = True if new_E <= old_E else bool(np.random.random() < np.exp(-(new_E - old_E) / kBT))
        if i == 0 or acceptation:
            for j in range(start, N):
                old_u[j, :D] = rotation(axe, angle, old_u[j, :D])
                old_v[j, :D] = rotation(axe, angle, old_v[j, :D])
                old_t[j, :D] = rotation(axe, angle, old_t[j, :D])
            old_E = new_E
            nacceptations += 1
    # new positions
    for i in range(1, N):
        ps[i, :D] = np.add(ps[i - 1, :D], np.multiply(lbs[i - 1], old_t[i - 1, :D]))
    # com to the middle of the box
    com = np.subtract(
        np.multiply(1.0 / N, np.sum(ps, axis=0)), np.array([0.5 * L, 0.5 * L, 0.5 * L])
    )
    return np.subtract(ps, com), old_u, old_v, old_t, nacceptations / mcs


@numba.jit(nopython=True)
def gaussian_chain(
    ps: np.ndarray,
    us: np.ndarray,
    vs: np.ndarray,
    ts: np.ndarray,
    lb: float,
    Temp: float,
    L: float,
    seed: int = 1,
) -> tuple:
    """return linear gaussian chain (no excluded volume constraints).
    Parameters
    ----------
    ps   : positions of the beads (np.ndarray)
    us   : normals (np.ndarray)
    vs   : cross(t,u) (np.ndarray)
    ts   : tangents (np.ndarray)
    lb   : bond length in nm (float)
    Temp : temperature in kelvin (float)
    L    : box edge (float)
    seed : seed for the numpy pRNGs (int)
           if seed < 0, do nothing
    Returns
    -------
    np.ndarray
    Raises
    ------
    """
    if seed > -1:
        np.random.seed(seed)
    kBT = get_kBT(Temp)
    N, D = ps.shape
    # initialisation
    new_us = np.zeros((N, D), np.double)
    new_vs = np.zeros((N, D), np.double)
    new_ts = np.zeros((N, D), np.double)
    new_ps = np.zeros((N, D), np.double)
    bn = np.zeros((1, D), np.double)
    # bond size
    k = 3.0 * get_kBT(Temp) / lb ** 2
    # lbond = np.multiply(np.sqrt(3.0) * lb, np.random.random(N))
    lbond = np.multiply(2.0 * lb, np.random.random(N))
    # new frames (no parallel transport here)
    for n in range(N):
        # bond vector distribution ~ exp(-3*r^2/2*lb^2)
        # new_ts[n, :3] = np.random.normal(0, lb / np.sqrt(3), size=3)
        # lbond[n] = np.sqrt(np.sum(np.square(new_ts[n, :3]))
        # new_ts[n, :3] = normalize(new_ts[n, :3])
        unif1 = 2.0 * np.pi * np.random.random()
        unif2 = np.arccos(2.0 * np.random.random() - 1.0)
        new_ts[n, :3] = (
            np.sin(unif2) * np.cos(unif1),
            np.sin(unif2) * np.sin(unif1),
            np.cos(unif2),
        )
        cos_b = np.dot(ts[n, :3], new_ts[n, :3])
        angle = np.arccos(np.minimum(1.0, np.maximum(-1.0, cos_b)))
        bn = normalize(my_cross(ts[n, :3], new_ts[n, :3]))
        if n == 0:
            new_us[n, :3] = rotation(bn, angle, us[n, :3])
            new_vs[n, :3] = rotation(bn, angle, vs[n, :3])
    # parallel transport
    for i in range(1, N):
        cos_b = np.dot(new_ts[n - 1], new_ts[n])
        angle = np.arccos(np.minimum(1.0, np.maximum(-1.0, cos_b)))
        bn = normalize(my_cross(new_ts[n - 1], new_ts[n]))
        new_us[n] = rotation(bn, angle, new_us[n - 1])
        new_vs[n] = rotation(bn, angle, new_vs[n - 1])
    # new positions
    for d in range(D):
        new_ps[0, d] = 0.0
    for i in range(1, N):
        new_ps[i] = np.add(new_ps[i - 1], np.multiply(lbond[i - 1], new_ts[i - 1]))
    # com to the middle of the box
    com = np.subtract(
        np.multiply(1.0 / N, np.sum(ps, axis=0)), np.array([0.5 * L, 0.5 * L, 0.5 * L])
    )
    return np.subtract(new_ps, com), new_us, new_vs, new_ts


@numba.jit(nopython=True)
def chain_with_rigid_bonds(
    ps: np.ndarray,
    us: np.ndarray,
    vs: np.ndarray,
    ts: np.ndarray,
    lb: float,
    Temp: float,
    L: float,
    seed: int = 1,
) -> tuple:
    """return chain with rigid bonds (no excluded volume constraints).
    Parameters
    ----------
    ps   : positions of the beads (np.ndarray)
    us   : normals (np.ndarray)
    vs   : cross(t,u) (np.ndarray)
    ts   : tangents (np.ndarray)
    lb   : bond length in nm (float)
    Temp : temperature in kelvin (float)
    L    : box edge (float)
    seed : seed for the numpy pRNGs (int)
           if seed < 0, do nothing
    Returns
    -------
    np.ndarray
    Raises
    ------
    """
    if seed > -1:
        np.random.seed(seed)
    kBT = get_kBT(Temp)
    N, D = ps.shape
    # initialisation
    new_us = np.zeros((N, D), np.double)
    new_vs = np.zeros((N, D), np.double)
    new_ts = np.zeros((N, D), np.double)
    new_ps = np.zeros((N, D), np.double)
    bn = np.zeros((1, D), np.double)
    # new frames (no parallel transport here)
    for n in range(N):
        unif1 = 2.0 * np.pi * np.random.random()
        unif2 = np.arccos(2.0 * np.random.random() - 1.0)
        new_ts[n, :3] = (
            np.sin(unif2) * np.cos(unif1),
            np.sin(unif2) * np.sin(unif1),
            np.cos(unif2),
        )
        cos_b = np.dot(ts[n], new_ts[n])
        bn = normalize(my_cross(ts[n], new_ts[n]))
        if n == 0:
            new_us[n] = rotation(bn, cos_b, us[n])
            new_vs[n] = rotation(bn, cos_b, vs[n])
    # parallel transport
    for i in range(1, N):
        cos_b = np.dot(new_ts[n - 1], new_ts[n])
        bn = normalize(my_cross(new_ts[n - 1], new_ts[n]))
        new_us[n] = rotation(bn, cos_b, new_us[n - 1])
        new_vs[n] = rotation(bn, cos_b, new_vs[n - 1])
    # new positions
    for d in range(D):
        new_ps[0, d] = 0.0
    for i in range(1, N):
        new_ps[i] = np.add(new_ps[i - 1], np.multiply(lb, new_ts[i - 1]))
    # com to the middle of the box
    com = np.subtract(
        np.multiply(1.0 / N, np.sum(ps, axis=0)), np.array([0.5 * L, 0.5 * L, 0.5 * L])
    )
    return np.subtract(new_ps, com), new_us, new_vs, new_ts


def puvt_chain(
    N: int, sigma: float, L: float, linear: bool, conformation: str = "circular"
) -> tuple:
    """return the positions of the beads inside the chain, as-well-as u, v, t frames.
    There is one frame per bead.
    For a ring of N bonds you have N beads while for a linear polymer of N bonds you have N+1 beads.
    Parameters
    ----------
    N            : number of bonds (int)
    sigma        : bond size (float)
    L            : size of the box (float)
    linear       : linear (True) or ring (False) (bool)
    conformation : if the chain is circular, the initial conformation is:
                   'circular'
                   'linear'
                   'helix_in'
                   'helix_out'
                   'double_helix' (str)
                   To specify the number of helix turns: 'helix_in45', 'helix_out12' or 'double_helix23' ...
    Returns
    -------
    If 'conformation' is 'linear' and 'linear' is True, the chain is a straight line (contour length N*sigma).
    The positions of the (N+1 = linear or N = ring) beads inside the chain (np.ndarray).
    The (N+1 = linear or N = ring) us (np.ndarray).
    The (N+1 = linear or N = ring) vs (np.ndarray).
    The (N+1 = linear or N = ring) ts (np.ndarray).
    1 if success else 0 (int).
    If the number of helix turns is too big for the number of bonds, the function returns:
    np.array([]), np.array([]), np.array([]), np.array([]) 0
    Raises
    ------
    """
    B = N + int(linear)
    twoPi = 2.0 * np.pi
    angle = twoPi / float(B + int(linear))
    R = 0.5 * sigma / np.sin(0.5 * angle)
    # positions
    if not linear:
        if conformation == "circular":
            ps = np.array(
                [
                    [
                        0.5 * L + R * np.cos((0.5 + i) * angle),
                        0.5 * L + R * np.sin((0.5 + i) * angle),
                        0.5 * L,
                    ]
                    for i in range(B)
                ],
                dtype=np.double,
            )
        elif conformation == "linear":
            right_hcircle = 4
            left_hcircle = right_hcircle + int((N % 2) != 0)
            half_segments = (N - right_hcircle - left_hcircle) // 2
            # first line (+)
            ps = np.array(
                [[0.0, i * sigma, 0.0] for i in range(half_segments + 1)],
                dtype=np.double,
            )
            # first half-circle
            axe = np.array([0.0, 0.0, 1.0], dtype=np.double)
            angle = np.pi / float(right_hcircle + 1)
            tangent = np.array([0.0, 1.0, 0.0], dtype=np.double)
            for i in range(right_hcircle):
                tangent = rotation(axe, angle, tangent)
                ps = np.append(
                    ps,
                    np.array(
                        [np.add(ps[-1], np.multiply(sigma, tangent))], dtype=np.double
                    ),
                    axis=0,
                )
            # second line (-)
            p_start = ps[-1]
            ps = np.concatenate(
                (
                    ps,
                    np.array(
                        [
                            [p_start[0], p_start[1] - (i + 1) * sigma, p_start[2]]
                            for i in range(half_segments)
                        ],
                        dtype=np.double,
                    ),
                ),
                axis=0,
            )
            # second half-circle
            angle = np.pi / float(left_hcircle + 1)
            tangent = np.array([0.0, -1.0, 0.0], dtype=np.double)
            for i in range(left_hcircle - 1):
                tangent = rotation(axe, angle, tangent)
                ps = np.append(
                    ps,
                    np.array(
                        [np.add(ps[-1], np.multiply(sigma, tangent))], dtype=np.double
                    ),
                    axis=0,
                )
        elif "helix" in conformation and not "double_helix" in conformation:
            helix_in_out = 1 * ("helix_out" in conformation) - 1 * (
                "helix_in" in conformation
            )
            # H steps
            # x=rh*cos(i)
            # y=rh*sin(i)
            # z=ch*i
            # distance between two points 2*rh^2*(1-cos(2*pi/H))+4*pi^2*ch^2*/H^2
            xsigma, rsigma = 2, 1
            turns = int((conformation.replace("helix_out", "")).replace("helix_in", ""))
            if turns <= 0:
                print(
                    "Number of turns from 'conformation' argument is <= 0, do nothing."
                )
                return np.array([]), np.array([]), np.array([]), np.array([]), 0
            if turns >= (B // 2):
                print(
                    "Number of turns from 'conformation' argument is too big, do nothing."
                )
                return np.array([]), np.array([]), np.array([]), np.array([]), 0
            I = turns * xsigma + 1
            H = B - I
            H -= 1
            if (H // turns) < 2:
                print("Only one bead per turn, do nothing.")
                return np.array([]), np.array([]), np.array([]), np.array([]), 0
            turns_2pi = turns * twoPi
            if H <= 0 or np.cos(turns_2pi / H) == 1.0:
                print(
                    "Number of turns from 'conformation' argument is too big, do nothing."
                )
                return np.array([]), np.array([]), np.array([]), np.array([]), 0
            # vertical separation (2*pi*ch) of helix's loop
            # height of the helix I*sigma (I is the number of beads to close the helix)
            ch = sigma / np.pi
            rh = (
                sigma
                * np.sqrt(
                    np.maximum(
                        0.0,
                        ((xsigma * turns) ** 2 - H ** 2) / (np.cos(turns_2pi / H) - 1),
                    )
                )
                / (np.sqrt(2.0) * H)
            )
            if rh < (rsigma * sigma):
                print(
                    "radius of the helix is lesser than "
                    + str(rsigma)
                    + "*sigma, do nothing."
                )
                return np.array([]), np.array([]), np.array([]), np.array([]), 0
            else:
                print("line=", I, "helix=", H, "beads=", B)
                print("rh/(" + str(rsigma) + "*sigma)=" + str(rh / (rsigma * sigma)))
            # first point
            ps = np.array([rh, 0.0, 0.0], dtype=np.double)
            # helix
            for h in np.multiply(turns_2pi / H, np.arange(1, H + 1)):
                ps = np.vstack(
                    (
                        ps,
                        np.array(
                            [rh * np.cos(h), rh * np.sin(h), ch * h], dtype=np.double
                        ),
                    )
                )
            # closure
            ps = np.vstack(
                (
                    ps,
                    np.add(
                        ps[H, :3],
                        np.array([helix_in_out * sigma, 0.0, 0.0], dtype=np.double),
                    ),
                )
            )
            for i in range(I - 1):
                ps = np.vstack(
                    (
                        ps,
                        np.add(
                            ps[H + 1, :3],
                            np.array([0.0, 0.0, -(i + 1) * sigma], dtype=np.double),
                        ),
                    )
                )
            # print([np.sqrt(np.sum(np.square(np.subtract(ps[i,:3],ps[i+1,:3]))))/sigma for i in range(B-1)])
            # print(np.sqrt(np.sum(np.square(np.subtract(ps[B-1,:3],ps[0,:3]))))/sigma)
            # print(B,ps.shape)
        elif "double_helix" in conformation:
            turns = int(conformation.replace("double_helix", ""))
            if turns >= (B // 2):
                print(
                    "Number of turns from 'conformation' argument is too big, do nothing."
                )
                return np.array([]), np.array([]), np.array([]), np.array([]), 0
            turns_2pi = turns * twoPi
            # x=rh*cos(i)
            # y=rh*sin(i)
            # z=ch*i
            # B=2*H+2*L (helices and lines to join)
            hL = 1
            L = 2 * hL
            rh = hL * sigma
            H = (B - 2 * L) // 2
            if (H // turns) < 5:
                print("Less than five beads per turn, do nothing.")
                return np.array([]), np.array([]), np.array([]), np.array([]), 0
            # vertical separation (2*pi*ch) of helix's loop
            # print("arg=", 2.0 * (np.cos(turns_2pi / H) - 1.0) * hL * hL + 1)
            ch = (
                sigma
                * H
                * np.sqrt(
                    np.maximum(0.0, 2.0 * (np.cos(turns_2pi / H) - 1.0) * hL * hL + 1)
                )
                / turns_2pi
            )
            if (twoPi * ch) < sigma:
                print(
                    "vertical separation (2*pi*ch) of helix's loop is lesser than sigma, do nothing."
                )
                return np.array([]), np.array([]), np.array([]), np.array([]), 0
            else:
                pass
            # first helix
            ps = np.array([rh, 0.0, 0.0], dtype=np.double)
            # helix
            for h in np.multiply(turns_2pi / H, np.arange(1, H + 1)):
                ps = np.vstack(
                    (
                        ps,
                        np.array(
                            [rh * np.cos(h), rh * np.sin(h), ch * h], dtype=np.double
                        ),
                    )
                )
            # line to join first and second helices
            N, D = ps.shape
            for l in range(L - 1):
                ps = np.vstack(
                    (
                        ps,
                        np.add(
                            ps[N - 1, :3],
                            np.array([-(l + 1) * sigma, 0.0, 0.0], dtype=np.double),
                        ),
                    )
                )
            # second helix
            sps = np.array([-rh, 0.0, 0.0], dtype=np.double)
            for h in np.multiply(turns_2pi / H, np.arange(1, H + 1)):
                sps = np.vstack(
                    (
                        sps,
                        np.array(
                            [-rh * np.cos(h), -rh * np.sin(h), ch * h], dtype=np.double
                        ),
                    )
                )
            # revert the order and stack with system's positions
            N, D = sps.shape
            for n in range(N):
                ps = np.vstack((ps, sps[N - 1 - n, :3]))
            # line to join second and first helices
            for l in range(L - 1):
                ps = np.vstack(
                    (
                        ps,
                        np.add(
                            sps[0, :3],
                            np.array([(l + 1) * sigma, 0.0, 0.0], dtype=np.double),
                        ),
                    )
                )
        else:
            print("no valid conformation, exit.")
            print(
                "try with circular, linear, helix_in${T}, helix_out${T} or double_helix${T} where T is the number of turns."
            )
            return np.array([]), np.array([]), np.array([]), np.array([]), 0
    else:
        if conformation == "circular":
            ps = np.array(
                [
                    [
                        0.5 * L + R * np.cos((0.5 + i) * angle),
                        0.5 * L + R * np.sin((0.5 + i) * angle),
                        0.5 * L,
                    ]
                    for i in range(B)
                ],
                dtype=np.double,
            )
        else:
            print("chain is a straight line (contour length nbonds*sigma)")
            ps = np.array(
                [[i * sigma, 0.5 * L, 0.5 * L] for i in range(B)], dtype=np.double
            )
    # com to the middle of the box
    com = np.subtract(
        np.multiply(1.0 / B, np.sum(ps, axis=0)),
        np.array([0.5 * L, 0.5 * L, 0.5 * L], dtype=np.double),
    )
    ps = np.subtract(ps, com)
    # tangents
    ts = np.array(
        [normalize(np.subtract(ps[from_i_to_bead(i + 1, B)], ps[i])) for i in range(B)],
        dtype=np.double,
    )
    # normals
    if "helix" in conformation and not "double_helix" in conformation:
        us = np.zeros((B, 3), np.double)
        us[0, :3] = rotation(
            ts[0, :3], 0.5 * np.pi, normalize(np.cross(ts[0, :3], ts[1, :3]))
        )
        for i in range(1, H):
            cos_tt = np.dot(ts[i - 1, :3], ts[i, :3])
            # print("H",cos_tt,normalize(np.cross(ts[i-1,:3],ts[i,:3])))
            us[i, :3] = rotation(
                normalize(np.cross(ts[i - 1, :3], ts[i, :3])),
                np.arccos(cos_tt),
                us[i - 1, :3],
            )
        # to the right (in) or to the left (out)
        start = H - 1
        cos_tt = np.dot(ts[start, :3], ts[start + 1, :3])
        # print("2",cos_tt,normalize(np.cross(ts[start,:3],ts[start+1,:3])),ts[start,:3],ts[start+1,:3])
        us[start + 1, :3] = rotation(
            normalize(np.cross(ts[start, :3], ts[start + 1, :3])),
            np.arccos(cos_tt),
            us[start, :3],
        )
        for i in range(I - 1):
            start = H
            cos_tt = np.dot(ts[start, :3], ts[start + 1, :3])
            # print("I",cos_tt,normalize(np.cross(ts[start,:3],ts[start+1,:3])))
            us[start + 1 + i, :3] = rotation(
                normalize(np.cross(ts[start, :3], ts[start + 1, :3])),
                np.arccos(cos_tt),
                us[start, :3],
            )
        # to the right (in) or to the left (out)
        start = H + 1 + I - 2
        cos_tt = np.dot(ts[start, :3], ts[start + 1, :3])
        # print("2",cos_tt)
        us[start + 1, :3] = rotation(
            normalize(np.cross(ts[start, :3], ts[start + 1, :3])),
            np.arccos(cos_tt),
            us[start, :3],
        )
    elif "double_helix" in conformation:
        N, D = ps.shape
        us = np.zeros((B, 3), dtype=np.double)
        us[0, :3] = rotation(
            ts[0, :3], -0.5 * np.pi, normalize(np.cross(ts[0, :3], ts[1, :3]))
        )
        for i in range(1, N):
            cos_tt = np.dot(ts[i - 1, :3], ts[i, :3])
            if np.absolute(cos_tt) < 1e-9:
                # bending angle is almost pi/2
                # therefore current normal takes the value of the previous one
                us[i, :3] = us[i - 1, :3]
            else:
                us[i, :3] = rotation(
                    normalize(np.cross(ts[i - 1, :3], ts[i, :3])),
                    np.arccos(cos_tt),
                    us[i - 1, :3],
                )
    else:
        us = np.array([[0.0, 0.0, 1.0]] * B, dtype=np.double)
    # v=cross(t,u)
    vs = np.array(
        [normalize(np.cross(ts[i], us[i])) for i in range(B)], dtype=np.double
    )
    return ps, us, vs, ts, 1


def from_np_to_mm_positions(positions: np.ndarray) -> list:
    return [mm.Vec3(p[0], p[1], p[2]) for p in positions]


def puvt_single_molecule(N: int, sigma: float) -> tuple:
    """return the positions of the N+1 beads inside the chain.
    Parameters
    ----------
    N      : number of bonds (int)
    sigma  : bond size (float)
    Returns
    -------
    The positions of the N+1 beads inside the linear chain (list of OpenMM.Vec3).
    The N+1 frames u,v,t. In the linear case the function return the closure condition frame too.
    Raises
    ------
    """
    B = N + 1
    # positions
    p = [mm.Vec3(0.5 * B * sigma, 0.5 * B * sigma, i * sigma) for i in range(B)]
    # tangents
    t = np.zeros((B, 3), np.double)
    for i in range(B):
        ip1 = from_i_to_bead(i + 1, B)
        t[i] = normalize(
            np.array([p[ip1][0] - p[i][0], p[ip1][1] - p[i][1], p[ip1][2] - p[i][2]])
        )
    # normals
    u = np.array([[1.0, 0.0, 0.0]] * B)
    # v=cross(t,u)
    v = np.array([normalize(np.cross(t[i], u[i])) for i in range(B)])
    return p, u, v, t


def create_pseudo_u_beads(positions: np.ndarray, u: list, distance: float):
    """return beads and pseudo-u beads positions (list of OpenMM.Vec3).
    Parameters
    ----------
    positions : positions of the N beads (np.ndarray).
    u         : u of the attached frame (list of OpenMM.Vec3).
    distance  : the distance between bead and pseudo-u bead (float).
    Returns
    -------
    list of OpenMM.Vec3
    Raises
    ------
    """
    B, D = positions.shape
    P = particles_per_bead.nparticles_per_bead
    new_positions = [mm.Vec3(0.0, 0.0, 0.0)] * (P * B)
    weight1 = 1.0
    weight2 = 1.0 - weight1
    for i, p in enumerate(positions):
        new_positions[i] = mm.Vec3(p[0], p[1], p[2])
        ip1 = from_i_to_bead(i + 1, B)
        # pseudo-u particles
        if P >= 2:
            ui = u[i]
            new_positions[i + B] = mm.Vec3(
                weight1 * p[0] + weight2 * positions[ip1, 0] + ui[0] * distance,
                weight1 * p[1] + weight2 * positions[ip1, 1] + ui[1] * distance,
                weight1 * p[2] + weight2 * positions[ip1, 2] + ui[2] * distance,
            )
        # pseudo-U particles
        if P >= 3:
            new_positions[i + 2 * B] = mm.Vec3(
                weight1 * p[0] + weight2 * positions[ip1, 0] - ui[0] * distance,
                weight1 * p[1] + weight2 * positions[ip1, 1] - ui[1] * distance,
                weight1 * p[2] + weight2 * positions[ip1, 2] - ui[2] * distance,
            )
    return new_positions


@numba.jit
def twist_each_uv(
    us: np.ndarray,
    vs: np.ndarray,
    ts: np.ndarray,
    tws: np.ndarray,
    linear: bool,
    use_tw: bool = False,
) -> tuple:
    """return 'us' and 'vs' twisted about 'tws'.
    If frame 'i' and 'i+1' are twisted about tw,
    they are twist about tw+tws[i] at the end.
    There is one u and one v per bead.
    If the length of the local twist list is equal to T,
    only the first T consecutive frames are twisted.
    Parameters
    ----------
    us     : normal vectors (np.ndarray)
    vs     : cross(t,u) vectors (np.ndarray)
    ts     : tangent vectors (np.ndarray)
    tws    : list of local twist values between frame 'i' and frame 'i+1' (np.ndarray)
    linear : True for linear and False for ring polymer (bool)
    use_tw : use already existing local twist (bool)
    Returns
    -------
    A tuple u,v of twisted frames u,v,t (np.ndarray, np.ndarray).
    The size of the list u and the size of the list v are
    equal to the length of the input variables 'us' and 'vs'.
    Raises
    ------
    """
    # number of beads (one pseudo-u per bead)
    B, D = us.shape
    # number of bonds
    N = B - int(linear)
    # number of twist
    T = min(N - int(linear), len(tws))
    # copy u and v
    new_u = np.copy(us)
    new_v = np.copy(vs)
    # get array of local twist before to twist
    old_tws = da.chain_twist(us, vs, ts, 1e-12)
    # twist and then parallel transport
    for i in range(T):
        # local twist + delta twist
        new_tw = old_tws[i] + tws[i]
        ip0 = i
        ip1 = from_i_to_bead(i + 1, B)
        # if ip0 == (N - 1) and ip1 == 0 and not linear and not use_tw:
        #     new_tw = tws[i]
        cos_tt = np.dot(ts[ip0], ts[ip1])
        if np.absolute(cos_tt - 1.0) < 1e-9:
            # twist between parallel tangents
            new_u[ip1] = rotation(ts[ip1], new_tw, new_u[ip0])
        elif np.absolute(cos_tt + 1.0) < 1e-9:
            # undefined twist between anti-parallel tangents
            # do nothing
            # print(
            #     "undefined twist between anti-parallel tangents "
            #     + str(ip0)
            #     + " and "
            #     + str(ip1)
            # )
            pass
        else:
            # binormal
            # np.cross does not work with Numba
            b = normalize(my_cross(ts[ip0], ts[ip1]))
            # twist and then parallel transport
            new_u[ip1] = rotation(
                b, np.arccos(cos_tt), rotation(ts[ip0], new_tw, new_u[ip0])
            )
        new_v[ip1] = my_cross(ts[ip1], new_u[ip1])
    return new_u, new_v


@numba.jit
def twist_each_uvt(
    vps: np.ndarray, tws: list, linear: bool, use_tw: bool = False
) -> tuple:
    """return 'us' and 'vs' twisted about 'tws'.
    If frame 'i' and 'i+1' are twisted about tw,
    they are twist about tw+tws[i] at the end.
    There is one u and one v per bead.
    If the length of the local twist list is equal to T,
    only the first T consecutive frames are twisted.
    Parameters
    ----------
    vps    : positions of the beads and pseudo-u beads return by 'get_vpositions_from_context' (np.ndarray)
    tws    : list of local twist values between frame 'i' and frame 'i+1' (list of float)
    linear : True for linear and False for ring polymer (bool)
    use_tw : use already existing local twist (bool)
    Returns
    -------
    A tuple u,v of twisted frames u,v,t (np.ndarray, np.ndarray).
    The size of the list u and the size of the list v are
    equal to the length of the input variables 'us' and 'vs'.
    Raises
    ------
    """
    # number of beads
    V, D = vps.shape
    B = V // particles_per_bead.nparticles_per_bead
    # number of bonds
    N = B - int(linear)
    # number of twist
    T = min(N - int(linear), len(tws))
    # get u, v, t frames
    u, v, t = get_uvt_from_positions(vpositions)
    pu = get_pseudo_u_from_positions(vpositions)
    new_u = np.copy(us)
    new_v = np.copy(vs)
    # get array of local twist before to twist
    old_tws = da.chain_twist(us, vs, ts, 1e-12)
    # twist and then parallel transport
    for i in range(T):
        # local twist + delta twist
        new_tw = old_tws[i] + tws[i]
        ip0 = i
        ip1 = from_i_to_bead(i + 1, B)
        cos_tt = np.dot(ts[ip0], ts[ip1])
        if np.absolute(cos_tt - 1.0) < 1e-9:
            # twist between parallel tangents
            new_u[ip1] = rotation(ts[ip1], new_tw, new_u[ip0])
        elif np.absolute(cos_tt + 1.0) < 1e-9:
            # undefined twist between anti-parallel tangents
            # do nothing
            # print(
            #     "undefined twist between anti-parallel tangents "
            #     + str(ip0)
            #     + " and "
            #     + str(ip1)
            # )
            pass
        else:
            # binormal
            # np.cross does not work with Numba
            b = normalize(my_cross(ts[ip0], ts[ip1]))
            # twist and then parallel transport
            new_u[ip1] = rotation(
                b, np.arccos(cos_tt), rotation(ts[ip0], new_tw, new_u[ip0])
            )
        new_v[ip1] = my_cross(ts[ip1], new_u[ip1])
    # new pseudo-u positions
    new_vpositions = np.copy(vpositions)
    for i in range(B):
        lbond = np.sqrt(
            np.sum(np.square(np.subtract(vpositions[2 * B + i, :D], vpositions[i, :D])))
        )
        new_vpositions[2 * B + i, :D] = np.add(
            vpositions[i, :D], np.multiply(lbond, new_u[i, :D])
        )
    return new_vpositions


# @numba.jit  # (nopython=True)
def get_bead_to_pseudo_bead_distance(positions: np.ndarray, uU: int = 0) -> float:
    """return the average distance between bead and pseudo-u bead.
    Parameters
    ----------
    positions : positions of the beads and pseudo-u beads return by 'get_vpositions_from_context' (np.ndarray).
    uU        : first (=0) or second (=1) pseudo-u bead (int) 
    Returns
    -------
    square root of the average of the quadratic distance between bead and pseudo-u bead (float).
    Raises
    ------
    """
    new_uU = max(0, min(1, uU))
    P = particles_per_bead.nparticles_per_bead
    if P >= (2 + new_uU):
        N, D = positions.shape
        N = N // P
        constraint_distance2_average = 0.0
        for i in range(N):
            constraint_distance2_average += np.sum(
                np.square(
                    np.subtract(
                        positions[
                            get_pseudo_u_from_i(i, N)
                            if new_uU == 0
                            else get_pseudo_U_from_i(i, N)
                        ],
                        positions[i],
                    )
                )
            )
        return np.sqrt(constraint_distance2_average / float(N))
    else:
        return 0.0


# @numba.jit
def get_uvt_from_positions(positions: np.ndarray) -> tuple:
    """return the attached frames u, v, t from the positions.
    For linear polymer last frame u, v, t is between last and first beads.
    Therefore, we have one frame per bead.
    Parameters
    ----------
    positions : positions (in nanometer) of the x * N beads return by 'get_vpositions_from_context' function (np.ndarray).
    Returns
    -------
    u,v,t (tuple)
    normal vectors u (np.ndarray)
    v ~ t x u (np.ndarray)
    tangent vectors t (np.ndarray)
    Raises
    ------
    """
    N, D = positions.shape
    # P = particles_per_bead.nparticles_per_bead
    P = 1
    if particles_per_bead.nparticles_per_bead:
        P = 2
    if particles_per_bead.nparticles_per_bead:
        P = 3
    N = N // P
    # u,v,t declaration
    # u = np.zeros((N, 3), dtype=numba.float64)
    # v = np.zeros((N, 3), dtype=numba.float64)
    # t = np.zeros((N, 3), dtype=numba.float64)
    u = np.zeros((N, 3), dtype=np.double)
    v = np.zeros((N, 3), dtype=np.double)
    t = np.zeros((N, 3), dtype=np.double)
    # build u,v,t
    for i in np.arange(0, N, 1):
        # tangent
        t[i, :3] = normalize(np.subtract(positions[from_i_to_bead(i + 1, N), :3], positions[i, :3]))
    if P >= 2:
        # pseudo-u
        pu = get_pseudo_u_from_positions(positions)
        for i in np.arange(0, N, 1):
            # third vector v from the frame pseudo-u,v,t
            v[i, :3] = normalize(my_cross(t[i, :3], pu[i, :3]))
            # vector u
            u[i, :3] = normalize(my_cross(v[i, :3], t[i, :3]))
        del pu
    return u, v, t


@numba.jit
def get_double_uvt_from_positions(positions: np.ndarray) -> tuple:
    """return the attached frames u, v, t from the positions.
    It returns u, v built from pseudo-u', u, v built from pseudo-u'' and tangent vectors t.
    For linear polymer last frame u, v, t is between last and first beads.
    Therefore, we have one frame per bead.
    Parameters
    ----------
    positions : positions (in nanometer) of the x * N beads return by 'get_vpositions_from_context' function (np.ndarray).
    Returns
    -------
    tuple
    Raises
    ------
    """
    N, D = positions.shape
    P = particles_per_bead.nparticles_per_bead
    N = N // P
    # u,v,t declaration
    us = np.zeros((N, 3), dtype=numba.float64)
    vs = np.zeros((N, 3), dtype=numba.float64)
    ts = np.zeros((N, 3), dtype=numba.float64)
    Us = np.zeros((N, 3), dtype=numba.float64)
    Vs = np.zeros((N, 3), dtype=numba.float64)
    # build u,v,t
    for i in np.arange(0, N, 1):
        # tangent
        vec = np.subtract(positions[from_i_to_bead(i + 1, N)], positions[i])
        ts[i] = normalize(vec)
    if P >= 2:
        # first pseudo-u
        pu = get_pseudo_u_from_positions(positions, 0)
        for i in np.arange(0, N, 1):
            # third vector v from the frame pseudo-u,v,t
            vec = my_cross(ts[i], pu[i])
            vs[i] = normalize(vec)
            # vector u
            vec = my_cross(vs[i], ts[i])
            us[i] = normalize(vec)
    if P >= 3:
        # second pseudo-u
        pu = get_pseudo_u_from_positions(positions, 1)
        for i in np.arange(0, N, 1):
            # third vector v from the frame pseudo-u,v,t
            vec = my_cross(ts[i], pu[i])
            Vs[i] = normalize(vec)
            # vector u
            vec = my_cross(Vs[i], ts[i])
            Us[i] = normalize(vec)
    return us, vs, Us, Vs, ts


# @numba.jit
def get_pseudo_u_from_positions(positions: np.ndarray, uU: int = 0) -> np.ndarray:
    """return the pseudo-u vector.
    Parameters
    ----------
    positions : positions (in nm) of the x * N beads return by 'get_vpositions_from_context' function (np.ndarray)
    uU        : first (=0) or second (=1) pseudo-u bead (int)
    Returns
    -------
    np.ndarray
    Raises
    ------
    """
    new_uU = max(0, min(1, uU))
    N, D = positions.shape
    P = particles_per_bead.nparticles_per_bead
    N = N // P
    # results = np.zeros((N, D), dtype=numba.float64)
    results = np.zeros((N, D), dtype=np.double)
    if P >= 2:
        i1 = np.arange(0, N, 1)
        i2 = np.arange((new_uU + 1) * N, (new_uU + 2) * N, 1)
        diff_p = np.subtract(positions[i2], positions[i1])
        for i in range(N):
            results[i] = normalize(diff_p[i])  # np.multiply(inv_norm[i],diff_p[i])
    return results


@numba.jit
def x_minus_ceil_or_floor(x: np.float64) -> np.float64:
    """return x - ceil(x) if x < 0, x - floor(x) otherwize
    Parameters
    ----------
    x : np.float64
    Returns
    -------
    np.float64
    Raises
    ------
    """
    if x < 0.0:
        return x - np.ceil(x)
    else:
        return x - np.floor(x)


@numba.jit
def floor_or_ceil_minus_x(x: np.float64) -> np.float64:
    """return floor(x) - x if x < 0, ceil(x) - x otherwize
    Parameters
    ----------
    x : np.float64
    Returns
    -------
    np.float64
    Raises
    ------
    """
    if x < 0.0:
        return np.floor(x) - x
    else:
        return np.ceil(x) - x


@numba.jit
def between_minus_pi_and_pi(x: np.float64) -> np.float64:
    """return x within the interval [-pi,pi]
    Parameters
    ----------
    x : function expects value to be in [0,2*pi] interval (np.float64)
    Returns
    -------
    np.float64
    Raises
    ------
    """
    Pi = np.pi
    twoPi = 2.0 * Pi
    if x < -Pi:
        return x + twoPi
    elif x > Pi:
        return x - twoPi
    else:
        return x


@numba.jit(nopython=True, parallel=True)
def change_overtwist(
    vpositions: np.ndarray,
    lbond: float,
    Lk0: float,
    from_overtwist: float,
    to_overtwist: float,
    linear: bool = False,
) -> np.ndarray:
    """change the overtwist (from Lk1 to Lk2) of the chain.
    For a given Wr, we change Tw to have Lk1+Lk2=Wr+Tw.
    Parameters
    ----------
    vpositions     : positions (in nm) of the x * N beads return by 'get_vpositions_from_context' function (np.ndarray)
    lbond          : bond length (float)
    Lk0            : Lk0 (float)
    from_overtwist : overtwist of the chain (float)
    to_overtwist   : new overtwist of the chain (float)
    linear         : linear chain (True) or ring chain (False) (bool)
                     linear: B beads, N = B - 1 bonds, B - 2 twist angles
                     ring  : B beads, N = B bonds, B twist angles
    Returns
    -------
    Raises
    ------
    """
    Pi = np.pi
    twoPi = 2.0 * Pi
    # number of beads
    V, D = vpositions.shape
    B = V // particles_per_bead.nparticles_per_bead
    # number of bonds
    N = B - int(linear)
    # get pseudo beads positions
    pu = get_pseudo_u_from_positions(vpositions)
    # get frames (one per bead)
    u, v, t = get_uvt_from_positions(vpositions)
    ui = np.zeros(u.shape)
    vi = np.zeros(v.shape)
    # old local twist angles
    old_tws = da.chain_twist(u, v, t, 1e-12)[: (B - 2 * int(linear))]
    Tw0 = np.sum(old_tws) / twoPi
    # change the twist / Lk, Lk = Tw + Wr(is constant)
    # Lk_0 = Tw_0 + Wr becomes Lk_1 = Tw_1 + Wr
    # therefore dTw = Lk_1 - Lk_0 (linking number deficit)
    dLk = (to_overtwist - from_overtwist) * Lk0
    if linear:
        # tw * (N - 1) = 2 * Pi * dLk
        tw = twoPi * dLk / (N - 1)
        tw_closure = 0.0
    else:
        # ring (N = 4)
        # tw_3 + tw O --- O tw_0 + tw
        #           |     |
        #           |     |
        # tw_2 + tw O --- O tw_1 + tw
        # tw * (N - 1) + b = 2 * Pi * dLk, we want b small
        iterations = 10000000
        tw0 = twoPi * dLk / (N - 1)
        tws = np.zeros(iterations, dtype=numba.float64)
        bs = np.zeros(iterations, dtype=numba.float64)
        cs = np.zeros(iterations, dtype=numba.float64)
        ds = np.zeros(iterations, dtype=numba.float64)
        # find best |tw * (N - 1) + b - 2 * Pi * dLk| ~ 0
        for k in prange(100):
            for s in range(k * (iterations // 100), (k + 1) * (iterations // 100)):
                tws[s] = tw0 * (0.0 + s * (2.0 - 0.0) / iterations)
                # cs[s]=twoPi*floor_or_ceil_minus_x(twoPi*floor_or_ceil_minus_x(Tw0+tws[s]*N/twoPi)+old_tws[0]+tws[s])
                # cs[s]=twoPi*floor_or_ceil_minus_x(Tw0+tws[s]*N/twoPi+(old_tws[0]+tws[s])/twoPi)
                cs[s] = between_minus_pi_and_pi(
                    twoPi * floor_or_ceil_minus_x(Tw0 + tws[s] * N / twoPi)
                )
                bs[s] = between_minus_pi_and_pi(
                    twoPi * x_minus_ceil_or_floor((cs[s] + old_tws[0] + tws[s]) / twoPi)
                )
                # bs[s]=between_minus_pi_and_pi(cs[s])
                # bs[s]=between_minus_pi_and_pi(twoPi*floor_or_ceil_minus_x(Tw0+tws[s]*N/twoPi)+old_tws[0]+tws[s])
                # cs[s]=twoPi*floor_or_ceil_minus_x(Tw0+tws[s]*N/twoPi)+old_tws[0]+tws[s]
                ds[s] = abs(tws[s] * N + bs[s] - old_tws[0] - twoPi * dLk)
        I = np.argmin(ds)
        for k in range(I - 1, I + 1 + 1):
            print(
                "ds=",
                ds[k],
                "tw=",
                tws[k],
                "bs=",
                bs[k],
                "cs=",
                cs[k],
                "tw_closure=",
                bs[k] - (old_tws[0] - Pi),
            )
        tw = tws[I]
        tw_closure = bs[I]
    # twist each frame
    if linear:
        new_tws = [tw] * (N - 1)
    else:
        new_tws = [tw] * N
    new_pu, pv = twist_each_uv(pu, v, t, new_tws, linear)
    new_u, new_v = twist_each_uv(u, v, t, new_tws, linear)
    # new positions beads and pseudo beads
    new_positions = np.concatenate(
        (
            np.concatenate(
                (
                    vpositions[:B, :],
                    np.add(vpositions[:B, :], np.multiply(lbond, new_u)),
                ),
                axis=0,
            ),
            np.add(vpositions[:B, :], np.multiply(-lbond, new_u)),
        ),
        axis=0,
    )
    return new_positions


def virtual_harmonic(
    B: int, g: float, sigma: float, PBC: bool = False
) -> mm.CustomBondForce:
    """return a harmonic force between bead and pseudo-u bead.
    Parameters
    ----------
    B     : number of beads (int)
    g     : harmonic strength (float)
    sigma : size of the bond between two consecutive beads in nm (float)
    PBC   : does it use the periodic boundary conditions ? (bool)
    Returns
    -------
    OpenMM CustomBondForce
    Raises
    ------
    """
    harmonic = mm.CustomBondForce("0.5*K_vharmonic*(r-r0_vharmonic)^2")
    harmonic.addGlobalParameter("K_vharmonic", g)
    harmonic.addGlobalParameter("r0_vharmonic", sigma)
    harmonic.addEnergyParameterDerivative("K_vharmonic")
    P = particles_per_bead.nparticles_per_bead
    if P >= 2:
        for i in range(B):
            harmonic.addBond(i, get_pseudo_u_from_i(i, B))
            if P >= 3:
                harmonic.addBond(i, get_pseudo_U_from_i(i, B))
    harmonic.setUsesPeriodicBoundaryConditions(PBC)
    return harmonic


def virtual_fene_wca(
    B: int, Temp: float, sigma: float, PBC: bool = False
) -> mm.CustomBondForce:
    """return a FENE + WCA force between bead and pseudo-u bead.
    Parameters
    ----------
    B      : number of beads (int)
    Temp   : temperature in Kelvin (float)
    sigma  : size of the bond between bead and pseudo-u bead in nm (float)
    PBC    : does it use the periodic boundary conditions ? (bool)
    Returns
    -------
    OpenMM CustomBondForce
    Raises
    ------
    """
    vfene = mm.CustomBondForce(
        "-0.5*K_vfene*vR0*vR0*log(1-(r*r/(vR0*vR0)))*step(0.999999*vR0-r)+4*epsilon_vwca*((sigma_vwca/r)^12-(sigma_vwca/r)^6+0.25)*step(cutoff_vwca-r)"
    )
    vfene.addGlobalParameter("K_vfene", 30.0 * get_kBT(Temp) / (sigma * sigma))
    vfene.addEnergyParameterDerivative("K_vfene")
    vfene.addGlobalParameter("vR0", 1.5 * sigma)
    vfene.addGlobalParameter("epsilon_vwca", get_kBT(Temp))
    vfene.addGlobalParameter("sigma_vwca", sigma)
    vfene.addGlobalParameter("cutoff_vwca", np.power(2.0, 1.0 / 6.0) * sigma)
    vfene.addEnergyParameterDerivative("epsilon_vwca")
    P = particles_per_bead.nparticles_per_bead
    if P >= 2:
        for i in range(B):
            vfene.addBond(i, get_pseudo_u_from_i(i, B))
            if P >= 3:
                vfene.addBond(i, get_pseudo_U_from_i(i, B))
    vfene.setUsesPeriodicBoundaryConditions(PBC)
    return vfene


def fene(
    N: int, Temp: float, sigma: float, linear: bool, PBC: bool, add_wca: bool = False
) -> mm.CustomBondForce:
    """return a FENE force between consecutive beads.
    Parameters
    ----------
    N       : number of bonds (int)
    Temp    : temperature in Kelvin (float)
    sigma   : size of the bond between two consecutive beads in nm (float)
    linear  : True (linear chain) or False (ring chain)
    PBC     : does it use the periodic boundary conditions ? (bool)
    add_wca : add WCA repulsive potential (bool)
    Returns
    -------
    OpenMM CustomBondForce (FENE potential between two consecutive bonds).
    """
    kBT = get_kBT(Temp)
    if add_wca:
        # FENE + WCA
        fene = mm.CustomBondForce(
            "-0.5*K_fene*R0*R0*log(1-(r*r/(R0*R0)))*step(0.999999*R0-r)+4*epsilon_fene*((sigma_fene/r)^12-(sigma_fene/r)^6+0.25)*step(cutoff_fene-r)"
        )
        fene.addGlobalParameter("K_fene", 30.0 * kBT / (sigma * sigma))
        fene.addGlobalParameter("R0", 1.5 * sigma)
        fene.addGlobalParameter("epsilon_fene", kBT)
        fene.addGlobalParameter("sigma_fene", sigma)
        fene.addGlobalParameter("cutoff_fene", np.power(2.0, 1.0 / 6.0) * sigma)
    else:
        # only FENE
        fene = mm.CustomBondForce(
            "-0.5*K_fene*R0*R0*log(1-(r*r/(R0*R0)))*step(0.999999*R0-r)"
        )
        fene.addGlobalParameter("K_fene", 30.0 * kBT / (sigma * sigma))
        fene.addGlobalParameter("R0", 1.5 * sigma)
    fene.addEnergyParameterDerivative("K_fene")
    B = N + int(linear)
    for i in range(N):
        fene.addBond(i, from_i_to_bead(i + 1, B))
    fene.setUsesPeriodicBoundaryConditions(PBC)
    return fene


def harmonic(
    N: int, Temp: float, sigma: float, linear: bool, PBC: bool = False
) -> mm.HarmonicBondForce:
    """return a harmonic force between consecutive beads.
    Parameters
    ----------
    N      : number of bonds (int)
    Temp   : temperature in Kelvin (float)
    sigma  : size of the bond between two consecutive beads in nm (float)
    linear : True (linear chain) or False (ring chain)
    PBC    : does it use the periodic boundary conditions ? (bool)
    Returns
    -------
    OpenMM HarmonicBondForce
    Raises
    ------
    """
    harmonic = mm.HarmonicBondForce()
    B = N + int(linear)
    for i in range(N):
        harmonic.addBond(
            i, from_i_to_bead(i + 1, B), 0.0, 3.0 * get_kBT(Temp) / sigma ** 2
        )
    harmonic.setUsesPeriodicBoundaryConditions(PBC)
    return harmonic


def rigid_bond(system: mm.System, N: int, sigma: float, linear: bool):
    """add rigid bond (between two consecutive beads) to the system of N beads.
    Parameters
    ----------
    system : OpenMM system
    N      : number of bonds (int)
    sigma  : size of the bond between two consecutive beads (float)
    linear : True (linear chain) or False (ring chain)
    Returns
    -------
    Raises
    ------
    """
    B = N + int(linear)
    for i in range(N):
        system.addConstraint(i, from_i_to_bead(i + 1, B), sigma)


def clamp_bead(
    N: int, k_clamp: float, x0: float = 0.0, y0: float = 0.0, z0: float = 0.0
) -> mm.CustomExternalForce:
    """return an external force to clamp a particle.
    Parameters
    ----------
    N       : index of the bead (int)
    k_clamp : strength of the force that clamps the particle (float)
    x0      : x coordinate (float)
    y0      : y coordinate (float)
    z0      : z coordinate (float)
    Returns
    -------
    OpenMM CustomExternalForce to deal with plane repulsion.
    Raises
    ------
    """
    expression = "0.5*k_clamp*((x-x0)^2+(y-y0)^2+(z-z0)^2)"
    clamp = mm.CustomExternalForce(expression)
    clamp.addPerParticleParameter("k_clamp")
    clamp.addPerParticleParameter("x0")
    clamp.addPerParticleParameter("y0")
    clamp.addPerParticleParameter("z0")
    clamp.addParticle(N, [k_clamp, x0, y0, z0])
    return clamp


def external_force(
    N: int, axe: str = "z", force: float = 0.0
) -> mm.CustomExternalForce:
    """return an external force.
    Parameters
    ----------
    N     : index of the bead (int)
    axe   : axe of the external force (str)
    force : module of the force (float)
    Returns
    -------
    OpenMM CustomExternalForce to deal with plane repulsion.
    Raises
    ------
    """
    expression = "f_external*" + axe
    external = mm.CustomExternalForce(expression)
    external.addPerParticleParameter("f_external")
    external.addParticle(N, [force])
    return external


def plane_repulsion(
    N: int,
    normal: str = "z",
    position: float = 0.0,
    sigma: float = 1.0,
    strength: float = 0.0,
) -> mm.CustomExternalForce:
    """return a soft plane repulsion (WCA potential).
    Parameters
    ----------
    N        : index of the bead (int)
    normal   : normal ('x', 'y' or 'z') to the plane (str)
    position : position of the plane (float)
               if normal i 'z' and position is -1 the plane is located at z=-1
    sigma    : size of the WCA potential (float)
    strength : plane repulsion strength (float)
    Returns
    -------
    OpenMM CustomExternalForce to deal with external force.
    Raises
    ------
    """
    expression = (
        "4*epsilon_plane*((sigma_plane/("
        + normal
        + "-p0))^12-(sigma_plane/("
        + normal
        + "-p0))^6+0.25)*step(cutoff_plane-("
        + normal
        + "1-p0))"
    )
    plane = mm.CustomExternalForce(expression)
    plane.addPerParticleParameter("p0")
    plane.addPerParticleParameter("epsilon_plane")
    plane.addPerParticleParameter("sigma_plane")
    plane.addPerParticleParameter("cutoff_plane")
    plane.addParticle(N, [position, strength, sigma, np.power(2.0, 1.0 / 6.0) * sigma])
    return plane


def tt_bending(N: int, Kb: float, linear: bool, PBC: bool) -> mm.CustomAngleForce:
    """return a bending force between three consecutive beads.
    Parameters
    ----------
    N   : the number of bonds (int)
    Kb  : the bending rigidity (float)
    PBC : does it use the periodic boundary conditions, False or True ?
    Returns
    -------
    OpenMM CustomAngleForce bending force between two consecutive tangent vector t.
    """
    # number of beads
    B = N + int(linear)
    bending = mm.CustomAngleForce("Kb_tt*(1-cos(theta-A_tt))")
    bending.addGlobalParameter("Kb_tt", Kb)
    bending.addGlobalParameter("A_tt", np.pi)
    bending.addEnergyParameterDerivative("Kb_tt")
    for i in range(N - int(linear)):
        bending.addAngle(i, from_i_to_bead(i + 1, B), from_i_to_bead(i + 2, B))
    bending.setUsesPeriodicBoundaryConditions(PBC)
    return bending


def ut_bending(
    N: int, Kb: float, linear: bool, PBC: bool = False
) -> mm.CustomAngleForce:
    """return a bending force between pseudo-u and tangent vector t.
    Parameters
    ----------
    N           : the number of bonds (int)
    Kb          : the bending rigidity in kBT unit (float)
    linear      : linear (True) or ring (False) (bool)
    PBC         : does it use the periodic boundary conditions ? (bool)
    Returns
    -------
    OpenMM CustomAngleForce bending force between pseudo-u and tangent vector t.
    Raises
    ------
    """
    # number of beads
    B = N + int(linear)
    # bending = mm.CustomAngleForce("0.5*Kb_ut*(theta-A_ut)^2")
    bending = mm.CustomAngleForce("Kb_ut*(1-cos(theta-A_ut))")
    bending.addGlobalParameter("Kb_ut", Kb)
    bending.addGlobalParameter("A_ut", 0.5 * np.pi)
    bending.addEnergyParameterDerivative("Kb_ut")
    P = particles_per_bead.nparticles_per_bead
    if P >= 2:
        for i in range(N):
            bending.addAngle(
                get_pseudo_u_from_i(i, B), i, from_i_to_bead(i + 1, B),
            )
            if P >= 3:
                bending.addAngle(
                    get_pseudo_U_from_i(i, B), i, from_i_to_bead(i + 1, B),
                )
    bending.setUsesPeriodicBoundaryConditions(PBC)
    return bending


def uU_bending(N: int, Kb: float, linear: bool, PBC: bool) -> mm.CustomAngleForce:
    """return a bending force between pseudo-u and pseudo-U beads.
    Parameters
    ----------
    N   : the number of bonds (int)
    Kb  : the bending rigidity (float)
    PBC : does it use the periodic boundary conditions ? (bool)
    Returns
    -------
    OpenMM CustomAngleForce
    """
    # number of beads
    B = N + int(linear)
    bending = mm.CustomAngleForce("Kb_uU*(1.0-cos(theta-A_uU))")
    bending.addGlobalParameter("Kb_uU", Kb)
    bending.addGlobalParameter("A_uU", np.pi)
    bending.addEnergyParameterDerivative("Kb_uU")
    if particles_per_bead.nparticles_per_bead >= 3:
        for i in range(B):
            bending.addAngle(
                get_pseudo_U_from_i(i, B), i, get_pseudo_u_from_i(i, B),
            )
    bending.setUsesPeriodicBoundaryConditions(PBC)
    return bending


def uUt_bending(
    N: int, Kb: float, linear: bool, PBC: bool
) -> mm.CustomCompoundBondForce:
    """return a bending force between tangent and pseudo-u to pseudo-U beads vector.
    Parameters
    ----------
    N   : the number of bonds (int)
    Kb  : the bending rigidity (float)
    PBC : does it use the periodic boundary conditions ? (bool)
    Returns
    -------
    OpenMM CustomCompundBondForce
    """
    # number of beads
    B = N + int(linear)
    xx = "(x2-x1)*(x4-x3)"
    yy = "(y2-y1)*(y4-y3)"
    zz = "(z2-z1)*(z4-z3)"
    # expression = "Kb_uUt*(1-("+xx+"+"+yy+"+"+zz+")/(distance(p2,p1)*distance(p4,p3)))"
    expression = (
        "Kb_uUt*abs(("
        + xx
        + "+"
        + yy
        + "+"
        + zz
        + ")/(distance(p2,p1)*distance(p4,p3)))"
    )
    bending = mm.CustomCompoundBondForce(4, expression)
    bending.addGlobalParameter("Kb_uUt", Kb)
    bending.addEnergyParameterDerivative("Kb_uUt")
    if particles_per_bead.nparticles_per_bead >= 2:
        for i in range(N - int(linear)):
            bending.addBond(
                [
                    get_pseudo_u_from_i(i, B),
                    get_pseudo_U_from_i(i, B),
                    i,
                    from_i_to_bead(i + 1, B),
                ]
            )
    return bending


def twist(
    N: int, Kt: float, twist0: list, linear: bool, PBC: bool, use_cos: bool = True
) -> mm.CustomCompoundBondForce:
    """return a twisting force between three consecutive beads and two pseudo-u beads.
    Parameters
    ----------
    N       : number of bonds (int)
    Kt      : the twisting rigidity (float)
    twist0  : twisting angle (list of float)
    linear  : linear (True) or ring (False) (bool)
    PBC     : does it use the periodic boundary conditions, False or True ? (bool)
    use_cos : use cosine or square of tw for twist potential (bool)
    Returns
    -------
    OpenMM CustomCompoundBondForce to deal with the twist between two consecutive frames uvt.
    Raises
    ------
    """
    # number of beads
    B = N + int(linear)
    # u' is the pseudo-u vector
    # v~cross(t,u') (1)
    v1x = "((y2-y1)*(z4-z1)-(z2-z1)*(y4-y1))"
    v1y = "((z2-z1)*(x4-x1)-(x2-x1)*(z4-z1))"
    v1z = "((x2-x1)*(y4-y1)-(y2-y1)*(x4-x1))"
    # v~cross(t,u') (2)
    v2x = "((y3-y2)*(z5-z2)-(z3-z2)*(y5-y2))"
    v2y = "((z3-z2)*(x5-x2)-(x3-x2)*(z5-z2))"
    v2z = "((x3-x2)*(y5-y2)-(y3-y2)*(x5-x2))"
    # u~cross(v,t)~cross(cross(t,u'),t) (1)
    u1x = "(" + v1y + "*(z2-z1)-" + v1z + "*(y2-y1))"
    u1y = "(" + v1z + "*(x2-x1)-" + v1x + "*(z2-z1))"
    u1z = "(" + v1x + "*(y2-y1)-" + v1y + "*(x2-x1))"
    # u~cross(v,t)~cross(cross(t,u'),t) (2)
    u2x = "(" + v2y + "*(z3-z2)-" + v2z + "*(y3-y2))"
    u2y = "(" + v2z + "*(x3-x2)-" + v2x + "*(z3-z2))"
    u2z = "(" + v2x + "*(y3-y2)-" + v2y + "*(x3-x2))"
    # dot(u(1),u(2))
    uu = (
        "("
        + u1x
        + "*"
        + u2x
        + "+"
        + u1y
        + "*"
        + u2y
        + "+"
        + u1z
        + "*"
        + u2z
        + ")"
        + "/sqrt(("
        + u1x
        + "*"
        + u1x
        + "+"
        + u1y
        + "*"
        + u1y
        + "+"
        + u1z
        + "*"
        + u1z
        + ")*("
        + u2x
        + "*"
        + u2x
        + "+"
        + u2y
        + "*"
        + u2y
        + "+"
        + u2z
        + "*"
        + u2z
        + "))"
    )
    # dot(v(1),v(2))
    vv = (
        "("
        + v1x
        + "*"
        + v2x
        + "+"
        + v1y
        + "*"
        + v2y
        + "+"
        + v1z
        + "*"
        + v2z
        + ")"
        + "/sqrt(("
        + v1x
        + "*"
        + v1x
        + "+"
        + v1y
        + "*"
        + v1y
        + "+"
        + v1z
        + "*"
        + v1z
        + ")*("
        + v2x
        + "*"
        + v2x
        + "+"
        + v2y
        + "*"
        + v2y
        + "+"
        + v2z
        + "*"
        + v2z
        + "))"
    )
    # dot(t(1),t(2))
    tt = "((x2-x1)*(x3-x2)+(y2-y1)*(y3-y2)+(z2-z1)*(z3-z2))/(distance(p2,p1)*distance(p3,p2))"
    # cos(tw) is (u_1.u_2+v_1.v_2)/(1+t_1.t_2)
    # sin(tw) is (u_2.v_1-v_2.u_1)/(1+t_1.t_2)
    if use_cos:
        expression = "Kt*(1.0-(" + uu + "+" + vv + ")/(1.0+" + tt + "))"
        # expression = (
        #     "(2.0*Kt)*(exp(0.5*(1.0-(" + uu + "+" + vv + ")/(1.0+" + tt + ")))-1)"
        # )
    else:
        expression = (
            "0.5*Kt*acos(max(-0.999999,min(0.999999,("
            + uu
            + "+"
            + vv
            + ")/(1.0+"
            + tt
            + "))))^2"
        )
    twisting = mm.CustomCompoundBondForce(5, expression)
    # Kt*<tw^2>=k_B*T and <tw^2>=l_b/l_t
    use_double_twist = False
    twisting.addGlobalParameter("Kt", 0.5 * Kt if use_double_twist else Kt)
    twisting.addGlobalParameter("overtwist", 0.0)
    # twisting.addPerBondParameter("twist0")
    twisting.addEnergyParameterDerivative("Kt")
    if particles_per_bead.nparticles_per_bead >= 2:
        for i in range(N - int(linear)):
            ip1 = from_i_to_bead(i + 1, B)
            ip2 = from_i_to_bead(ip1 + 1, B)
            twisting.addBond(
                [i, ip1, ip2, get_pseudo_u_from_i(i, B), get_pseudo_u_from_i(ip1, B)]
            )  # ,[np.sqrt(1.0-np.arccos(twist0[i]))])
            if use_double_twist:
                twisting.addBond(
                    [
                        i,
                        ip1,
                        ip2,
                        get_pseudo_U_from_i(i, B),
                        get_pseudo_U_from_i(ip1, B),
                    ]
                )
    twisting.setUsesPeriodicBoundaryConditions(PBC)
    return twisting


def double_twist(
    N: int, Kt: float, twist0: list, linear: bool, PBC: bool
) -> mm.CustomCompoundBondForce:
    """return a twisting force between three consecutive beads and two pseudo-u beads.
    Parameters
    ----------
    N       : number of bonds (int)
    Kt      : the twisting rigidity (float)
    twist0  : twisting angle (list of float)
    linear  : linear (True) or ring (False) (bool)
    PBC     : does it use the periodic boundary conditions, False or True ? (bool)
    Returns
    -------
    OpenMM CustomCompoundBondForce to deal with the twist between two consecutive frames uvt.
    Raises
    ------
    """
    # number of beads
    B = N + int(linear)
    # u' is the first pseudo-u vector
    # v~cross(t,u') (1)
    v1x = "((y2-y1)*(z4-z1)-(z2-z1)*(y4-y1))"
    v1y = "((z2-z1)*(x4-x1)-(x2-x1)*(z4-z1))"
    v1z = "((x2-x1)*(y4-y1)-(y2-y1)*(x4-x1))"
    # v~cross(t,u') (2)
    v2x = "((y3-y2)*(z5-z2)-(z3-z2)*(y5-y2))"
    v2y = "((z3-z2)*(x5-x2)-(x3-x2)*(z5-z2))"
    v2z = "((x3-x2)*(y5-y2)-(y3-y2)*(x5-x2))"
    # u~cross(v,t)~cross(cross(t,u'),t) (1)
    u1x = "(" + v1y + "*(z2-z1)-" + v1z + "*(y2-y1))"
    u1y = "(" + v1z + "*(x2-x1)-" + v1x + "*(z2-z1))"
    u1z = "(" + v1x + "*(y2-y1)-" + v1y + "*(x2-x1))"
    # u~cross(v,t)~cross(cross(t,u'),t) (2)
    u2x = "(" + v2y + "*(z3-z2)-" + v2z + "*(y3-y2))"
    u2y = "(" + v2z + "*(x3-x2)-" + v2x + "*(z3-z2))"
    u2z = "(" + v2x + "*(y3-y2)-" + v2y + "*(x3-x2))"
    # dot(u(1),u(2))
    uu = (
        "("
        + u1x
        + "*"
        + u2x
        + "+"
        + u1y
        + "*"
        + u2y
        + "+"
        + u1z
        + "*"
        + u2z
        + ")"
        + "/sqrt(("
        + u1x
        + "*"
        + u1x
        + "+"
        + u1y
        + "*"
        + u1y
        + "+"
        + u1z
        + "*"
        + u1z
        + ")*("
        + u2x
        + "*"
        + u2x
        + "+"
        + u2y
        + "*"
        + u2y
        + "+"
        + u2z
        + "*"
        + u2z
        + "))"
    )
    # dot(v(1),v(2))
    vv = (
        "("
        + v1x
        + "*"
        + v2x
        + "+"
        + v1y
        + "*"
        + v2y
        + "+"
        + v1z
        + "*"
        + v2z
        + ")"
        + "/sqrt(("
        + v1x
        + "*"
        + v1x
        + "+"
        + v1y
        + "*"
        + v1y
        + "+"
        + v1z
        + "*"
        + v1z
        + ")*("
        + v2x
        + "*"
        + v2x
        + "+"
        + v2y
        + "*"
        + v2y
        + "+"
        + v2z
        + "*"
        + v2z
        + "))"
    )
    # dot(u(2),v(1))
    uv = (
        "("
        + u2x
        + "*"
        + v1x
        + "+"
        + u2y
        + "*"
        + v1y
        + "+"
        + u2z
        + "*"
        + v1z
        + ")"
        + "/sqrt(("
        + u2x
        + "*"
        + u2x
        + "+"
        + u2y
        + "*"
        + u2y
        + "+"
        + u2z
        + "*"
        + u2z
        + ")*("
        + v1x
        + "*"
        + v1x
        + "+"
        + v1y
        + "*"
        + v1y
        + "+"
        + v1z
        + "*"
        + v1z
        + "))"
    )
    # dot(v(2),u(1))
    vu = (
        "("
        + v2x
        + "*"
        + u1x
        + "+"
        + v2y
        + "*"
        + u1y
        + "+"
        + v2z
        + "*"
        + u1z
        + ")"
        + "/sqrt(("
        + v2x
        + "*"
        + v2x
        + "+"
        + v2y
        + "*"
        + v2y
        + "+"
        + v2z
        + "*"
        + v2z
        + ")*("
        + u1x
        + "*"
        + u1x
        + "+"
        + u1y
        + "*"
        + u1y
        + "+"
        + u1z
        + "*"
        + u1z
        + "))"
    )
    # u'' is the second pseudo-u vector
    # V~cross(t,u'') (1)
    v1x = "((y2-y1)*(z6-z1)-(z2-z1)*(y6-y1))"
    v1y = "((z2-z1)*(x6-x1)-(x2-x1)*(z6-z1))"
    v1z = "((x2-x1)*(y6-y1)-(y2-y1)*(x6-x1))"
    # V~cross(t,u'') (2)
    v2x = "((y3-y2)*(z7-z2)-(z3-z2)*(y7-y2))"
    v2y = "((z3-z2)*(x7-x2)-(x3-x2)*(z7-z2))"
    v2z = "((x3-x2)*(y7-y2)-(y3-y2)*(x7-x2))"
    # U~cross(V,t)~cross(cross(t,u''),t) (1)
    u1x = "(" + v1y + "*(z2-z1)-" + v1z + "*(y2-y1))"
    u1y = "(" + v1z + "*(x2-x1)-" + v1x + "*(z2-z1))"
    u1z = "(" + v1x + "*(y2-y1)-" + v1y + "*(x2-x1))"
    # U~cross(V,t)~cross(cross(t,u''),t) (2)
    u2x = "(" + v2y + "*(z3-z2)-" + v2z + "*(y3-y2))"
    u2y = "(" + v2z + "*(x3-x2)-" + v2x + "*(z3-z2))"
    u2z = "(" + v2x + "*(y3-y2)-" + v2y + "*(x3-x2))"
    # dot(U(1),U(2))
    UU = (
        "("
        + u1x
        + "*"
        + u2x
        + "+"
        + u1y
        + "*"
        + u2y
        + "+"
        + u1z
        + "*"
        + u2z
        + ")"
        + "/sqrt(("
        + u1x
        + "*"
        + u1x
        + "+"
        + u1y
        + "*"
        + u1y
        + "+"
        + u1z
        + "*"
        + u1z
        + ")*("
        + u2x
        + "*"
        + u2x
        + "+"
        + u2y
        + "*"
        + u2y
        + "+"
        + u2z
        + "*"
        + u2z
        + "))"
    )
    # dot(V(1),V(2))
    VV = (
        "("
        + v1x
        + "*"
        + v2x
        + "+"
        + v1y
        + "*"
        + v2y
        + "+"
        + v1z
        + "*"
        + v2z
        + ")"
        + "/sqrt(("
        + v1x
        + "*"
        + v1x
        + "+"
        + v1y
        + "*"
        + v1y
        + "+"
        + v1z
        + "*"
        + v1z
        + ")*("
        + v2x
        + "*"
        + v2x
        + "+"
        + v2y
        + "*"
        + v2y
        + "+"
        + v2z
        + "*"
        + v2z
        + "))"
    )
    # dot(U(2),V(1))
    UV = (
        "("
        + u2x
        + "*"
        + v1x
        + "+"
        + u2y
        + "*"
        + v1y
        + "+"
        + u2z
        + "*"
        + v1z
        + ")"
        + "/sqrt(("
        + u2x
        + "*"
        + u2x
        + "+"
        + u2y
        + "*"
        + u2y
        + "+"
        + u2z
        + "*"
        + u2z
        + ")*("
        + v1x
        + "*"
        + v1x
        + "+"
        + v1y
        + "*"
        + v1y
        + "+"
        + v1z
        + "*"
        + v1z
        + "))"
    )
    # dot(V(2),U(1))
    VU = (
        "("
        + v2x
        + "*"
        + u1x
        + "+"
        + v2y
        + "*"
        + u1y
        + "+"
        + v2z
        + "*"
        + u1z
        + ")"
        + "/sqrt(("
        + v2x
        + "*"
        + v2x
        + "+"
        + v2y
        + "*"
        + v2y
        + "+"
        + v2z
        + "*"
        + v2z
        + ")*("
        + u1x
        + "*"
        + u1x
        + "+"
        + u1y
        + "*"
        + u1y
        + "+"
        + u1z
        + "*"
        + u1z
        + "))"
    )
    # dot(t(1),t(2))
    tt = "((x2-x1)*(x3-x2)+(y2-y1)*(y3-y2)+(z2-z1)*(z3-z2))/(distance(p2,p1)*distance(p3,p2))"
    # cos(tw) is (u_1.u_2+v_1.v_2)/(1+t_1.t_2)
    # sin(tw) is (u_2.v_1-v_2.u_1)/(1+t_1.t_2)
    # local twist build from u'
    tw = (
        "(2*step(("
        + uv
        + "-"
        + vu
        + ")/(1.0+"
        + tt
        + "))-1)*acos(max(-0.999999,min(0.999999,("
        + uu
        + "+"
        + vv
        + ")/(1.0+"
        + tt
        + "))))"
    )
    # local twist build from u''
    TW = (
        "(2*step(("
        + UV
        + "-"
        + VU
        + ")/(1.0+"
        + tt
        + "))-1)*acos(max(-0.999999,min(0.999999,("
        + UU
        + "+"
        + VV
        + ")/(1.0+"
        + tt
        + "))))"
    )
    expression = "0.125*Kt*(" + tw + "+" + TW + "-tw0)^2"
    twisting = mm.CustomCompoundBondForce(7, expression)
    # Kt*<tw^2>=k_B*T and <tw^2>=l_b/l_t
    twisting.addGlobalParameter("Kt", Kt)
    twisting.addGlobalParameter("overtwist", 0.0)
    twisting.addPerBondParameter("tw0")
    twisting.addEnergyParameterDerivative("Kt")
    if particles_per_bead.nparticles_per_bead >= 3:
        for i in range(N - int(linear)):
            ip1 = from_i_to_bead(i + 1, B)
            ip2 = from_i_to_bead(ip1 + 1, B)
            twisting.addBond(
                [
                    i,
                    ip1,
                    ip2,
                    get_pseudo_u_from_i(i, B),
                    get_pseudo_u_from_i(ip1, B),
                    get_pseudo_U_from_i(i, B),
                    get_pseudo_U_from_i(ip1, B),
                ],
                [0.0],
            )
    twisting.setUsesPeriodicBoundaryConditions(PBC)
    return twisting


def twist32(
    N: int, Kt: float, twist0: list, linear: bool, PBC: bool, use_cos: bool = True
) -> mm.CustomCompoundBondForce:
    """return a twisting force between three consecutive beads and two pseudo-u beads.
    Parameters
    ----------
    N       : number of bonds (int)
    Kt      : the twisting rigidity (float)
    twist0  : twisting angle (list of float)
    linear  : linear (True) or ring (False) (bool)
    PBC     : does it use the periodic boundary conditions, False or True ? (bool)
    use_cos : use cosine or square of tw for twist potential (bool)
    Returns
    -------
    OpenMM CustomCompoundBondForce to deal with the twist between two consecutive frames uvt.
    Raises
    ------
    """
    # number of beads
    B = N + int(linear)
    # u' is the pseudo-u vector
    # v~cross(t,u') (1)
    v1x = "((y2-y1)*(z4-z1)-(z2-z1)*(y4-y1))"
    v1y = "((z2-z1)*(x4-x1)-(x2-x1)*(z4-z1))"
    v1z = "((x2-x1)*(y4-y1)-(y2-y1)*(x4-x1))"
    # v~cross(t,u') (2)
    v2x = "((y3-y2)*(z5-z2)-(z3-z2)*(y5-y2))"
    v2y = "((z3-z2)*(x5-x2)-(x3-x2)*(z5-z2))"
    v2z = "((x3-x2)*(y5-y2)-(y3-y2)*(x5-x2))"
    # u~cross(v,t)~cross(cross(t,u'),t) (1)
    u1x = "(" + v1y + "*(z2-z1)-" + v1z + "*(y2-y1))"
    u1y = "(" + v1z + "*(x2-x1)-" + v1x + "*(z2-z1))"
    u1z = "(" + v1x + "*(y2-y1)-" + v1y + "*(x2-x1))"
    # u~cross(v,t)~cross(cross(t,u'),t) (2)
    u2x = "(" + v2y + "*(z3-z2)-" + v2z + "*(y3-y2))"
    u2y = "(" + v2z + "*(x3-x2)-" + v2x + "*(z3-z2))"
    u2z = "(" + v2x + "*(y3-y2)-" + v2y + "*(x3-x2))"
    # dot(u(1),u(2))
    uu = (
        "("
        + u1x
        + "*"
        + u2x
        + "+"
        + u1y
        + "*"
        + u2y
        + "+"
        + u1z
        + "*"
        + u2z
        + ")"
        + "/sqrt(("
        + u1x
        + "*"
        + u1x
        + "+"
        + u1y
        + "*"
        + u1y
        + "+"
        + u1z
        + "*"
        + u1z
        + ")*("
        + u2x
        + "*"
        + u2x
        + "+"
        + u2y
        + "*"
        + u2y
        + "+"
        + u2z
        + "*"
        + u2z
        + "))"
    )
    # dot(v(1),v(2))
    vv = (
        "("
        + v1x
        + "*"
        + v2x
        + "+"
        + v1y
        + "*"
        + v2y
        + "+"
        + v1z
        + "*"
        + v2z
        + ")"
        + "/sqrt(("
        + v1x
        + "*"
        + v1x
        + "+"
        + v1y
        + "*"
        + v1y
        + "+"
        + v1z
        + "*"
        + v1z
        + ")*("
        + v2x
        + "*"
        + v2x
        + "+"
        + v2y
        + "*"
        + v2y
        + "+"
        + v2z
        + "*"
        + v2z
        + "))"
    )
    # dot(t(1),t(2))
    tt = "((x2-x1)*(x3-x2)+(y2-y1)*(y3-y2)+(z2-z1)*(z3-z2))/(distance(p2,p1)*distance(p3,p2))"
    # cos(tw) is (u_1.u_2+v_1.v_2)/(1+t_1.t_2)
    # sin(tw) is (u_2.v_1-v_2.u_1)/(1+t_1.t_2)
    if use_cos:
        expression = "Kt*(1.0-(" + uu + "+" + vv + ")/(1.0+" + tt + "))"
        # expression = (
        #     "(2.0*Kt)*(exp(0.5*(1.0-(" + uu + "+" + vv + ")/(1.0+" + tt + ")))-1)"
        # )
    else:
        expression = (
            "0.5*Kt*acos(max(-0.999999,min(0.999999,("
            + uu
            + "+"
            + vv
            + ")/(1.0+"
            + tt
            + "))))^2"
        )
    twisting = mm.CustomCompoundBondForce(5, expression)
    use_double_twist = False
    twisting.addGlobalParameter("Kt", 0.5 * Kt if use_double_twist else Kt)
    twisting.addGlobalParameter("overtwist", 0.0)
    # twisting.addPerBondParameter("twist0")
    twisting.addEnergyParameterDerivative("Kt")
    if particles_per_bead.nparticles_per_bead >= 2:
        for i in range(N - int(linear)):
            ip1 = from_i_to_bead(i + 1, B)
            ip2 = from_i_to_bead(ip1 + 1, B)
            twisting.addBond(
                [i, ip1, ip2, get_pseudo_u_from_i(i, B), get_pseudo_u_from_i(ip1, B)]
            )  # ,[np.sqrt(1.0-np.arccos(twist0[i]))])
            if use_double_twist:
                twisting.addBond(
                    [
                        i,
                        ip1,
                        ip2,
                        get_pseudo_U_from_i(i, B),
                        get_pseudo_U_from_i(ip1, B),
                    ]
                )
    twisting.setUsesPeriodicBoundaryConditions(PBC)
    return twisting


def twist34(
    N: int, Kt: float, twist0: list, linear: bool, PBC: bool, use_cos: bool = True
) -> mm.CustomCompoundBondForce:
    """return a twisting force between three consecutive beads and four pseudo-u beads.
    Parameters
    ----------
    N       : number of bonds (int)
    Kt      : the twisting rigidity (float)
    twist0  : twisting angle (list of float)
    linear  : linear (True) or ring (False) (bool)
    PBC     : does it use the periodic boundary conditions, False or True ? (bool)
    use_cos : use cosine or square of tw for twist potential (bool)
    Returns
    -------
    OpenMM CustomCompoundBondForce to deal with the twist between two consecutive frames uvt.
    Raises
    ------
    """
    # number of beads
    B = N + int(linear)
    # u' is the pseudo-u vector
    # [i, ip1, ip2, get_pseudo_u_from_i(i, B), get_pseudo_U_from_i(i, B), get_pseudo_u_from_i(ip1, B), get_pseudo_U_from_i(ip1, B)]
    # [i, ip1, ip2, get_pseudo_u_from_i(i, B), get_pseudo_u_from_i(ip1, B)]
    # v~cross(t,u') (1)
    v1x = "((y2-y1)*(z4-z5)-(z2-z1)*(y4-y5))"
    v1y = "((z2-z1)*(x4-x5)-(x2-x1)*(z4-z5))"
    v1z = "((x2-x1)*(y4-y5)-(y2-y1)*(x4-x5))"
    # v~cross(t,u') (2)
    v2x = "((y3-y2)*(z6-z7)-(z3-z2)*(y6-y7))"
    v2y = "((z3-z2)*(x6-x7)-(x3-x2)*(z6-z7))"
    v2z = "((x3-x2)*(y6-y7)-(y3-y2)*(x6-x7))"
    # u~cross(v,t)~cross(cross(t,u'),t) (1)
    u1x = "(" + v1y + "*(z2-z1)-" + v1z + "*(y2-y1))"
    u1y = "(" + v1z + "*(x2-x1)-" + v1x + "*(z2-z1))"
    u1z = "(" + v1x + "*(y2-y1)-" + v1y + "*(x2-x1))"
    # u~cross(v,t)~cross(cross(t,u'),t) (2)
    u2x = "(" + v2y + "*(z3-z2)-" + v2z + "*(y3-y2))"
    u2y = "(" + v2z + "*(x3-x2)-" + v2x + "*(z3-z2))"
    u2z = "(" + v2x + "*(y3-y2)-" + v2y + "*(x3-x2))"
    # dot(u(1),u(2))
    uu = (
        "("
        + u1x
        + "*"
        + u2x
        + "+"
        + u1y
        + "*"
        + u2y
        + "+"
        + u1z
        + "*"
        + u2z
        + ")"
        + "/sqrt(("
        + u1x
        + "*"
        + u1x
        + "+"
        + u1y
        + "*"
        + u1y
        + "+"
        + u1z
        + "*"
        + u1z
        + ")*("
        + u2x
        + "*"
        + u2x
        + "+"
        + u2y
        + "*"
        + u2y
        + "+"
        + u2z
        + "*"
        + u2z
        + "))"
    )
    # dot(v(1),v(2))
    vv = (
        "("
        + v1x
        + "*"
        + v2x
        + "+"
        + v1y
        + "*"
        + v2y
        + "+"
        + v1z
        + "*"
        + v2z
        + ")"
        + "/sqrt(("
        + v1x
        + "*"
        + v1x
        + "+"
        + v1y
        + "*"
        + v1y
        + "+"
        + v1z
        + "*"
        + v1z
        + ")*("
        + v2x
        + "*"
        + v2x
        + "+"
        + v2y
        + "*"
        + v2y
        + "+"
        + v2z
        + "*"
        + v2z
        + "))"
    )
    # dot(t(1),t(2))
    tt = "((x2-x1)*(x3-x2)+(y2-y1)*(y3-y2)+(z2-z1)*(z3-z2))/(distance(p2,p1)*distance(p3,p2))"
    # cos(tw) is (u_1.u_2+v_1.v_2)/(1+t_1.t_2)
    # sin(tw) is (u_2.v_1-v_2.u_1)/(1+t_1.t_2)
    if use_cos:
        expression = "Kt*(1.0-(" + uu + "+" + vv + ")/(1.0+" + tt + "))"
        # expression = (
        #     "(2.0*Kt)*(exp(0.5*(1.0-(" + uu + "+" + vv + ")/(1.0+" + tt + ")))-1)"
        # )
    else:
        expression = (
            "0.5*Kt*acos(max(-0.999999,min(0.999999,("
            + uu
            + "+"
            + vv
            + ")/(1.0+"
            + tt
            + "))))^2"
        )
    twisting = mm.CustomCompoundBondForce(7, expression)
    twisting.addGlobalParameter("Kt", Kt)
    twisting.addGlobalParameter("overtwist", 0.0)
    # twisting.addPerBondParameter("twist0")
    twisting.addEnergyParameterDerivative("Kt")
    if particles_per_bead.nparticles_per_bead == 3:
        for i in range(N - int(linear)):
            ip1 = from_i_to_bead(i + 1, B)
            ip2 = from_i_to_bead(ip1 + 1, B)
            twisting.addBond(
                [
                    i,
                    ip1,
                    ip2,
                    get_pseudo_u_from_i(i, B),
                    get_pseudo_U_from_i(i, B),
                    get_pseudo_u_from_i(ip1, B),
                    get_pseudo_U_from_i(ip1, B),
                ]
            )  # ,[np.sqrt(1.0-np.arccos(twist0[i]))])
    twisting.setUsesPeriodicBoundaryConditions(PBC)
    return twisting


def add_torque_to_bond(
    B: int, N: int, torque: float = 0.0, linear: bool = True, PBC: bool = False
) -> mm.CustomCompoundBondForce:
    """return a torque along the tangent to the bond 'B'.
    Parameters
    ----------
    B      : index of the bond (int)
    N      : number of bonds (int)
    torque : torque applied to the bond (float)
    linear : linear (True) or ring (False) (bool)
    PBC    : does it use the periodic boundary conditions, False or True ? (bool)
    Returns
    -------
    OpenMM CustomCompoundBondForce.
    Raises
    ------
    References
    ----------
    The frame u,v,t is built with u' the pseudo-u vector :
    v~cross(t,u')
    u~cross(v,t)~cross(cross(t,u'),t)    
    To add a torque around t I added a force F=a*v along v.
    The force F acts on to the pseudo-u bead.
    The torque is then cross(u',F)= that differs from cross(u,F)=a*t.
    cross(u',n)=t
    """
    # number of beads
    M = N + int(linear)
    if False:
        # u' is the pseudo-u vector
        # v~cross(t,u')
        vx = "((y2-y1)*(z3-z1)-(z2-z1)*(y3-y1))"
        vy = "((z2-z1)*(x3-x1)-(x2-x1)*(z3-z1))"
        vz = "((x2-x1)*(y3-y1)-(y2-y1)*(x3-x1))"
        # expression
        norm_up = "distance(p3,p1)"
        norm_v = (
            "sqrt(" + vx + "*" + vx + "+" + vy + "*" + vy + "+" + vz + "*" + vz + ")"
        )
        expression = (
            "torque*("
            + vx
            + "*x3+"
            + vy
            + "*y3+"
            + vz
            + "*z3)/("
            + norm_v
            + "*"
            + norm_up
            + ")"
        )
        # custom compound bond force to deal with the torque
        torque_to_bond = mm.CustomCompoundBondForce(3, expression)
        torque_to_bond.addPerBondParameter("torque")
        torque_to_bond.addBond(
            [B, from_i_to_bead(B + 1, M), get_pseudo_u_from_i(B, M)], [torque]
        )
        torque_to_bond.setUsesPeriodicBoundaryConditions(PBC)
        return torque_to_bond
    else:
        torque_to_bond = mm.CustomTorsionForce("torque")
        torque_to_bond.addPerTorsionParameter("torque")
        # torque_to_bond.addTorsion(from_i_to_bead(B-1,M),B,from_i_to_bead(B+1,M),from_i_to_bead(B+2,M),[torque])
        ip1 = from_i_to_bead(B + 1, M)
        torque_to_bond.addTorsion(
            get_pseudo_u_from_i(B, M), B, ip1, get_pseudo_u_from_i(ip1, M), [torque]
        )
        return torque_to_bond


def wall_lj93(
    N_per_C: list,
    normal: str,
    position: float,
    epsilon: float,
    sigma: float,
    cutoff: float,
) -> mm.CustomCompoundBondForce:
    """return a wall/lj93 force.
    Parameters
    ----------
    N_per_C  : number of beads per chain (list of int)
    normal   : normal to the wall 'x', 'y', 'z' or '-x', '-y', '-z' (str)
    position : position of the wall (float)
    epsilon  : lj93 epsilon (float)
    sigma    : lj93 sigma (float)
    cutoff   : lj93 cutoff (float)
    Returns
    -------
    OpenMM CustomCompoundBondForce wall/lj93 force.
    Raises
    ------
    if 'normal' argument is not amongst 'x', 'y', 'z', '-x', '-y', '-z', return empty force.
    """
    if not normal in ["-x", "x", "-y", "y", "-z", "z"]:
        print(
            "normal to the wall is not amongst 'x', 'y', 'z', '-x', '-y', '-z', return empty force."
        )
        return mm.CustomCompoundBondForce()
    if "-" in normal:
        expression = (
            "epsilon_wall*((2/15)*(sigma_wall/d)^9-(sigma_wall/d)^3)*step(cutoff_wall-d);d="
            + str(position)
            + normal
            + "1"
        )
    else:
        expression = (
            "epsilon_wall*((2/15)*(sigma_wall/d)^9-(sigma_wall/d)^3)*step(cutoff_wall-d);d="
            + normal
            + "1-"
            + str(position)
        )
    print("wall/lj93 expression", expression)
    wall = mm.CustomCompoundBondForce(1, expression)
    wall.addGlobalParameter("epsilon_wall", epsilon)
    wall.addGlobalParameter("sigma_wall", sigma)
    wall.addGlobalParameter("cutoff_wall", cutoff)
    C = len(N_per_C)
    index = 0
    for c in range(C):
        Nc = N_per_C[c]
        for i in range(Nc):
            wall.addBond([index])
            index += 1
    wall.setForceGroup(6)
    return wall


def wall_lj126(
    N_per_C: list,
    normal: str,
    position: float,
    epsilon: float,
    sigma: float,
    cutoff: float,
) -> mm.CustomCompoundBondForce:
    """return a wall/lj126 force.
    Parameters
    ----------
    N_per_C  : number of beads per chain (list of int)
    normal   : normal to the wall 'x', 'y', 'z' or '-x', '-y', '-z' (str)
    position : position of the wall (float)
    epsilon  : lj126 epsilon (float)
    sigma    : lj126 sigma (float)
    cutoff   : lj126 cutoff (float)
    Returns
    -------
    OpenMM CustomCompoundBondForce wall/lj126 force.
    Raises
    ------
    if 'normal' argument is not amongst 'x', 'y', 'z', '-x', '-y', '-z', return empty force.
    """
    if not normal in ["-x", "x", "-y", "y", "-z", "z"]:
        print(
            "normal to the wall is not amongst 'x', 'y', 'z', '-x', '-y', '-z', return empty force."
        )
        return mm.CustomCompoundBondForce()
    if "-" in normal:
        expression = (
            "4.0*epsilon_wall*((sigma_wall/d)^12-(sigma_wall/d)^6)*step(cutoff_wall-d);d="
            + str(position)
            + normal
            + "1"
        )
    else:
        expression = (
            "4.0*epsilon_wall*((sigma_wall/d)^12-(sigma_wall/d)^6)*step(cutoff_wall-d);d="
            + normal
            + "1-"
            + str(position)
        )
    print("wall/lj126 expression", expression)
    wall = mm.CustomCompoundBondForce(1, expression)
    wall.addGlobalParameter("epsilon_wall", epsilon)
    wall.addGlobalParameter("sigma_wall", sigma)
    wall.addGlobalParameter("cutoff_wall", cutoff)
    C = len(N_per_C)
    index = 0
    for c in range(C):
        Nc = N_per_C[c]
        for i in range(Nc):
            wall.addBond([index])
            index += 1
    wall.setForceGroup(7)
    return wall


def wca(
    N: int, Temp: float, sigma: float, linear: bool, PBC: bool
) -> mm.NonbondedForce:
    """return a WCA potential to resolve excluded volume constraints.
    Parameters
    ----------
    N      : number of bonds (int)
    Temp   : temperature (float)
    sigma  : size of the bead (float)
    linear : linear or ring chain (False or True)
    PBC    : uses periodic boundary conditions (False or True)
    Returns
    -------
    OpenMM NonbondedForce (WCA potential).
    Raises
    ------
    """
    # number of beads
    B = N + int(linear)
    P = particles_per_bead.nparticles_per_bead
    M = P * B
    wca = mm.NonbondedForce()
    if PBC:
        wca.setNonbondedMethod(mm.NonbondedForce.CutoffPeriodic)
    else:
        wca.setNonbondedMethod(mm.NonbondedForce.CutoffNonPeriodic)
    wca.setCutoffDistance(np.power(2.0, 1.0 / 6.0) * sigma)
    for i in range(B):
        wca.addParticle(0.0, sigma, get_kBT(Temp))
    if True:
        for i in range(B, M):
            wca.addParticle(0.0, 0.0, 0.0)
        # no wca between bead and its pseudo-u beads
        if P >= 2:
            for i in range(B):
                wca.addException(i, get_pseudo_u_from_i(i, B), 0.0, sigma, 0.0, True)
                if P >= 3:
                    wca.addException(
                        i, get_pseudo_U_from_i(i, B), 0.0, sigma, 0.0, True
                    )
                    wca.addException(
                        get_pseudo_u_from_i(i, B),
                        get_pseudo_U_from_i(i, B),
                        0.0,
                        sigma,
                        0.0,
                        True,
                    )
    return wca


@numba.jit
def rotation(a: np.ndarray, angle: float, b: np.ndarray) -> np.ndarray:
    """return (x',y',z') that is the rotation of b=(x,y,z) around a=(X,Y,Z) with angle.
    Parameters
    ----------
    a     : rotation axis (np.ndarray)
    angle : rotation angle (float)
    b     : vector to rotate (np.ndarray)
    Returns
    -------
    the rotated vector (np.ndarray)
    """
    if a[0] == b[0] and a[1] == b[1] and a[2] == b[2]:
        return b
    else:
        n = np.sqrt(b[0] ** 2 + b[1] ** 2 + b[2] ** 2)
        c = np.cos(angle)
        s = np.sin(angle)
        new_b = normalize(b)
        res = np.multiply(c, new_b)
        ab = np.dot(a, new_b)
        # axb=np.cross(a,new_b)
        axb = my_cross(a, new_b)
        res = np.add(res, np.add(np.multiply((1.0 - c) * ab, a), np.multiply(s, axb)))
    return np.multiply(n, normalize(res))


@numba.jit
def flangevin(x: float) -> float:
    """return the value of the Langevin function at x : 1/tanh(x)-1/x.
    Parameters
    ----------
    x : the x where to evaluate the Langevin function (float)
    Returns
    -------
    the value of the Langevin function at x (float)
    """
    return 0.0 if x == 0.0 else 1.0 / np.tanh(x) - 1.0 / x


@numba.jit
def bending_rigidity(kbp: int, resolution: float, Temp: float) -> float:
    """return the bending rigidity for a specific resolution (in bp) and Kuhn length (in bp).
    Parameters
    ----------
    kbp        : Kuhn length in bp (int)
    resolution : resolution of a segment in bp (float)
    Temp       : temperature (float)
    Returns
    -------
    bending rigidity in units of kB*Temperature (float)
    Raises
    ------
    """
    if resolution >= float(kbp):
        return 0.0
    mcos = (kbp / resolution - 1.0) / (kbp / resolution + 1.0)
    a = 1e-10
    b = 1e4
    precision = 1e-10
    fa = flangevin(a) - mcos
    fb = flangevin(b) - mcos
    m = 0.5 * (a + b)
    fm = flangevin(m) - mcos
    while abs(a - b) > precision:
        m = 0.5 * (a + b)
        fm = flangevin(m) - mcos
        fa = flangevin(a) - mcos
        if fm == 0.0:
            break
        if (fa * fm) > 0.0:
            a = m
        else:
            b = m
    return m * get_kBT(Temp)


def twisting_rigidity(twisting_persistence: float, sigma: float, Temp: float) -> float:
    """return the twisting rigidity (harmonic potential 0.5*Kt*Tw^2 with <Tw^2>=twisting persistence/bond length).
    Parameters
    ----------
    twisting_persistence : the twisting peristence (float)
    sigma                : the bond size (float)
    Temp                 : temperature (float)
    Returns
    -------
    twisting rigidity in units of kB*Temperature (float)
    Raises
    ------
    """
    return (twisting_persistence / sigma) * get_kBT(Temp)


def v_gaussian(Temp: float, masses: list, seed: int = 1) -> list:
    """return a list of velocities distributed according to the Boltzmann distribution.
    size of the list is equal to the size of 'masses'.
    Parameters
    ----------
    Temp   : temperature in Kelvin (float)
    masses : mass of the beads in amu unit (list of float)
    seed   : seed argument for np.random.seed (int)
           if 'seed' < 0, do nothing
    Returns
    -------
    list of OpenMM.Vec3 (list)
    Raises
    ------
    """
    if seed > -1:
        np.random.seed(seed)
    kBT, K = get_kBT(Temp), 0.0
    # velocities according to Boltzmann distribution
    vxyz = np.zeros((len(masses), 3), dtype=np.double)
    for v, m in enumerate(masses):
        if m > 0.0:
            unif1 = 2.0 * np.pi * np.random.random()
            unif2 = np.arccos(2.0 * np.random.random() - 1.0)
            vt = np.sqrt(3.0 * kBT / m) * np.random.normal(0.0, 1.0)
            vxyz[v, :3] = (
                vt * np.sin(unif2) * np.cos(unif1),
                vt * np.sin(unif2) * np.sin(unif1),
                vt * np.cos(unif2),
            )
            # K += 0.5 * m * np.sum(np.square(vxyz[v, :3]))
    return [mm.Vec3(v[0], v[1], v[2]) for v in vxyz]


def write_draw_conformation(
    positions: np.ndarray,
    velocities: np.ndarray,
    norm_p: float = 1.0,
    xyz_lim: list = [0.0, 2.0, 0.0, 2.0, 0.0, 2.0],
    name_write: str = "write.out",
    name_draw: str = "draw.png",
    mode: str = "write_draw",
    bstack: bool = False,
):
    """write and/or draw conformation (in nanometer) using matplotlib to file name.out/png.
    write x y z for the first position, then go to a new line and write for the second position, ...
    Parameters
    ----------
    positions  : list of positions returns by 'get_positions_from_context' function (np.ndarray)
    velocities : list of velocities returns by 'get_velocities_from_context' function (np.ndarray)
    norm_p     : normalization factor (in nanometer) for the positions (float)
    xyz_lim    : plot x, y, and z range (list of float)
    name_write : name of the file to write (str)
    name_draw  : name of the file to draw (str)
    mode       : a string that contains write and/or draw (str)
                 if 'add_velocities' in 'mode' the output file is like
                 first position
                 first velocity
                 second position
                 second velocity
                 ...
    bstack     : stack conformation add at the end of the file (bool)
    Returns
    -------
    Raises
    ------
    """
    # write
    if "write" in mode:
        if "add_velocities" in mode:
            with open(name_write, "a" if bstack else "w") as out_file:
                for i, p in enumerate(positions):
                    out_file.write(str(p[0]) + " " + str(p[1]) + " " + str(p[2]) + "\n")
                    out_file.write(
                        str(velocities[i, 0])
                        + " "
                        + str(velocities[i, 1])
                        + " "
                        + str(velocities[i, 2])
                        + "\n"
                    )
        else:
            with open(name_write, "a" if bstack else "w") as out_file:
                for p in positions:
                    out_file.write(str(p[0]) + " " + str(p[1]) + " " + str(p[2]) + "\n")
    # draw
    if "draw" in mode:
        N, D = positions.shape
        np.multiply(1.0 / norm_p, positions, out=positions)
        px, py, pz = zip(*[(p[0], p[1], p[2]) for p in positions])
        fig = plt.figure("draw")
        ax = fig.add_subplot(111, projection="3d")
        ax.scatter(
            px,
            py,
            pz,
            c=[float(p) / float(N) for p in range(N)],
            s=1.5,
            cmap="rainbow",
            alpha=0.5,
        )
        ax.set_xlim(xyz_lim[0], xyz_lim[1])
        ax.set_ylim(xyz_lim[2], xyz_lim[3])
        ax.set_zlim(xyz_lim[4], xyz_lim[5])
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")
        fig.savefig(name_draw)
        fig.clf()
        plt.close("draw")


def overtwist_to_chain_no_closure_tw(
    N: int, resolution: float, helix: float, linear: bool = False
):
    """ (N-1)*Tw=x*2*Pi
    Parameters
    ----------
    N          : number of bonds (int)
    resolution : resolution in bp (float)
    helix      : helix in bp (float)
    linear     : linear (True) or ring polymer (False) (bool)
    Returns
    -------
    The twist Tw(i) between two consecutive frames (i,i+1) such that sum(Tw(i))=Lk, Tw closure (float) if circular chain, error on the Lk (float) that is 0.0 for linear chain.
    Raises
    ------
    """
    Lk0 = N * resolution / helix
    with open("test.out", "w") as out_file:
        out_file.write("overtwist local_tw\n")
        for x in np.arange(N // 2):
            out_file.write(str(x / Lk0) + " " + str(x * 2.0 * np.pi / (N - 1)) + "\n")


@numba.njit(parallel=True)
def overtwist_to_local_twist(
    N: int,
    resolution: float,
    helix: float,
    linear: bool,
    overtwist: float = 0.0,
    iterations: int = 1000000,
) -> tuple:
    """return the local twist (tw) between frame 'i' and frame 'i+1'
    such that global twist (Tw) is equal to 2 * pi * overtwist * Lk0.
    The chain is such that Tw = Wr = 0 (before the procedure).
    For ring polymer Tw is equal to (N - 1) * tw + tw_closure.
    For linear polymer we have tw_closure = 0.
    Parameters
    ----------
    N          : number of bonds (int)
    resolution : resolution in bp (float)
    helix      : helix in bp (float)
    linear     : linear (True) or ring polymer (False) (bool)
    overtwist  : overtwist (float)
                 if abs(overtwist) > 1 then abs(overtwist) = 1.
    iterations : number of iterations (int)
    Returns
    -------
    tuple
    twist tw(i) between two consecutive frames (i, i+1) such that sum(tw(i)) = Lk,
    tw closure (float) if circular chain, error on the Lk (float) that is 0.0 for linear chain.
    Raises
    ------
    """
    Pi = np.pi
    twoPi = 2.0 * Pi
    # overtwist interval within [-1,1]
    if np.absolute(overtwist) > 1.0:
        overtwist = -1.0 if overtwist < 0.0 else 1.0
    # sign of the twist
    sign_overtwist = -1.0 if overtwist < 0.0 else 1.0
    # compute local twist
    if overtwist == 0.0:
        return 0.0, 0.0, 0.0
    else:
        # linking number deficit
        Lk = overtwist * N * resolution / helix
        if linear:
            # tw * (N - 1) = 2 * Pi * Lk
            tw = twoPi * Lk / (N - 1)
            tw_closure = 0.0
        else:
            # tw * (N - 1) + b = 2 * Pi * Lk, we want b small
            tw0 = twoPi * Lk / (N - 1)
            tws = np.zeros(iterations, dtype=numba.float64)
            bs = np.zeros(iterations, dtype=numba.float64)
            ds = np.zeros(iterations, dtype=numba.float64)
            for k in prange(100):
                for s in range(k * (iterations // 100), (k + 1) * (iterations // 100)):
                    tws[s] = tw0 * (0.0 + s * (2.0 - 0.0) / iterations)
                    bs[s] = between_minus_pi_and_pi(
                        twoPi * floor_or_ceil_minus_x(tws[s] * (N - 1) / twoPi)
                    )
                    ds[s] = abs(tws[s] * (N - 1) + bs[s] - twoPi * Lk) + abs(bs[s])
            I = np.argmin(ds)
            tw = tws[I]
            tw_closure = bs[I]
        # intrinsic twist between two consecutive base-pair
        if np.fmod(resolution, helix) != 0.0:
            tw -= twoPi * np.fmod(resolution, helix)
        return (
            tw,
            tw_closure,
            1.0 - (tw * (N - 1) + tw_closure) / (twoPi * Lk),
        )


def overtwist_to_chain(
    N: int, resolution: float, helix: float, linear: bool, overtwist: float = 0.0
) -> tuple:
    """return the local twist (tw) between frame 'i' and frame 'i+1'
    such that global twist (Tw) is equal to 2*pi*overtwist*Lk0.
    The chain is such that Tw=Wr=0 (before the procedure).
    For ring polymer Tw is equal to (N-1)*tw+tw_closure.
    For linear polymer we have tw_closure=0.
    Parameters
    ----------
    N          : number of bonds (int)
    resolution : resolution in bp (float)
    helix      : helix in bp (float)
    linear     : linear (True) or ring polymer (False) (bool)
    overtwist  : overtwist (float)
    Returns
    -------
    tuple
    twist tw(i) between two consecutive frames (i,i+1) such that sum(tw(i))=Lk,
    tw closure (float) if circular chain, error on the Lk (float) that is 0.0 for linear chain.
    Raises
    ------
    if abs(overtwist) is greater than 1, abs(overtwist)=1.
    """
    Pi = np.pi
    twoPi = 2.0 * Pi
    # overtwist interval within [-1,1]
    if np.absolute(overtwist) > 1.0:
        print("If abs(overtwist) is greater than 1, then abs(overtwist)=1.")
        overtwist = -1.0 if overtwist < 0.0 else 1.0
    # sign of the twist
    sign_overtwist = -1.0 if overtwist < 0.0 else 1.0
    # compute local twist
    if overtwist == 0.0:
        return 0.0, 0.0, 0.0
    else:
        # linking number deficit
        Lk = np.divide(overtwist * N * resolution, helix, dtype=np.double)
        if linear:
            # tw*(N-1)=2*Pi*Lk
            tw = twoPi * Lk / (N - 1)
            tw_closure = 0.0
        else:
            # tw * (N - 1) + tw_closure = 2 * Pi * Lk
            tw = np.divide(twoPi * Lk, N - 1, dtype=np.double)
            # local twist between frame 'N-1' and frame '0'
            # modf, fmod or rint ?
            tw_closure = twoPi * (np.rint(Lk, dtype=np.double) - Lk)
            # remove it
            tw -= np.divide(tw_closure, N - 1, dtype=np.double)
            # new local twist between frame 'N-1' and frame '0'
            tw_closure = twoPi * (
                np.rint(tw * (N - 1) / twoPi, dtype=np.double) - tw * (N - 1) / twoPi
            )
        # intrinsic twist between two consecutive base-pair
        if resolution != helix:
            tw -= twoPi * np.fmod(resolution, helix)
        return (
            tw,
            tw_closure,
            1.0 - (tw * (N - 1) + tw_closure) / (twoPi * Lk),
        )


def create_and_run_replica(
    old_Ks: np.ndarray,
    new_Ks: np.ndarray,
    resolution: float,
    dt: float,
    tau: float,
    N: int,
    L: float,
    seed: int = 0,
    linear: bool = True,
    PBC: bool = False,
    platform: str = "CPU1_double",
    initial: str = "random",
    steps: int = 10000,
    checkpoint: str = "",
    hamiltonian: str = "",
    xyz: str = "",
    iterations: int = 0,
) -> np.ndarray:
    """create replica of a system, run and return bending and twisting energy.
    it also writes a file with an history of diffusion through the parameters space.
    Parameters
    ----------
    old_Ks      : list of parameters, each one of them has an associated system with specific hamiltonian (np.ndarray)
                  bending strength, twisting strength, overtwist, temperature, bond length, mass, Lk0
                  previous iteration
    new_Ks      : list of parameters, each one of them has an associated system with specific hamiltonian (np.ndarray)
                  bending strength, twisting strength, overtwist, temperature, bond length, mass, Lk0
                  current iteration
    N           : number of bonds
    L           : box size (float)
    dt          : time-step (float)
    tau         : time scale, inverse of the coupling frequency to the thermostat (float)
    seed        : seed for the pRNG (int)
                  if seed < 0, do nothing
    linear      : linear (True) or ring (False)
    PBC         : periodic boundary conditions (bool)
    platform    : something like 'name_implementation_precision'
                  example: 'CPU1_double', 'GeForce_RTX_2080_OpenCL_single', 'GeForce_GTX_1080_Ti_CUDA_double'
    initial     : start from 'linear', 'circular' or 'random' conformation (str)
    steps       : number of steps (int)
    checkpoint  : file name of OpenMM checkpoint (str)
    hamiltonian : file name where to save Hamiltonian (str)
    xyz         : file name where to save xyz conformation (str)
    iterations  : current number of iterations (int)
    Returns
    -------
    np.ndarray
    Raises
    ------
    if precision is not in (single, mixed, double), exit.
    if implementation is not in (OpenCL, CUDA, CPU), exit.
    """
    bond = "fene"
    twist1 = particles_per_bead(2)
    twist2 = particles_per_bead(3)
    # platform creation
    precision = "double"
    if "single" in platform:
        precision = "single"
    elif "mixed" in platform:
        precision = "mixed"
    elif "double" in platform:
        precision = "double"
    else:
        print("Please consider single, mixed or double for the precision.")
        sys.exit()
    # 'platform_name' has to be like 'name_implementation_'
    platform_name = platform.replace(precision, "")
    if "OpenCL" in platform:
        implementation = "OpenCL"
    elif "CUDA" in platform:
        implementation = "CUDA"
    elif "CPU" in platform:
        implementation = "CPU"
        precision = "double"
    else:
        print("function 'create_and_run_replica' did not find implementation, exit.")
        sys.exit()
    # 'platform_name' has to be like 'name__'
    platform_name.replace(implementation, "")
    # 'platform_name' has to be like 'name'
    platform_name = platform_name[:-1]
    platform, properties = create_platform(platform_name, precision)
    # number of beads
    B = N + int(linear)
    # system creation (does not remove com motion)
    system, masses = create_system(L, B, new_Ks[5], -1)
    # create integrator
    integrator = create_integrator(
        "gjf",
        new_Ks[3],
        dt,
        B,
        system.getNumConstraints(),
        new_Ks[5],
        tau,
        seed,
        False,
        False,
    )
    # checkpoint ?
    try:
        # check
        with open(checkpoint, "rb") as rfile:
            bcheckpoint = True
    except IOError:
        bcheckpoint = False
    # Hamiltonian file ?
    try:
        with open(hamiltonian, "r") as rfile:
            bhamiltonian = True
    except IOError:
        bhamiltonian = False
        with open(hamiltonian, "w") as wfile:
            wfile.write("Kb Kt overtwist temperature sigma mass Eb Et Tw\n")
    # local twist
    tw, tw_closure, Lk_error = overtwist_to_chain(
        N, resolution, 10.5, linear, new_Ks[2]
    )
    # positions and frames u,v,t
    if initial == "random":
        positions, u, v, t = puvt_chain(
            N,
            new_Ks[4],
            L,
            linear,
            "linear" if np.random.random() < 0.5 else "circular",
        )
    else:
        positions, u, v, t = puvt_chain(N, new_Ks[4], L, linear, initial)
    u, v = twist_each_uv(u, v, t, [tw] * (N - 1) + [0.0], linear)
    # u, v = twist_each_uv(u, v, t, [tw+tw_closure]+[tw] * (N - 2) + [tw_closure], linear)
    # pseudo-u beads
    new_positions = create_pseudo_u_beads(positions, u, new_Ks[4])
    # rigid or FENE bond ?
    if bond == "rigid":
        rigid_bond(system, N, new_Ks[4], linear)
    else:
        system.addForce(fene(N, new_Ks[3], new_Ks[4], linear, PBC, False))
    # fene + wca (pseudo-u beads)
    system.addForce(virtual_fene_wca(B, new_Ks[3], new_Ks[4], PBC))
    # wca
    system.addForce(wca(N, new_Ks[3], new_Ks[4], linear, PBC))
    # bending
    system.addForce(tt_bending(N, new_Ks[0], linear, PBC))
    system.addForce(ut_bending(N, get_pKb(new_Ks[3]), linear, PBC))
    system.addForce(uU_bending(N, get_pKb(new_Ks[3]), linear, PBC))
    # twisting
    system.addForce(double_twist(N, new_Ks[1], [0.0] * (N - int(linear)), linear, PBC))
    system.addForce(twist(N, new_Ks[1], [0.0] * (N - int(linear)), linear, PBC, False))
    # create context
    if not bcheckpoint:
        context = create_context(
            system,
            integrator,
            platform,
            properties,
            new_positions,
            v_gaussian(new_Ks[3], masses, -1),
        )
        print("no checkpoint")
    else:
        # load OpenMM checkpoint
        context = mm.Context(system, integrator, platform, properties)
        with open(checkpoint, "rb") as in_file:
            read_write_checkpoint(context, checkpoint, "read")
        print("load OpenMM checkpoint")
        if context.getSystem().getNumForces() != 8:
            print("number of forces is not 8, exit.")
            sys.exit()
    # change Lk
    # get the overtwist before replica exchange
    # if it differs from the new one, change it
    u, v, t = get_uvt_from_positions(get_vpositions_from_context(context))
    Tw = np.divide(np.sum(da.chain_twist(u, v, t, 1e-12)), 2.0 * np.pi)
    if np.absolute(old_Ks[2] - new_Ks[2]) > (1e-2 * np.absolute(old_Ks[2])):
        print("change overtwist from {0:f} to {1:f}".format(old_Ks[2], new_Ks[2]))
        new_positions = change_overtwist(
            get_vpositions_from_context(context),
            new_Ks[4],
            N * resolution / 10.5,
            old_Ks[2],
            new_Ks[2],
            linear,
        )
        # to the context
        context.setPositions([mm.Vec3(p[0], p[1], p[2]) for p in new_positions])
    # print(dir(platform),platform.getPropertyNames(),properties)
    # print_variables(context,linear,steps)
    context.getIntegrator().step(steps)
    # print("x_sigma_gjf",(context.getIntegrator().getPerDofVariableByName("x_sigma_gjf"))[1023:1025])
    # print("v_sigma_gjf",(context.getIntegrator().getPerDofVariableByName("v_sigma_gjf"))[1023:1025])
    # print("x2",(context.getIntegrator().getPerDofVariableByName("x2"))[0],(context.getIntegrator().getPerDofVariableByName("x2"))[1024],(context.getIntegrator().getPerDofVariableByName("x2"))[2048])
    # checkpoint
    read_write_checkpoint(context, checkpoint, "write")
    # xyz conformation
    write_draw_conformation(
        get_vpositions_from_context(context), name_write=xyz, mode="write"
    )
    # compute energy
    state0 = context.getState(getParameterDerivatives=True, enforcePeriodicBox=False)
    # write the Hamiltonian
    Eb = new_Ks[0] * (state0.getEnergyParameterDerivatives())["Kb_tt"]
    Et = new_Ks[1] * (state0.getEnergyParameterDerivatives())["Kt"]
    with open(hamiltonian, "a") as out_file:
        out_file.write(
            str(iterations)
            + " "
            + str(new_Ks[0])
            + " "
            + str(new_Ks[1])
            + " "
            + str(new_Ks[2])
            + " "
            + str(new_Ks[3])
            + " "
            + str(new_Ks[4])
            + " "
            + str(new_Ks[5])
            + " "
            + str(Eb)
            + " "
            + str(Et)
            + " "
            + str(Tw)
            + "\n"
        )
    return np.array([Eb, Et, Tw], dtype=np.double)


def replica_exchange(
    Ks: np.ndarray,
    resolution: float,
    dt: float,
    tau: float,
    N: int,
    L: float,
    steps: int = 10000,
    exchanges: int = 100,
    seed: int = 0,
    linear: bool = True,
    initial: str = "random",
    platform: str = "CPU1_double",
    path_to: str = "",
    replica: int = 0,
    lp_lt: bool = True,
    bdask: bool = True,
    scheduler: str = "local",
    queue: str = "",
):
    """compute replica exchange.
    Parameters
    ----------
    Ks        : list of parameters, each one of them has an associated system with specific hamiltonian (np.ndarray)
                bending strength, twisting strength, overtwist, temperature, bond length, mass, Lk0
    steps     : number of steps between replica exchange (int)
    exchanges : number of replica exchange iterations (int)
    N         : number of particles (int)
    L         : box size (float)
    dt        : time-step (float)
    tau       : time scale, inverse of the coupling frequency to the thermostat (float)
    seed      : seed for the pRNG (int)
                if seed < 0, do nothing
    linear    : linear (True) or ring (False) (bool)
    initial   : start from 'linear', 'circular' or 'random' conformation (str)
    platform  : something like 'name_implementation_precision' (str)
                example : 'GeForce_RTX_2080_OpenCL_single', 'GeForce_GTX_1080_Ti_CUDA_double'
    path_to   : path to the folder where to write the data (str)
    replica   : index of the replica to follow (int)
    lp_lt     : does it run replica exchange on l_p and l_t ? (bool)
                if lp_lt is False, replica exchange uses Lk
    bdask     : does it use Dask to compute on cluster ? (bool)
    scheduler : type of cluster 'local', 'SGE', 'PBS' or 'SLURM' (str)
    queue     : name of the queue you want to use (str)
                it is not required for local clsuter
    Returns
    -------
    Raises
    ------
    if the function does not find OpenMM checkpoints (print a message).
    """
    # init numpy seed
    if seed > -1:
        np.random.seed(seed)
    # copy the parameters
    old_Ks = np.copy(Ks)
    new_Ks = np.copy(Ks)
    # number of replicas
    R, P = Ks.shape
    if P != 7:
        print(
            "replica exchange expects array of bending, twist, overtwist, temperature, bond length, mass."
        )
        sys.exit()
    print(str(R) + " replicas")
    print(str(exchanges) + " exchange iterations")
    kBT = get_kBT(Ks[0, 3])
    bhpc = False
    if bdask:
        # memory estimation
        GB = max(0.4, 10 * (6 * particles_per_bead.nparticles_per_bead * N * 8) * 1e-9)
        print("memory estimation {0:f} GB".format(GB))
        # start scheduler
        # cluster
        print("platform", platform)
        if scheduler == "local":
            cluster = LocalCluster(
                n_workers=R,
                processes=True,
                threads_per_worker=1,
                # dashboard_address=None,
                # memory_limit=str(GB)+'GB',
                silence_logs=logging.DEBUG,
            )
        if not "gpu" in queue and not "GPU" in queue and scheduler == "PBS":
            cluster = PBSCluster(
                cores=1,
                memory=str(GB) + "GB",
                shebang="#!/usr/bin/env bash",
                processes=1,
                local_directory=path_to,
                resource_spec="select=1:ncpus=24:mem=10GB",
                queue=queue,
                project="dask_replica_exchange",
                walltime="02:00:00",
            )
            bhpc = True
        if not "gpu" in queue and not "GPU" in queue and scheduler == "SGE":
            cluster = SGECluster(
                cores=1,
                memory=str(GB) + "GB",
                shebang="#!/usr/bin/env bash",
                processes=1,
                interface="ib0",
                local_directory=path_to,
                # resource_spec='m_mem_free=1G',
                queue=queue,
                # project='dask_replica_exchange',
                walltime="02:00:00",
                extra=["--nprocs 1 --nthreads 1"]
                # n_workers=1
            )
            bhpc = True
        if ("gpu" in queue or "GPU" in queue) and scheduler == "PBS":
            cluster = PBSCluster(
                cores=1,
                memory=str(GB) + "GB",
                shebang="#!/usr/bin/env bash",
                processes=1,
                interface="ib0",
                local_directory=path_to,
                # resource_spec='select=1:ncpus='+str(R)+':mem=10GB',
                queue=queue,
                project="dask_replica_exchange",
                walltime="02:00:00",
                # n_workers=R,
                # extra=['--nprocs 1 --nthreads 1 --resources process=1,GPU='+str(R)]
                extra=["--nprocs 1 --nthreads 1 --resources GPU=" + str(R)],
            )
            bhpc = True
        if ("gpu" in queue or "GPU" in queue) and scheduler == "SGE":
            cluster = SGECluster(
                cores=1,
                memory=str(GB) + "GB",
                shebang="#!/usr/bin/env bash",
                processes=1,
                interface="ib0",
                local_directory=path_to,
                # resource_spec='select=1:ncpus='+str(R)+':mem=10GB',
                queue=queue,
                project="dask_replica_exchange",
                walltime="02:00:00",
                # n_workers=R,
                # extra=['--nprocs 1 --nthreads 1 --resources process=1,GPU='+str(R)]
                extra=["--nprocs 1 --nthreads 1 --resources GPU=" + str(R)],
            )
            bhpc = True
        # scale cluster
        cluster.scale(R)  # jobs=R)
        # job script ?
        if bhpc:
            print(cluster.job_script())
        # client from cluster
        client = Client(cluster)  # ,n_workers=1)
        print(client)
        print(dask.config)
        print(client.scheduler_info)
    # read the Hamiltonians
    irestarts = np.zeros(R, dtype=np.int64)
    for r in range(R):
        try:
            with open(
                path_to + "/replica" + str(r) + "/hamiltonian.out", "r"
            ) as in_file:
                lines = in_file.readlines()
                for i, l in enumerate(lines):
                    # skip header line
                    if i > 0:
                        sl = l.split()
                        irestarts[r] = max(irestarts[r], int(sl[0]))
        except IOError:
            print("No Hamiltonian found for the replica " + str(r) + ".")
        try:
            with open(
                path_to + "/replica" + str(r) + "/restart/restart.chk", "rb"
            ) as in_file:
                pass
        except IOError:
            print("No checkpoint found for the replica " + str(r) + ".")
    # do the replicas end after same number of iterations ?
    irestart = max(irestarts)
    n_irestart = np.count_nonzero(irestarts == irestart)
    irestart *= int(n_irestart == R)
    current_iteration = irestart
    renergy = np.zeros((R, 2), dtype=np.double)
    Tws = np.zeros(R, dtype=np.double)
    # store if replica returns exception
    bexception = np.full(R, 1)
    # loop over the number of exchanges
    acceptations = 0
    for e in range(exchanges):
        # no replica has been cancelled ?
        # wrong initialization so we capture
        bexception[r] = 1
        while int(np.sum(bexception)) > 0:
            # step all replicas
            if not bdask:
                for r in range(R):
                    print("replica{0:d}: {1:d}/{2:d}".format(r, e, exchanges))
                    bexception[r] = 0
                    renergy[r, 0], renergy[r, 1], Tws[r] = create_and_run_replica(
                        old_Ks[r],
                        new_Ks[r],
                        resolution,
                        dt,
                        tau,
                        N,
                        L,
                        int(((r + 1) * current_iteration) % 1013),
                        linear,
                        False,
                        platform,
                        initial,
                        steps,
                        path_to + "/replica" + str(r) + "/restart/restart.chk",
                        path_to + "/replica" + str(r) + "/hamiltonian.out",
                        path_to
                        + "/replica"
                        + str(r)
                        + "/out/xyz"
                        + str(current_iteration)
                        + ".out",
                        irestart,
                    )
                    print(renergy[r, 0], renergy[r, 1], Tws[r])
            else:
                # Dask distributed
                # # scatter data
                # big_futures=[]
                # for r in range(R):
                #     rname=path_to+"/replica"+str(r)+"/restart/restart.chk"
                #     hname=path_to+"/replica"+str(r)+"/hamiltonian.out"
                #     big_futures.append(client.scatter((new_Ks[r],resolution,dt,tau,N,L,seed,linear,False,platform,steps,rname,hname,irestart),broadcast=True))
                # submit
                futures = []
                for r in range(R):
                    print("submit replica{0:d}: {1:d}/{2:d}".format(r, e, exchanges))
                    bexception[r] = 0
                    rname = path_to + "/replica" + str(r) + "/restart/restart.chk"
                    hname = path_to + "/replica" + str(r) + "/hamiltonian.out"
                    xyzname = (
                        path_to
                        + "/replica"
                        + str(r)
                        + "/out/xyz"
                        + str(current_iteration)
                        + ".out"
                    )
                    if (
                        "CPU" in platform
                        and not "OpenCL" in platform
                        and not "CUDA" in platform
                    ):
                        futures.append(
                            client.submit(
                                create_and_run_replica,
                                old_Ks[r],
                                new_Ks[r],
                                resolution,
                                dt,
                                tau,
                                N,
                                L,
                                int(((r + 1) * current_iteration) % 1013),
                                linear,
                                False,
                                platform,
                                initial,
                                steps,
                                rname,
                                hname,
                                xyzname,
                                irestart,
                            )
                        )
                        # resources={'process':1}
                    if "OpenCL" in platform:
                        futures.append(
                            client.submit(
                                create_and_run_replica,
                                old_Ks[r],
                                new_Ks[r],
                                resolution,
                                dt,
                                tau,
                                N,
                                L,
                                int(((r + 1) * current_iteration) % 1013),
                                linear,
                                False,
                                platform,
                                initial,
                                steps,
                                rname,
                                hname,
                                xyzname,
                                irestart,
                                resources={"GPU": 1},
                            )
                        )
                        # resources={'process':1,'GPU':1}
                # gather
                print("gather ...")
                results = client.gather(futures)
                wait(futures)
                # block until it finished
                for r in range(R):
                    print("gather replica{0:d}: {1:d}".format(r, e), new_Ks[r])
                    # future done ?
                    bdone = futures[r].done()
                    while not bdone:
                        bdone = futures[r].done()
                    bexception[r] = 0 if futures[r].exception() == None else 1
                    # print(futures[r], futures[r].exception(), futures[r].result(), results[r])
                    renergy[r, 0], renergy[r, 1], Tws[r] = (
                        results[r][0],
                        results[r][1],
                        results[r][2],
                    )
                # delete futures
                # for r in np.arange(0, R, 1):
                #     futures[R - 1 - r].cancel()
                #     del futures[R - 1 - r]
                #     # del big_futures[R-1-r]
                del futures
                # del big_futures
        current_iteration = irestart + (e + 1) * steps
        # copy parameters before next exchange
        old_Ks = np.copy(new_Ks)
        # replica exchange
        sequence = np.arange(0, R, 1)
        np.random.shuffle(sequence)
        for r in range(0, R - 1, 2):
            # context 0,1
            sequence0, sequence1 = sequence[r], sequence[r + 1]
            energy_i, energy_j = (
                renergy[sequence0, 0] + renergy[sequence0, 1],
                renergy[sequence1, 0] + renergy[sequence1, 1],
            )
            Tw_i, Tw_j = Tws[sequence0], Tws[sequence1]
            Kb0, Kt0, o0 = new_Ks[sequence0, :3]
            Kb1, Kt1, o1 = new_Ks[sequence1, :3]
            if sequence0 == sequence1:
                print(
                    str(sequence0) + " is not supposed to be equal to " + str(sequence1)
                )
                sys.exit()
            # dLk (Lk0 is the same)
            dLk = (o1 - o0) * new_Ks[sequence0, 6]
            # acceptance-rejection criteria
            if not lp_lt:
                # get twisting persistence length
                l_Tw = new_Ks[0][1] * new_Ks[0][4] / kBT
                factor = -4.0 * np.pi * np.pi * l_Tw * (Tw_i - Tw_j + dLk) * dLk / L
                # print(factor, 4.0 * np.pi * np.pi * l_Tw, (Tw_i - Tw_j + dLk) * dLk / L)
            else:
                # energy after the swap
                energy_i_exchange = (Kb1 / Kb0) * renergy[sequence0, 0] + (
                    Kt1 / Kt0
                ) * renergy[sequence0, 1]
                energy_j_exchange = (Kb0 / Kb1) * renergy[sequence1, 0] + (
                    Kt0 / Kt1
                ) * renergy[sequence1, 1]
                # delta energy
                factor = (
                    energy_i + energy_j - energy_i_exchange - energy_j_exchange
                ) / kBT
            bacceptation = (
                True if factor > 0.0 else bool(np.random.random() <= np.exp(factor))
            )
            # replica exchange ?
            if bacceptation:
                if sequence0 == replica:
                    replica = sequence1
                elif sequence1 == replica:
                    replica = sequence0
                else:
                    pass
                # swap
                new_Ks[sequence0, :3] = Kb1, Kt1, o1
                new_Ks[sequence1, :3] = Kb0, Kt0, o0
                acceptations += 1
            else:
                pass
        if R > 1:
            print(
                "acceptation ratio="
                + str(float(acceptations) / float((e + 1) * R // 2))
            )
    # close client
    if bdask:
        # print("before cancel")
        # client.cancel(futures)
        print("before scheduler close")
        # cluster.scheduler.close(close_workers=True)
        cluster.scheduler.close()
        with open(
            path_to + "/replica" + str(r) + "/dask_scheduler_logs.out", "w"
        ) as out_file:
            for i, l in enumerate(client.get_scheduler_logs()):
                out_file.write("scheduler log {0:d}".format(i))
                out_file.write(l)
        print("before client close")
        client.close()
        print("before cluster close")
        cluster.close()
        del cluster, client
        print("cluster and client down")


def jqf_replica_exchange(
    Ks: list,
    Temp: float,
    resolution: float,
    sigma: float,
    m: float,
    tau: float,
    overtwist: float,
    N: int,
    L: float,
    steps: int = 10000,
    iterations: int = 10,
    seed: int = 0,
    linear: bool = True,
    path_to: str = "",
    replica: int = 0,
):
    """performs replica exchange.
    Parameters
    ----------
    Ks         : list of parameters, each one of them has an associated system with specific hamiltonian (list)
    steps      : number of steps between replica exchange (int)
    iterations : number of replica exchange iterations (int)
    N          : number of particles
    L          : box size (float)
    Temp       : temperature (float)
    sigma      : bond size (float)
    m          : mass of the particles (float)
    tau        : time scale, inverse of the coupling frequency to the thermostat (float)
    overtwist  : normalized linking number deficit (float)
    seed       : seed for the pRNG (int)
                 if seed < 0, do nothing
    linear     : linear (True) or ring (False)
    path_to    : path to the folder where to write the data (string)
    replica    : index of the replica to follow (int)
    Returns
    -------
    Raises
    ------
    if the function does not find OpenMM checkpoints (print a message).
    if precision is not in (single,mixed,double) print a message and precision sets to single.
    """

    @mpi_task(cluster_id="cluster_repex")
    def run_context_and_return_checkpoint(
        checkpoint: str, steps: int, replica: int
    ) -> str:
        """run a context for 'steps' time-steps.
        Parameters
        ----------
        checkpoint : checkpoint from createCheckpoint OpenMM function
        steps      : number of steps (int)
        replica    : index of the replica (int)
        Returns
        -------
        checkpoint corresponding to the replica.
        Raises
        ------
        """
        # from mpi4py import MPI
        context = mm.Context()
        context.loadCheckpoint(checkpoint)
        context.getIntegrator().step(steps)
        # comm=MPI.COMM_WORLD
        # rank=comm.Get_rank()
        # print("replica %i(%i) finished"%(replica,rank))
        print("replica %i finished" % (replica))

    return context.createCheckpoint()

    @on_cluster(cluster=custom_cluster, cluster_id=repex_mpiCluster)
    def run_n_replicas(checkpoints: list, steps: int):
        """run 'n' contexts corresponding to 'n' replicas.
        Parameters
        ----------
        checkpoints : list of checkpoints from createCheckpoint OpenMM function
        steps       : number of steps (int)
        Returns
        -------
        checkpoints corresponding to the 'n' replicas.
        Raises
        ------
        """
        for i, c in enumerate(checkpoints):
            checkpoints[i] = run_context_and_return_checkpoint(c, steps, i)
        return checkpoints

    @mpi_task(cluster_id="cluster_repex")
    def observables_replica(
        checkpoint: str, replica: int, iteration: int, path_to: str
    ):
        context = mm.Context()
        # draw and write the conformation
        write_draw_conformation(
            context.loadCheckpoint(checkpoint),
            path_to
            + "/replica"
            + str(replica)
            + "/png/xyz.s"
            + str(iteration)
            + ".out",
            path_to
            + "/replica"
            + str(replica)
            + "/png/xyz.s"
            + str(iteration)
            + ".png",
            "write_draw",
        )
        # write the Hamiltonian
        with open(
            path_to + "/replica" + str(replica) + "/hamiltonian.out", "a"
        ) as out_file:
            out_file.write(
                str(iteration)
                + " "
                + str(context.getParameter("Kb_tt"))
                + " "
                + str(context.getParameter("Kt"))
                + "\n"
            )
        # state=context.getState(getPositions=True,getEnergy=True,getParameterDerivatives=True,enforcePeriodicBox=False)
        # contact_ij,Ps,sample=contact_matrix(contact_ij,Ps,sample,2,positions,40.0,linear,path_to+"/Ps.s"+str(iteration))
        # draw_symmetric_matrix(contact_ij,path_to+"/contact.matrix.s"+str(iteration),True)
        # plectonemes=get_plectonemes(context,sigma,linear,path_to+"/get.plectonemes.s"+str(iteration))
        # plectonemes=get_plectonemes(positions,1.25*sigma,int(100.0/sigma),linear)
        # draw_conformation(context,path_to+"/xyz.s"+str(iteration)+".out",path_to+"/xyz.s"+str(iteration)+".png","draw")

    @on_cluster(cluster=custom_cluster, cluster_id="cluster_repex")
    def observables_n_replicas(checkpoints: list, iteration: int, path_to: str):
        for i, c in enumerate(checkpoints):
            observables_replica(c, i, iteration, path_to)

    # variables
    if seed > -1:
        np.random.seed(seed)
    kBT = get_kBT(Temp)
    polymer = ["ring", "linear"][int(linear)]
    replicas = [r for r in range(R)]
    # for the contacts
    contact_ij = []
    Ps = []
    sample = 0
    # read the Hamiltonians
    restart_iterations = [0] * R
    for r in range(R):
        try:
            with open(
                path_to + "/replica" + str(r) + "/hamiltonian.out", "r"
            ) as in_file:
                lines = in_file.readlines()
                for l in lines:
                    sl = l.split()
                    restart_iterations[r] = max(restart_iterations[r], int(sl[0]))
        except IOError:
            print("No Hamiltonian found for the replica " + str(r) + ".")
    # do the replicas end at the same iterations ?
    restart_iteration = max(restart_iterations)
    n_restart_iteration = sum(
        [int(restart_iterations[r] == restart_iteration) for r in range(R)]
    )
    restart_iteration *= int(n_restart_iteration == R)
    # read the checkpoints, load the Hamiltonian parameters (from the checkpoints)
    checkpoints, context = create_replica(
        Ks,
        Temp,
        resolution,
        sigma,
        m,
        dt,
        tau,
        overtwist,
        N,
        L,
        seed,
        linear,
        platform,
        replica,
    )
    contexts = [context]
    for r in range(R):
        try:
            with open(
                path_to
                + "/replica"
                + str(r)
                + "/restart/restart."
                + polymer
                + ".s"
                + str(restart_iteration)
                + ".chk",
                "rb",
            ) as in_file:
                checkpoints[r] = in_file.read()
                contexts[0].loadCheckpoint(checkpoints[r])
                Ks[r] = [
                    contexts[0].getParameter("Kb_tt"),
                    contexts[0].getParameter("Kt"),
                ]
        except IOError:
            print("No checkpoint found for the replica " + str(r) + ".")
    # cluster
    set_default_cluster(CustomSLURMCluster)
    custom_cluster = CustomSLURMCluster(
        name="cluster_repex",
        walltime="01:00:00",
        nodes=R,
        mpi_mode=True,
        fork_mpi=True,
        queue="devel",
    )
    checkpoints = run_n_replicas(checkpoints, steps)
    # loop over replica exchange iterations
    for i in range(iterations):
        current_iteration = restart_iteration + (i + 1) * steps
        # step the replicas
        checkpoints = run_n_replicas(checkpoints, steps)
        # observables
        observables_n_replicas(checkpoints, current_iteration, path_to)
        # replica exchange
        exchanges = 0
        sequence = replicas
        while exchanges < (R - 2):
            replica_i = np.random.randint(0, exchanges, size=(R - 1))
            sequence[replica_i], sequence[exchanges] = (
                sequence[exchanges],
                sequence[replica_i],
            )
            exchanges += 1
            replica_j = np.random.randint(0, exchanges, size=(R - 1))
            sequence[replica_j], sequence[exchanges] = (
                sequence[exchanges],
                sequence[replica_j],
            )
            exchanges += 1
        acceptation_ratio = 0
        for r in range(0, R - 1, 2):
            sequence0 = sequence[r]
            sequence1 = sequence[r + 1]
            # context 0
            contexts[0].loadCheckpoint(checkpoints[sequence0])
            Ks0 = [contexts[0].getParameter("Kb_tt"), contexts[0].getParameter("Kt")]
            state0 = contexts[0].getState(
                getParameterDerivatives=True, enforcePeriodicBox=False
            )
            energy_i = (
                Ks0[0] * (state0.getEnergyParameterDerivatives())["Kb_tt"]
                + Ks0[1] * (state0.getEnergyParameterDerivatives())["Kt"]
            )
            # context 1
            contexts[0].loadCheckpoint(checkpoints[sequence1])
            Ks1 = [contexts[0].getParameter("Kb_tt"), contexts[0].getParameter("Kt")]
            state1 = contexts[0].getState(
                getParameterDerivatives=True, enforcePeriodicBox=False
            )
            energy_j = (
                Ks1[0] * (state1.getEnergyParameterDerivatives())["Kb_tt"]
                + Ks1[1] * (state1.getEnergyParameterDerivatives())["Kt"]
            )
            # energy after the swap
            energy_i_exchange = (
                Ks1[0] * (state0.getEnergyParameterDerivatives())["Kb_tt"]
                + Ks1[1] * (state0.getEnergyParameterDerivatives())["Kt"]
            )
            energy_j_exchange = (
                Ks0[0] * (state1.getEnergyParameterDerivatives())["Kb_tt"]
                + Ks0[1] * (state1.getEnergyParameterDerivatives())["Kt"]
            )
            # print(energy_i,energy_j,energy_i_exchange,energy_j_exchange)
            factor = (energy_i + energy_j - energy_i_exchange - energy_j_exchange) / kBT
            if factor > 0.0:
                acceptation = True
            else:
                acceptation = bool(np.random.random() <= np.exp(factor))
            # replica exchange ?
            if acceptation:
                if sequence0 == replica:
                    replica = sequence1
                elif sequence1 == replica:
                    replica = sequence0
                else:
                    pass
                # swap
                contexts[0].loadCheckpoint(checkpoints[sequence0])
                contexts[0].setParameter("Kb_tt", Ks1[0])
                contexts[0].setParameter("Kt", Ks1[1])
                checkpoints[sequence0] = contexts[0].createCheckpoint()
                contexts[0].loadCheckpoint(checkpoints[sequence1])
                contexts[0].setParameter("Kb_tt", Ks0[0])
                contexts[0].setParameter("Kt", Ks0[1])
                checkpoints[sequence1] = contexts[0].createCheckpoint()
                acceptation_ratio += 1
            else:
                pass
        print(
            "acceptation ratio="
            + str(float(acceptation_ratio) / float(R // 2))
            + " ("
            + str(acceptation_ratio)
            + "/"
            + str(R // 2)
            + ")"
        )
        # save the checkpoints
        for r in range(R):
            context.loadCheckpoint(checkpoints[r])
            read_write_checkpoint(
                context,
                path_to
                + "/replica"
                + str(r)
                + "/restart/restart."
                + polymer
                + ".s"
                + str(current_iteration)
                + ".chk",
                "write",
            )


def create_integrator(
    iname: str,
    Temp: float,
    dt: float,
    N: int,
    C: int,
    mass: float,
    tau: float,
    seed: int = -1,
    limit: bool = False,
    rescale: bool = False,
):
    """return a integrator with spatial step limit and/or rescaling of the velocities.
    Parameters
    ----------
    iname      : "Gronech-Jensen-Farago" (gjf), 'global', 'langevin', 'mnve' or 'verlet'
    Temp       : temperature (float)
    dt         : time step in picosecond (float)
    N          : number of beads (int)
    C          : number of constraints (int)
    mass       : mass of the beads in amu unit (float)
    tau        : inverse of the coupling frequency to the thermostat (float)
    seed       : seed for the integrator (int)
                 set seed to random number if and only if seed is non-negative
    limit      : limit for the ||x(t+dt)-x(t)|| (bool)
    rescale    : scale the kinetic average to its thermal average value (bool)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    if iname is not equal to 'gjf', 'global', 'langevin', 'nve' or 'verlet.
    """
    if iname == "gjf":
        integrator = gjf_integrator(dt, tau, seed, N, C, Temp, limit, rescale)
    elif iname == "global":
        integrator = global_integrator(dt, tau, seed, N, C, Temp, limit, rescale, False)
    elif iname == "langevin":
        integrator = mm.LangevinIntegrator(Temp, 1.0 / tau, dt)
    elif iname == "mnve":
        integrator = mnve_integrator(dt, N, C, Temp, tau, seed, limit, rescale)
    elif iname == "verlet":
        integrator = verlet_integrator(dt, N, C, Temp, limit, rescale)
    else:
        print(
            "Please consider using 'gjf', 'global', 'langevin', 'mnve' or 'verlet' for input 'iname' variable."
        )
        print(
            "Therefore, 'iname' takes the value 'langevin' with 'Temp=300.0', 'tau=1.0' and 'dt=0.001'."
        )
        integrator = mm.LangevinIntegrator(300.0, 1.0, 0.001)
    integrator.setConstraintTolerance(1e-5)
    if iname != "verlet":
        integrator.setRandomNumberSeed(seed)
    return integrator


def gjf_integrator(
    dt: float,
    tau: float,
    seed: int,
    N: int,
    C: int,
    Temp: float,
    limit: bool = False,
    rescale: bool = False,
) -> mm.Integrator:
    """return a Gronbech-Jensen-Farago (GJF) Langevin integrator with spatial step limit and rescaling of the velocities.
    Parameters
    ----------
    dt      : the time step in picosecond (float)
    tau     : inverse of the coupling frequency to the thermostat (float)
    seed    : seed for the integrator (int)
    N       : the number of beads (int)
    C       : the number of constraints (int)
    Temp    : temperature of the system in Kelvin (float)
    limit   : limit displacement length (bool)
    rescale : rescaling of the kinetic energy (bool)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    References
    ----------
    Niels Gronbech-Jensen and Oded Farago.
    A simple and effective Verlet-type algorithm for simulating Langevin dynamics.
    https://doi.org/10.1080/00268976.2012.760055
    """
    integrator = mm.CustomIntegrator(dt)
    integrator.setRandomNumberSeed(seed)
    integrator.addGlobalVariable("kBT", get_kBT(Temp))
    integrator.addGlobalVariable("mke", get_kinetic_energy_average(Temp, N, C))
    integrator.addGlobalVariable("ke", 0.0)
    integrator.addGlobalVariable("inv_tau", 1.0 / tau)
    if limit:
        integrator.addPerDofVariable("dx_lim", 0.0)
    integrator.addPerDofVariable("f_n", 0.0)
    integrator.addPerDofVariable("beta_n_1", 0.0)
    if C > 0:
        integrator.addPerDofVariable("x1", 0.0)
    integrator.addGlobalVariable("a_gjf", 0.0)
    integrator.addGlobalVariable("b_gjf", 0.0)
    integrator.addComputeGlobal("a_gjf", "(1-0.5*dt*inv_tau)/(1+0.5*dt*inv_tau)")
    integrator.addComputeGlobal("b_gjf", "1/(1+0.5*dt*inv_tau)")
    integrator.addComputePerDof("beta_n_1", "sqrt(2*m*inv_tau*kBT*dt)*gaussian")
    integrator.addUpdateContextState()
    integrator.addComputePerDof("f_n", "f")
    if limit:
        integrator.addComputePerDof("dx_lim", "dt*sqrt(kBT/m)")
        integrator.addComputePerDof(
            "x",
            "x+max(-dx_lim,min(dx_lim,b_gjf*(dt*v+0.5*dt*dt*f/m+0.5*dt*beta_n_1/m)))",
        )
    else:
        integrator.addComputePerDof(
            "x", "x+b_gjf*(dt*v+0.5*dt*dt*f/m+0.5*dt*beta_n_1/m)"
        )
    if C > 0:
        integrator.addComputePerDof("x1", "x")
        integrator.addConstrainPositions()
        integrator.addComputePerDof(
            "v", "a_gjf*v+0.5*dt*(a_gjf*f_n+f)/m+b_gjf*beta_n_1/m+(x-x1)/dt",
        )
        integrator.addConstrainVelocities()
    else:
        integrator.addComputePerDof(
            "v", "a_gjf*v+0.5*dt*(a_gjf*f_n+f)/m+b_gjf*beta_n_1/m",
        )
    if rescale:
        integrator.addComputeSum("ke", "0.5*m*v*v")
        integrator.addComputePerDof("v", "v*sqrt(mke/ke)")
    return integrator


def mnve_integrator(
    dt: float,
    N: int,
    C: int,
    Temp: float,
    tau: float,
    seed: int,
    limit: bool,
    rescale: bool,
    Vstep: int = 1000,
) -> mm.Integrator:
    """return a Verlet/Gronbech-Jensen-Farago (GJF) Langevin integrator with spatial step limit and rescaling of the velocities.
    GJF integrator is used to reset the velocities over 1/(coupling frequency*dt) time-steps.
    Parameters
    ----------
    dt      : the time step in picosecond (float)
    tau     : inverse of the coupling frequency to the thermostat (float)
    seed    : seed for the integrator (int)
    N       : the number of beads (int)
    C       : the number of constraints (int)
    Temp    : temperature of the system in Kelbin (float)
    limit   : limit displacement length (bool)
    rescale : rescaling of the kinetic energy (bool)
    Vstep   : Verlet integrator number of steps (int)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    References
    ----------
    Niels Gronbech-Jensen and Oded Farago.
    A simple and effective Verlet-type algorithm for simulating Langevin dynamics.
    https://doi.org/10.1080/00268976.2012.760055
    """
    integrator = mm.CustomIntegrator(dt)
    integrator.setRandomNumberSeed(seed)
    integrator.addGlobalVariable("inv_tau", 1.0 / tau)
    integrator.addGlobalVariable("gjf_count", 0)
    integrator.addGlobalVariable("vreset", int(tau / dt) + 1)
    integrator.addGlobalVariable("verlet_count", 0)
    integrator.addGlobalVariable("mcount", Vstep)
    integrator.addGlobalVariable("kBT", get_kBT(Temp))
    integrator.addGlobalVariable("mke", get_kinetic_energy_average(Temp, N, C))
    integrator.addGlobalVariable("ke", 0.0)
    # GJF variables
    integrator.addGlobalVariable("a_gjf", 0.0)
    integrator.addGlobalVariable("b_gjf", 0.0)
    integrator.addComputeGlobal("a_gjf", "(1-0.5*dt*inv_tau)/(1+0.5*dt*inv_tau)")
    integrator.addComputeGlobal("b_gjf", "1/(1+0.5*dt*inv_tau)")
    integrator.addPerDofVariable("f_n", 0.0)
    integrator.addPerDofVariable("beta_n_1", 0.0)
    if limit:
        integrator.addPerDofVariable("dx_lim", 0.0)
    integrator.addPerDofVariable("x1", 0.0)
    integrator.addUpdateContextState()
    # Verlet integrator ?
    integrator.beginIfBlock("verlet_count<mcount")
    integrator.addComputePerDof("v", "v+0.5*dt*f/m")
    if limit:
        integrator.addComputePerDof("dx_lim", "dt*sqrt(kBT/m)")
        integrator.addComputePerDof("x", "x+max(-dx_lim,min(dx_lim,dt*v))")
    else:
        integrator.addComputePerDof("x", "x+dt*v")
    if C > 0:
        integrator.addComputePerDof("x1", "x")
        integrator.addConstrainPositions()
        integrator.addComputePerDof("v", "v+0.5*dt*f/m+(x-x1)/dt")
        integrator.addConstrainVelocities()
    else:
        integrator.addComputePerDof("v", "v+0.5*dt*f/m")
    if rescale:
        integrator.addComputeSum("ke", "0.5*m*v*v")
        integrator.addComputePerDof(
            "v",
            "v*(step(0.01-abs(1.0-mke/ke))+(1-step(0.01-abs(1.0-mke/ke)))*sqrt(mke/ke))",
        )
    integrator.addComputeGlobal("verlet_count", "verlet_count+1")
    integrator.endBlock()
    # GJF integrator ?
    integrator.beginIfBlock("gjf_count<vreset")
    integrator.addComputePerDof("beta_n_1", "sqrt(2*m*inv_tau*kBT*dt)*gaussian")
    integrator.addComputePerDof("f_n", "f")
    if limit:
        integrator.addComputePerDof("dx_lim", "dt*sqrt(kBT/m)")
        integrator.addComputePerDof(
            "x",
            "x+max(-dx_lim,min(dx_lim,b_gjf*(dt*v+0.5*dt*dt*f/m+0.5*dt*beta_n_1/m)))",
        )
    else:
        integrator.addComputePerDof(
            "x", "x+b_gjf*(dt*v+0.5*dt*dt*f/m+0.5*dt*beta_n_1/m)"
        )
    if C > 0:
        integrator.addComputePerDof("x1", "x")
        integrator.addConstrainPositions()
        integrator.addComputePerDof(
            "v", "a_gjf*v+0.5*dt*(a_gjf*f_n+f)/m+b_gjf*beta_n_1/m+(x-x1)/dt",
        )
        integrator.addConstrainVelocities()
    else:
        integrator.addComputePerDof(
            "v", "a_gjf*v+0.5*dt*(a_gjf*f_n+f)/m+b_gjf*beta_n_1/m",
        )
    if rescale:
        integrator.addComputeSum("ke", "0.5*m*v*v")
        integrator.addComputePerDof("v", "v*sqrt(mke/ke)")
    integrator.addComputeGlobal("gjf_count", "gjf_count+1")
    integrator.endBlock()
    # change integrator to GJF ?
    integrator.beginIfBlock("verlet_count=mcount")
    integrator.addComputeGlobal("gjf_count", "0")
    integrator.addComputeGlobal("verlet_count", "mcount+1")
    integrator.endBlock()
    # change integrator to Verlet ?
    integrator.beginIfBlock("gjf_count=vreset")
    integrator.addComputeGlobal("gjf_count", "vreset+1")
    integrator.addComputeGlobal("verlet_count", "0")
    integrator.endBlock()
    return integrator


def verlet_integrator(
    dt: float, N: int, C: int, Temp: float, limit: bool, rescale: bool
) -> mm.Integrator:
    """create and return a Velocity-Verlet integrator with spatial step limit and rescaling of the velocities
    Parameters
    ----------
    dt      : time step in picosecond (float)
    N       : number of particles (int)
    C       : number of constraints (int)
    Temp    : temperature in Kelvin (float)
    limit   : limit the displacement length (bool)
    rescale : rescaling of the kinetic energy (bool)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    """
    if limit or rescale:
        kBT = get_kBT(Temp)
        integrator = mm.CustomIntegrator(dt)
        integrator.addGlobalVariable("Temp", Temp)
        integrator.addGlobalVariable("kBT", kBT)
        integrator.addGlobalVariable("mke", get_kinetic_energy_average(Temp, N, C))
        integrator.addGlobalVariable("ke", 0.0 * kBT)
        if limit:
            integrator.addPerDofVariable("dx_lim", 0.0)
        integrator.addPerDofVariable("x1", 0.0)
        integrator.addUpdateContextState()
        integrator.addComputePerDof("v", "v+0.5*dt*f/m")
        if limit:
            integrator.addComputePerDof("dx_lim", "3*dt*sqrt(kBT/m)")
            integrator.addComputePerDof("x", "x+max(-dx_lim,min(dx_lim,dt*v))")
        else:
            integrator.addComputePerDof("x", "x+dt*v")
        integrator.addComputePerDof("x1", "x")
        integrator.addConstrainPositions()
        integrator.addComputePerDof("v", "v+0.5*dt*f/m+(x-x1)/dt")
        integrator.addConstrainVelocities()
        if rescale:
            integrator.addComputeSum("ke", "0.5*m*v*v")
            integrator.addComputePerDof(
                "v",
                "v*(step(0.01-abs(1.0-mke/ke))+(1-step(0.01-abs(1.0-mke/ke)))*sqrt(mke/ke))",
            )
    else:
        integrator = mm.VerletIntegrator(dt)
    return integrator


def global_integrator(
    dt: float,
    tau: float,
    seed: int,
    N: int,
    C: int,
    Temp: float,
    limit: bool = False,
    rescale: bool = False,
    remove: bool = False,
) -> mm.CustomIntegrator:
    """create and return a global thermostat integrator with spatial step limit and rescaling of the velocities.
    it also randomly removes com motion and angular momentum to avoid "Flying-Ice-Cube" phenomena.
    Parameters
    ----------
    dt      : time step in picosecond (float)
    tau     : inverse of the coupling frequency to the thermostat (float)
    seed    : seed for the integrator (int)
    N       : number of particles (int)
    C       : number of constraints (int)
    Temp    : temperature of the system (float)
    limit   : limit for the displacement ||x(t+dt)-x(t)|| (False or True) (bool)
    rescale : rescaling of the kinetic energy (False or True) (bool)
    remove  : does it remove com motion and angular momentum about com ? (bool)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    References
    ----------
    Giovanni Bussi and Michele Parrinello.
    Stochastics thermostats: comparison of local and global schemes.
    http://dx.doi.org/10.1016/j.cpc.2008.01.006
    """
    P = particles_per_bead.nparticles_per_bead
    dofs = 3 * N * P - 6 * int(remove) - C
    integrator = mm.CustomIntegrator(dt)
    integrator.setRandomNumberSeed(seed)
    integrator.addGlobalVariable("rlimit", int(limit))
    integrator.addGlobalVariable("rescale", int(rescale))
    integrator.addGlobalVariable("c_global", np.exp(-2.0 * dt / tau))
    integrator.addGlobalVariable("dofs_global", dofs)
    integrator.addGlobalVariable("kBT", get_kBT(Temp))
    integrator.addGlobalVariable("mke", 0.5 * dofs * get_kBT(Temp))
    integrator.addGlobalVariable("ke", 0.0)
    integrator.addGlobalVariable("KE", 0.0)
    integrator.addGlobalVariable("alpha", 0.0)
    integrator.addGlobalVariable("sign_alpha", 0)
    integrator.addPerDofVariable("x_global", 0)
    x_global = []
    for n in range(N * P - 1):
        x_global.append(mm.Vec3(1, 1, 1))
    x_global.append(mm.Vec3(1, 1, 0))
    integrator.setPerDofVariableByName("x_global", x_global)
    integrator.addGlobalVariable("R_global", 0.0)
    integrator.addGlobalVariable("S_global", 0.0)
    integrator.addPerDofVariable("x1", 0.0)
    integrator.addPerDofVariable("dx_lim_global", 0.0)
    integrator.addPerDofVariable("R2", 0.0)
    integrator.addGlobalVariable("sum_R2", 0.0)
    # remove com motion and angular momentum about com ?
    if remove:
        integrator.addGlobalVariable("giter", 0)
        integrator.addGlobalVariable("inv_3", 1.0 / 3.0)
        integrator.addGlobalVariable("inv_9", 1.0 / 9.0)
        for s in [
            "mtot",
            "inv_mtot",
            "correction",
            "det",
            "xcom",
            "ycom",
            "zcom",
            "vxcom",
            "vycom",
            "vzcom",
            "Iwx",
            "Iwy",
            "Iwz",
        ]:
            integrator.addGlobalVariable(s, 0.0)
        for s in ["am", "amx", "amy", "amz", "tx", "tv"]:
            integrator.addPerDofVariable(s, 0.0)
        for i in [0, 1, 2, 4, 5, 8]:
            integrator.addGlobalVariable("I3_" + str(i), 0.0)
        integrator.addComputeGlobal("giter", "giter+1")
        integrator.beginIfBlock("giter=100")
        integrator.addComputeGlobal("giter", "0")
        # kinetic energy before ...
        integrator.addComputeSum("ke", "0.5*m*v*v")
        # com and com motion
        integrator.addComputeSum("mtot", "m")
        integrator.addComputeGlobal("inv_mtot", "3/mtot")
        # store in vectors with the same values for x, y and z
        integrator.addComputeSum("xcom", "_x(x)*m*inv_mtot*inv_3")
        integrator.addComputeSum("ycom", "_y(x)*m*inv_mtot*inv_3")
        integrator.addComputeSum("zcom", "_z(x)*m*inv_mtot*inv_3")
        integrator.addComputeSum("vxcom", "_x(v)*m*inv_mtot*inv_3")
        integrator.addComputeSum("vycom", "_y(v)*m*inv_mtot*inv_3")
        integrator.addComputeSum("vzcom", "_z(v)*m*inv_mtot*inv_3")
        # 1-delta(m) to count only particle with mass>0
        integrator.addComputePerDof(
            "tx", "(1-delta(m))*vector(_x(x)-xcom,_y(x)-ycom,_z(x)-zcom)"
        )
        integrator.addComputePerDof(
            "tv", "(1-delta(m))*vector(_x(v)-vxcom,_y(v)-vycom,_z(v)-vzcom)"
        )
        # inertia (store in vectors with the same values for x, y and z)
        integrator.addComputeSum(
            "I3_0", "(dot(_y(tx),_y(tx))+dot(_z(tx),_z(tx)))*inv_9*(1-delta(m))"
        )
        integrator.addComputeSum("I3_1", "-dot(_y(tx),_x(tx))*inv_9*(1-delta(m))")
        integrator.addComputeSum("I3_2", "-dot(_z(tx),_x(tx))*inv_9*(1-delta(m))")
        integrator.addComputeSum(
            "I3_4", "(dot(_x(tx),_x(tx))+dot(_z(tx),_z(tx)))*inv_9*(1-delta(m))"
        )
        integrator.addComputeSum("I3_5", "-dot(_z(tx),_y(tx))*inv_9*(1-delta(m))")
        integrator.addComputeSum(
            "I3_8", "(dot(_x(tx),_x(tx))+dot(_y(tx),_y(tx)))*inv_9*(1-delta(m))"
        )
        # determinant
        integrator.addComputeGlobal(
            "det",
            "I3_0*I3_5*I3_5+I3_1*I3_1*I3_8-2*I3_1*I3_2*I3_5+I3_2*I3_2*I3_4-I3_0*I3_4*I3_8",
        )
        # w=inertia inverse times angular momentum
        integrator.beginIfBlock("abs(det)>0.000000001")
        integrator.addComputePerDof("am", "(1-delta(m))*cross(tx,tv)")
        integrator.addComputeSum(
            "Iwx",
            "(1-delta(m))*dot(vector(I3_5*I3_5-I3_4*I3_8,I3_1*I3_8-I3_2*I3_5,I3_2*I3_4-I3_1*I3_5),am)/det",
        )
        integrator.addComputeSum(
            "Iwy",
            "(1-delta(m))*dot(vector(I3_1*I3_8-I3_2*I3_5,I3_2*I3_2-I3_0*I3_8,I3_0*I3_5-I3_1*I3_2),am)/det",
        )
        integrator.addComputeSum(
            "Iwz",
            "(1-delta(m))*dot(vector(I3_2*I3_4-I3_1*I3_5,I3_0*I3_5-I3_1*I3_2,I3_1*I3_1-I3_0*I3_4),am)/det",
        )
        integrator.addComputePerDof("amx", "(1-delta(m))*Iwx*inv_3")
        integrator.addComputePerDof("amy", "(1-delta(m))*Iwy*inv_3")
        integrator.addComputePerDof("amz", "(1-delta(m))*Iwz*inv_3")
        # remove com motion
        integrator.addComputePerDof("v", "tv")
        # remove angular momentum with respect to the com
        integrator.addComputePerDof("v", "v-cross(vector(amx,amy,amz),tx)")
        # kinetic energy after ...
        integrator.addComputeSum("KE", "0.5*m*v*v")
        integrator.addComputePerDof("v", "v*sqrt(ke/KE)")
        integrator.endBlock()
        integrator.endBlock()
    # global thermostat coefficients
    integrator.addComputeSum("ke", "0.5*m*v*v")
    # "S_global" is approximate because we suppose "dofs_global" -> inf
    # "dofs_global" has to be > 10000 (at least) to use this approximation
    integrator.addComputeGlobal("R_global", "gaussian")
    integrator.beginIfBlock("dofs_global>10000")
    integrator.addComputeGlobal(
        "S_global", "sqrt(2*(dofs_global-1))*gaussian+(dofs_global-1)"
    )
    integrator.endBlock()
    integrator.beginIfBlock("dofs_global<=10000")
    # integrator.addComputePerDof("R2", "gaussian^2")
    # integrator.addComputeSum("sum_R2", "R2")
    # integrator.addComputeGlobal("S_global", "sum_R2-gaussian^2")
    integrator.addComputePerDof("R2", "x_global*gaussian^2")
    integrator.addComputeSum("sum_R2", "R2")
    integrator.addComputeGlobal("S_global", "sum_R2")
    integrator.endBlock()
    integrator.addComputeGlobal(
        "alpha",
        "sqrt(c_global+((1-c_global)*(S_global+R_global*R_global)*mke)/(dofs_global*ke)+2*R_global*sqrt(c_global*(1-c_global)*mke/(dofs_global*ke)))",
    )
    integrator.addComputeGlobal(
        "sign_alpha",
        "2*step(R_global+sqrt(c_global*dofs_global*ke/((1-c_global)*mke)))-1",
    )
    # momenta
    integrator.addComputePerDof("v", "sign_alpha*alpha*v")
    # velocity-Verlet
    integrator.addUpdateContextState()
    integrator.addComputePerDof("v", "v+0.5*dt*f/m")
    # limit the displacement ?
    integrator.beginIfBlock("rlimit=1")
    integrator.addComputePerDof("dx_lim_global", "dt*sqrt(kBT/m)")
    integrator.addComputePerDof("x", "x+max(-dx_lim_global,min(dx_lim_global,dt*v))")
    integrator.endBlock()
    integrator.beginIfBlock("rlimit=0")
    integrator.addComputePerDof("x", "x+dt*v")
    integrator.endBlock()
    # constraints
    integrator.addComputePerDof("x1", "x")
    integrator.addConstrainPositions()
    integrator.addComputePerDof("v", "v+0.5*dt*f/m+(x-x1)/dt")
    integrator.addConstrainVelocities()
    # rescale ?
    integrator.beginIfBlock("rescale=1")
    integrator.addComputeSum("ke", "0.5*m*v*v")
    integrator.addComputePerDof("v", "v*sqrt(mke/ke)")
    integrator.endBlock()
    return integrator


def remove_com_motion(context: mm.Context) -> np.ndarray:
    """Reset first and second momenta com motion.
    The global thermostat can lead to the "flying-ice-cube" phenomena.
    We have to cancel the first (com motion) and second (inertia) momenta to minimize the effect.
    Parameters
    ----------
    context : OpenMM context (mm.Context)
    Returns
    -------
    the velocities that have been corrected (list of mm.Vec3).
    Raises
    ------
    References
    ----------
    Stephen C. Harvey, Robert K.-Z. Tan, Thomas E. Cheatham III.
    The flying ice cube : Velocity rescaling in molecular dynamics leads to violation of energy equipartition.
    https://doi.org/10.1002/(SICI)1096-987X(199805)19:7%3C726::AID-JCC4%3E3.0.CO;2-S
    """
    com, vcom = get_com_from_context(context)
    masses = get_masses_from_context(context)
    # com to 0,0,0
    positions = np.subtract(get_vpositions_from_context(context, False), com)
    # com motion to 0,0,0
    velocities = np.subtract(get_velocities_from_context(context), vcom)
    # inertia (symmetric)
    I3 = np.zeros((3, 3), dtype=np.double)
    for i, p in enumerate(positions):
        I3[0, 0] += masses[i] * (p[1] ** 2 + p[2] ** 2)
        I3[1, 1] += masses[i] * (p[0] ** 2 + p[2] ** 2)
        I3[2, 2] += masses[i] * (p[0] ** 2 + p[1] ** 2)
        I3[0, 1] -= masses[i] * p[0] * p[1]
        I3[1, 2] -= masses[i] * p[1] * p[2]
        I3[0, 2] -= masses[i] * p[0] * p[2]
    I3[1, 0] = I3[0, 1]
    I3[2, 1] = I3[1, 2]
    I3[2, 0] = I3[0, 2]
    # determinant
    d0 = np.linalg.det(I3)
    if d0 != 0.0:
        # invert inertia matrix (symmetric)
        inv_I3 = np.linalg.inv(I3)
        # angular momentum L
        am = np.zeros(3, dtype=np.double)
        count = 0
        for p, v in zip(positions, velocities):
            am = np.add(am, np.multiply(masses[count], np.cross(p, v)))
            count += 1
        # w=I^-1*L
        omega = np.array(
            [
                np.dot(inv_I3[0, :3], am),
                np.dot(inv_I3[1, :3], am),
                np.dot(inv_I3[2, :3], am),
            ],
            dtype=np.double,
        )
        # new_v = v - cross(w,r-com)
        velocities = np.array(
            [np.subtract(v, np.cross(omega, p)) for p, v in zip(positions, velocities)]
        )
    return velocities


def get_com_from_context(context: mm.Context) -> tuple:
    """return com and com motion.
    Parameters
    ----------
    context : OpenMM context (mm.Context)
    Returns
    -------
    np.ndarray, np.ndarray
    Raises
    ------
    """
    positions = get_vpositions_from_context(context, False)
    velocities = get_velocities_from_context(context)
    masses = get_masses_from_context(context)
    mtot = np.sum(masses)
    # com and com motion
    com = np.zeros(3, dtype=np.double)
    vcom = np.zeros(3, dtype=np.double)
    for i, m in enumerate(masses):
        com = np.add(com, np.multiply(m, positions[i, :3]))
        vcom = np.add(vcom, np.multiply(m, velocities[i, :3]))
    com = np.multiply(1.0 / mtot, com)
    vcom = np.multiply(1.0 / mtot, vcom)
    return com, vcom


def get_info_system(context: mm.Context):
    """print the info about the system you created.
    Parameters
    ----------
    context : the OpenMM context
    Returns
    -------
    Raises
    ------
    """
    print(
        "system uses PBC: " + str(context.getSystem().usesPeriodicBoundaryConditions())
    )
    print(
        "default periodic box: "
        + str(context.getSystem().getDefaultPeriodicBoxVectors())
    )
    print(str(context.getSystem().getNumParticles()) + " particles")
    for f in range(context.getSystem().getNumForces()):
        print("force " + str(f))
        print(
            "PBC "
            + str(context.getSystem().getForce(f).usesPeriodicBoundaryConditions())
        )
        try:
            context.getSystem().getForce(f).updateParametersInContext(context)
        except:
            print("no updateParametersInContext() for the current force")
        try:
            print(
                "non bonded method "
                + str(context.getSystem().getForce(f).getNonbondedMethod())
                + "/"
                + str(mm.NonbondedForce.CutoffPeriodic)
            )
            print(
                "cutoff distance: "
                + str(context.getSystem().getForce(f).getCutoffDistance())
            )
            print(
                "use long range correction: "
                + str(context.getSystem().getForce(f).getUseLongRangeCorrection())
            )
            print(
                "use switching function: "
                + str(context.getSystem().getForce(f).getUseSwitchingFunction())
            )
            print(
                str(context.getSystem().getForce(f).getNumPerParticleParameters())
                + " per particle parameters"
            )
            print(
                str(context.getSystem().getForce(f).getNumGlobalParameters())
                + " global parameters"
            )
        except:
            error = True
    print(str(context.getSystem().getNumConstraints()) + " constraints")
    for n in context.getPlatform().getPropertyNames():
        print(n + ": " + str(context.getPlatform().getPropertyValue(context, n)))


def create_context(
    system: mm.System,
    integrator: mm.Integrator,
    platform: mm.Platform,
    properties,
    positions: list,
    velocities: list,
) -> mm.Context:
    """create and return an OpenMM context.
    Parameters
    ----------
    system     : OpenMM system
    integrator : OpenMM integrator
    platform   : OpenMM platform
    properties : OpenMM properties
    positions  : positions of the beads (list of OpenMM.Vec3)
    velocities : velocities of the beads (list of OpenMM.Vec3)
    Returns
    -------
    OpenMM context
    Raises
    ------
    """
    context = mm.Context(system, integrator, platform, properties)
    context.setPositions(positions)
    context.setVelocities(velocities)
    return context


def get_platforms(pname: str = "") -> tuple:
    """return the platform index and device index corresponding to the name of the hardware you would like to use.
    Write it to a json file 'get_platforms.json'.
    The name of the hardware is like 'GeForce_GTX_980_Ti_OpenCL' (OpenCL implementation).
    If the name of the hardware is like 'GeForce_GTX_980_GeForce_GTX_780_OpenCL' we ask for multiple devices.
    Do not forget to add '_OpenCL' or '_CUDA' to ask for the OpenCL implementation whatever it is CPU or GPU.
    Parameters
    ----------
    pname : name of the hardware (str)
    Returns
    -------
    index of the platform, index of the device (int,int).
    Raises
    ------
    exit if no platform and/or device (with name!='') have been found.
    """
    S = 16
    platform_and_device = {}
    for x in ["single", "mixed", "double"]:
        index_of_platform = -1
        index_of_device = -1
        # look for the OpenCLPlatformIndex and GPU
        Np = []
        for d in range(S):
            for p in range(S):
                integrator_test = mm.VerletIntegrator(0.001)
                system_test = mm.System()
                system_test.addParticle(1.0)
                platform_test = mm.Platform.getPlatformByName("OpenCL")
                try:
                    context_test = mm.Context(
                        system_test,
                        integrator_test,
                        platform_test,
                        {
                            "OpenCLPlatformIndex": str(p),
                            "DeviceIndex": str(d),
                            "Precision": x,
                        },
                    )
                    bcontext = True
                except:
                    bcontext = False
                if bcontext:
                    devices_names = context_test.getPlatform().getPropertyValue(
                        context_test, "DeviceName"
                    )
                    if (
                        "GeForce" in devices_names
                        or "Tesla" in devices_names
                        or "CPU" in devices_names
                    ):
                        if p not in Np:
                            Np.append(p)
                    del context_test
                del integrator_test
                del system_test
                del platform_test
        # loop over implementations
        if "CPU" in pname and not "CUDA" in pname and not "OpenCL" in pname:
            bplatform = ["CPU"]
        elif "CUDA" in pname:
            bplatform = ["CUDA"]
        elif "OpenCL" in pname:
            bplatform = ["OpenCL"]
        else:
            bplatform = []
            for c in ["CPU", "CUDA", "OpenCL"]:
                try:
                    mm.Platform.getPlatformByName(c)
                    bplatform.append(c)
                except:
                    print("'get_platforms' function did not find " + c + " platform.")
                    pass
        for c in bplatform:
            if not c in platform_and_device.keys():
                platform_and_device[c] = {}
            if not x in platform_and_device[c].keys():
                platform_and_device[c][x] = {}
            # loop over platforms
            if c == "OpenCL":
                Np_imp = Np
            else:
                Np_imp = range(S)
            for p in Np_imp:
                # loop over devices
                for d in range(S - (S - 1) * int(pname.count("GeForce") > 1)):
                    integrator_test = mm.VerletIntegrator(0.001)
                    system_test = mm.System()
                    system_test.addParticle(1.0)
                    platform_test = mm.Platform.getPlatformByName(c)
                    bcontext = True
                    if c == "CPU":
                        context_test = mm.Context(
                            system_test,
                            integrator_test,
                            platform_test,
                            {"Threads": "1"},
                        )
                    elif c == "CUDA":
                        try:
                            context_test = mm.Context(
                                system_test,
                                integrator_test,
                                platform_test,
                                {"DeviceIndex": str(d), "CudaPrecision": x},
                            )
                        except:
                            bcontext = False
                    elif c == "OpenCL":
                        if pname.count("GeForce") > 1 and d < 2:
                            bDeviceIndex = ["0,1", "0,1,2", "0,1,2,3"][
                                int(pname.count("GeForce")) - 2
                            ]
                            try:
                                context_test = mm.Context(
                                    system_test,
                                    integrator_test,
                                    platform_test,
                                    {
                                        "OpenCLPlatformIndex": str(p),
                                        "DeviceIndex": bDeviceIndex,
                                        "Precision": x,
                                    },
                                )
                                if "Experimental" in context_test.getPlatform().getPropertyValue(
                                    context_test, "OpenCLPlatformName"
                                ):
                                    bcontext = False
                            except:
                                bcontext = False
                        else:
                            try:
                                context_test = mm.Context(
                                    system_test,
                                    integrator_test,
                                    platform_test,
                                    {
                                        "OpenCLPlatformIndex": str(p),
                                        "DeviceIndex": str(d),
                                        "Precision": x,
                                    },
                                )
                                if "Experimental" in context_test.getPlatform().getPropertyValue(
                                    context_test, "OpenCLPlatformName"
                                ):
                                    bcontext = False
                            except:
                                bcontext = False
                    if bcontext:
                        if (
                            "DeviceName"
                            in context_test.getPlatform().getPropertyNames()
                        ):
                            current_DeviceName = (
                                context_test.getPlatform().getPropertyValue(
                                    context_test, "DeviceName"
                                )
                            ).replace(" ", "_")
                            if pname.count("GeForce") > 1:
                                current_DeviceName = current_DeviceName.replace(
                                    ",", "_"
                                )
                            # add info to dict
                            # if c=='OpenCL':
                            #     print(c,p,d,context_test.getPlatform().getPropertyValue(context_test,"DeviceName"),context_test.getPlatform().getPropertyValue(context_test,"OpenCLPlatformName"),context_test.getPlatform().getPropertyValue(context_test,"OpenCLPlatformIndex"))
                            if (
                                not current_DeviceName
                                in platform_and_device[c][x].keys()
                            ):
                                platform_and_device[c][x][current_DeviceName] = {}
                            for n in context_test.getPlatform().getPropertyNames():
                                platform_and_device[c][x][current_DeviceName][
                                    n
                                ] = context_test.getPlatform().getPropertyValue(
                                    context_test, n
                                )
                            # if the current device is what we ask for ...
                            if (current_DeviceName + "_" + c) == pname or (
                                current_DeviceName + "_" + c
                            ).replace("NVIDIA_", "") == pname:
                                index_of_platform = p
                                index_of_device = d
                        del context_test
                    del integrator_test
                    del system_test
                    del platform_test
    with open("get_platforms.json", "w") as out_file:
        json.dump(platform_and_device, out_file, indent=1)
    if (index_of_platform == -1 or index_of_device == -1) and pname != "":
        print("No platform has been found: " + pname)
        sys.exit()
    return index_of_platform, index_of_device


def create_platform(platform_name: str = "CPU2", precision: str = "double"):
    """create and return platform and properties
    Parameters
    ----------
    platform_name : 'CPU2' to run on CPU with two threads, the number of threads is needed
                    'GeForce_GTX_780_OpenCL' to run on GeForce GTX 780 with OpenCL implementation
                    'Intel(R)_Xeon(R)_CPU_E5-2637_v4_@_3.50GHz_OpenCL' to run on this specific CPU with OpenCL implementation
                    to use two GPUs with OpenCL implementation: 'GeForce_RTX_2080_GeForce_GTX_1080_OpenCL' (str)
    precision     : single, mixed or double (str)
    Returns
    -------
    OpenMM platform, OpenMM properties
    Raises
    ------
    if 'platform_name' is 'CPU' exit
    """
    if platform_name == "CPU":
        print("Number of threads is not specified, exit.")
        sys.exit()
        # platform = mm.Platform.getPlatformByName("CPU")
        # properties = {}
    elif (
        "CPU" in platform_name
        and not "OpenCL" in platform_name
        and not "CUDA" in platform_name
    ):
        platform = mm.Platform.getPlatformByName("CPU")
        properties = {"Threads": platform_name.replace("CPU", "")}
        properties = {"Threads": os.environ["OPENMM_CPU_THREADS"]}
        # properties = {}
        # os.environ["OPENMM_CPU_THREADS"] = platform_name.replace("CPU", "")
    elif "CUDA" in platform_name:
        platform = mm.Platform.getPlatformByName("CUDA")
        platform_index, device_index = get_platforms(platform_name)
        # precision
        if precision == "double":
            if platform.supportsDoublePrecision():
                properties = {"DeviceIndex": str(device_index), "Precision": "double"}
            else:
                properties = {"DeviceIndex": str(device_index), "Precision": "single"}
        else:
            properties = {"DeviceIndex": str(device_index), "CudaPrecision": precision}
    elif "OpenCL" in platform_name:
        # get OpenCL platform
        platform = mm.Platform.getPlatformByName("OpenCL")
        platform_index, device_index = get_platforms(platform_name)
        # multiple device ?
        if platform_name.count("GeForce") > 1:
            str_device_index = ["0,1", "0,1,2", "0,1,2,3"][
                int(platform_name.count("GeForce")) - 2
            ]
        else:
            str_device_index = str(device_index)
        # precision
        if precision == "double":
            if platform.supportsDoublePrecision():
                properties = {
                    "OpenCLPlatformIndex": str(platform_index),
                    "DeviceIndex": str_device_index,
                    "Precision": "double",
                }
            else:
                properties = {
                    "OpenCLPlatformIndex": str(platform_index),
                    "DeviceIndex": str_device_index,
                    "Precision": "single",
                }
        else:
            properties = {
                "OpenCLPlatformIndex": str(platform_index),
                "DeviceIndex": str_device_index,
                "Precision": precision,
            }
    return platform, properties


def create_system(L: float, N: int, mass: float, F: int = 100) -> tuple:
    """create and return a system as-well-as a list of masses.
    Parameters
    ----------
    L    : box size in nanometer (float)
    N    : number of beads (int)
    mass : mass of the beads (float)
    F    : remove the com motion every F steps (int)
    Returns
    -------
    OpenMM system, list of float
    Raises
    ------
    """
    masses = []
    system = mm.System()
    system.setDefaultPeriodicBoxVectors(
        mm.Vec3(L, 0.0, 0.0), mm.Vec3(0.0, L, 0.0), mm.Vec3(0.0, 0.0, L)
    )
    # beads + pseudo-beads
    xN = particles_per_bead.nparticles_per_bead * N
    masses = [mass] * xN
    for i in range(xN):
        # masses.append(mass if i < N else 0.5 * mass)
        system.addParticle(masses[i])
    # remove the COM motion every F steps
    if F > -1:
        system.addForce(mm.CMMotionRemover(F))
    return system, masses


def read_write_checkpoint(context: mm.Context, name: str, mode: str):
    """read/write from/a OpenMM checkpoint file.
    Parameters
    ----------
    context : OpenMM context
    name    : name of the checkpoint file (str)
    mode    : read or write (str)
    Raises
    ------
    if no checkpoint file has been found in read mode.
    if mode is not equal to read or to write.
    """
    if mode == "read":
        try:
            with open(name, "rb") as in_file:
                context.loadCheckpoint(in_file.read())
        except IOError:
            print("no checkpoint file found : " + name)
    elif mode == "write":
        with open(name, "wb") as out_file:
            out_file.write(context.createCheckpoint())
    else:
        print("Please consider using read or write option.")
        sys.exit()


def get_bonds(context: mm.Context) -> int:
    """return the number of bonds in the context.getSystem().
    Parameters
    ----------
    context : OpenMM context (OpenMM.Context)
    Returns
    -------
    the number of bonds in the context.getSystem() (int).
    Raises
    ------
    """
    F = context.getSystem().getNumForces()
    n_bonds = 0
    for f in range(F):
        try:
            n_bonds += context.getSystem().getForce(f).getNumBonds()
        except:
            pass
    return n_bonds


def get_number_of_chains(context: mm.Context) -> int:
    """return the number of chains in the context.getSystem().
    it assumes the number of beads per chain to be the same.
    Parameters
    ----------
    context : OpenMM context (OpenMM.Context)
    Returns
    -------
    the number of bonds in the context.getSystem() (int).
    Raises
    ------
    """
    # C chains, N particles per chain, C*(N-1) bonds
    # return context.getSystem().getNumParticles()-get_bonds(context)
    return 1


@numba.jit
def get_kinetic_energy_average(Temp: float, N: int, C: int = 0) -> float:
    # x * N particles per bead
    return (3 * N - C) * 0.5 * get_kBT(Temp)


@numba.jit
def get_square_kinetic_energy_average(Temp: float, N: int, C: int = 0) -> float:
    # x * N particles per bead
    X = 3 * N - C
    return (X / 2) * (X / 2 + 1) * get_kBT(Temp) ** 2


@numba.jit(int32(int32, int32))
def get_pseudo_u_from_i(i: int, N: int) -> int:
    return i + N


@numba.jit(int32(int32, int32))
def get_pseudo_U_from_i(i: int, N: int) -> int:
    return i + 2 * N
