#!/bin/bash
seed=1
thermostat="langevin"
bond="fene"
overtwist=0.0
force=0.0
torque=0.0
resolution=10.5
rerun=0
IC="circular"
pair_style="wca"
platform="CPU2"
linear="no"
start=0
iterations=1000000
every=100000
N=5714
bp_per_nm3=0.0001
precision="single"
replica_exchange="no"
replica_overtwist=1
replica_bending=1
replica_twist=10
if [[ $(uname -n) == "c6420node1" || $(uname -n) == "c6420node2" ]];
then
    replica_twist=32
fi;
if [[ $(uname -n) == "epyc1" ]];
then
    replica_twist=64
fi;
if [[ $(uname -n) == "epyc2" || $(uname -n) == "epyc3" ]];
then
    replica_twist=128
fi;
bdask="no"
# path to write/read data
tmpdir=${PWD}
# path to openmm
path_to_openmm=/scratch/pcarriva/anaconda3/bin
while getopts hs:t:b:o:F:T:r:R:i:g:l:N:P:S:I:e:E:w:a:B:D: option
do
    case "${option}"
    in
	s) seed=${OPTARG};;
	t) thermostat=${OPTARG};;
	b) bond=${OPTARG};;
	o) overtwist=${OPTARG};;
	F) force=${OPTARG};;
	T) torque=${OPTARG};;
	r) resolution=${OPTARG};;
	R) rerun=${OPTARG};;
	i) IC=${OPTARG};;
	g) platform=${OPTARG};;
	l) linear=${OPTARG};;
	N) N=${OPTARG};;
	P) precision=${OPTARG};;
	S) start=${OPTARG};;
	I) iterations=${OPTARG};;
	e) every=${OPTARG};;
	E) replica_exchange=${OPTARG};;
	w) tmpdir=${OPTARG};;
	a) path_to_openmm=${OPTARG};;
	B) save_for_blender=${OPTARG};;
	D) bdask=${OPTARG};;
	h) echo "Usage :"
	   echo "The following lines describe how to use run_model.sh, what are the options, example values."
	   echo "The purpose of this bash script is just to create folders according to the simulation parameters."
	   echo "If overtwist is specified, run_model.sh launch the 'openmm_plectoneme.py' script."
	   echo "If force and torque are specified, run_model.sh launch the 'single_molecule.py' script."
	   echo "./run_model.sh -s <seed>"
	   echo "               -t <thermostat=gjf,global,langevin,nve>"
	   echo "               -b <bond=rigid,fene>"
	   echo "               -o <overtwist=-0.03>"
	   echo "               -F <force in piconewton=1.0>"
	   echo "               -T <torque in kBT=1.0>"
	   echo "               -r <resolution=10.5bp>"
	   echo "               -R <rerun=0>"
	   echo "               -i <initial conformation=circular, linear, helix_in or helix_out>"
	   echo "               -g <platform and implementation=CPU1,GeForce_RTX_2080_Ti_OpenCL>"
	   echo "               -l <linear or ring polymer=yes,no>"
	   echo "               -N <number of bonds=476,5714>"
	   echo "               -P <precision=single,mixed,double>"
	   echo "               -S <restart>"
	   echo "               -I <number of iterations>"
	   echo "               -e <save every this many steps>"
	   echo "               -E <replica exchange=no,yes>"
	   echo "               -w <path to write/read data=/scratch>"
	   echo "               -a <path to openmm=/home/anaconda3/bin>"
	   echo "               -B <save for blender visualisation no,yes>"
	   echo "               -D <compute replica exchange with Dask no,yes>"
	   echo "example 1:"
	   echo "run a FENE model at 10.5 bp resolution, 5714 monomers, circular molecule (helix with 30 turns), global thermostat, OpenCL platform, single precision, save conformation every 1000000 steps (101 conformations saved)."
	   echo "./run_model.sh -s 1 -t global -b fene -o 0.0 -r 10.5 -R 0 -i helix_in30 -g GeForce_RTX_2060_SUPER_OpenCL -l no -N 5714 -P single -S 0 -E no -B yes -I 1000000 -e 10000"
	   echo "example 2:"
	   echo "run a FENE model (1000000 iterations) at 10.5 bp resolution, 1000 monomers, linear molecule, langevin thermostat, OpenCL platform, double precision, save conformation every 100000 steps (11 conformations saved)."
	   echo "./run_model.sh -s 1 -t langevin -b fene -o 0.0 -F 0.3 -T 0.4 -r 10.5 -R 0 -g GeForce_GTX_980_OpenCL -l yes -N 1000 -P double -S 0 -E no -B no -I 1000000 -e 100000"
	   echo "example 3:"
	   echo "run a FENE model at 10.5 bp resolution, 5714 monomers, circular molecule, global thermostat, OpenCL platform, single precision, save conformation every 1000000 steps (101 conformations saved) and replica exchange."
	   echo "./run_model.sh -s 1 -t global -b fene -o 0.0 -r 10.5 -R 0 -i circular -g GeForce_RTX_2060_SUPER_OpenCL -l no -N 4096 -P single -S 0 -E yes -B yes -I 100000 -e 1000 -D yes"
	   exit 0
	   ;;
    esac
done

if [[ $platform == *"CPU"* ]];
then
    precision="double"
fi;

if [[ ${linear} == "yes" ]];
then
    polymer="linear";
else
    polymer="ring";
fi;
mkdir ${tmpdir}/${polymer};

# ???
if [[ ${torque} == 0.0 ]];
then
    if [[ ${replica_exchange} == "yes" ]];
    then
	mkdir ${polymer}/replica_exchange;
	polymer=${polymer}/replica_exchange;
    fi;
    mkdir ${tmpdir}/${polymer}/${pair_style};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_${overtwist};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_${overtwist}/seed${seed};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_${overtwist}/seed${seed}/${IC};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_${overtwist}/seed${seed}/${IC}/rerun${rerun};
    path_to=${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_${overtwist}/seed${seed}/${IC}/rerun${rerun};

    if [[ ${replica_exchange} == "yes" ]];
    then
	for ((r=0;r<$((${replica_bending}*${replica_twist}));r=$(($r+1))));
	do
	    mkdir ${path_to}/replica${r};
	    if [[ ${start} -eq 0 ]];
	    then
		rm -rf ${path_to}/replica${r}/out;
		mkdir ${path_to}/replica${r}/out;
		rm -rf ${path_to}/replica${r}/restart;
		mkdir ${path_to}/replica${r}/restart;
		rm -rf ${path_to}/replica${r}/visualisation_with_blender;
		mkdir ${path_to}/replica${r}/visualisation_with_blender;
	    fi;
	done;
	echo "running the model with replica exchange and OpenMM";
	echo "${path_to_openmm}/python3.7 openmm_plectoneme_replica_exchange.py -o $overtwist -n $N -r $resolution -s $seed -R $rerun -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat -c $pair_style --replica_bending=$replica_bending --replica_twist=$replica_twist -l $linear --start=$start --iterations=${iterations} --every=${every} --precision=$precision --write_data=$path_to";
	if [[ ${bdask} == "no" ]];
	then
	    ${path_to_openmm}/python3.7 openmm_plectoneme_replica_exchange.py -o $overtwist -n $N -r $resolution -s $seed -R $rerun -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat --replica_bending=$replica_bending --replica_twist=$replica_twist -l $linear --start=$start --iterations=${iterations} --every=${every} --precision=$precision --write_data=$path_to;
	else
	    ${path_to_openmm}/python3.7 openmm_plectoneme_replica_exchange.py -o $overtwist -n $N -r $resolution -s $seed -R $rerun -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat --replica_bending=$replica_bending --replica_twist=$replica_twist -l $linear --start=$start --iterations=${iterations} --every=${every} --precision=$precision --dask --scheduler="local" --write_data=$path_to;
	    # ${path_to_openmm}/python3.7 openmm_plectoneme_replica_exchange.py -o $overtwist -n $N -r $resolution -s $seed -R $rerun -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat --replica_bending=$replica_bending --replica_twist=$replica_twist -l $linear --start=$start --iterations=${iterations} --every=${every} --precision=$precision --dask --scheduler="SGE" --queue="monointeldeb128" --write_data=$path_to;
	    # ${path_to_openmm}/python3.7 openmm_plectoneme_replica_exchange.py -o $overtwist -n $N -r $resolution -s $seed -R $rerun -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat --replica_bending=$replica_bending --replica_twist=$replica_twist -l $linear --start=$start --iterations=${iterations} --every=${every} --precision=$precision --dask --scheduler="SGE" --queue="r720gpuGTX980" --write_data=$path_to;
	    # ${path_to_openmm}/python3.7 openmm_plectoneme_replica_exchange.py -o $overtwist -n $N -r $resolution -s $seed -R $rerun -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat --replica_bending=$replica_bending --replica_twist=$replica_twist -l $linear --start=$start --iterations=${iterations} --every=${every} --precision=$precision --dask --scheduler="PBS" --queue="mc_gpu_long" --write_data=$path_to;
	fi;
    else
	if [[ ${start} -eq 0 ]];
	then
	    rm -rf ${path_to}/out;
	    mkdir ${path_to}/out;
	    rm -rf ${path_to}/restart;
	    mkdir ${path_to}/restart;
	    rm -rf ${path_to}/visualisation_with_blender;
	    mkdir ${path_to}/visualisation_with_blender;
	fi;
	echo "running the model with OpenMM";
	echo "${path_to_openmm}/python3.7 openmm_plectoneme.py -o $overtwist -n $N -r $resolution -s $seed -R $rerun -i $IC -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat -c $pair_style -l $linear --start=$start --iterations=${iterations} --every=${every} --precision=$precision --write_data=$path_to";
	if [[ ${save_for_blender} == "yes" ]];
	then
	    ${path_to_openmm}/python3.7 openmm_plectoneme.py -o $overtwist -n $N -r $resolution -s $seed -R $rerun -i $IC -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat -l $linear --start=$start --iterations=${iterations} --every=${every} --precision=$precision --write_data=$path_to --save_for_blender
	else
	    ${path_to_openmm}/python3.7 openmm_plectoneme.py -o $overtwist -n $N -r $resolution -s $seed -R $rerun -i $IC -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat -l $linear --start=$start --iterations=${iterations} --every=${every} --precision=$precision --write_data=$path_to
	fi;
    fi;
fi;

# single-molecule
if [[ ${torque} != 0.0 ]];
then
    replica_exchange="no"
    if [[ ${replica_exchange} == "yes" ]];
    then
	mkdir ${polymer}/replica_exchange;
	polymer=${polymer}/replica_exchange;
    fi;
    mkdir ${tmpdir}/${polymer};
    mkdir ${tmpdir}/${polymer}/${pair_style};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_F${force}_T${torque};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_F${force}_T${torque}/seed${seed};
    mkdir ${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_F${force}_T${torque}/seed${seed}/rerun${rerun};
    path_to=${tmpdir}/${polymer}/${pair_style}/${thermostat}/${platform}/${precision}/run_${resolution}bp_C1_N${N}_F${force}_T${torque}/seed${seed}/rerun${rerun};

    if [[ ${replica_exchange} == "yes" ]];
    then
	for ((r=0;r<$((${replica_bending}*${replica_twist}));r=$(($r+1))));
	do
	    mkdir ${path_to}/replica${r};
	    if [[ ${start} -eq 0 ]];
	    then
		rm -rf ${path_to}/replica${r}/out;
		mkdir ${path_to}/replica${r}/out;
		rm -rf ${path_to}/replica${r}/restart;
		mkdir ${path_to}/replica${r}/restart;
		rm -rf ${path_to}/replica${r}/visualisation_with_blender;
		mkdir ${path_to}/replica${r}/visualisation_with_blender;
	    fi;
	done;
	echo "running the model with replica exchange and OpenMM";
	echo "(${path_to_openmm}/python3.7 single_molecule_replica_exchange.py -F $force -T $torque -n $N -r $resolution -s $seed -R $rerun -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat -c $pair_style --replica_bending=$replica_bending --replica_twist=$replica_twist -l $linear --start=$start --iterations=${iterations} --every=${every} --precision=$precision --write_data=$path_to)";
	${path_to_openmm}/python3.7 single_molecule_replica_exchange.py -F $force -T $torque -n $N -r $resolution -s $seed -R $rerun -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat --replica_bending=$replica_bending --replica_twist=$replica_twist -l $linear --start=$start --iterations=${iterations} --every=${every} --precision=$precision --write_data=$path_to
    else
	if [[ ${start} -eq 0 ]];
	then
	    rm -rf ${path_to}/out;
	    mkdir ${path_to}/out;
	    rm -rf ${path_to}/restart;
	    mkdir ${path_to}/restart;
	    rm -rf ${path_to}/visualisation_with_blender;
	    mkdir ${path_to}/visualisation_with_blender;
	fi;
	echo "running the model with OpenMM";
	echo "(${path_to_openmm}/python3.7 single_molecule.py -F $force -T $torque -n $N -r $resolution -s $seed -R $rerun -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat -c $pair_style -l $linear --start=$start --iterations=${iterations} --every=${every} --precision=$precision --write_data=$path_to)";
	${path_to_openmm}/python3.7 single_molecule.py -F $force -T $torque -n $N -r $resolution -s $seed -R $rerun -g $platform --bp_per_nm3=$bp_per_nm3 -t $thermostat -l $linear --start=$start --iterations=${iterations} --every=${every} --precision=$precision --write_data=$path_to
    fi;
fi;
