#!/bin/bash
tmpdir=${PWD};
path_to_python="/scratch/pcarriva/anaconda3/bin/python3.7";
path_to_pydoc="/scratch/pcarriva/anaconda3/bin/pydoc3.7";
what_to_do="all";

while getopts ht:p:d:w: option
do
    case "${option}"
    in
	t) tmpdir=${OPTARG};;
	p) path_to_python=${OPTARG};;
	d) path_to_pydoc=${OPTARG};;
	w) what_to_do=${OPTARG};;
	h) echo "Usage :"
	   echo "      ./run_test.sh -t <path to 'openmm_plectoneme_functions.py'>"
	   echo "                    -p <path to python=/home/anaconda3/bin/python3.7>"
	   echo "                    -d <path to pydoc=/home/anaconda3/bin/pydoc3.7>"
	   exit 0
	   ;;
    esac
done

# folder test
if [[ !(-d ${tmpdir}/test) ]];
then
    mkdir ${tmpdir}/test;
fi;
echo "make html documentation";
${path_to_pydoc} -w ${tmpdir}/data_analysis.py;
${path_to_pydoc} -w ${tmpdir}/openmm_plectoneme_functions.py;
if [[ $what_to_do == "all" || $what_to_do == "analysis" ]];
then
    echo "run the test (data_analysis)";
    ${path_to_python} ${tmpdir}/unittest_data_analysis.py;
fi;
if [[ $what_to_do == "all" || $what_to_do == "plectoneme" ]];
then
    echo "run the test (openmm_plectoneme_functions)";
    ${path_to_python} ${tmpdir}/unittest_openmm_plectoneme_functions.py;
fi;
