#!/usr/bin/python
# -*- coding: utf-8 -*-
"""@package docstring
The module 'openmm_plectoneme_functions.py' is a collection of functions used to define
a Kremer-Grest polymer with bending and twisting rigidities.
This module goes with a python script 'openmm_plectoneme.py' that is an example on how to
create and run your model.
The following code (has been developped by Pascal Carrivain) is distributed under MIT licence.
"""
import os
try:
    from jobqueue_features.clusters import CustomSLURMCluster
    from jobqueue_features.decorators import on_cluster, mpi_task
    from jobqueue_features.mpi_wrapper import mpi_wrap
    from jobqueue_features.functions import set_default_cluster
except ImportError:
    print("no jobqueue_features")
import math
import sys
import random
import json
import time
import simtk.openmm as mm
import simtk.unit as unit
from mpl_toolkits import mplot3d
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import numba
from numba import prange, int32

def get_kBT(Temp: unit.Quantity) -> unit.Quantity:
    return unit.BOLTZMANN_CONSTANT_kB*unit.AVOGADRO_CONSTANT_NA*Temp
    # return unit.BOLTZMANN_CONSTANT_kB*Temp

@numba.jit
def from_i_to_bead(i: int,N: int) -> int:
    """return the index 'i' projected to the ring chain.
    Parameters
    ----------
    i : index (int)
    N : the number of beads in the ring chain (int)
    Returns
    -------
    return the index 'i' (int) projected onto the ring chain.
    Raises
    ------
    """
    if i<0:
        return N-(-i)%N
    elif i>=N:
        return i%N
    else:
        return i

def print_variables(context: mm.Context,linear: bool,iterations: int=0):
    """print variables from the context.
    Parameters
    ----------
    context    : OpenMM context
    linear     : linear (True) or ring polymer (False)
    iterations : current iteration of the context
    Returns
    -------
    Raises
    ------
    """
    C=context.getSystem().getNumConstraints()
    N=2*context.getSystem().getNumParticles()/get_particles_per_bead()
    C=context.getSystem().getNumConstraints()
    positions=context.getState(getPositions=True).getPositions()
    Temp=context.getIntegrator().getTemperature()
    print("----- iterations: "+str(iterations))
    print("<bond>="+str(average_bond_size(positions,linear)))
    print("kinetic energy="+str((context.getState(getEnergy=True).getKineticEnergy())/((3*N-C)*(0.5*get_kBT(Temp)))))
    print("diff(wca,epsilon_wca_virtual_sites)="+str((context.getState(getParameterDerivatives=True).getEnergyParameterDerivatives())["epsilon_wca_virtual_sites"]))
    print("diff(virtual_harmonic,K_harmonic)="+str((context.getState(getParameterDerivatives=True).getEnergyParameterDerivatives())["K_harmonic"]))
    print("diff(twist,Kt)="+str((context.getState(getParameterDerivatives=True).getEnergyParameterDerivatives())["Kt"]))
    print("diff(bending,Kb_tt)="+str((context.getState(getParameterDerivatives=True).getEnergyParameterDerivatives())["Kb_tt"]))
    print("diff(pseudo bending,Kb_ut)="+str((context.getState(getParameterDerivatives=True).getEnergyParameterDerivatives())["Kb_ut"]))
    print("<pseudo-u bead distance>=",get_bead_to_pseudo_bead_distance(positions))
    print_correlations_uvt(positions,linear)
    
def get_positions_from_context(context: mm.Context,PBC: bool=False) -> list:
    """return the positions (in nanometer) from the OpenMM context (do not consider virtual sites).
    Parameters                                       
    ----------
    context : OpenMM context
    PBC     : enforce periodic box ? (bool)
    Returns
    -------
    A list of the positions of the particles (from the context) in nano-meter.
    Raises
    ------
    """
    pbuffer=context.getState(getPositions=True,enforcePeriodicBox=PBC).getPositions()
    N=len(pbuffer)//get_particles_per_bead()
    pbuffer=pbuffer[:N]
    return [(p[0]/unit.nanometer,p[1]/unit.nanometer,p[2]/unit.nanometer) for p in pbuffer]

def get_vpositions_from_context(context: mm.Context,PBC: bool=False) -> list:
    """return the positions (in nanometer) from the OpenMM context.
    Parameters                                       
    ----------
    context : OpenMM context
    PBC     : enforce periodic box ? (bool)
    Returns
    -------
    A list of the positions of the particles (from the context) in nano-meter.
    Raises
    ------
    """
    pbuffer=context.getState(getPositions=True,enforcePeriodicBox=PBC).getPositions()
    return [(p[0]/unit.nanometer,p[1]/unit.nanometer,p[2]/unit.nanometer) for p in pbuffer]

def get_xyz_from_context(context: mm.Context,PBC: bool=False) -> list:
    """return the xyz (in nanometer) from the OpenMM context (do not consider virtual sites).
    Parameters                                       
    ----------
    context : OpenMM context
    PBC     : enforce periodic box ? (bool)
    Returns
    -------
    A tuple xyz of the particles (from the context) in nano-meter.
    Raises
    ------
    """
    pbuffer=context.getState(getPositions=True,enforcePeriodicBox=PBC).getPositions()
    N=len(pbuffer)//get_particles_per_bead()
    pbuffer=pbuffer[:N]
    return [p[0]/unit.nanometer for p in pbuffer],[p[1]/unit.nanometer for p in pbuffer],[p[2]/unit.nanometer for p in pbuffer]

def puvt_chain(N: int,sigma: unit.Quantity,L: unit.Quantity,linear: bool) -> tuple:
    """return the positions of the N+1 beads inside the chain (list of OpenMM.Vec3).
    if the chain is linear, the positions are like a ring minus one bond.
    for a ring of N bonds you have N beads while for a linear polymer of N bonds you have N+1 beads.
    Parameters
    ----------
    N      : number of bonds (int)
    sigma  : bond size (OpenMM.Quantity)
    L      : size of the box (OpenMM.Quantity)
    linear : linear (True) or ring (False) (bool)
    Returns
    -------
    The positions of the (N+1 = linear or N = ring) beads inside the chain (list of OpenMM.Vec3).
    The (N+1 = linear or N = ring) frames u,v,t. In the linear case the function return the closure condition frame too.
    Raises
    ------
    """
    B=N+int(linear)
    angle=2.0*math.pi/float(B+int(linear))
    R=0.5*sigma/math.sin(0.5*angle)
    # positions
    p=[mm.Vec3(0.5*L+R*math.cos((0.5+i)*angle),0.5*L+R*math.sin((0.5+i)*angle),0.5*L) for i in range(B)]
    # tangents
    t=[normalize(mm.Vec3(p[from_i_to_bead(i+1,B)][0]-p[i][0],p[from_i_to_bead(i+1,B)][1]-p[i][1],p[from_i_to_bead(i+1,B)][2]-p[i][2])) for i in range(B)]
    # normals
    u=[mm.Vec3(0.0,0.0,1.0)]*B
    # v=cross(t,u)
    v=[normalize(a_cross_b(t[i],u[i])) for i in range(B)]
    return p,u,v,t

def puvt_single_molecule(N: int,sigma: unit.Quantity) -> tuple:
    """return the positions of the N+1 beads inside the chain (list of OpenMM.Vec3).
    Parameters
    ----------
    N      : number of bonds (int)
    sigma  : bond size (OpenMM.Quantity)
    Returns
    -------
    The positions of the N+1 beads inside the linear chain (list of OpenMM.Vec3).
    The N+1 frames u,v,t. In the linear case the function return the closure condition frame too.
    Raises
    ------
    """
    B=N+1
    # positions
    p=[mm.Vec3(0.5*B*sigma,0.5*B*sigma,i*sigma) for i in range(B)]
    # tangents
    t=[normalize(mm.Vec3(p[from_i_to_bead(i+1,B)][0]-p[i][0],p[from_i_to_bead(i+1,B)][1]-p[i][1],p[from_i_to_bead(i+1,B)][2]-p[i][2])) for i in range(B)]
    # normals
    u=[mm.Vec3(1.0,0.0,0.0)]*B
    # v=cross(t,u)
    v=[normalize(a_cross_b(t[i],u[i])) for i in range(B)]
    return p,u,v,t

def create_virtual_sites(positions: list,u: list,distance: unit.Quantity):
    """return the virtual sites as a list of OpenMM virtual sites.
    Parameters
    ----------
    positions : the positions of the N beads (list of OpenMM.Vec3).
    u         : u of the attached frame (list of OpenMM.Vec3).
    distance  : the distance between particle and virtual site (OpenMM.Quantity).
    Returns
    -------
    The virtual sites as a list of OpenMM virtual sites.
    The positions of all the particles in the system (list of OpenMM.Vec3).
    Raises
    ------
    """
    B=len(positions)
    virtual_sites=[]# size of 2*B at the end
    new_positions=[mm.Vec3(0.0,0.0,0.0)]*(4*B)
    weight1=1.0
    weight2=1.0-weight1
    for i in range(B):
        pi=positions[i]
        new_positions[i]=pi
        ip1=from_i_to_bead(i+1,B)
        # virtual site attached to each bead
        virtual_sites.append(mm.TwoParticleAverageSite(i,ip1,weight1,weight2))
        mx=weight1*pi[0]+weight2*positions[ip1][0]
        my=weight1*pi[1]+weight2*positions[ip1][1]
        mz=weight1*pi[2]+weight2*positions[ip1][2]
        new_positions[i+B]=mm.Vec3(mx,my,mz)
    for i in range(B):
        pi=positions[i]
        pj=positions[ip1]
        ip1=from_i_to_bead(i+1,B)
        # virtual site attached to mid-bond
        virtual_sites.append(mm.TwoParticleAverageSite(i,ip1,0.5,0.5))
        mx=0.5*(pi[0]+pj[0])
        my=0.5*(pi[1]+pj[1])
        mz=0.5*(pi[2]+pj[2])
        new_positions[i+3*B]=mm.Vec3(mx,my,mz)
        # pseudo-u particles
        ui=u[i]
        new_positions[i+2*B]=mm.Vec3(mx+ui[0]*distance,my+ui[1]*distance,mz+ui[2]*distance)
    return virtual_sites,new_positions

def link_virtual_sites(system: mm.System,virtual_sites: mm.TwoParticleAverageSite):
    """link virtual sites to particles.
    Parameters
    ----------
    system        : the OpenMM system.
    virtual_sites : list of OpenMM virtual sites.
    Returns
    -------
    Raises
    ------
    """
    # number of virtual sites
    V=len(virtual_sites)
    # number of beads
    B=V//2
    for i in range(B):
        system.setVirtualSite(get_virtual_p_from_i(i,B),virtual_sites[i])
    for i in range(B):
        system.setVirtualSite(get_virtual_mid_from_i(i,B),virtual_sites[B+i])

@numba.jit(nopython=True)
def a_is_between_b_and_c(a: int,b: int,c: int,N: int,linear: bool) -> bool:
    """return if bead 'a' is between bead 'b' and bead 'c', b and c are excluded (for linear or ring polymer).
    Parameters
    ----------
    a      : the value to test
    b      : the left value of the interval ]b,c[
    c      : the right value of the interval ]b,c[
    N      : the number of beads
    linear : linear polymer (True) or ring polymer (False)
    Returns
    -------
    if 'a' is between 'b' and 'c' (bool).
    Raises
    ------
    """
    if linear:
        return (a>b and a<c)
    else:
        is_between=False
        # start and end
        start=b
        # travel from start to end, check if 'a' appears
        while start!=c:
            if start==a and start!=b:
                is_between=True
                break
            start=from_i_to_bead(start+1,N)
        return is_between

@numba.jit
def local_minima(signal: list,w: int=3) -> list:
    """returns the local minima.
    Parameters
    ----------
    signal : list of float
    w      : how many points to the left and to the right do we consider ?
    Returns
    -------
    the local minima as a list of integer.
    Raises
    ------
    if w <= 0, the function sets w to 1.
    """
    if w<=0:
        w=1
        print("w <= 0, the function sets w to 1.")
    S=len(signal)
    local_minima=[]
    for s in range(w,S-w,1):
        is_local_minima=True
        for i in range(s-w,s+w+1):
            is_local_minima=is_local_minima and signal[s]<=signal[i]
        if is_local_minima:
            local_minima.append(s)
    return local_minima

@numba.jit
def greater_than_x(signal: list,x: float) -> list:
    """returns the index s of the signal[s] greater than x.
    Parameters
    ----------
    signal : list of float
    x      : the value to compare with
    Returns
    -------
    the index 's' with 'signal[s]>x' as a list of integer.
    Raises
    ------
    """
    S=len(signal)
    greater_than_x=[]
    for s in range(S):
        if signal[s]>x:
            greater_than_x.append(s)
    return greater_than_x

@numba.njit(parallel=True)
def Rs2(positions: list,start: int,end: int,linear: bool) -> list:
    """calculates the square internal distances <R^2(j-i)> within [start,end].
    Parameters
    ----------
    positions   : list of positions returns by 'get_positions_from_context'
    start       : start of the part (int)
    end         : end of the part (int)
    linear      : (True) linear polymer or (False) ring polymer
    Returns
    -------
    The square internal distances (list of float).
    Raises
    ------
    """
    int_linear=int(linear)
    # positions
    px=[p[0] for p in positions]
    py=[p[1] for p in positions]
    pz=[p[2] for p in positions]
    B=len(px)
    # number of beads per chain
    C=1
    B_per_C=B//C
    M=B_per_C*int_linear+(B_per_C//2+1)*(1-int_linear)
    # <R(s)^2>
    Rs2=np.zeros(M)
    counts=np.zeros(M)
    J=int(min(end+1,B))
    for i in range(start,end):
        pxi=px[i]
        pyi=py[i]
        pzi=pz[i]
        I=int(i+1)
        for j in prange(I,J):
            dx=pxi-px[j]
            dy=pyi-py[j]
            dz=pzi-pz[j]
            ds=(j-i)*int_linear+min(j-i,B_per_C-j+i)*(1-int_linear)
            Rs2[ds]+=dx*dx+dy*dy+dz*dz
            counts[ds]+=1
    # normalize and return
    for i in range(1,M):
        Rs2[i]/=max(1,counts[i])
    return Rs2

def draw_Rs2(Rs2: list,persistance: float,bond: float,name: str):
    """draw the square internal distances to a file 'name'.
    Parameters
    ----------
    Rs2         : a list for the square internal distances (list of float)
    persistance : persistance length (in nanometer) of the chain (float)
    bond        : size of the bond in nanometer (float)
    name        : file name (string) of the *.png
    Returns
    -------
    Raises
    ------
    """
    # write the Rs2(s) down to a file
    with open(name+".out","w") as out_file:
        out_file.write("Rs2(s)\n")
        for r in Rs2:
            out_file.write(str(r)+"\n")
    # plot the Rs2(s)
    M=len(Rs2)
    L=[i for i in range(M)]
    persistance=50.0
    wlc=[0.0]+[2.0*persistance*bond*l*(1.0-(persistance/(bond*l))*(1.0-math.exp(-bond*l/persistance))) for l in L[1:]]
    plt.figure(1)
    plt.plot(L,Rs2)
    plt.plot(L,wlc)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim(1,M)
    plt.ylim(1.0,10000000.0)
    plt.xlabel("s")
    plt.ylabel("R(s)^2 in nm^2")
    plt.savefig(name+".png")
    plt.close("all")
        
@numba.jit(nopython=True)
def Rg2_and_shape(positions: list,start: int,end: int,linear: bool) -> tuple:
    """calculates the tensor and radius of gyration for the part within [start,end].
    Parameters
    ----------
    positions   : list of positions returns by 'get_positions_from_context'
    start       : start of the part (int)
    end         : end of the part (int)
    linear      : (True) linear polymer or (False) ring polymer (bool)
    Returns
    -------
    A tuple made of the radius of gyration (float), the asphericity (float), the acylindricity (float), the shape factor (float), the 1d size of each part.
    Raises
    ------
    if start is equal to end, return a ZeroDivisionError.
    """
    N=len(positions)
    # if the polymer is linear the start < end for each parts we ask for
    if linear and start>end:
        start,end=end,start
    # com
    pi=positions[end]
    cx=pi[0]
    cy=pi[1]
    cz=pi[2]
    ii=start
    P=1
    while ii!=end:
        pi=positions[ii]
        cx+=pi[0]
        cy+=pi[1]
        cz+=pi[2]
        ii=from_i_to_bead(ii+1,N)
        P+=1
    cx/=P
    cy/=P
    cz/=P
    # gyration tensor
    Tij=[0.0]*9
    Rg2=0.0
    ii=start
    for p in range(P):
        pi=positions[ii]
        ax=pi[0]-cx
        ay=pi[1]-cy
        az=pi[2]-cz
        Tij[0]+=ax*ax
        Tij[1]+=ax*ay
        Tij[2]+=ax*az
        Tij[4]+=ay*ay
        Tij[5]+=ay*az
        Tij[8]+=az*az
        Rg2+=ax*ax+ay*ay+az*az
        ii=from_i_to_bead(ii+1,N)
    Tij[3]=Tij[1]
    Tij[6]=Tij[2]
    Tij[7]=Tij[5]
    Rg2/=P
    # eigenvalues of the gyration tensor
    np_Tij=np.array([[Tij[0]/P,Tij[1]/P,Tij[2]/P],[Tij[3]/P,Tij[4]/P,Tij[5]/P],[Tij[6]/P,Tij[7]/P,Tij[8]/P]])
    # print(np.linalg.eigvals(np_Tij))
    li=np.linalg.eigvals(np_Tij)
    # sort the eigenvalues
    li.sort()
    # abs_tol=1e-6
    # rel_tol=1e-4
    # if abs(Rg2-(li[0]+li[1]+li[2]))>(abs_tol+rel_tol*abs(li[0]+li[1]+li[2])):
    #     print("warning",Rg2,li[0]+li[1]+li[2])
    # shape
    asphericity=li[2]-0.5*(li[0]+li[1])
    acylindricity=li[1]-li[0]
    kappa2=(asphericity*asphericity+0.75*acylindricity*acylindricity)/((li[0]+li[1]+li[2])**2)
    # # find the local minima of the Rg^2
    # Rg2_local_minima=local_minima([Rg2[s] for s in range(S)])
    return Rg2,asphericity,acylindricity,kappa2,P

def draw_Rg2(iteration: int,Rg2_nm2: float,norm_Rg2_nm2: float=1.0,linear: bool=True,path_to: str="/scratch"):
    """draw the square of the gyration radius (in nm^2) as a function of iterations.
    The file (two columns) is like 'iteration' 'square radius of gyration in nm^2'.
    Parameters
    ----------
    iteration    : current iteration (int)
    Rg2_nm2      : square gyration radius (in nanometer^2) corresponding to the current 'iteration' (float)
    norm_Rg2_nm2 : normalization factor in nanometer^2 (float)
    linear       : linear polymer (True) or ring polymer (False) (bool)
    name         : name of the file to save (name.png) (str)
    Returns
    -------
    Raises
    ------
    """
    # header ?
    try:
        with open(path_to+".out","r") as in_file:
            lines=in_file.readlines()
            sl=lines[0].split()
            write_header=not bool(sl[0]=="iteration" and sl[1]=="Rg2_nm2")
            # cannot write iteration<max(iteration)
            n_lines=len(lines)
            if n_lines>1:
                if int((lines[n_lines-1].split())[0])>=iteration:
                    write_header=True
    except IOError:
        write_header=True
    if write_header:
        with open(path_to+".out","w") as out_file:
            out_file.write("iteration Rg2_nm2\n")
    # write the last gyration radius
    with open(path_to+".out","a") as out_file:
        out_file.write(str(iteration)+" "+str(Rg2_nm2)+"\n")
    # read the complete statistics
    iterations=[]
    nRg2t_nm2=[]
    with open(path_to+".out","r") as in_file:
        lines=in_file.readlines()
        for l in lines[1:]:
            sl=l.split()
            iterations.append(int(sl[0]))
            nRg2t_nm2.append(float(sl[1])/norm_Rg2_nm2)
    # draw
    fig=plt.figure("Rg2t_nm2")
    if norm_Rg2_nm2!=1.0:
        if linear:
            ax=fig.add_axes([0.15,0.15,0.75,0.75],xlim=(0,iteration),ylim=(0,2.0),xlabel="iterations",ylabel=r'$6R_g^2/Nk^2$')#,xscale='log',yscale='log')
        else:
            ax=fig.add_axes([0.15,0.15,0.75,0.75],xlim=(0,iteration),ylim=(0,2.0),xlabel="iterations",ylabel=r'$12R_g^2/Nk^2$')#,xscale='log',yscale='log')
    else:
        ax=fig.add_axes([0.15,0.15,0.75,0.75],xlim=(0,iteration),ylim=(0,200.0),xlabel="iterations",ylabel=r'$R_g^2$')#,xscale='log',yscale='log')
    ax.plot(iterations,nRg2t_nm2)
    plt.savefig(path_to+".png")
    fig.clf()
    plt.close("Rg2t_nm2")

def draw_kappa2(iteration: int,kappa2: float,path_to: str="/scratch"):
    """draw the relative shape anisotropy (kappa^2) as a function of iterations.
    The file (two columns) is like 'iteration' 'kappa^2'.
    Parameters
    ----------
    iteration   : current iteration (int)
    kappa2      : relative shape anisotropy corresponding to the current 'iteration' (float)
    name        : name of the file to save (name.png) (str)
    Returns
    -------
    Raises
    ------
    """
    # header ?
    try:
        with open(path_to+".out","r") as in_file:
            lines=in_file.readlines()
            sl=lines[0].split()
            write_header=not bool(sl[0]=="iteration" and sl[1]=="kappa2")
            # cannot write iteration<max(iteration)
            n_lines=len(lines)
            if n_lines>1:
                if int((lines[n_lines-1].split())[0])>=iteration:
                    write_header=True
    except IOError:
        write_header=True
    if write_header:
        with open(path_to+".out","w") as out_file:
            out_file.write("iteration kappa2\n")
    # write the last relative shape anisotropy
    with open(path_to+".out","a") as out_file:
        out_file.write(str(iteration)+" "+str(kappa2)+"\n")
    # read the complete statistics
    iterations=[]
    kappa2t=[]
    with open(path_to+".out","r") as in_file:
        lines=in_file.readlines()
        for l in lines[1:]:
            sl=l.split()
            iterations.append(int(sl[0]))
            kappa2t.append(float(sl[1]))
    # draw
    fig=plt.figure("kappa2t")
    ax=fig.add_axes([0.15,0.15,0.75,0.75],xlim=(0,iteration),ylim=(0,1.0),xlabel="iterations",ylabel=r'$\kappa^2$')#,xscale='log',yscale='log')
    ax.plot(iterations,kappa2t)
    plt.savefig(path_to+".png")
    fig.clf()
    plt.close("kappa2t")

def draw_close_ij(positions: list,cut: float=1.0,cut_1d: int=1,linear: bool=True,norm_p: float=1.0,name: str="/scratch/close_ij.png"):
    """draw the chain with 'red' bead for close pairwize (i,j).
    Parameters
    ----------
    positions : list of positions returns by 'get_positions_from_context' function
    cut       : cut to define a contact (float)
    cut_1d    : 1d cut to keep only the pairwize (i,j) such that abs(j-i)>=cut_1d
    linear    : linear polymer (True) or ring polymer (False) (bool)
    norm_p    : normalization factor for the positions (float)
    name      : name of the file to save (str)
    Returns
    -------
    Raises
    ------
    """
    if cut_1d<0:
        cut_1d=0
    # get the close (i,j)
    close_ij,C=get_close_ij(positions,cut,cut_1d,linear)
    # colors
    N=len(positions)
    colors=["black"]*N
    for c in range(C):
        ci=close_ij[c][0]
        cj=close_ij[c][1]
        colors[ci]="red"
        colors[cj]="red"
    # draw
    px,py,pz=zip(*[(p[0]/norm_p,p[1]/norm_p,p[2]/norm_p) for p in positions])
    fig=plt.figure(1)
    ax=fig.add_subplot(111,projection='3d')
    ax.scatter(px,py,pz,c=colors,s=1.5,alpha=0.5)
    ax.set_xlim(0.0,2.0)
    ax.set_ylim(0.0,2.0)
    ax.set_zlim(0.0,2.0)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    plt.savefig(name,format='png')
    fig.clf()
    plt.close("all")
    
@numba.jit(nopython=True)
def get_plectonemes(positions: list,cut: float=1.0,cut_1d: int=1,linear: bool=True) -> list:
    """simple algorithm to detect plectonemes.
    first step  : calculate the pairwize (i,j) that are in close proximity (cutoff is given by input argument 'cut').
    second step : calculate the relative shape anisotropy of the polymer between 'i' and 'j' and between 'j' and 'i'.
    if the relative shape anisotropy is greater than 0.6 we consider the polymer between 'i' and 'j' (or 'j' and 'i') to be a plectoneme.
    we measure the relative shape anisotropy of a ring polymer to be lesser than 0.5.
    a ring has a relative shape anisotropy equal to 0.25.
    a line has a relative shape anisotropy equal to 1.
    Parameters
    ----------
    positions : list of positions returns by 'get_positions_from_context'
    cut       : cut to define a contact (float)
    cut_1d    : 1d cut to keep only the pairwize (i,j) such that abs(j-i)>=cut_1d
    linear    : linear polymer (True) or ring polymer (False) (bool)
    Returns
    -------
    return a list start,end,size,gyration radius,relative shape anisotropy for each plectoneme.
    Raises
    ------
    """
    if cut_1d<0:
        cut_1d=0
    N=len(positions)
    N_per_C=1#N//get_number_of_chains(context)
    # get the close (i,j)
    close_ij,C=get_close_ij(positions,cut,cut_1d,linear)
    # loop over close proximity (i,j), do 'i' and 'j' between 'a' and 'b' ?
    plectonemes=[]
    start=[0]*C
    end=[0]*C
    size=[0]*C
    Rg2=[0.0]*C
    kappa2=[0.0]*C
    # for each pairwize get the most probable direction of the plectoneme
    P=0
    for c in close_ij:
        sc=c[0]
        ec=c[1]
        # shape from sc to ec
        Rg2_se,asphericity,acylindricity,kappa2_se,size_se=Rg2_and_shape(positions,sc,ec,linear)
        # shape from ec to sc
        Rg2_es,asphericity,acylindricity,kappa2_es,size_es=Rg2_and_shape(positions,ec,sc,linear)
        # go to the left or to the right, shape factor kappa^2 has to be > 0.5
        if kappa2_se>0.6 or kappa2_es>0.6:
            plectonemes.append([-1,0])
            if kappa2_se>kappa2_es or linear:
                start[P]=sc
                end[P]=ec
                size[P]=size_se
                Rg2[P]=Rg2_se
                kappa2[P]=kappa2_se
            else:
                start[P]=ec
                end[P]=sc
                size[P]=size_es
                Rg2[P]=Rg2_es
                kappa2[P]=kappa2_es
            P+=1
    for p in range(P-1):
        sc=start[p]
        ec=end[p]
        # loop over close proximity (a,b)
        for d in range(p+1,P):
            sd=start[d]
            ed=end[d]
            # is it between ... ?
            b0=a_is_between_b_and_c(sc,sd,ed,N,linear)
            if b0:
                b1=a_is_between_b_and_c(ec,sd,ed,N,linear)
            else:
                b1=False
            b2=a_is_between_b_and_c(sd,sc,ec,N,linear)
            if b2:
                b3=a_is_between_b_and_c(ed,sc,ec,N,linear)
            else:
                b3=False
            # cases
            if b0 and b1 and b2 and b3:
                # print("warning",sc,sd,ed)
                # print("warning",ec,sd,ed)
                # print("warning",sd,sc,ec)
                # print("warning",ed,sc,ec)
                # print("size",size[p],size[d])
                # print("Rg2",Rg2[p],Rg2[d])
                # print("kappa2",kappa2[p],kappa2[d])
                # print("")
                pass
            elif b0 and b1:
                pd=plectonemes[d]
                if pd[0]==-1:
                    plectonemes[p][0]=d# 'd' is the start of the current plectoneme for now
                elif p!=pd[0]:# p is not parents of itself
                    plectonemes[p][0]=pd[0]
                else:
                    pass
            elif b2 and b3:
                pp=plectonemes[p]
                if pp[0]==-1:
                    plectonemes[d][0]=p# 'p' is the start of the current plectoneme for now
                elif d!=pp[0]:# d is not parents of itself
                    plectonemes[d][0]=pp[0]
                else:
                    pass
            else:
                pass
    # parents of each pairwize
    for p in range(P):
        parents=plectonemes[p][0]
        while parents!=-1:
            plectonemes[p][0]=parents
            parents=plectonemes[parents][0]
    # how many childrens per parents ?
    for p in range(P):
        parents=plectonemes[p][0]
        if parents!=-1:
            plectonemes[parents][1]+=1
    # return the plectonemes (initialize to tell numba the type)
    pbuffer=[np.array([0,0,0,0.0,1.0])]#,dtype=float64)]#[[0,0,0],[0.0,1.0]]]
    for p in range(P):
        # start and end of the current plectoneme
        if plectonemes[p][0]==-1 and plectonemes[p][1]>1:
            pbuffer.append(np.array([start[p],end[p],size[p],Rg2[p],kappa2[p]]))#,dtype=float64))#,dtype='f8'))
    # print(pbuffer)
    # print(pbuffer[0].dtype)
    # print(type(pbuffer))
    # print(len(pbuffer),len(plectonemes))
    return pbuffer[1:]

def draw_plectonemes_stats(plectonemes: list,positions: list,iteration: int,path_to: str="/scratch"):
    """draw plectonemes statistics (number of plectonemes, 1d and spatial sizes ...) that are returned by 'get_plectonemes'.
    If no header 'iteration start end 1d_size Rg_nm' is found, a new file is created with it.
    The file (five columns) is like 'iteration' 'start' 'end' '1d size' 'square radius of gyration'.
    Parameters
    ----------
    plectonemes : list of 4-tuple made of 'start' (int), 'end' (int), '1d size' (int) and square radius of gyration 'Rg2' (float)
    positions   : list of positions returns by 'get_positions_from_context'
    iteration   : current iteration (int)
    path_to     : path to the folder where to save (str)
    Returns
    -------
    Raises
    ------
    if the file does not have header, it erases the file entries and writes the header.
    """
    # number of beads
    B=len(positions)
    # number of plectonemes
    P=len(plectonemes)
    # header ?
    try:
        with open(path_to+"/plectonemes_stats.out","r") as in_file:
            lines=in_file.readlines()
            sl=lines[0].split()
            write_header=not bool(sl[0]=="iteration" and sl[1]=="start" and sl[2]=="end" and sl[3]=="1d_size" and sl[4]=="Rg_nm" and sl[5]=="kappa2")
            # cannot write iteration<max(iteration)
            n_lines=len(lines)
            if n_lines>1:
                if int((lines[n_lines-1].split())[0])>=iteration:
                    write_header=True
    except IOError:
        write_header=True
    if write_header:
        with open(path_to+"/plectonemes_stats.out","w") as out_file:
            out_file.write("iteration start end 1d_size Rg_nm kappa2\n")
    # write the last statistic about plectonemes
    with open(path_to+"/plectonemes_stats.out","a") as out_file:
        for p in plectonemes:
            # out_file.write(str(iteration)+" "+str(p[0][0])+" "+str(p[0][1])+" "+str(p[0][2])+" "+str(p[1][0])+" "+str(p[1][1])+"\n")
            out_file.write(str(iteration)+" "+str(int(p[0]))+" "+str(int(p[1]))+" "+str(int(p[2]))+" "+str(p[3])+" "+str(p[4])+"\n")
    # read the complete statistics
    iterations=[]
    starts=[]
    ends=[]
    size1d=[]
    Rg2_nm2=[]
    kappa2=[]
    with open(path_to+"/plectonemes_stats.out","r") as in_file:
        lines=in_file.readlines()
        for l in lines[1:]:
            sl=l.split()
            iterations.append(int(sl[0]))
            starts.append(int(sl[1]))
            ends.append(int(sl[2]))
            size1d.append(int(sl[3]))
            Rg2_nm2.append(float(sl[4]))
            kappa2.append(float(sl[5]))
    # number of plectonemes
    I=len(iterations)
    # if the number of plectonemes is equal to 0 or 1, do nothing
    if I>1:
        number_of_plectonemes=[]
        np=1
        for i in range(1,I):
            if iterations[i-1]!=iterations[i] or i==(I-1):
                number_of_plectonemes.append([iterations[i-1],np])
                np=1
            else:
                np+=1
        fig=plt.figure("number_of_plectonemes")
        ax=fig.add_axes([0.15,0.15,0.75,0.75],xlim=(0,iteration),ylim=(0,0.005),xlabel="iterations",ylabel="number of plectonemes per bond")
        ax.plot([n[0] for n in number_of_plectonemes],[n[1]/B for n in number_of_plectonemes])
        plt.savefig(path_to+"/number_of_plectonemes.png")
        fig.clf()
        plt.close("number_of_plectonemes")
        # mean and sd 1d size of the plectonemes
        mean_1d_size,sd_1d_size=mean_and_sd_per_iteration(iterations,size1d)
        # draw 1d size of the plectonemes
        fig=plt.figure("mean_1d_size_of_plectonemes")
        ax=fig.add_axes([0.15,0.15,0.75,0.75],xlim=(0,iteration),ylim=(0,0.1),xlabel="iterations",ylabel="plectoneme mean 1d size per bead")
        ax.errorbar([m[0] for m in mean_1d_size],[m[1]/B for m in mean_1d_size],yerr=[s[1]/B for s in sd_1d_size],fmt='o')
        plt.savefig(path_to+"/mean_size1d_of_plectonemes.png")
        fig.clf()
        plt.close("mean_1d_size_of_plectonemes")
        fig=plt.figure("size1d_of_plectonemes")
        ax=fig.add_axes([0.15,0.15,0.75,0.75],xlim=(0,iteration),ylim=(0,200),xlabel="iterations",ylabel="1d size of the plectonemes")
        ax.plot(iterations,size1d,'bo',markersize=2)
        plt.savefig(path_to+"/size1d_of_plectonemes.png")
        fig.clf()
        plt.close("size1d_of_plectonemes")
        # mean Rg2 (in nm) of the plectonemes
        mean_Rg2,sd_Rg2=mean_and_sd_per_iteration(iterations,Rg2_nm2)
        # draw Rg (in nm) of the plectonemes
        fig=plt.figure("mean_Rg2_of_plectonemes")
        ax=fig.add_axes([0.15,0.15,0.75,0.75],xlim=(0,iteration),ylim=(0,200.0*200.0),xlabel="iterations",ylabel=r'$\langle R_g^2\rangle~\text{of plectonemes}$')
        ax.errorbar([r[0] for r in mean_Rg2],[r[1] for r in mean_Rg2],yerr=[s[1] for s in sd_Rg2],fmt='o')
        plt.savefig(path_to+"/mean_Rg2_of_plectonemes.png")
        fig.clf()
        plt.close("mean_Rg2_of_plectonemes")
        fig=plt.figure("Rg_of_plectonemes")
        ax=fig.add_axes([0.15,0.15,0.75,0.75],xlim=(0,iteration),ylim=(0,200.0*200.0),xlabel="iterations",ylabel=r'$R_g^2~\text{of the plectonemes}')
        ax.plot(iterations,Rg2_nm2,'bo',markersize=2)
        plt.savefig(path_to+"/Rg_of_plectonemes.png")
        fig.clf()
        plt.close("Rg_of_plectonemes")
        # mean kappa^2 (between 0 and 1) of the plectonemes
        mean_kappa2,sd_kappa2=mean_and_sd_per_iteration(iterations,kappa2)
        # draw kappa^2 of the plectonemes
        fig=plt.figure("mean_kappa2_of_plectonemes")
        ax=fig.add_axes([0.15,0.15,0.75,0.75],xlim=(0,iteration),ylim=(0.4,1),xlabel="iterations",ylabel=r'$\kappa^2$')
        ax.errorbar([k[0] for k in mean_kappa2],[k[1] for k in mean_kappa2],yerr=[s[1] for s in sd_kappa2],fmt='o')
        plt.savefig(path_to+"/mean_kappa2_of_plectonemes.png")
        fig.clf()
        plt.close("mean_kappa2_of_plectonemes")
        fig=plt.figure("kappa2_of_plectonemes")
        ax=fig.add_axes([0.15,0.15,0.75,0.75],xlim=(0,iteration),ylim=(0.4,1),xlabel="iterations",ylabel=r'$\kappa^2$')
        ax.plot(iterations,kappa2,'bo',markersize=2)
        plt.savefig(path_to+"/kappa2_of_plectonemes.png")
        fig.clf()
        plt.close("kappa2_of_plectonemes")

def draw_plectonemes(plectonemes: list,positions: list,norm_p: float=1.0,name: str="/scratch/plectonemes.png"):
    """draw plectonemes that are returned by 'get_plectonemes'.
    Parameters
    ----------
    plectonemes : list of 4-tuple made of 'start' (int), 'end' (int), '1d size' (int) and square radius of gyration 'Rg2' (float)
    positions   : list of positions returns by 'get_positions_from_context' function
    norm_p      : normalization factor for the positions (float)
    name        : name of the file to save (str)
    Returns
    -------
    Raises
    ------
    """
    # colors
    P=len(plectonemes)
    N=len(positions)
    colors=["black"]*N
    for p in plectonemes:
        sp=int(p[0])
        ep=from_i_to_bead(int(p[1])+1,N)
        while sp!=ep:
            colors[sp]="blue"
            sp=from_i_to_bead(sp+1,N)
    # draw
    px,py,pz=zip(*[(p[0]/norm_p,p[1]/norm_p,p[2]/norm_p) for p in positions])
    fig=plt.figure(1)
    ax=fig.add_subplot(111,projection='3d')
    ax.scatter(px,py,pz,c=colors,s=1.5,alpha=0.5)
    ax.set_xlim(0.0,2.0)
    ax.set_ylim(0.0,2.0)
    ax.set_zlim(0.0,2.0)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    plt.savefig(name,format='png')
    fig.clf()
    plt.close("all")

@numba.jit(nopython=True)
def mean_and_sd_per_iteration(iterations: list,data_i: list) -> tuple:
    """return the mean and standard-deviation for each entry of iterations.
    Parameters
    ----------
    iterations : list of iterations (list of int)
    data_i     : list of data, each entry corresponds to one iteration (list of float)
    Returns
    -------
    list of [iteration,mean] (list of [int,int]), list of [iteration,standard-deviation] (list of [int,int])
    Raises
    ------
    """
    I=len(iterations)
    # mean and sd of data_i
    # mean
    np=1
    mean_i=data_i[0]
    means=[]
    for i in range(1,I):
        if iterations[i-1]!=iterations[i] or i==(I-1):
            means.append([iterations[i-1],mean_i/np])
            mean_i=data_i[i]
            np=1
        else:
            mean_i+=data_i[i]
            np+=1
    # sd
    n_sd=0
    np=1
    mean_i=(data_i[0]-means[0][1])**2
    sds=[]
    for i in range(1,I):
        if iterations[i-1]!=iterations[i] or i==(I-1):
            sds.append([iterations[i-1],math.sqrt(mean_i/max(np-1,1))/math.sqrt(np)])
            mean_i=(data_i[i]-means[n_sd][1])**2
            np=1
            n_sd+=1
        else:
            mean_i+=(data_i[i]-means[n_sd][1])**2
            np+=1
    return means,sds

def distance(pi: mm.Vec3,pj: mm.Vec3) -> unit.Quantity:
    """return the distance between pi and pj.
    Parameters
    ----------
    pi : position of the point i (OpenMM.Vec3)
    pj : position of the point j (OpenMM.Vec3)
    Returns
    -------
    distance between point i and point j (unit.Quantity)
    Raises
    ------
    """
    dx=pi[0]-pj[0]
    dy=pi[1]-pj[1]
    dz=pi[2]-pj[2]
    return unit.sqrt(dx*dx+dy*dy+dz*dz)

def twist_each_uv(u: list,v: list,t: list,Tw: list,linear: bool) -> tuple:
    """return u and v with twist Tw.
    If the length of the twist list is T only the first T consecutive frames are twisted.
    The twist is restrained to the interval [-pi,pi].
    Parameters
    ----------
    u      : normal vectors (list of OpenMM.Vec3)
    v      : cross(t,u) vectors (list of OpenMM.Vec3)
    t      : tangent vectors (list of OpenMM.Vec3)
    Tw     : list of twist values between frame 'i' and frame 'i+1' (list of float)
    linear : True for linear and False for ring polymer (bool)
    Returns
    -------
    A tuple u,v of twisted frames u,v,t (list of OpenMM.Vec3, list of OpenMM.Vec3).
    The size of the list u and the size of the list v are equal to the length of the input variables u and v.
    Raises
    ------
    """
    # number of beads (one pseudo-u per bead)
    B=len(u)
    # number of bonds
    N=B-int(linear)
    # number of twist
    T=min(N,len(Tw))
    new_u=u
    new_v=v
    # twist and then parallel transport
    Pi=math.pi
    for i in range(T):
        if Tw[i]!=0.0:
            Tw_i=min(Pi,max(-Pi,Tw[i]))
            ip1=from_i_to_bead(i+1,B)
            cos_tt=a_dot_b(t[i],t[ip1])
            if abs(cos_tt-1.0)<0.00000001:
                # twist between parallel tangents
                new_u[ip1]=rotation(t[ip1],Tw_i,new_u[i])
            elif abs(cos_tt+1.0)<0.00000001:
                # undefined twist between anti-parallel tangents
                print("undefined twist between anti-parallel tangents "+str(i)+" and "+str(i+1))
                pass
            else :
                angle=math.acos(cos_tt)
                # binormal
                b=normalize(a_cross_b(t[i],t[ip1]))
                # twist and then parallel transport
                new_u[ip1]=rotation(b,angle,rotation(t[i],Tw_i,new_u[i]))
            new_v[ip1]=a_cross_b(t[ip1],new_u[ip1])
    return new_u,new_v

def chain_twist(u: list,v: list,t: list,linear: bool,plot: bool=False,path_to: str="/scratch") -> list:
    """return the twist (within the interval [-pi,pi]) between each two consecutive frames.
    Parameters
    ----------
    u      : normal vector
    v      : v ~ cross(t,u)
    t      : tangent vector
    linear : True for linear and False for ring polymer (bool)
    plot   : plot the twist along the chain (bool)
    Returns
    -------
    A list of float made of the twist between each two consecutive frames.
    The size of the list is N-1 for linear and N for ring, where N is the number of bonds.
    Raises
    ------
    """
    # number of beads (one pseudo-u per bead)
    B=len(u)
    # number of bonds
    N=B-int(linear)
    # twist
    T=N-int(linear)
    Tw=[0.0]*T
    for i in range(T):
        ip1=from_i_to_bead(i+1,B)
        cos_tt=a_dot_b(t[i],t[ip1])
        # anti-parallel tangents ?
        if abs(cos_tt+1.)<0.00000001:
            pass
        else:
            sign_Tw=1-2*int(max(-1.0,min(1.0,((a_dot_b(u[ip1],v[i])-a_dot_b(v[ip1],u[i]))/(1.0+cos_tt))))<0.0)
            Tw[i]=sign_Tw*math.acos(max(-1.0,min(1.0,(a_dot_b(u[i],u[ip1])+a_dot_b(v[i],v[ip1]))/(1.0+cos_tt))))
    if plot:
        plt.figure(1)
        plt.plot([i/float(N-int(linear)) for i in range(N-int(linear))],Tw)
        plt.xlim(0,1)
        plt.ylim(-math.pi,math.pi)
        plt.xlabel("s")
        plt.ylabel("Tw(s)")
        plt.savefig(path_to+"/Tw_instantaneous.png")
        plt.close("all")
    return Tw

def normalize(a: mm.Vec3) -> mm.Vec3:
    """return the normalize vector a : a/sqrt(a[0]^2+a[1]^2+a[2]^2).
    Parameters
    ----------
    a : OpenMM.Vec3(x,y,z)
    Returns
    -------
    the normalized vector a OpenMM.Vec3(x,y,z)
    Raises
    ------
    """
    n=unit.sqrt(a[0]*a[0]+a[1]*a[1]+a[2]*a[2])
    zero_vector=False
    if type(n) is int:
        if n==0:
            zero_vector=True
    elif type(n) is float:
        if n==0.0:
            zero_vector=True
    elif type(n) is unit.Quantity:
        if n._value==0.0:
            zero_vector=True
    if zero_vector:
        # print("the vector a has a norm equal to 0, the vector is returned without normalization.")
        return a
    else:
        return mm.Vec3(a[0]/n,a[1]/n,a[2]/n)

def a_dot_b(a: mm.Vec3,b: mm.Vec3) -> unit.Quantity:
    """return the scalar product of a with b.
    Parameters
    ----------
    a : [x,y,z] 3-tuple of OpenMM.Quantity
    b : [x',y',z'] 3-tuple of OpenMM.Quantity
    Returns
    -------
    scalar product of a with b (OpenMM.Quantity).
    Raises
    ------
    if a is not OpenMM.Vec3.
    if b is not OpenMM.Vec3.
    """
    if type(a) is not mm.Vec3:
        print("Input argument a must be OpenMM.Vec3.")
        sys.exit()
    if type(b) is not mm.Vec3:
        print("Input argument a must be OpenMM.Vec3.")
        sys.exit()
    return a[0]*b[0]+a[1]*b[1]+a[2]*b[2]

def a_cross_b(a: mm.Vec3,b: mm.Vec3) -> mm.Vec3:
    """return the cross product of a with b.
    Parameters
    ----------
    a : OpenMM.Vec3(x,y,z)
    b : OpenMM.Vec3(x',y',z')
    Returns
    -------
    cross product of a with b (OpenMM.Vec3)
    Raises
    ------
    if a is not OpenMM.Vec3.
    if b is not OpenMM.Vec3.
    """
    if type(a) is not mm.Vec3:
        print("Input argument a must be OpenMM.Vec3.")
        sys.exit()
    if type(b) is not mm.Vec3:
        print("Input argument a must be OpenMM.Vec3.")
        sys.exit()
    return mm.Vec3(a[1]*b[2]-a[2]*b[1],a[2]*b[0]-a[0]*b[2],a[0]*b[1]-a[1]*b[0])

def get_bead_to_pseudo_bead_distance(positions: list) -> unit.Quantity:
    """return the average distance between bead and pseudo-u bead.
    Parameters
    ----------
    positions : positions of the N beads + N virtual sites + N pseudo-u beads return by 'get_vpositions_from_context'.
    Returns
    -------
    square root of the average of the quadratic distance between bead and pseudo-u bead (unit.Quantity).
    Raises
    ------
    if the distance between bead and pseudo-u bead is less than 1 nm, exit.
    """
    N=len(positions)//get_particles_per_bead()
    constraint_distance2_average=0.0*unit.nanometer*unit.nanometer
    for i in range(N):
        im=get_virtual_mid_from_i(i,N)
        ip=get_pseudo_u_from_i(i,N)
        dx=positions[ip][0]-positions[im][0]
        dy=positions[ip][1]-positions[im][1]
        dz=positions[ip][2]-positions[im][2]
        if (dx*dx+dy*dy+dz*dz)<=(1.0*unit.nanometer*unit.nanometer):
            print("warning pseudo-u bead, you have to change the harmonic potential strength.")
            sys.exit()
        constraint_distance2_average+=dx*dx+dy*dy+dz*dz
    return unit.sqrt(constraint_distance2_average/float(N))

def get_uvt_from_positions(positions: list) -> tuple:
    """return the attached frames from the positions as u,v,t=list of mm.Vec3, list of mm.Vec3, list of mm.Vec3.
    Parameters
    ----------
    positions : positions (in nanometer) of the N beads + N virtual sites + N pseudo-u particles return by 'get_vpositions_from_context'.
    Returns
    -------
    u,v,t
    u the normal vectors (list of mm.Vec3)
    v ~ t x u (list of mm.Vec3)
    t the tangent vectors (list of mm.Vec3)
    Raises
    ------
    """
    N=len(positions)//get_particles_per_bead()
    pu=get_pseudo_u_from_positions(positions)# pseudo-u
    u=[mm.Vec3(0.0,0.0,0.0)]*N
    v=[mm.Vec3(0.0,0.0,0.0)]*N
    t=[mm.Vec3(0.0,0.0,0.0)]*N
    for i in range(N):
        ip1=from_i_to_bead(i+1,N)
        pi=positions[i]
        Pi=positions[ip1]
        t[i]=normalize(mm.Vec3(Pi[0]-pi[0],Pi[1]-pi[1],Pi[2]-pi[2]))# tangent
        v[i]=normalize(a_cross_b(t[i],pu[i]))# third vector v from the frame pseudo-u,v,t
        u[i]=normalize(a_cross_b(v[i],t[i]))# vector u
    return u,v,t

def get_pseudo_u_from_positions(positions: list) -> list:
    """return the pseudo-u vector
    Parameters
    ----------
    positions : positions of the N beads + N virtual sites + N pseudo-u particles return by 'get_vpositions_from_context'.
    Returns
    -------
    vector from bead to pseudo-u bead (OpenMM.Vec3)
    Raises
    ------
    """
    N=len(positions)//get_particles_per_bead()
    return [normalize(mm.Vec3(positions[get_pseudo_u_from_i(i,N)][0]-positions[get_virtual_mid_from_i(i,N)][0],positions[get_pseudo_u_from_i(i,N)][1]-positions[get_virtual_mid_from_i(i,N)][1],positions[get_pseudo_u_from_i(i,N)][2]-positions[get_virtual_mid_from_i(i,N)][2])) for i in range(N)]

def print_correlations_uvt(positions: list,linear: bool):
    """print the correlations between u, v and t.
    Parameters
    ----------
    positions : positions of the beads return by 'get_vpositions_from_context'.
    linear    : linear (True) or ring (False)
    Returns
    -------
    Raises
    ------
    if the bead to pseudo-u bead vector is close to be parallel with the tangent vector, exit.
    """
    u,v,t=get_uvt_from_positions(positions)
    pseudou=get_pseudo_u_from_positions(positions)
    N=len(u)
    M=N-int(linear)
    print(N,len(positions),len(positions)//get_particles_per_bead())
    uv_bar=0.0
    ut_bar=0.0
    vt_bar=0.0
    tt_bar=0.0
    pseudout_bar=0.0
    for i in range(M):
        uv_bar+=a_dot_b(u[i],v[i])
        ut_bar+=a_dot_b(u[i],t[i])
        vt_bar+=a_dot_b(v[i],t[i])
        tt_bar+=a_dot_b(t[i],t[from_i_to_bead(i+1,N)])
        pseudout_bar+=a_dot_b(pseudou[i],t[i])
        if a_dot_b(pseudou[i],u[i])<0.1:
            dx=positions[i][0]-positions[get_pseudo_u_from_i(i,N)][0]
            dy=positions[i][1]-positions[get_pseudo_u_from_i(i,N)][1]
            dz=positions[i][2]-positions[get_pseudo_u_from_i(i,N)][2]
            print("warning pseudo(u).u",i,":",a_dot_b(pseudou[i],u[i]),a_dot_b(pseudou[i],t[i]),unit.sqrt(dx*dx+dy*dy+dz*dz))
            sys.exit()
    print("<uv>="+str(uv_bar/M)+" <ut>="+str(ut_bar/M)+" <vt>="+str(vt_bar/M)+" <tt>="+str(tt_bar/M)+" <pseudo(u)t>="+str(pseudout_bar/M))

def virtual_harmonic(B: int,g: unit.Quantity,sigma: unit.Quantity,PBC: bool) -> mm.CustomBondForce:
    """return a harmonic force between pseudo-bead and the corresponding u-bead.
    Parameters
    ----------
    B     : number of beads (integer)
    g     : harmonic strength (unit.Quantity)
    sigma : size of the bond between two consecutive beads (unit.Quantity)
    PBC   : does it use the periodic boundary conditions, False or True ?
    Returns
    -------
    OpenMM CustomNonbondedForce (harmonic potential between bead and pseudo-u bead).
    Raises
    ------
    """
    if type(B) is not int:
        print("Input variable N must be integer.")
        sys.exit()
    if type(sigma) is not unit.Quantity:
        print("Input variable sigma must be unit.Quantity.")
        sys.exit()
    if type(PBC) is not bool:
        print("Input variable PBC must be bool.")
        sys.exit()
    harmonic=mm.CustomBondForce('0.5*K_harmonic*(r-r0_harmonic)^2')
    harmonic.addGlobalParameter('K_harmonic',g)
    harmonic.addGlobalParameter('r0_harmonic',sigma)
    harmonic.addEnergyParameterDerivative("K_harmonic")
    for i in range(B):
        harmonic.addBond(get_virtual_mid_from_i(i,B),get_pseudo_u_from_i(i,B))
    harmonic.setUsesPeriodicBoundaryConditions(PBC)
    return harmonic

def fene(N: int,Temp: unit.Quantity,sigma: unit.Quantity,linear: bool,PBC: bool) -> mm.CustomBondForce:
    """return a FENE force between consecutive beads.
    Parameters
    ----------
    N      : number of bonds (int)
    Temp   : temperature (unit.Quantity)
    sigma  : size of the bond between two consecutive beads (unit.Quantity)
    linear : True (linear chain) or False (ring chain)
    PBC    : does it use the periodic boundary conditions, False or True ?
    Returns
    -------
    OpenMM CustomBondForce (FENE potential between two consecutive bonds).
    """
    kBT=get_kBT(Temp)
    fene=mm.CustomBondForce('-0.5*K_fene*R0*R0*log(1-(r*r/(R0*R0)))*step(0.99*R0-r)+4*epsilon_fene*((sigma_fene/r)^12-(sigma_fene/r)^6+0.25)*step(cut_fene-r)')
    fene.addGlobalParameter('K_fene',30.0*kBT/(sigma*sigma))
    fene.addGlobalParameter('R0',1.5*sigma)
    fene.addGlobalParameter('epsilon_fene',kBT)
    fene.addGlobalParameter('sigma_fene',sigma)
    fene.addGlobalParameter('cut_fene',math.pow(2.0,1.0/6.0)*sigma)
    B=N+int(linear)
    for i in range(N):
        fene.addBond(i,from_i_to_bead(i+1,B))
    fene.setUsesPeriodicBoundaryConditions(PBC)
    return fene

def rigid_bond(system: mm.System,N: int,sigma: unit.Quantity,linear: bool):
    """add rigid bond (between two consecutive beads) to the system of N beads.
    Parameters
    ----------
    system : OpenMM system
    N      : number of bonds (integer)
    sigma  : size of the bond between two consecutive beads (unit.Quantity)
    linear : True (linear chain) or False (ring chain)
    Returns
    -------
    Raises
    ------
    """
    B=N+int(linear)
    for i in range(N):
        system.addConstraint(i,from_i_to_bead(i+1,B),sigma)

def clamp_bead(N: int,k_clamp: unit.Quantity,x0: unit.Quantity=0.0*unit.nanometer,y0: unit.Quantity=0.0*unit.nanometer,z0: unit.Quantity=0.0*unit.nanometer) -> mm.CustomExternalForce:
    """return an external force to clamp a particle.
    Parameters
    ----------
    N       : index of the bead (int)
    k_clamp : strength of the force that clamps the particle (unit.Quantity)
    x0      : x coordinate (unit.Quantity)
    y0      : y coordinate (unit.Quantity)
    z0      : z coordinate (unit.Quantity)
    Returns
    -------
    OpenMM CustomExternalForce to deal with plane repulsion.
    Raises
    ------
    """
    expression="0.5*k_clamp*((x-x0)^2+(y-y0)^2+(z-z0)^2)"
    clamp=mm.CustomExternalForce(expression)
    clamp.addPerParticleParameter("k_clamp")
    clamp.addPerParticleParameter("x0")
    clamp.addPerParticleParameter("y0")
    clamp.addPerParticleParameter("z0")
    clamp.addParticle(N,[k_clamp,x0,y0,z0])
    return clamp

def external_force(N: int,axe: str="z",force: unit.Quantity=0.0*unit.piconewton) -> mm.CustomExternalForce:
    """return an external force.
    Parameters
    ----------
    N     : index of the bead (int)
    axe   : axe of the external force (str)
    force : module of the force (unit.Quantity)
    Returns
    -------
    OpenMM CustomExternalForce to deal with plane repulsion.
    Raises
    ------
    """
    expression="f_external*"+axe
    external=mm.CustomExternalForce(expression)
    external.addPerParticleParameter("f_external")
    external.addParticle(N,[force])
    return external

def plane_repulsion(N: int,normal: str="z",position: unit.Quantity=0.0*unit.nanometer,sigma: unit.Quantity=1.0*unit.nanometer,strength: unit.Quantity=0.0*unit.joule) -> mm.CustomExternalForce:
    """return a soft plane repulsion (WCA potential).
    Parameters
    ----------
    N        : index of the bead (int)
    normal   : normal ('x', 'y' or 'z') to the plane (str)
    position : position of the plane (unit.Quantity)
               if normal i 'z' and position is -1 the plane is located at z=-1
    sigma    : size of the WCA potential (unit.Quantity)
    strength : plane repulsion strength (unit.Quantity)
    Returns
    -------
    OpenMM CustomExternalForce to deal with external force.
    Raises
    ------
    """
    expression="4*epsilon_plane*((sigma_plane/("+normal+"-p0))^12-(sigma_plane/("+normal+"-p0))^6+0.25)*step(cutoff_plane-("+normal+"1-p0))"
    plane=mm.CustomExternalForce(expression)
    plane.addPerParticleParameter("p0")
    plane.addPerParticleParameter("epsilon_plane")
    plane.addPerParticleParameter("sigma_plane")
    plane.addPerParticleParameter("cutoff_plane")
    plane.addParticle(N,[position,strength,sigma,math.pow(2.0,1.0/6.0)*sigma])
    return plane

def tt_bending(N: int,Kb: unit.Quantity,linear: bool,PBC: bool) -> mm.CustomAngleForce:
    """return a bending force between three consecutive beads.
    Parameters
    ----------
    N   : the number of bonds (int)
    Kb  : the bending rigidity (unit.Quantity)
    PBC : does it use the periodic boundary conditions, False or True ?
    Returns
    -------
    OpenMM CustomAngleForce bending force between two consecutive tangent vector t.
    """
    # number of beads
    B=N+int(linear)
    bending=mm.CustomAngleForce("Kb_tt*(1.0-cos(theta-A_tt))")
    bending.addGlobalParameter("Kb_tt",Kb)
    bending.addGlobalParameter("A_tt",math.pi)
    bending.addEnergyParameterDerivative("Kb_tt")
    for i in range(N-int(linear)):
        bending.addAngle(i,from_i_to_bead(i+1,B),from_i_to_bead(i+2,B))
    bending.setUsesPeriodicBoundaryConditions(PBC)
    return bending

def ut_bending(N: int,Kb: unit.Quantity,linear: bool,PBC: bool) -> mm.CustomAngleForce:
    """return a bending force between pseudo-u and tangent vector t.
    Parameters
    ----------
    N      : the number of bonds (integer)
    Kb     : the bending rigidity (unit.Quantity)
    linear : linear (True) or ring (False)
    PBC    : does it use the periodic boundary conditions, False or True ?
    Returns
    -------
    OpenMM CustomAngleForce bending force between pseudo-u and tangent vector t.
    Raises
    ------
    """
    # number of beads
    B=N+int(linear)
    bending=mm.CustomAngleForce("Kb_ut*(1.0-cos(theta-A_ut))")
    bending.addGlobalParameter("Kb_ut",Kb)
    bending.addGlobalParameter("A_ut",0.5*math.pi)
    bending.addEnergyParameterDerivative("Kb_ut")
    for i in range(N):
        bending.addAngle(get_pseudo_u_from_i(i,B),get_virtual_mid_from_i(i,B),get_virtual_p_from_i(from_i_to_bead(i+1,B),B))
    bending.setUsesPeriodicBoundaryConditions(PBC)
    return bending

def twist(N: int,Kt: unit.Quantity,twist0: list,linear: bool,PBC: bool) -> mm.CustomCompoundBondForce:
    """return a twisting force between three consecutive beads and the virtual sites attached to them.
    Parameters
    ----------
    N      : number of bonds (int)
    Kt     : the twisting rigidity (unit.Quantity)
    twist0 : twisting angle (list of floats)
    linear : linear (True) or ring (False) (bool)
    PBC    : does it use the periodic boundary conditions, False or True ? (bool)
    Returns
    -------
    OpenMM CustomCompoundBondForce to deal with the twist between two consecutive frames uvt.
    Raises
    ------
    """
    # number of beads
    B=N+int(linear)
    # u' is the pseudo-u vector
    # v~cross(t,u') (1)
    v1x="((y2-y1)*(z4-z1)-(z2-z1)*(y4-y1))"
    v1y="((z2-z1)*(x4-x1)-(x2-x1)*(z4-z1))"
    v1z="((x2-x1)*(y4-y1)-(y2-y1)*(x4-x1))"
    # v~cross(t,u') (2)
    v2x="((y3-y2)*(z5-z2)-(z3-z2)*(y5-y2))"
    v2y="((z3-z2)*(x5-x2)-(x3-x2)*(z5-z2))"
    v2z="((x3-x2)*(y5-y2)-(y3-y2)*(x5-x2))"
    # u~cross(v,t)~cross(cross(t,u'),t) (1)
    u1x="("+v1y+"*(z2-z1)-"+v1z+"*(y2-y1))"
    u1y="("+v1z+"*(x2-x1)-"+v1x+"*(z2-z1))"
    u1z="("+v1x+"*(y2-y1)-"+v1y+"*(x2-x1))"
    # u~cross(v,t)~cross(cross(t,u'),t) (2)
    u2x="("+v2y+"*(z3-z2)-"+v2z+"*(y3-y2))"
    u2y="("+v2z+"*(x3-x2)-"+v2x+"*(z3-z2))"
    u2z="("+v2x+"*(y3-y2)-"+v2y+"*(x3-x2))"
    # dot(u(1),u(2))
    uu="("+u1x+"*"+u2x+"+"+u1y+"*"+u2y+"+"+u1z+"*"+u2z+")"+"/sqrt(("+u1x+"*"+u1x+"+"+u1y+"*"+u1y+"+"+u1z+"*"+u1z+")*("+u2x+"*"+u2x+"+"+u2y+"*"+u2y+"+"+u2z+"*"+u2z+"))"
    # dot(v(1),v(2))
    vv="("+v1x+"*"+v2x+"+"+v1y+"*"+v2y+"+"+v1z+"*"+v2z+")"+"/sqrt(("+v1x+"*"+v1x+"+"+v1y+"*"+v1y+"+"+v1z+"*"+v1z+")*("+v2x+"*"+v2x+"+"+v2y+"*"+v2y+"+"+v2z+"*"+v2z+"))"
    # dot(t(1),t(2))
    tt="((x2-x1)*(x3-x2)+(y2-y1)*(y3-y2)+(z2-z1)*(z3-z2))/(distance(p2,p1)*distance(p3,p2))"
    expression="Kt*(1.0-("+uu+"+"+vv+")/(1.0+"+tt+"))"
    twisting=mm.CustomCompoundBondForce(5,expression)
    twisting.addGlobalParameter("Kt",Kt)
    # twisting.addPerBondParameter("twist0")
    twisting.addEnergyParameterDerivative("Kt")
    for i in range(N-int(linear)):
        ip1=from_i_to_bead(i+1,B)
        ip2=from_i_to_bead(ip1+1,B)
        # twisting.addBond([i,ip1,ip2,get_pseudo_u_from_i(i,B),get_pseudo_u_from_i(ip1,B)],[math.sqrt(1.0-math.cos(twist0[i]))])
        twisting.addBond([i,ip1,ip2,get_pseudo_u_from_i(i,B),get_pseudo_u_from_i(ip1,B)])
    twisting.setUsesPeriodicBoundaryConditions(PBC)
    return twisting

def add_torque_to_bond(B: int,N: int,torque: unit.Quantity=0.0*unit.joule,linear: bool=True,PBC: bool=False) -> mm.CustomCompoundBondForce:
    """return a torque along the tangent to the bond 'B'.
    Parameters
    ----------
    B      : index of the bond (int)
    N      : number of bonds (int)
    torque : torque applied to the bond (unit.Quantity)
    linear : linear (True) or ring (False) (bool)
    PBC    : does it use the periodic boundary conditions, False or True ? (bool)
    Returns
    -------
    OpenMM CustomCompoundBondForce.
    Raises
    ------
    References
    ----------
    The frame u,v,t is built with u' the pseudo-u vector :
    v~cross(t,u')
    u~cross(v,t)~cross(cross(t,u'),t)    
    To add a torque around t I added a force F=a*v along v.
    The force F acts on to the pseudo-u bead.
    The torque is then cross(u',F)= that differs from cross(u,F)=a*t.
    cross(u',n)=t
    """
    # number of beads
    M=N+int(linear)
    if False:
        # u' is the pseudo-u vector
        # v~cross(t,u')
        vx="((y2-y1)*(z3-z1)-(z2-z1)*(y3-y1))"
        vy="((z2-z1)*(x3-x1)-(x2-x1)*(z3-z1))"
        vz="((x2-x1)*(y3-y1)-(y2-y1)*(x3-x1))"
        # expression
        norm_up="distance(p3,p1)"
        norm_v="sqrt("+vx+"*"+vx+"+"+vy+"*"+vy+"+"+vz+"*"+vz+")"
        expression="torque*("+vx+"*x3+"+vy+"*y3+"+vz+"*z3)/("+norm_v+"*"+norm_up+")"
        # custom compound bond force to deal with the torque
        torque_to_bond=mm.CustomCompoundBondForce(3,expression)
        torque_to_bond.addPerBondParameter("torque")
        # torque_to_bond.addBond([get_virtual_p_from_i(B,M),get_virtual_p_from_i(from_i_to_bead(B+1,M),M),get_pseudo_u_from_i(B,M)],[torque])
        torque_to_bond.addBond([B,from_i_to_bead(B+1,M),get_pseudo_u_from_i(B,M)],[torque])
        torque_to_bond.setUsesPeriodicBoundaryConditions(PBC)
        return torque_to_bond
    else:
        torque_to_bond=mm.CustomTorsionForce("torque")
        torque_to_bond.addPerTorsionParameter("torque")
        # torque_to_bond.addTorsion(from_i_to_bead(B-1,M),B,from_i_to_bead(B+1,M),from_i_to_bead(B+2,M),[torque])
        ip1=from_i_to_bead(B+1,M)
        torque_to_bond.addTorsion(get_pseudo_u_from_i(B,M),B,ip1,get_pseudo_u_from_i(ip1,M),[torque])
        return torque_to_bond

def wca(N: int,Temp: unit.Quantity,sigma: unit.Quantity,linear: bool,PBC: bool) -> mm.CustomNonbondedForce:
    """return a WCA potential to resolve excluded volume constraints.
    Parameters
    ----------
    N      : number of bonds (integer)
    Temp   : temperature (unit.Quantity)
    sigma  : size of the bead (unit.Quantity)
    linear : linear or ring chain (False or True)
    PBC    : uses periodic boundary conditions (False or True)
    Returns
    -------
    OpenMM CustomNonbondedForce (WCA potential).
    Raises
    ------
    """
    # number of beads
    B=N+int(linear)
    M=get_particles_per_bead()*B
    wca=mm.NonbondedForce()
    if PBC:
        wca.setNonbondedMethod(mm.NonbondedForce.CutoffPeriodic)
    else:
        wca.setNonbondedMethod(mm.NonbondedForce.CutoffNonPeriodic)
    wca.setCutoffDistance(math.pow(2.0,1.0/6.0)*sigma)
    for i in range(B):
        wca.addParticle(0.0,sigma,get_kBT(Temp))
    for i in range(B,M):
        wca.addParticle(0.0,sigma,0.0)
    # no wca between beads belonging to the same bond
    for i in range(N):
        wca.addException(i,from_i_to_bead(i+1,B),0.0,sigma,0.0,True)
    # no wca between bead and its virtuals sites
    for i in range(B):
        ip1=from_i_to_bead(i+1,B)
        wca.addException(i,get_virtual_p_from_i(i,B),0.0,sigma,0.0,True)
        wca.addException(i,get_pseudo_u_from_i(i,B),0.0,sigma,0.0,True)
        wca.addException(i,get_virtual_mid_from_i(i,B),0.0,sigma,0.0,True)
        wca.addException(get_virtual_mid_from_i(i,B),ip1,0.0,sigma,0.0,True)
        wca.addException(get_pseudo_u_from_i(i,B),ip1,0.0,sigma,0.0,True)
    return wca

def wca_virtual_sites(N: int,Temp: unit.Quantity,sigma: unit.Quantity,linear: bool,PBC: bool) -> mm.CustomBondForce:
    """return a WCA potential to resolve excluded volume constraints.
    Parameters
    ----------
    N      : number of bonds (int)
    Temp   : temperature (unit.Quantity)
    sigma  : size of the bead (unit.Quantity)
    linear : linear (True) or ring (False)
    PBC    : uses periodic boundary conditions (False or True)
    Returns
    -------
    OpenMM CustomBondForce (WCA potential).
    Raises
    ------
    """
    # number of beads
    B=N+int(linear)
    wca=mm.CustomBondForce("4*epsilon_wca_virtual_sites*((sigma_wca_virtual_sites/r)^12-(sigma_wca_virtual_sites/r)^6+0.25)*step(cutoff_wca_virtual_sites-r)")
    wca.addGlobalParameter("epsilon_wca_virtual_sites",get_kBT(Temp))
    wca.addGlobalParameter("sigma_wca_virtual_sites",sigma)
    wca.addGlobalParameter("cutoff_wca_virtual_sites",math.pow(2.0,1.0/6.0)*sigma)
    wca.addEnergyParameterDerivative("epsilon_wca_virtual_sites")
    if linear:
        wca.addBond(get_pseudo_u_from_i(0,B),get_virtual_p_from_i(from_i_to_bead(1,B),B))
        for i in range(1,N):
            wca.addBond(get_pseudo_u_from_i(i,B),get_virtual_p_from_i(from_i_to_bead(i-1,B),B))
            wca.addBond(get_pseudo_u_from_i(i,B),get_virtual_p_from_i(from_i_to_bead(i+1,B),B))
        wca.addBond(get_pseudo_u_from_i(N,B),get_virtual_p_from_i(N-1,B))
    else:
        for i in range(B):
            wca.addBond(get_pseudo_u_from_i(i,B),get_virtual_p_from_i(from_i_to_bead(i-1,B),B))
            wca.addBond(get_pseudo_u_from_i(i,B),get_virtual_p_from_i(from_i_to_bead(i+1,B),B))
    wca.setUsesPeriodicBoundaryConditions(PBC)
    return wca

def average_bond_size(positions: list,linear: bool) -> unit.Quantity:
    """return the average bond size.
    Parameters
    ----------
    positions : positions of the N beads return by context.getState(getPositions=True).getPositions()
    linear    : linear (True) or ring (False)
    Returns
    -------
    square root of the average quadratic bond size (unit.Quantity)
    Raises
    ------
    unit.sqrt raises an error for average_bond_size<0
    """
    # the beads - one virtual site per bead - reference particles
    B=len(positions)//get_particles_per_bead()
    N=B-int(linear)
    # average
    average_bond_size=0.0*positions[0][0]*positions[0][0]
    for i in range(N):
        pi=positions[i]
        pj=positions[from_i_to_bead(i+1,B)]
        dx=pi[0]-pj[0]
        dy=pi[1]-pj[1]
        dz=pi[2]-pj[2]
        average_bond_size+=dx*dx+dy*dy+dz*dz
    return unit.sqrt(average_bond_size/float(N))

def rotation(a: mm.Vec3,angle: float,b: mm.Vec3) -> mm.Vec3:
    """return (x',y',z') that is the rotation of b=(x,y,z) around a=(X,Y,Z) with angle.
    Parameters
    ----------
    a     : rotation axis (OpenMM.Vec3)
    angle : rotation angle (float)
    b     : the OpenMM.Vec3 to rotate
    Returns
    -------
    the rotated vector (x',y',z') (OpenMM.Vec3).
    """
    c=math.cos(angle)
    s=math.sin(angle)
    n=unit.sqrt(b[0]*b[0]+b[1]*b[1]+b[2]*b[2])
    if a[0]==b[0] and a[1]==b[1] and a[2]==b[2]:
        return b
    else:
        res=[c*b[0],c*b[1],c*b[2]]
        ab=a[0]*b[0]+a[1]*b[1]+a[2]*b[2]
        avb=[a[1]*b[2]-a[2]*b[1],a[2]*b[0]-a[0]*b[2],a[0]*b[1]-a[1]*b[0]]
        res[0]+=(1.-c)*ab*a[0]+s*avb[0]
        res[1]+=(1.-c)*ab*a[1]+s*avb[1]
        res[2]+=(1.-c)*ab*a[2]+s*avb[2]
        return normalize(mm.Vec3(n*res[0],n*res[1],n*res[2]))

def flangevin(x: float) -> float:
    """return the value of the Langevin function at x : 1/tanh(x)-1/x.
    Parameters
    ----------
    x : the x where to evaluate the Langevin function (float)
    Returns
    -------
    the value of the Langevin function at x (float)
    """
    if x==0.0:
        return 0.0
    else:
        return 1./math.tanh(x)-1./x

def bending_rigidity(k_bp: int,resolution: int,Temp: unit.Quantity) -> unit.Quantity:
    """return the bending rigidity for a specific resolution (in bp) and Kuhn length (in bp).
    Parameters
    ----------
    k_bp       : Kuhn length in bp (integer)
    resolution : resolution of a segment in bp (integer)
    Temp       : temperature (unit.Quantity)
    Returns
    -------
    bending rigidity in units of kB*Temperature (unit.Quantity)
    Raises
    ------
    """
    if resolution>=k_bp:
        return 0.0
    mcos=(k_bp/resolution-1.0)/(k_bp/resolution+1.0)
    a=1e-10
    b=1e2
    precision=1e-10
    fa=flangevin(a)-mcos;
    fb=flangevin(b)-mcos;
    m=.5*(a+b);
    fm=flangevin(m)-mcos;
    while abs(a-b)>precision:
        m=0.5*(a+b)
        fm=flangevin(m)-mcos
        fa=flangevin(a)-mcos
        if fm==0.0:
            break
        if (fa*fm)>0.:
            a=m
        else:
            b=m;
    return m*get_kBT(Temp)

def twisting_rigidity(twisting_persistence: unit.Quantity,sigma: unit.Quantity,Temp: unit.Quantity) -> unit.Quantity:
    """return the twisting rigidity (harmonic potential 0.5*Kt*Tw^2 with <Tw^2>=twisting persistence/bond length).
    Parameters
    ----------
    twisting_persistence : the twisting peristence (unit.Quantity)
    sigma                : the bond size (unit.Quantity)
    Temp                 : temperature (unit.Quantity)
    Returns
    -------
    twisting rigidity in units of kB*Temperature (unit.Quantity)
    Raises
    ------
    """
    return (twisting_persistence/sigma)*get_kBT(Temp)

def v_gaussian(V: int,C: int,Temp: unit.Quantity,m: unit.Quantity,seed: int) -> list:
    """return a list of velocities (of size V) distributed according to the Boltzmann distribution with a com motion to 0,0,0
    Parameters
    ----------
    V    : number of velocities (integer)
    C    : number of constraints (integer)
    T    : temperature (unit.Quantity)
    m    : mass of the beads (unit.Quantity)
    seed : seed (integer)
    """
    random.seed(seed)
    kBT=get_kBT(Temp)
    # velocities
    vcom_x=0.0*unit.sqrt(kBT/m)
    vcom_y=0.0*unit.sqrt(kBT/m)
    vcom_z=0.0*unit.sqrt(kBT/m)
    # velocities according to Boltzmann distribution
    vx=[unit.sqrt(kBT/m)*random.gauss(0.0,1.0) for v in range(V)]
    vy=[unit.sqrt(kBT/m)*random.gauss(0.0,1.0) for v in range(V)]
    vz=[unit.sqrt(kBT/m)*random.gauss(0.0,1.0) for v in range(V)]
    # com motion
    for v in range(V):
        vcom_x+=vx[v]
        vcom_y+=vy[v]
        vcom_z+=vz[v]
    vcom_x/=float(V)
    vcom_y/=float(V)
    vcom_z/=float(V)
    # kinetic energy
    K=0.0*kBT/m
    for v in range(V):
        vx[v]-=vcom_x
        vy[v]-=vcom_y
        vz[v]-=vcom_z
        K+=(vx[v]*vx[v]+vy[v]*vy[v]+vz[v]*vz[v])
    # rescaling the kinetic energy
    rK=unit.sqrt((3.0*V-C)*0.5*kBT/(0.5*m*K))
    return [mm.Vec3(vx[v%V]*rK,vy[v%V]*rK,vz[v%V]*rK) for v in range(get_particles_per_bead()*V)]

def draw_symmetric_matrix(matrix: list,name: str,log: bool):
    """draw the matrix and save it to file name.png.
    Parameters
    ----------
    matrix : the symmetric matrix to draw (N*(N+1)/2-tuple of float)
    name   : the name of the file to save the colormap (string)
    log    : if True plot the log of each matrix element (bool)
    Returns
    -------
    Raises
    ------
    """
    # The symmetric matrix NxN is stored in a L-tuple of float
    # get the L=N*(N+1)/2
    L=len(matrix)
    # get the N=(sqrt(8*L+1)-1)/2
    N=int((math.sqrt(8*L+1)-1)/2)
    # build the numpy matrix from the L-tuple
    np_matrix=np.zeros(shape=(N,N))
    nzero=0
    matrix_max=max(matrix)
    matrix_max+=1*int(matrix_max==0)
    offset=0.000001*matrix_max
    # list to numpy 2d array
    for i in range(N):
        for j in range(i,N):
            buffer_ij=matrix[i*N-i*(i+1)//2+j]+offset
            np_matrix[i,j]=buffer_ij
            np_matrix[j,i]=buffer_ij
            nzero+=int(buffer_ij>0)
    # draw the numpy matrix
    cmap=clr.LinearSegmentedColormap.from_list('from_blue_to_red',['blue','white','red'],N=256)
    plt.figure(1)
    if log and nzero>0:
        plt.matshow(np_matrix,cmap=cmap,norm=clr.LogNorm(vmin=offset,vmax=matrix_max+offset))
    else:
        plt.matshow(np_matrix,cmap=cmap)
    plt.savefig(name+".png")
    plt.close("all")

@numba.jit(nopython=True)
def where_the_beads_are(positions: list,size: float) -> tuple:
    """return a partition of the space.
    Parameters
    ----------
    positions : list of positions from 'get_positions_from_context'
    size      : size of each partition block (in nanometer)
    Returns
    -------
    the partition as a python dictionnary (dict), the bead to block (list) correspondance.
    Raises
    ------
    """
    size=1.0/size
    N=len(positions)
    # space partitioning
    min_x=positions[0][0]*size
    min_y=positions[0][1]*size
    min_z=positions[0][2]*size
    max_x=min_x
    max_y=min_y
    max_z=min_z
    for p in positions:
        min_x=min(min_x,p[0]*size-1)
        min_y=min(min_y,p[1]*size-1)
        min_z=min(min_z,p[2]*size-1)
        max_x=max(max_x,p[0]*size+1)
        max_y=max(max_y,p[1]*size+1)
        max_z=max(max_z,p[2]*size+1)
    Px=int(max_x-min_x)+1
    Py=int(max_y-min_y)+1
    Pz=int(max_z-min_z)+1
    P=Px*Py*Pz
    # space partitioning
    # step 1
    bead_to_block=[-1]*N
    beads_in_block=[0]*P
    for i in range(N):
        pi=positions[i]
        index=int(pi[0]*size-min_x)*Py*Pz+int(pi[1]*size-min_y)*Pz+int(pi[2]*size-min_z)
        beads_in_block[index]+=1
        bead_to_block[i]=index
    # step 2
    max_beads_in_block=0
    for i in range(N):
        index=bead_to_block[i]
        max_beads_in_block=max(max_beads_in_block,beads_in_block[index])
    # step 3
    block_to_beads=np.full((P,max_beads_in_block),-1)
    beads_in_block=[0]*P
    for i in range(N):
        index=bead_to_block[i]
        block_to_beads[index][beads_in_block[index]]=i
        beads_in_block[index]+=1
    return block_to_beads,beads_in_block,bead_to_block,Px,Py,Pz

@numba.jit(nopython=True)
def get_close_ij(positions: list,cut: float,cut_ij: int=1,linear: bool=True) -> tuple:
    """simple algorithm to return the pairwize with distance <= cut.
    Parameters
    ----------
    positions : list of positions returns by 'get_positions_from_context'
    cut       : cutoff (in nanometer) to define close (i,j) (float)
    cut_ij    : keep only the pairwize with |j-i|>=cut_ij (integer)
    linear    : linear polymer (True) or ring polymer (False)
    Returns
    -------
    a list made of the close (i,j) with |j-i|>=cut_ij.
    Raises
    ------
    """
    N=len(positions)
    # space partitioning
    block_to_beads,beads_in_block,bead_to_block,Px,Py,Pz=where_the_beads_are(positions,cut)
    # loop over cells
    close_ij=[]
    C=0
    for i in range(N):
        # block bead 'i'
        block_i=bead_to_block[i]
        iz=block_i%Pz
        iy=((block_i-iz)//Pz)%Py
        ix=(block_i-iz-iy*Pz)//(Py*Pz)
        sx=max(0,ix-1)
        ex=min(ix+1,Px-1)+1
        sy=max(0,iy-1)
        ey=min(iy+1,Py-1)+1
        sz=max(0,iz-1)
        ez=min(iz+1,Pz-1)+1
        # position bead 'i'
        pi=positions[i]
        px=pi[0]
        py=pi[1]
        pz=pi[2]
        # loop over neighboor cells
        for x in range(sx,ex):
            for y in range(sy,ey):
                for z in range(sz,ez):
                    block=x*Py*Pz+y*Pz+z
                    partition_block=block_to_beads[block][:beads_in_block[block]]
                    # loop over bead "j"
                    for j in partition_block:
                        if j>=i:
                            break
                        pj=positions[j]
                        dx=pj[0]-px
                        dy=pj[1]-py
                        dz=pj[2]-pz
                        ij=(i-j)*int(linear)+min(N-(i-j),i-j)*int(not linear)
                        if (dx*dx+dy*dy+dz*dz)<=(cut*cut) and ij>=cut_ij:
                            close_ij.append([j,i])
                            C+=1
    return close_ij,C

@numba.jit(nopython=True)
def ind_contact_matrix(matrix: list,Ps: list,sample: int,sbin: int,positions: list,icut: float=0.0,scut: float=1.0,linear: bool=False) -> tuple:
    """return the contact matrix calculated from the overlap of pairwize (i,j).
    a contact is +1 if the distance is within the interval [icut,scut]
    if the matrix is N by N and the bin size is B the function returns a G by G matrix with G=N/B.
    Parameters
    ----------
    matrix    : contact matrix, the new snapshot is aggregated to the matrix (list of G*(G+1)/2 float)
    Ps        : a list for the contact probability (list of float)
    sample    : the number of snapshots (integer)
    sbin      : size of the bin for the contact matrix (integer)
    positions : list of positions returns by 'get_positions_from_context'
    icut      : inf threshold (in nanometer) for the +1 contact (float)
    scut      : sup threshold (in nanometer) for the +1 contact (float)
    linear    : True (linear chain) or False (ring chain)
    Returns
    -------
    sum of contact matrices (N*(N+1)/2 tuple of float).
    the new list for the contact probability (list of float).
    the new number of snapshots (integer).
    Raises
    ------
    if sbin < 1, the function takes sbin = 1.
    if the length of the matrix is different of G*(G+1)/2, resize to G*(G+1)/2 and set sample to 0.
    if the length of the P is not the size of polymer, resize to M and set sample to 0.
    """
    if sbin<1:
        sbin=1
        print("sbin is < 1, the function sets sbin to 1.")
    int_linear=int(linear)
    # number of chains
    C=1#get_number_of_chains(context)
    # number of particles
    N=len(positions)
    # number of particles per chain
    N_per_C=N//C
    # min(j-i,N_per_C-(j-i))
    # j-i=N_per_C/2
    M=N_per_C*int_linear+(N_per_C//2+1)*(1-int_linear)
    # number of particles per bin
    G=N//sbin+int((N%sbin)>0)
    # contact matrix
    if len(matrix)!=(G*(G+1)//2):
        print("The length of the matrix is 0, resize to G*(G+1)/2.")
        matrix=[0]*(G*(G+1)//2)
        sample=0
    # P(s,t)
    if len(Ps)!=M:
        print("Resize the P(s) to the correct value ...")
        Ps=[0.0]*M
        sample=0
    Pst=[0]*M
    # get the close (i,j)
    close_ij,C=get_close_ij(positions,scut,1,linear)
    # get the distances within [icut,scut]
    for c in close_ij:
        ii=c[0]
        jj=c[1]
        pi=positions[ii]
        pj=positions[jj]
        dx=pj[0]-pi[0]
        dy=pj[1]-pi[1]
        dz=pj[2]-pi[2]
        # contact matrix
        ds2=dx*dx+dy*dy+dz*dz
        if ds2>=(icut*icut) and ds2<=(scut*scut):
            mi=ii//sbin
            Mi=jj//sbin
            matrix[mi*G-mi*(mi+1)//2+Mi]+=1
            # P(s,t) : same chain c*N_per_C+n ?
            if (ii//N_per_C)==(jj//N_per_C):
                ji=(jj-ii)*int_linear+min(jj-ii,N_per_C-jj+ii)*(1-int_linear)
                Pst[ji]+=1
    # P(s) normalization
    if linear:
        for i in range(M):
            Ps[i]=(sample*Ps[i]+Pst[i]/(N_per_C-i))/(sample+1)
    else:
        if (N_per_C%2)==0:
            for i in range(M-1):
                Ps[i]=(sample*Ps[i]+Pst[i]/N_per_C)/(sample+1)
            Ps[M-1]=(sample*Ps[M-1]+Pst[M-1]/(N_per_C//2))/(sample+1)
        else:
            for i in range(M):
                Ps[i]=(sample*Ps[i]+Pst[i]/N_per_C)/(sample+1)
    sample+=1
    return matrix,Ps,sample

def draw_Ps(Ps: list,name: str="/scratch/Ps"):
    """draw the contact probability to a file 'name'.
    Parameters
    ----------
    Ps        : a list for the contact probability (list of float)
    name      : file name of the *.png (str)
    Returns
    -------
    Raises
    ------
    """
    # write the P(s) down to a file
    with open(name+".out","w") as out_file:
        out_file.write("P(s)"+"\n")
        for p in Ps:
            out_file.write(str(p)+"\n")
    M=len(Ps)
    # plot the P(s)
    if not all(v==0.0 for v in Ps):
        plt.figure(1)
        plt.plot([i for i in range(M)],Ps)
        plt.xscale('log')
        plt.yscale('log')
        plt.xlim(1,M)
        plt.ylim(0.001,2)
        plt.xlabel("s")
        plt.ylabel("P(s)")
        plt.savefig(name+".png")
        plt.close("all")

def write_draw_conformation(positions: list,norm_p: float=1.0,xyz_lim: list=[0.0,2.0,0.0,2.0,0.0,2.0],name_write: str="write.out",name_draw: str="draw.png",mode: str="write_draw"):
    """write and/or draw conformation (in nanometer) using matplotlib to file name.out/png.
    Parameters
    ----------
    positions  : list of positions returns by 'get_positions_from_context' function
    norm_p     : normalization factor (in nanometer) for the positions (float)
    xyz_lim    : x,y, and z range (list of float)
    name_write : name of the file to write (string)
    name_draw  : name of the file to draw (string)
    mode       : a string that contains write and/or draw
    Returns
    -------
    Raises
    ------
    """
    # write
    if "write" in mode:
        with open(name_write,"w") as out_file:
            for p in positions:
                out_file.write(str(p[0])+' '+str(p[1])+' '+str(p[2])+'\n')
    # draw
    if "draw" in mode:
        N=len(positions)
        px,py,pz=zip(*[(p[0]/norm_p,p[1]/norm_p,p[2]/norm_p) for p in positions])
        fig=plt.figure("draw")
        ax=fig.add_subplot(111,projection='3d')
        ax.scatter(px,py,pz,c=[float(p)/float(N) for p in range(N)],s=1.5,cmap='rainbow',alpha=0.5)
        ax.set_xlim(xyz_lim[0],xyz_lim[1])
        ax.set_ylim(xyz_lim[2],xyz_lim[3])
        ax.set_zlim(xyz_lim[4],xyz_lim[5])
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_zlabel("z")
        fig.savefig(name_draw)
        fig.clf()
        plt.close("draw")

def overtwist_to_chain(N: int,resolution: int,linear: bool,overtwist: float=0.0) -> float:
    """return the twist between frame 'i' and frame 'i+1' to get the 'overtwist' over the chain.
    The twist is within the interval [-pi,pi].
    Parameters
    ----------
    N          : number of bonds (int)
    resolution : resolution in bp (int)
    linear     : linear (True) or ring polymer (False) (bool)
    overtwist  : overtwist (float)
    Returns
    -------
    The twist Tw(i) between two consecutive frames (i,i+1) such that sum(Tw(i))=Lk.
    Raises
    ------
    if abs(overtwist) is greater than 1, abs(overtwist)=1.
    """
    Pi=math.pi
    # overtwist interval within [-1,1]
    if abs(overtwist)>1.0:
        print("If abs(overtwist) is greater than 1, abs(overtwist)=1.")
    # sign of the twist
    sign_overtwist=1.0-2.0*int(overtwist<0.0)
    # linking number deficit
    Lk=overtwist*N*resolution/10.5
    sign_overtwist=1-2*int(overtwist<0.0)
    # Tw
    if overtwist==0.0:
        Tw=0.0
    else:
        if linear:
            # Tw*(N-1)=2*Pi*Lk
            Tw=2.0*Pi*Lk/(N-1)
        else:
            # a*(N-1)+b=2*Pi*Lk
            Tw0=abs(2.0*Pi*Lk/(N-1))
            for i in range(1000000+1):
                Tw=(0.9+0.2*i/1000000)*Tw0
                cum=Tw*(N-1)
                mcum=cum-2.0*Pi*int(cum/(2.0*Pi))
                b=-(mcum-int(mcum>Pi)*2.0*Pi)
                n=sign_overtwist*(cum+b)
                if abs(1.0-n/(2.0*Pi*Lk))<0.0001:
                    Tw*=sign_overtwist
                    break
    # twist within the interval [-pi,pi]
    Tw=Tw-2.0*Pi*(Tw>Pi)+2.0*Pi*(Tw<(-Pi))
    # intrinsic twist between two consecutive base-pair
    Tw-=2.0*math.pi*math.fmod(resolution,10.5)
    # twist within the interval [-pi,pi]
    Tw=Tw-2.0*Pi*(Tw>Pi)+2.0*Pi*(Tw<(-Pi))
    return Tw

def create_replica(Ks: list,Temp: unit.Quantity,resolution: float,sigma: unit.Quantity,m: unit.Quantity,dt: unit.Quantity,tau: unit.Quantity,overtwist: float,N: int,L: unit.Quantity,seed: int=0,linear: bool=True,platform: str="CPU1",replica: int=0) -> tuple:
    """create replica of a system.
    Parameters
    ----------
    Ks         : list of parameters, each one of them has an associated system with specific hamiltonian (list)
    N          : number of bonds
    L          : box size (unit.Quantity)
    Temp       : temperature (unit.Quantity)
    sigma      : bond size (unit.Quantity)
    m          : mass of the particles (unit.Quantity)
    dt         : time-step (unit.Quantity)
    tau        : time scale, inverse of the coupling frequency to the thermostat (unit.Quantity)
    overtwist  : normalized linking number deficit (float)
    seed       : seed for the pRNG (integer)
    linear     : linear (True) or ring (False)
    platform   : something like 'name_implementation_precision'
                 example : 'CPU1', 'GeForce_RTX_2080_OpenCL_single', 'GeForce_GTX_1080_Ti_CUDA_double'
    path_to    : path to the folder where to write the data (string)
    replica    : index of the replica to follow (integer)
    Returns
    -------
    a list of checkpoints from mm.Context.createCheckpoint().
    the last context that has been created.
    Raises
    ------
    """
    random.seed(seed)
    # number of beads
    B=N+int(linear)
    kBT=get_kBT(Temp)
    K_unit=Ks[0][0].unit
    polymer=["ring","linear"][int(linear)]
    bond="fene"
    PBC=False
    thermostat="langevin"
    Tw=overtwist_to_chain(N,resolution,linear,overtwist)
    title="N"+str(N)+".seed"+str(seed)+"."+str(overtwist)
    # number of replicas
    R=len(Ks)
    # platform creation
    precision="double"
    if "single" in platform:
        precision="single"
    elif "mixed" in platform:
        precision="mixed"
    elif "double" in platform:
        precision="double"
    else:
        print("Please consider single, mixed or double for the precision. Precision sets to double.")
    # 'platform_name' has to be like 'name_implementation_'
    platform_name=platform.replace(precision,"")
    P=1
    if "OpenCL" in platform:
        implementation="OpenCL"
    elif "CUDA" in platform:
        implementation="CUDA"
    elif "CPU" in platform:
        implementation="CPU"
        P=int(platform_name.replace("CPU",""))
    else:
        pass
    # 'platform_name' has to be like 'name__'
    platform_name.replace(implementation,"")
    # 'platform_name' has to be like 'name'
    platform_name=platform_name[:-1]
    platform,properties=create_platform(platform_name,P,precision)
    # positions and frames u,v,t
    positions,u,v,t=puvt_chain(N,sigma,L,linear)
    u,v=twist_each_uv(u,v,t,[Tw]*(N-1)+[0.0])
    # creation of the contexts
    checkpoints=[]
    # system creation (com motion removed every 10 steps)
    system=create_system(L,B,m,10)
    # virtual sites
    virtual_sites,new_positions=create_virtual_sites(positions,u,sigma)
    link_virtual_sites(system,virtual_sites)
    if bond=="fene":
        system.addForce(fene(N,Temp,sigma,linear,PBC))
    elif bond=="rigid":
        rigid_bond(system,N,sigma,linear)
    else:
        system.addForce(fene(N,Temp,sigma,linear,PBC))
    # harmonic pseudo-u vector
    system.addForce(virtual_harmonic(B,8.0*get_kBT(Temp),sigma,PBC))
    # wca
    system.addForce(wca(N,Temp,sigma,linear,PBC))
    system.addForce(wca_virtual_sites(N,Temp,sigma,linear,PBC))
    # bending
    system.addForce(tt_bending(N,get_kBT(Temp),linear,PBC))
    system.addForce(ut_bending(N,256.0*get_kBT(Temp),linear,PBC))
    # twisting
    system.addForce(twist(N,get_kBT(Temp),[0.0]*(N-int(linear)),linear,PBC))
    # creating the integrator
    integrator=create_integrator("langevin",Temp,dt,B,system.getNumConstraints(),m,tau,seed,False,False)
    # creating the context
    context=create_context(system,integrator,platform,properties,new_positions,v_gaussian(B,system.getNumConstraints(),Temp,m,seed))
    for r in range(R):
        context.setParameter("Kb_tt",Ks[r][0])
        context.setParameter("Kt",Ks[r][1])
        checkpoints.append(context.createCheckpoint())
    for r in range(R):
        context.loadCheckpoint(checkpoints[r])
        print(context.getParameter("Kt"))
    return checkpoints,context

def replica_exchange(Ks: list,Temp: unit.Quantity,resolution: float,sigma: unit.Quantity,m: unit.Quantity,dt: unit.Quantity,tau: unit.Quantity,overtwist: float,N: int,L: unit.Quantity,steps: int=10000,iterations: int=10,seed: int=0,linear: bool=True,platform: str="CPU1",path_to: str="",replica: int=0):
    """performs replica exchange.
    Parameters
    ----------
    Ks         : list of parameters, each one of them has an associated system with specific hamiltonian (list)
    steps      : number of steps between replica exchange (integer)
    iterations : number of replica exchange iterations (integer)
    N          : number of particles
    L          : box size (unit.Quantity)
    Temp       : temperature (unit.Quantity)
    sigma      : bond size (unit.Quantity)
    m          : mass of the particles (unit.Quantity)
    dt         : time-step (unit.Quantity)
    tau        : time scale, inverse of the coupling frequency to the thermostat (unit.Quantity)
    overtwist  : normalized linking number deficit (float)
    seed       : seed for the pRNG (integer)
    linear     : linear (True) or ring (False)
    platform   : something like 'name_implementation_precision'
                 example : 'GeForce_RTX_2080_OpenCL_single', 'GeForce_GTX_1080_Ti_CUDA_double'
    path_to    : path to the folder where to write the data (string)
    replica    : index of the replica to follow (integer)
    Returns
    -------
    Raises
    ------
    if the function does not find OpenMM checkpoints (print a message).
    if precision is not in (single,mixed,double) print a message and precision sets to single.
    """
    random.seed(seed)
    kBT=get_kBT(Temp)
    K_unit=Ks[0][0].unit
    polymer=["ring","linear"][int(linear)]
    # number of replicas
    R=len(Ks)
    replicas=[r for r in range(R)]
    # number of contexts to use
    C=1
    # for the contacts
    contact_ij=[0]
    Ps=[0.0]
    sample=0
    # read the Hamiltonians
    restart_iterations=[0]*R
    for r in range(R):
        try:
            with open(path_to+"/replica"+str(r)+"/hamiltonian.out","r") as in_file:
                lines=in_file.readlines()
                for l in lines:
                    sl=l.split()
                    restart_iterations[r]=max(restart_iterations[r],int(sl[0]))
        except IOError:
            print("No Hamiltonian found for the replica "+str(r)+".")
    # do the replicas end at the same iterations ?
    restart_iteration=max(restart_iterations)
    n_restart_iteration=sum([int(restart_iterations[r]==restart_iteration) for r in range(R)])
    restart_iteration*=int(n_restart_iteration==R)
    # read the checkpoints, load the Hamiltonian parameters (from the checkpoints)
    checkpoints,context=create_replica(Ks,Temp,resolution,sigma,m,dt,tau,overtwist,N,L,seed,linear,platform,replica)
    contexts=[context]
    for r in range(R):
        try:
            with open(path_to+"/replica"+str(r)+"/restart/restart."+polymer+".s"+str(restart_iteration)+".chk","rb") as in_file:
                checkpoints[r]=in_file.read()
                contexts[0].loadCheckpoint(checkpoints[r])
                Ks[r]=[contexts[0].getParameter("Kb_tt"),contexts[0].getParameter("Kt")]
        except IOError:
            print("No checkpoint found for the replica "+str(r)+".")
    # loop over replica exchange iterations
    Rg_linear=math.sqrt(N*N*sigma/(12.0-6.0*int(linear)))
    for i in range(iterations):
        current_iteration=restart_iteration+(i+1)*steps
        # loop over replicas
        for r in range(R):
            cc=r%C
            contexts[cc].loadCheckpoint(checkpoints[r])
            contexts[cc].getIntegrator().step(steps)
            checkpoints[r]=contexts[cc].createCheckpoint()
            # write and draw the conformation
            write_draw_conformation(contexts[cc],path_to+"/replica"+str(r)+"/out/xyz."+polymer+".s"+str(current_iteration)+".out",path_to+"/replica"+str(r)+"/png/xyz."+polymer+".s"+str(current_iteration)+".png",["write","write_draw"][(current_iteration%100000)==0])
            # write the Hamiltonian
            with open(path_to+"/replica"+str(r)+"/hamiltonian.out","a") as out_file:
                out_file.write(str(current_iteration)+" "+str(contexts[cc].getParameter("Kb_tt"))+" "+str(contexts[cc].getParameter("Kt"))+"\n")
            # print information about replica
            if r==replica:
                print("replica "+str(r)+" :")
                print_variables(contexts[cc],linear,current_iteration)
                write_draw_conformation(contexts[cc],path_to+"/xyz."+polymer+".s"+str(current_iteration)+".out",path_to+"/xyz."+polymer+".s"+str(current_iteration)+".png","draw")
                if current_iteration>=3000000:
                    positions=get_positions_from_context(contexts[cc])
                    contact_ij,Ps,sample=contact_matrix(contact_ij,Ps,sample,2,positions,40.0,60.0,linear)
                    draw_symmetric_matrix(contact_ij,path_to+"/contact.matrix."+polymer,True)
                    draw_Ps(Ps,path_to+"/Ps."+polymer)
                    plectonemes=get_plectonemes(positions,1.25*sigma/unit.nanometer,int(100.0*unit.nanometer/sigma),linear)
                    draw_plectonemes(plectonemes,positions,Rg_linear,path_to+"/get.plectonemes."+polymer+".s"+str(current_iteration))
        # replica exchange
        exchanges=0
        sequence=replicas
        while exchanges<(R-2):
            replica_i=random.randint(exchanges,R-1)
            sequence[replica_i],sequence[exchanges]=sequence[exchanges],sequence[replica_i]
            exchanges+=1
            replica_j=random.randint(exchanges,R-1)
            sequence[replica_j],sequence[exchanges]=sequence[exchanges],sequence[replica_j]
            exchanges+=1
        acceptation_ratio=0
        for r in range(0,R-1,2):
            sequence0=sequence[r]
            sequence1=sequence[r+1]
            # context 0
            contexts[0].loadCheckpoint(checkpoints[sequence0])
            Ks0=[contexts[0].getParameter("Kb_tt"),contexts[0].getParameter("Kt")]
            state0=contexts[0].getState(getParameterDerivatives=True,enforcePeriodicBox=False)
            energy_i=Ks0[0]*(state0.getEnergyParameterDerivatives())["Kb_tt"]+Ks0[1]*(state0.getEnergyParameterDerivatives())["Kt"]
            # context 1
            contexts[0].loadCheckpoint(checkpoints[sequence1])
            Ks1=[contexts[0].getParameter("Kb_tt"),contexts[0].getParameter("Kt")]
            state1=contexts[0].getState(getParameterDerivatives=True,enforcePeriodicBox=False)
            energy_j=Ks1[0]*(state1.getEnergyParameterDerivatives())["Kb_tt"]+Ks1[1]*(state1.getEnergyParameterDerivatives())["Kt"]
            # energy after the swap
            energy_i_exchange=Ks1[0]*(state0.getEnergyParameterDerivatives())["Kb_tt"]+Ks1[1]*(state0.getEnergyParameterDerivatives())["Kt"]
            energy_j_exchange=Ks0[0]*(state1.getEnergyParameterDerivatives())["Kb_tt"]+Ks0[1]*(state1.getEnergyParameterDerivatives())["Kt"]
            # print(energy_i,energy_j,energy_i_exchange,energy_j_exchange)
            factor=(energy_i+energy_j-energy_i_exchange-energy_j_exchange)*K_unit/kBT
            if factor>0.0:
                acceptation=True
            elif random.random()<=math.exp(factor):
                acceptation=True
            else:
                acceptation=False
            # replica exchange ?
            if acceptation:
                if sequence0==replica:
                    replica=sequence1
                elif sequence1==replica:
                    replica=sequence0
                else:
                    pass
                # swap
                contexts[0].loadCheckpoint(checkpoints[sequence0])
                contexts[0].setParameter("Kb_tt",Ks1[0])
                contexts[0].setParameter("Kt",Ks1[1])
                checkpoints[sequence0]=contexts[0].createCheckpoint()
                contexts[0].loadCheckpoint(checkpoints[sequence1])
                contexts[0].setParameter("Kb_tt",Ks0[0])
                contexts[0].setParameter("Kt",Ks0[1])
                checkpoints[sequence1]=contexts[0].createCheckpoint()
                acceptation_ratio+=1
            else:
                pass
        print("acceptation ratio="+str(float(acceptation_ratio)/float(R//2))+" ("+str(acceptation_ratio)+"/"+str(R//2)+")")
    # save the checkpoints
    for r in range(R):
        contexts[0].loadCheckpoint(checkpoints[r])
        read_write_checkpoint(contexts[0],path_to+"/replica"+str(r)+"/restart/restart."+polymer+".s"+str(current_iteration)+".chk","write")
    
def jqf_replica_exchange(Ks: list,Temp: unit.Quantity,resolution: float,sigma: unit.Quantity,m: unit.Quantity,tau: unit.Quantity,overtwist: float,N: int,L: unit.Quantity,steps: int=10000,iterations: int=10,seed: int=0,linear: bool=True,path_to: str="",replica: int=0):
    """performs replica exchange.
    Parameters
    ----------
    Ks         : list of parameters, each one of them has an associated system with specific hamiltonian (list)
    steps      : number of steps between replica exchange (integer)
    iterations : number of replica exchange iterations (integer)
    N          : number of particles
    L          : box size (unit.Quantity)
    Temp       : temperature (unit.Quantity)
    sigma      : bond size (unit.Quantity)
    m          : mass of the particles (unit.Quantity)
    tau        : time scale, inverse of the coupling frequency to the thermostat (unit.Quantity)
    overtwist  : normalized linking number deficit (float)
    seed       : seed for the pRNG (integer)
    linear     : linear (True) or ring (False)
    path_to    : path to the folder where to write the data (string)
    replica    : index of the replica to follow (integer)
    Returns
    -------
    Raises
    ------
    if the function does not find OpenMM checkpoints (print a message).
    if precision is not in (single,mixed,double) print a message and precision sets to single.
    """
    @mpi_task(cluster_id="cluster_repex")
    def run_context_and_return_checkpoint(checkpoint: str,steps: int,replica: int) -> str:
        """run a context for 'steps' time-steps.
        Parameters
        ----------
        checkpoint : checkpoint from createCheckpoint OpenMM function
        steps      : number of steps (integer)
        replica    : index of the replica (integer)
        Returns
        -------
        checkpoint corresponding to the replica.
        Raises
        ------
        """
        # from mpi4py import MPI
        context=mm.Context()
        context.loadCheckpoint(checkpoint)
        context.getIntegrator().step(steps)
        # comm=MPI.COMM_WORLD
        # rank=comm.Get_rank()
        # print("replica %i(%i) finished"%(replica,rank))
        print("replica %i finished"%(replica))
    return context.createCheckpoint()
    @on_cluster(cluster=custom_cluster,cluster_id=repex_mpiCluster)
    def run_n_replicas(checkpoints: list,steps: int):
        """run 'n' contexts corresponding to 'n' replicas.
        Parameters
        ----------
        checkpoints : list of checkpoints from createCheckpoint OpenMM function
        steps       : number of steps (integer)
        Returns
        -------
        checkpoints corresponding to the 'n' replicas.
        Raises
        ------
        """
        for i,c in enumerate(checkpoints):
            checkpoints[i]=run_context_and_return_checkpoint(c,steps,i)
        return checkpoints
    @mpi_task(cluster_id="cluster_repex")
    def observables_replica(checkpoint: str,replica: int,iteration: int,path_to: str):
        context=mm.Context()
        # draw and write the conformation
        write_draw_conformation(context.loadCheckpoint(checkpoint),path_to+"/replica"+str(replica)+"/png/xyz.s"+str(iteration)+".out",path_to+"/replica"+str(replica)+"/png/xyz.s"+str(iteration)+".png","write_draw")
        # write the Hamiltonian
        with open(path_to+"/replica"+str(replica)+"/hamiltonian.out","a") as out_file:
            out_file.write(str(iteration)+" "+str(context.getParameter("Kb_tt"))+" "+str(context.getParameter("Kt"))+"\n")
        # state=context.getState(getPositions=True,getEnergy=True,getParameterDerivatives=True,enforcePeriodicBox=False)
        # contact_ij,Ps,sample=contact_matrix(contact_ij,Ps,sample,2,positions,40.0,linear,path_to+"/Ps.s"+str(iteration))
        # draw_symmetric_matrix(contact_ij,path_to+"/contact.matrix.s"+str(iteration),True)
        # plectonemes=get_plectonemes(context,sigma,linear,path_to+"/get.plectonemes.s"+str(iteration))
        # plectonemes=get_plectonemes(positions,1.25*sigma/unit.nanometer,int(100.0*unit.nanometer/sigma),linear)
        # draw_conformation(context,path_to+"/xyz.s"+str(iteration)+".out",path_to+"/xyz.s"+str(iteration)+".png","draw")
    @on_cluster(cluster=custom_cluster,cluster_id="cluster_repex")
    def observables_n_replicas(checkpoints: list,iteration: int,path_to: str):
        for i,c in enumerate(checkpoints):
            observables_replica(c,i,iteration,path_to)
    # variables
    random.seed(seed)
    kBT=get_kBT(Temp)
    K_unit=Ks[0][0].unit
    polymer=["ring","linear"][int(linear)]
    replicas=[r for r in range(R)]
    # for the contacts
    contact_ij=[]
    Ps=[]
    sample=0
    # read the Hamiltonians
    restart_iterations=[0]*R
    for r in range(R):
        try:
            with open(path_to+"/replica"+str(r)+"/hamiltonian.out","r") as in_file:
                lines=in_file.readlines()
                for l in lines:
                    sl=l.split()
                    restart_iterations[r]=max(restart_iterations[r],int(sl[0]))
        except IOError:
            print("No Hamiltonian found for the replica "+str(r)+".")
    # do the replicas end at the same iterations ?
    restart_iteration=max(restart_iterations)
    n_restart_iteration=sum([int(restart_iterations[r]==restart_iteration) for r in range(R)])
    restart_iteration*=int(n_restart_iteration==R)
    # read the checkpoints, load the Hamiltonian parameters (from the checkpoints)
    checkpoints,context=create_replica(Ks,Temp,resolution,sigma,m,dt,tau,overtwist,N,L,seed,linear,platform,replica)
    contexts=[context]
    for r in range(R):
        try:
            with open(path_to+"/replica"+str(r)+"/restart/restart."+polymer+".s"+str(restart_iteration)+".chk","rb") as in_file:
                checkpoints[r]=in_file.read()
                contexts[0].loadCheckpoint(checkpoints[r])
                Ks[r]=[contexts[0].getParameter("Kb_tt"),contexts[0].getParameter("Kt")]
        except IOError:
            print("No checkpoint found for the replica "+str(r)+".")
    # cluster
    set_default_cluster(CustomSLURMCluster)
    custom_cluster=CustomSLURMCluster(name="cluster_repex",walltime="01:00:00",nodes=R,mpi_mode=True,fork_mpi=True,queue='devel')
    checkpoints=run_n_replicas(checkpoints,steps)
    # loop over replica exchange iterations
    for i in range(iterations):
        current_iteration=restart_iteration+(i+1)*steps
        # step the replicas
        checkpoints=run_n_replicas(checkpoints,steps)
        # observables
        observables_n_replicas(checkpoints,current_iteration,path_to)
        # replica exchange
        exchanges=0
        sequence=replicas
        while exchanges<(R-2):
            replica_i=random.randint(exchanges,R-1)
            sequence[replica_i],sequence[exchanges]=sequence[exchanges],sequence[replica_i]
            exchanges+=1
            replica_j=random.randint(exchanges,R-1)
            sequence[replica_j],sequence[exchanges]=sequence[exchanges],sequence[replica_j]
            exchanges+=1
        acceptation_ratio=0
        for r in range(0,R-1,2):
            sequence0=sequence[r]
            sequence1=sequence[r+1]
            # context 0
            contexts[0].loadCheckpoint(checkpoints[sequence0])
            Ks0=[contexts[0].getParameter("Kb_tt"),contexts[0].getParameter("Kt")]
            state0=contexts[0].getState(getParameterDerivatives=True,enforcePeriodicBox=False)
            energy_i=Ks0[0]*(state0.getEnergyParameterDerivatives())["Kb_tt"]+Ks0[1]*(state0.getEnergyParameterDerivatives())["Kt"]
            # context 1
            contexts[0].loadCheckpoint(checkpoints[sequence1])
            Ks1=[contexts[0].getParameter("Kb_tt"),contexts[0].getParameter("Kt")]
            state1=contexts[0].getState(getParameterDerivatives=True,enforcePeriodicBox=False)
            energy_j=Ks1[0]*(state1.getEnergyParameterDerivatives())["Kb_tt"]+Ks1[1]*(state1.getEnergyParameterDerivatives())["Kt"]
            # energy after the swap
            energy_i_exchange=Ks1[0]*(state0.getEnergyParameterDerivatives())["Kb_tt"]+Ks1[1]*(state0.getEnergyParameterDerivatives())["Kt"]
            energy_j_exchange=Ks0[0]*(state1.getEnergyParameterDerivatives())["Kb_tt"]+Ks0[1]*(state1.getEnergyParameterDerivatives())["Kt"]
            # print(energy_i,energy_j,energy_i_exchange,energy_j_exchange)
            factor=(energy_i+energy_j-energy_i_exchange-energy_j_exchange)*K_unit/kBT
            if factor>0.0:
                acceptation=True
            elif random.random()<=math.exp(factor):
                acceptation=True
            else:
                acceptation=False
            # replica exchange ?
            if acceptation:
                if sequence0==replica:
                    replica=sequence1
                elif sequence1==replica:
                    replica=sequence0
                else:
                    pass
                # swap
                contexts[0].loadCheckpoint(checkpoints[sequence0])
                contexts[0].setParameter("Kb_tt",Ks1[0])
                contexts[0].setParameter("Kt",Ks1[1])
                checkpoints[sequence0]=contexts[0].createCheckpoint()
                contexts[0].loadCheckpoint(checkpoints[sequence1])
                contexts[0].setParameter("Kb_tt",Ks0[0])
                contexts[0].setParameter("Kt",Ks0[1])
                checkpoints[sequence1]=contexts[0].createCheckpoint()
                acceptation_ratio+=1
            else:
                pass
        print("acceptation ratio="+str(float(acceptation_ratio)/float(R//2))+" ("+str(acceptation_ratio)+"/"+str(R//2)+")")
        # save the checkpoints
        for r in range(R):
            context.loadCheckpoint(checkpoints[r])
            read_write_checkpoint(context,path_to+"/replica"+str(r)+"/restart/restart."+polymer+".s"+str(current_iteration)+".chk","write")

def create_integrator(thermostat: str,Temp: unit.Quantity,dt: unit.Quantity,N: int,C: int,m: unit.Quantity,tau: unit.Quantity,seed: int,limit: bool,rescale: bool):
    """return a integrator with spatial step limit and/or rescaling of the velocities.
    Parameters
    ----------
    thermostat : "Gronech-Jensen-Farago" (gjf), "langevin" or "nve"
    Temp       : temperature (unit.Quantity)
    dt         : the time step in picosecond (unit.Quantity)
    N          : the number of beads (integer)
    C          : the number of constraints (integer)
    m          : mass of the beads (unit.Quantity)
    tau        : inverse of the coupling frequency to the thermostat (unit.Quantity)
    seed       : seed for the integrator (integer)
    limit      : limit for the ||x(t+dt)-x(t)|| (False or True)
    rescale    : scale the kinetic average to its thermal average value (False or True)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    if thermostat is not equal to "langevin" or "nve".
    """
    if thermostat=="langevin":
        integrator=mm.LangevinIntegrator(Temp,1.0/tau,dt)
        integrator.setRandomNumberSeed(seed)
        integrator.setConstraintTolerance(0.00001)
    elif thermostat=="gjf":
        integrator=gjf_integrator(dt,N,C,Temp,m,tau,seed,limit,rescale)
        integrator.setConstraintTolerance(0.00001)
    elif thermostat=="nve":
        integrator=verlet_integrator(dt,N,C,Temp,m,limit,rescale)
        integrator.setConstraintTolerance(0.00001)
    else:
        print("Please consider using 'nve', 'langevin' or 'gjf' for input 'thermostat' variable.")
        print("'thermostat' takes the value 'langevin' with 'Temp=300.0*unit.kelvin', 'tau=1.0/unit.picosecond' and 'dt=0.001*unit.picosecond'.")
        integrator=mm.LangevinIntegrator(300.0*unit.kelvin,1.0/unit.picosecond,0.001*unit.picosecond)
        integrator.setRandomNumberSeed(1)
        integrator.setConstraintTolerance(0.00001)
    return integrator
    
def gjf_integrator(dt: unit.Quantity,N: int,C: int,Temp: unit.Quantity,m: unit.Quantity,tau: unit.Quantity,seed: int,limit: bool,rescale: bool) -> mm.Integrator:
    """return a Gronbech-Jensen-Farago (GJF) Langevin integrator with spatial step limit and rescaling of the velocities.
    Parameters
    ----------
    dt      : the time step in picosecond (unit.Quantity)
    N       : the number of beads (integer)
    C       : the number of constraints (integer)
    Temp    : temperature of the system (unit.Quantity)
    m       : mass of the beads (unit.Quantity)
    tau     : inverse of the coupling frequency to the thermostat (unit.Quantity)
    seed    : seed for the integrator (integer)
    limit   : limit for the displacement ||x(t+dt)-x(t)|| (False or True)
    rescale : rescaling of the kinetic energy (False or True)
    Returns
    -------
    OpenMM integrator
    Raises
    ------
    """
    kBT=get_kBT(Temp)
    integrator=mm.CustomIntegrator(dt)
    integrator.addGlobalVariable("mke",(3*N*(get_particles_per_bead()-2)-C)*0.5*kBT)
    integrator.addGlobalVariable("ke",0.0*kBT)
    integrator.addGlobalVariable("a_gjf",(1.0-(dt*m/tau)/(2.0*m))/(1.0+(dt*m/tau)/(2.0*m)))
    integrator.addGlobalVariable("b_gjf",1.0/(1.0+(dt*m/tau)/(2.0*m)))
    integrator.addGlobalVariable("x_sigma_gjf",unit.sqrt((m/tau)*kBT*dt*dt*dt/(2.0*m*m)))
    integrator.addGlobalVariable("v_sigma_gjf",unit.sqrt(2.0*(m/tau)*kBT*dt/(m*m)))
    if limit:
        integrator.addGlobalVariable("dx_lim",dt*unit.sqrt(kBT/m))
    integrator.addPerDofVariable("f_n",0.0)
    integrator.addPerDofVariable("beta_n_1",0.0)
    integrator.addUpdateContextState()
    integrator.addComputePerDof("beta_n_1","gaussian")
    integrator.addComputePerDof("f_n","f")
    if limit:
        integrator.addComputePerDof("x","x+min(b_gjf*(dt*v+dt*dt*f_n/(2*m)+x_sigma_gjf*beta_n_1),dx_lim)")
    else:
        integrator.addComputePerDof("x","x+b_gjf*(dt*v+dt*dt*f_n/(2*m)+x_sigma_gjf*beta_n_1)")
    integrator.addConstrainPositions()
    integrator.addComputePerDof("v","a_gjf*v+(dt/(2*m))*(a_gjf*f_n+f)+b_gjf*v_sigma_gjf*beta_n_1")
    integrator.addConstrainVelocities()
    if rescale:
        integrator.addComputeSum("ke","0.5*m*v*v")
        integrator.addComputePerDof("v","v*sqrt(mke/ke)")
    integrator.setRandomNumberSeed(seed)
    return integrator

def verlet_integrator(dt: unit.Quantity,N: int,C: int,Temp: unit.Quantity,m: unit.Quantity,limit: bool,rescale: bool) -> mm.Integrator:
    """create and return a Velocity-Verlet integrator with spatial step limit and rescaling of the velocities
    Parameters
    ----------
    dt      : time step in picosecond (unit.Quantity)
    N       : number of particles (integer)
    C       : number of constraints (integer)
    Temp    : temperature of the system (unit.Quantity)
    m       : mass of the beads (unit.Quantity)
    limit   : limit for the displacement ||x(t+dt)-x(t)|| (False or True)
    rescale : rescaling of the kinetic energy (False or True)
    Returns
    -------
    OpenMM integrator
    """
    if limit or rescale:
        kBT=get_kBT(Temp)
        integrator=mm.CustomIntegrator(dt)
        integrator.addGlobalVariable("mke",(3*N*(get_particles_per_bead()-2)-C)*0.5*kBT)
        integrator.addGlobalVariable("ke",0.0*kBT)
        if limit:
            integrator.addGlobalVariable("dx_lim",dt*unit.sqrt(kBT/m))
        integrator.addPerDofVariable("x1",0.0)
        integrator.addUpdateContextState()
        integrator.addComputePerDof("v","v+0.5*dt*f/m")
        if limit:
            integrator.addComputePerDof("x","x+min(dx_lim,dt*v)")
        else:
            integrator.addComputePerDof("x","x+dt*v")
        integrator.addComputePerDof("x1","x")
        integrator.addConstrainPositions()
        integrator.addComputePerDof("v","v+0.5*dt*f/m+(x-x1)/dt")
        integrator.addConstrainVelocities()
        if rescale:
            integrator.addComputeSum("ke","0.5*m*v*v")
            integrator.addComputePerDof("v","v*(step(0.01-abs(1.0-mke/ke))+(1-step(0.01-abs(1.0-mke/ke)))*sqrt(mke/ke))")
    else:
        integrator=mm.VerletIntegrator(dt)
    return integrator

def get_info_system(context: mm.Context):
    """print the info about the system you created.
    Parameters
    ----------
    context : the OpenMM context
    Returns
    -------
    Raises
    ------
    """
    print("system uses PBC : "+str(context.getSystem().usesPeriodicBoundaryConditions()))
    print("default periodic box : "+str(context.getSystem().getDefaultPeriodicBoxVectors()))
    for f in range(context.getSystem().getNumForces()):
        print("force "+str(f))
        print('PBC '+str(context.getSystem().getForce(f).usesPeriodicBoundaryConditions()))
        try:
            context.getSystem().getForce(f).updateParametersInContext(context)
        except:
            print("no updateParametersInContext() for the current force")
        try:
            print('non bonded method '+str(context.getSystem().getForce(f).getNonbondedMethod())+'/'+str(mm.NonbondedForce.CutoffPeriodic))
            print("cutoff distance : "+str(context.getSystem().getForce(f).getCutoffDistance()))
            print("use long range correction : "+str(context.getSystem().getForce(f).getUseLongRangeCorrection()))
            print("use switching function : "+str(context.getSystem().getForce(f).getUseSwitchingFunction()))
            print(str(context.getSystem().getForce(f).getNumPerParticleParameters())+" per particle parameters")
            print(str(context.getSystem().getForce(f).getNumGlobalParameters())+" global parameters")
        except:
            error=True
    print(str(context.getSystem().getNumConstraints())+" constraints")
    for n in context.getPlatform().getPropertyNames():
        print(n+" : "+str(context.getPlatform().getPropertyValue(context,n)))

def create_context(system: mm.System,integrator: mm.Integrator,platform: mm.Platform,properties,positions: list,velocities: list) -> mm.Context:
    """create and return an OpenMM context.
    Parameters
    ----------
    system     : OpenMM system
    integrator : OpenMM integrator
    platform   : OpenMM platform
    properties : OpenMM properties
    positions  : positions of the beads (list of OpenMM.Vec3)
    velocities : velocities of the beads (list of OpenMM.Vec3)
    Returns
    -------
    OpenMM context
    Raises
    ------
    """
    context=mm.Context(system,integrator,platform,properties)
    context.setPositions(positions)
    context.setVelocities(velocities)
    return context

def get_platforms(name: str="") -> tuple:
    """return the platform index and device index corresponding to the name of the
    hardware you would like to use. Write it to a json file "get_platforms.json".
    The name of the hardware is like "GeForce_GTX_980_Ti_OpenCL".
    Do not forget to add "_OpenCL" or "_CUDA" to ask for the OpenCL implementation whatever it is CPU or GPU.
    Parameters
    ----------
    name : name of the hardware, default to "" (str)
    Returns
    -------
    index of the platform, index of the device (int,int).
    Raises
    ------
    exit if no platform and/or device (with name!="") have been found.
    """
    platform_and_device={}
    integrator_test=[]
    system_test=[]
    platform_test=[]
    context_test=[]
    itest=0
    stest=0
    ptest=0
    ctest=0
    index_of_platform=-1
    index_of_device=-1
    for c in ["CPU","CUDA","OpenCL"]:
        platform_and_device[c]={}
        for p in range(10):# loop over platforms
            for d in range(10):# loop over device
                try:
                    integrator_test.append(mm.VerletIntegrator(0.001))
                    itest+=1
                    system_test.append(mm.System())
                    system_test[stest].addParticle(1.0)
                    stest+=1
                    platform_test.append(mm.Platform.getPlatformByName(c))
                    ptest+=1
                    if c=="CPU":
                        context_test.append(mm.Context(system_test[stest-1],integrator_test[itest-1],platform_test[ptest-1],{"CpuThreads":"1"}))
                    elif c=="CUDA":
                        context_test.append(mm.Context(system_test[stest-1],integrator_test[itest-1],platform_test[ptest-1],{"CudaPlatformIndex":str(p),"CudaDeviceIndex":str(d)}))
                    elif c=="OpenCL":
                        context_test.append(mm.Context(system_test[stest-1],integrator_test[itest-1],platform_test[ptest-1],{"OpenCLPlatformIndex":str(p),"OpenCLDeviceIndex":str(d)}))
                    ctest+=1
                    current_DeviceName=(context_test[ctest-1].getPlatform().getPropertyValue(context_test[ctest-1],'DeviceName')).replace(" ","_")
                    platform_and_device[c][current_DeviceName]={}
                    for n in context_test[ctest-1].getPlatform().getPropertyNames():
                        platform_and_device[c][current_DeviceName][n]=context_test[ctest-1].getPlatform().getPropertyValue(context_test[ctest-1],n)
                    if (current_DeviceName+"_"+c)==name:# if the current device is what we ask for ...
                        index_of_platform=p
                        index_of_device=d
                except:
                    error=True
    with open("get_platforms.json","w") as out_file:
        json.dump(platform_and_device,out_file,indent=0)
    if (index_of_platform==-1 or index_of_device==-1) and name!="":
        print("No platform has been found : "+name)
        sys.exit()
    return index_of_platform,index_of_device

def create_platform(platform_name: str="CPU",threads: int=1,precision: str="double"):
    """create and return platform and properties based on :
    Parameters
    ----------
    platform_name : "CPU", "GeForce_GTX_780_OpenCL" ... (string)
    threads       : number of threads for the CPU platform (integer)
    precision     : single, mixed or double (string)
    Returns
    -------
    OpenMM platform, OpenMM properties
    Raises
    ------
    """
    if "CPU" in platform_name and not "OpenCL" in platform_name and not "CUDA" in platform_name:
        platform=mm.Platform.getPlatformByName("CPU")
        properties={"CpuThreads":str(threads)}
    elif "CUDA" in platform_name:
        platform=mm.Platform.getPlatformByName("CUDA")
        platform_index,device_index=get_platforms(platform_name)
        if precision=="double":
            if platform.supportsDoublePrecision():
                properties={"CudaPlatformIndex":str(platform_index),"CudaDeviceIndex":str(device_index),"CudaPrecision":"double"}
            else:
                properties={"CudaPlatformIndex":str(platform_index),"CudaDeviceIndex":str(device_index),"CudaPrecision":"single"}
        else:
            properties={"CudaPlatformIndex":str(platform_index),"CudaDeviceIndex":str(device_index),"CudaPrecision":precision}
    elif "OpenCL" in platform_name:
        platform=mm.Platform.getPlatformByName("OpenCL")
        platform_index,device_index=get_platforms(platform_name)
        if precision=="double":
            if platform.supportsDoublePrecision():
                properties={"OpenCLPlatformIndex":str(platform_index),"OpenCLDeviceIndex":str(device_index),"OpenCLPrecision":"double"}
            else:
                properties={"OpenCLPlatformIndex":str(platform_index),"OpenCLDeviceIndex":str(device_index),"OpenCLPrecision":"single"}
        else:
            properties={"OpenCLPlatformIndex":str(platform_index),"OpenCLDeviceIndex":str(device_index),"OpenCLPrecision":precision}
    return platform,properties

def create_system(L: unit.Quantity,N: int,m: unit.Quantity,F: int) -> mm.System:
    """create and return a system.
    Parameters
    ----------
    L : box size in nanometer (unit.Quantity)
    N : number of beads (integer)
    m : mass of the beads (unit.Quantity)
    F : remove the com motion every F steps (integer)
    Returns
    -------
    OpenMM system
    Raises
    ------
    """
    system=mm.System()
    system.setDefaultPeriodicBoxVectors(mm.Vec3(L,0.0,0.0),mm.Vec3(0.0,L,0.0),mm.Vec3(0.0,0.0,L))
    for i in range(get_particles_per_bead()*N):
        system.addParticle(m*int(i<N or (i>=(2*N) and i<(3*N))))# bead, virtual, pseudo and virtual
    # remove the COM motion every F steps
    system.addForce(mm.CMMotionRemover(F))
    return system

def read_write_checkpoint(context: mm.Context,name: str,mode: str):
    """read/write from/a OpenMM checkpoint file.
    Parameters
    ----------
    context : OpenMM context
    name    : name of the checkpoint file (string)
    mode    : read or wirte (string)
    Raises
    ------
    if no checkpoint file has been found in read mode.
    if mode is not equal to read or to write.
    """
    if mode=="read":
        try:
            with open(name,"rb") as in_file:
                context.loadCheckpoint(in_file.read())
        except IOError:
            print("no checkpoint file found : "+name)
    elif mode=="write":
        with open(name,"wb") as out_file:
            out_file.write(context.createCheckpoint())
    else:
        print("Please consider using read or write option.")
        sys.exit()

def get_bonds(context: mm.Context) -> int:
    """return the number of bonds in the context.getSystem().
    Parameters
    ----------
    context : OpenMM context (OpenMM.Context)
    Returns
    -------
    the number of bonds in the context.getSystem() (integer).
    Raises
    ------
    """
    F=context.getSystem().getNumForces()
    n_bonds=0
    for f in range(F):
        try:
            n_bonds+=context.getSystem().getForce(f).getNumBonds()
        except:
            pass
    return n_bonds

def get_number_of_chains(context: mm.Context) -> int:
    """return the number of chains in the context.getSystem().
    it assumes the number of beads per chain to be the same.
    Parameters
    ----------
    context : OpenMM context (OpenMM.Context)
    Returns
    -------
    the number of bonds in the context.getSystem() (integer).
    Raises
    ------
    """
    # C chains, N particles per chain, C*(N-1) bonds
    # return context.getSystem().getNumParticles()-get_bonds(context)
    return 1

@numba.jit(int32())
def get_particles_per_bead() -> int:
    return 4

@numba.jit(int32(int32,int32))
def get_virtual_p_from_i(i: int,N: int) -> int:
    return i+N

@numba.jit(int32(int32,int32))
def get_pseudo_u_from_i(i: int,N: int) -> int:
    return i+2*N

@numba.jit(int32(int32,int32))
def get_virtual_mid_from_i(i: int,N: int) -> int:
    return i+3*N
