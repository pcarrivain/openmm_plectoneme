# Plectonemes with OpenMM

I am trying to use the [OpenMM](http://openmm.org/)
functionalities to simulate DNA and bacterial plectonemes.
In particular, I built a frame that is
attached to every bond of the polymer model
we consider (here we use FENE model).
This frame is used to calculate the twist
and the bending angles and apply torsionnal
constraints as well.
A detailed description of the frame implementation
can be find
[here](https://gitlab.com/pcarrivain/openmm_plectoneme/-/blob/master/openmm_plectoneme.pdf).

---

**Background**

Bacterial DNA is known to form specific conformations called *plectonemes*
because of internal twisting constraints.
This physical mechanism participates to the compaction of the genome.
The *plectonemes* are braided structures you often experiment with phone cables.
In order to study such a system we need to introduce a
[linking number](https://en.wikipedia.org/wiki/Linking_number)
deficit into a circular polymer.
The Linking number ($`Lk=Tw+Wr`$) is the sum of the
twist ($`Tw`$, cumulative helicity of the DNA)
and the writhe ($`Wr`$, global intracity).
In the case of circular DNA that is topologically constrained any variation of
the twist affects the Writhe and therefore the conformation.
In particular, does slow change of the twist lead to the same conformation
that the one we get from a rapidly change in the twist ?
We then tackle the question: does the introduction protocol of
[linking number](https://en.wikipedia.org/wiki/Linking_number) inside
circular molecule matter ?
Indeed, does a rapidly
[linking number](https://en.wikipedia.org/wiki/Linking_number)
injection freeze the conformation in
braided structures where *plectonemes* do not merge/move along the DNA ?
Does the memory of initial conformation matter ?

**Building, test and run**

To install the module you simply need to clone the repository.
I use a conda environment and python 3.7.
The module *openmm_plectoneme* uses :

1. [OpenMM](http://openmm.org/)
2. [Numba](http://numba.pydata.org)
3. [Dask](https://dask.org)
4. [Numpy](numpy.org)
5. [matplotlib](https://matplotlib.org/)
6. [pydoc3.7](https://docs.python.org/3/library/pydoc.html) to generate the documentation (html format)

The list of scripts is :

1. openmm_plectoneme.py is an example script to run overtwisted circular DNA
2. single_molecule.py is an example script to run single molecule experiment (WIP)
3. openmm_plectoneme_replica_exchange.py is an example script to run replica exchange (WIP)
4. openmm_plectoneme_functions.py is a collection of functions
5. unittest_openmm_plectoneme_functions.py is unittest script
6. data_analysis.py is a module with some functionalities to run polymer analysis
   [up-to-date version](https://gitlab.com/pcarrivain/bacteria_analysis)

You can also find a
[jupyter-notebook](https://gitlab.com/pcarrivain/openmm_plectoneme/-/blob/master/demonstration/openmm_plectoneme_demonstration.ipynb)
as a demonstration of the module **openmm_plectoneme_functions**.
It is currently not up-to-date.

You can use
[data analysis module](https://gitlab.com/pcarrivain/bacteria_analysis)
to analyze polymer conformations.

I provide a bash script
[run_test.sh](https://gitlab.com/pcarrivain/openmm_plectoneme/-/blob/master/run_test.sh)
to generate documentation and to run tests.

I provide a bash script
[run_model.sh](https://gitlab.com/pcarrivain/openmm_plectoneme/-/blob/master/run_model.sh)
that creates folder according to the parameters you choos
and run the simulation.

All the bash scripts come with the option -h to get the help.

Bending and twisting following sections include tests.

---

**Bending**

The bending contribution $`E_b`$ to the total potential energy of the system is :
```math
E_b=g_b(1-\cos\theta)
```
As a test example I ran a simulation of a linear DNA polymer
($`100`$ bonds, 10.5 bp per bond and no excluded volume)
with the following command :
```bash
/scratch/pcarriva/anaconda3/bin/python3.7 openmm_plectoneme.py -o 0.0 -n 100 -r 10.5 -s 1 -R 0 -i circular -g GeForce_GTX_1660_Ti_OpenCL -t gjf -c no -l yes --start=0 --iterations=4000000 --every=10000 --test_internal_distances --precision=single
```
and I plot the
[internal distances $`\left\langle R(s)^2\right\rangle`$](https://gitlab.com/pcarrivain/openmm_plectoneme/-/blob/master/Rs2/Rs2.png)
I measure from the simulation that is in
pretty good agreement with the
[Worm-Like-Chain](https://en.wikipedia.org/wiki/Worm-like_chain) theory.
At the end of the simulation it plots the following average:
$`\sqrt{\left\langle\left(1-\frac{\text{simulation}}{\text{theory}}\right)^2\right\rangle}`$
over first half of the chain.

**Twisting**

The twisting contribution $`E_t`$ to the total potential energy of the system is :
```math
E_t=g_t(1-\cos\text{Tw})
```
The twist angle $`\text{Tw}`$ definition is based on the orientation of two frames.
The previous example tests the bending implementation.
I propose the following example where I measure
the average of the square twist angle $`\text{Tw}^2`$ over a ring chain.
```bash
/scratch/pcarriva/anaconda3/bin/python3.7 openmm_plectoneme.py -o 0.0 -n 16392 -r 10.5 -s 1 -R 0 -i circular -g GeForce_RTX_2060_SUPER_OpenCL -t gjf -c wca -l no --start=0 --iterations=1000000 --every=100000 --test_twist_implementation --precision=single
```
The simulation runs a circular chain (```bash -l no```)
made of $`16392`$ FENE bonds ($`10.5~\text{bp}`$ each ```bash -b fene -r 10.5```)
at $`\sigma=0.0`$ overtwist, seed is $`1`$ (```bash -s 1```),
platform is GeForce RTX 2060 SUPER with OpenCL implementation (```bash -g GeForce_RTX_2060_SUPER_OpenCL```)
and single precision (```bash --precision=single```).
You can also use rigid bond with ```bash -b rigid```.
I choose
[global-thermostat](https://www.sciencedirect.com/science/article/pii/S0010465508000106?via%3Dihub) (```bash -t global```)
and
[WCA](http://www.sklogwiki.org/SklogWiki/index.php/Weeks-Chandler-Andersen_perturbation_theory)
repulsive potential ```bash -c wca```.
The simulation average the local squared twist over the chain (```bash --test_twist_implementation```)
every $`100000`$ steps for $`1000000`$ iterations
(```bash -iterations=1000000 --every=100000 --test_twist_implementation```).
The option ```bash --bp_per_nm3``` is work-in-progress
(it would give the size of the box such that the number of base-pair per cubic nanometer holds).
I used a twisting constraint strength that is the persistance
length of the DNA $`l_t=86.0~\text{nm}`$
divided by the size of the bond $`l=3.57~\text{nm}`$.
I got the average from simulation $`0.0418\pm 0.0005`$ (mean and standard error)
that is in good agreement with the theoritical value of $`\frac{l}{l_t}=0.0407`$.

**Thermostat**

In the previous example I use the
[global thermostat](https://www.sciencedirect.com/science/article/pii/S0010465508000106?via%3Dihub)
I implemented as an
[OpenMM CustomIntegrator](https://simtk.org/api_docs/openmm/api4_1/python/classsimtk_1_1openmm_1_1openmm_1_1CustomIntegrator.html).
This thermostat is known to lead to the
[Flying-Ice-Cube](https://onlinelibrary.wiley.com/doi/abs/10.1002/%28SICI%291096-987X%28199805%2919%3A7%3C726%3A%3AAID-JCC4%3E3.0.CO%3B2-S)
phenomena.
Therefore, we need to periodically remove the linear
and angular motion with respect to the center-of-mass of the system.
I added a function *correction_flying_ice_cube* you can
call outside of the
[OpenMM context](https://simtk.org/api_docs/openmm/api10/classOpenMM_1_1Context.html).
To avoid calculation outside the context I added
the correction directly in the
[CustomIntegrator](http://docs.openmm.org/latest/api-python/generated/simtk.openmm.openmm.CustomIntegrator.html).
You can choose the local version of the Langevin (```bash -t langevin```)
thermostat already implemented in OpenMM.
You can choose the
[Gronbech-Jensen-Farago](https://www.tandfonline.com/doi/abs/10.1080/00268976.2012.760055).
I implemented using
[OpenMM CustomIntegrator](https://simtk.org/api_docs/openmm/api4_1/python/classsimtk_1_1openmm_1_1openmm_1_1CustomIntegrator.html)
class from OpenMM.
In particular, I added a limit displacement and rescaling of the velocities options.

**Initial conformation**

You can choose to start from circular, linear or helix conformations.
```bash
-i circular
-i linear
-i helix_in30
-i helix_out30
```
You can close the helix inside or outside.
The number of helix turns is limited by the polymer length.

To build an initial conformation from
[Worm-Like-Chain](https://en.wikipedia.org/wiki/Worm-like_chain)
equilibrium conformations please consider the function:
```python
@numba.jit(nopython=True)
def wlc_conformation(ps: np.ndarray,us: np.ndarray,vs: np.ndarray,ts: np.ndarray,lb: float,lp: float,lt: float,Kb: float,Kt: float,Temp: float,L: float,mcs: int=1000,seed: int=1) -> tuple:
"""return equilibrated WLC conformation (Monte-Carlo with pivot moves).
    Parameters
    ----------
    ps   : positions of the beads (np.ndarray)
    us   : normals (np.ndarray)
    vs   : cross(t,u) (np.ndarray)
    ts   : tangents (np.ndarray)
    lb   : bond length in nm (float)
    lp   : persistence length in nm (float)
    lt   : twisting persistence length in nm (float)
    Kb   : bending rigidity in kBT (float)
           Eb = Kb * (1 - cos(bending))
    Kt   : twisting rigidity in kBT (float)
           Et = 0.5 * Kt * tw^2
    Temp : temperature in kelvin (float)
    L    : box edge (float)
    mcs  : number of Monte-Carlo steps (int)
    seed : seed for the numpy pRNGs (int)
           if seed < 0, do nothing
    Returns
    -------
    np.ndarray
    Raises
    ------
    """
```
It works for linear molecule.
The circular version is work-in-progress.

**Run**

To run a DNA molecule of $`8192\times 10.5`$ base-pairs
with FENE bonds,
[Gronbech-Jensen-Farago](https://www.tandfonline.com/doi/abs/10.1080/00268976.2012.760055)
thermostat with helix initial conformation:
```bash
./run_model.sh -s 1 -t gjf -b fene -o 0.0 -r 10.5 -R 0 -i helix_in30 -g GeForce_RTX_2060_SUPER_OpenCL -l no -N 8192 -P single -S 0 -E no -I 1000000 -e 100000
```

To run a DNA molecule of $`8192\times 10.5`$ base-pairs
with FENE bonds,
[Gronbech-Jensen-Farago](https://www.tandfonline.com/doi/abs/10.1080/00268976.2012.760055)
thermostat with circular initial conformation,
zero overtwist and $`-0.02`$ overtwist:
```bash
./run_model.sh -s 1 -t gjf -b fene -o 0.0 -r 10.5 -R 0 -i circular -g GeForce_RTX_2060_SUPER_OpenCL -l no -N 8192 -P single -S 0 -E no -I 1000000 -e 100000
./run_model.sh -s 1 -t gjf -b fene -o -0.02 -r 10.5 -R 0 -i circular -g GeForce_RTX_2060_SUPER_OpenCL -l no -N 8192 -P single -S 0 -E no -I 1000000 -e 100000
```

The three previous example saves conformation file
(three columns x y z format)
every $`100000`$ time-steps and run for $`1000000`$ iterations.
The script print a log like every $`100000`$ time-steps.
It uses GeForce RTX 2060 Super with OpenCL implementation
and single precision.

You can find the data in the folder:
```bash
ring/wca/gjf/GeForce_RTX_2060_SUPER_OpenCL/single/run_10.5bp_C1_N8192_0.0/seed1/helix_in30/rerun0/
ring/wca/gjf/GeForce_RTX_2060_SUPER_OpenCL/single/run_10.5bp_C1_N8192_0.0/seed1/circular/rerun0/
ring/wca/gjf/GeForce_RTX_2060_SUPER_OpenCL/single/run_10.5bp_C1_N8192_-0.02/seed1/circular/rerun0/
```
and a json file with the simulation parameters.

**Writhe**

Super-braided bacterial DNA conformation arises from
internal twisting constraints.
Overtwist is related to the linking number deficit $`\Delta\text{Lk}`$
that itself is related to writhe and twist of the DNA.
I implemented the *writhe* double integral computation from
[Klenin & Langowski](https://onlinelibrary.wiley.com/doi/full/10.1002/1097-0282%2820001015%2954%3A5%3C307%3A%3AAID-BIP20%3E3.0.CO%3B2-Y)
with the help of HPC software [Dask](https://dask.org)
and
[Numba](http://numba.pydata.org)
to speed-up nested loop computation.
The function is helpful when you would like
to check the relation $`\text{Lk}=\text{Wr}+\text{Tw}`$
between linking number, writhe and twist
([Calugareanu theorem](https://www.pnas.org/content/68/4/815)).
You can find an
[example](https://gitlab.com/pcarrivain/openmm_plectoneme/-/tree/master/helix/writhe_helix.py)
of writhe calculation for helix curves in the
[folder](https://gitlab.com/pcarrivain/openmm_plectoneme/-/tree/master/helix).

**Replica exchange (WIP)**

Of note, we wrote a function to run a
[replica exchange](https://en.wikipedia.org/wiki/Parallel_tempering)
in the mechanical space parameters of the bacterial DNA.
The function uses High-Performance-Computing
[Dask jobqueue](https://dask.org) software.
This work is still in progress.
Please consider to run the following command:
```bash
./run_model.sh -s 1 -t gjf -b fene -o 0.0 -r 10.5 -R 0 -i circular -g CPU1 -l no -N 4096 -P double -S 0 -E yes -B yes -I 10000 -e 1000 -D yes -a /scratch/anaconda3/bin
```
It runs circular DNA made of $`4096`$ bonds with
[Dask joqueue](https://jobqueue.dask.org/en/latest/)
implementation, no overtwist, at $`10.5`$ base-pairs resolution.
It swaps replica every $`1000`$ molecular dynamics time-steps.
Total number of iterations is $`10000`$.
By default

**Plectonemes detection (WIP)**

For each contact between monomer $`i`$ and monomer $`j`$
we calculate the relative shape anisotropy factor $`\kappa_{ij}^2`$
from $`i`$ to $`j`$ and $`\kappa_{ji}^2`$ from $`j`$ to $`i`$.
We calculate the $`\kappa_0^2`$ for the circular polymer too.
The $`i`$ to $`j`$ part is a plectoneme if
$`\kappa_{ij}^2>\kappa_{ji}^2`$, $`\kappa_{ij}^2>\kappa_0^2`$
and $`\kappa_{ij}^2>\ \kappa_c^2`$ are satisfied.
We need two cut-off : one for the contact definition and one
for the relative shape anisotropy $`\kappa_c^2=0.9`$.
You can ask for the function *get_plectonemes* to return the
plectonemes from the conformation under overtwist constraint.
The function is still experimental.

**Single-molecule experiment (WIP)**

We can use this module to model single-molecule DNA under
[magnetic or optical tweezers](https://en.wikipedia.org/wiki/Magnetic_tweezers)
too.
In this kind of setup the molecule is clamped on a plate and
to a magnetic bead at the other extremity.
The bead is used to apply stretching force and/or rotational constraint.
The position of the bead is used to monitor the response of
the molecule to the mechanical constraints.
From the mechanical constraints you can extract the mechanical
properties of your molecule of interest.
The module provides the function *external_force*
to apply a force to one extremity
of the molecule while the function *plane_repulsion*
mimics the plate on which the
DNA is anchored/clamped. I am currently implementing
a function to clamp the frame
that is anchored to the plate. It is needed to avoid
the torque applied to the
other extremity vanishes. I implemented a function
*add_torque_to_bond* to apply
a torque to a bond (along the tangent).
This work is still in progress.

**Data analysis**

[up-to-date version](https://gitlab.com/pcarrivain/bacteria_analysis)
of data analysis module.

**Visualisation with [Blender](https://www.blender.org)**

[up-to-date version](https://gitlab.com/pcarrivain/visualisation_with_blender)
of visualisation with Blender module.
Use
```bash
-B yes
```
to save file for visualisation with [Blender](https://www.blender.org).