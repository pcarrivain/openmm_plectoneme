#!/usr/bin/python
# -*- coding: utf-8 -*-
"""@package docstring
The module 'openmm_plectoneme_functions.py' is a collection of functions used to define
a Kremer-Grest polymer with bending and twisting rigidities.
This module goes with a python script 'single_molecule.py' that is an example on how to
create and run your model. In particular, the script creates a linear DNA molecule and
applies a torque T to one extremity and a torque -T to the other extremity.
The following code (has been developped by Pascal Carrivain) is distributed under MIT licence.
"""
import math
import numpy as np
import sys
import getopt
import simtk.openmm as mm
import simtk.unit as unit
# import openmm_plectoneme_functions4 as opf
import openmm_plectoneme_functions as opf
from operator import add

def main(argv):
    seed=1
    resolution=10.5
    overtwist=0.0
    force=0.5
    torque=0.25
    N=1000
    bp_per_nm3=0.0001
    P=1
    device=0
    platform_name='CPU'
    PBC=False
    mode="run"
    pair_style="wca"
    rerun=0
    bond="fene"
    thermostat="langevin"
    linear=True
    precision="double"
    start=0
    try:
        opts,args=getopt.getopt(argv,"ho:F:T:n:p:r:s:R:g:m:b:t:c:l:",["overtwist=","force=","torque=","chain_length=","np=","resolution=","seed=","rerun=","platform_name=","bp_per_nm3=","mode=","bond=","thermostat=","pair_style=","linear=","start=","precision=","tmpdir=","get_platforms"])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)
    for opt,arg in opts:
        if opt=='-h':
            print("python3.7 single_molecule.py -o <overtwist> -F <force in pico-Newton> -T <torque in kBT units> -n <chain length, number of bonds> -p <np> -r <resolution> -s <seed> -R <rerun> -g <platform name> --bp_per_nm3=<bp per nm^3> -m <mode> -b <bond type(fene,rigid)> -t <thermostat(gjf,langevin,nve)> -c <pairwise style(wca)> -l <linear(no)> --precision=<single,mixed,double> --tmpdir=<where to save the data>")
            print("/scratch/pcarriva/anaconda3/bin/python3.7 single_molecule.py -F 0.3 -T 0.25 -n 800 -p 1 -r 10.5 -s 1 -g OpenCL --bp_per_nm3=0.0001 -m run -b fene -t langevin -l no --precision=double")
            print("/scratch/pcarriva/anaconda3/bin/python3.7 single_molecule.py --get_platforms prints the available platforms and devices and exit.")
            print("/scratch/pcarriva/anaconda3/bin/pydoc3.7 -w /scratch/pcarriva/simulations/openmm_plectoneme/openmm_plectoneme_functions.py")
            sys.exit()
        elif opt=="--get_platforms":
            opf.get_platforms()
            sys.exit()
        elif opt in ("-o","--overtwist"):
            overtwist=float(arg)
            overtwist=0.0
        elif opt in ("-F","--force"):
            force=float(arg)
        elif opt in ("-T","--torque"):
            torque=float(arg)
        elif opt in ("-n","--chain_length"):
            N=int(arg)
        elif opt in ("-p","--processors"):
            P=int(arg)
            device=0
        elif opt in ("-r","--resolution"):
            resolution=float(arg)
        elif opt in ("-s","--seed"):
            seed=int(arg)
        elif opt in ("-R","--rerun"):
            rerun=int(arg)
        elif opt in ("-g","--platform_name"):
            platform_name=str(arg)
        elif opt in ("--bp_per_nm3"):
            bp_per_nm3=float(arg)
        elif opt in ("-m","--mode"):
            mode=str(arg)
            mode="run"
        elif opt in ("-b","--bond"):
            bond=str(arg)
        elif opt in ("-t","--thermostat"):
            thermostat=str(arg)
        elif opt in ("-c","--pair_style"):
            pair_style=str(arg)
            pair_style="wca"
        elif opt in ("-l","--linear"):
            linear=bool(arg=="yes")
        elif opt in ("--start"):
            start=int(arg)
        elif opt in ("--precision"):
            precision=arg
        elif opt in ("--tmpdir"):
            tmpdir=arg

    # type of polymer
    polymer=["ring","linear"][int(linear)]
    # path to ... (you have to change)
    if overtwist==0.0:
        path_to=tmpdir+"/"+polymer+"/"+pair_style+"/"+thermostat+"/"+platform_name+"/"+precision+"/run_"+str(resolution)+"bp_C1_N"+str(N)+"_F"+str(force)+"_T"+str(torque)+"/seed"+str(seed)+"/rerun"+str(rerun)
    else:
        path_to=tmpdir+"/"+polymer+"/"+pair_style+"/"+thermostat+"/"+platform_name+"/"+precision+"/run_"+str(resolution)+"bp_C1_N"+str(N)+"_"+str(overtwist)+"/seed"+str(seed)+"/rerun"+str(rerun)
    # number of beads from the number of bonds
    B=N+int(linear)
    # Kuhn length in bp
    k_bp=300.0
    # Temperature in Kelvin
    Temp=300.0*unit.kelvin
    # torque, 1 kBT ~ 4.141947 pN.nm
    torque=torque*opf.get_kBT(Temp)
    # mass
    m=resolution*700.0*unit.amu
    # bond size in nm
    sigma=(100.0*resolution/k_bp)*unit.nanometer
    # box size in nm
    L=math.pow(N*resolution/bp_per_nm3,1.0/3.0)*unit.nanometer
    # bending strength
    Kb=opf.bending_rigidity(k_bp,resolution,Temp)
    # twisting strength
    Kt=opf.twisting_rigidity(95.0*unit.nanometer,sigma,Temp)
    # time scale
    tau=int(thermostat=="nve")*sigma*unit.sqrt(m/Kt)+int(thermostat=="langevin" or thermostat=="gjf")*sigma*unit.sqrt(m/opf.get_kBT(Temp))
    # positions and frames u,v,t
    positions,u,v,t=opf.puvt_single_molecule(N,sigma)
    # system creation (com motion removed every 5 steps)
    system=opf.create_system(L,B,m,5)
    # virtual sites
    virtual_sites,positions=opf.create_virtual_sites(positions,u,sigma)
    # link the virtual site to its bead
    opf.link_virtual_sites(system,virtual_sites)

    # creation of the system, integrator, platform and context
    # type of bond
    if bond=="fene":
        # Kremer-Grest model
        system.addForce(opf.fene(N,Temp,sigma,linear,PBC))
    elif bond=="rigid":
        # rigid bond
        opf.rigid_bond(system,N,sigma,linear)
    else:
        # Kremer-Grest model by default
        system.addForce(opf.fene(N,Temp,sigma,linear,PBC))
    # harmonic pseudo-u vector
    system.addForce(opf.virtual_harmonic(B,8.0*opf.get_kBT(Temp),sigma,PBC))
    # wca
    system.addForce(opf.wca(N,Temp,sigma,linear,PBC))
    system.addForce(opf.wca_virtual_sites(N,Temp,sigma,linear,PBC))
    # bending
    system.addForce(opf.tt_bending(N,Kb,linear,PBC))
    system.addForce(opf.ut_bending(N,64.0*opf.get_kBT(Temp),linear,PBC))
    # twist
    system.addForce(opf.twist(N,Kt,[0.0]*(N-int(linear)),linear,PBC))
    # clamp first bead
    system.addForce(opf.clamp_bead(0,2.0*opf.get_kBT(Temp),positions[0][0],positions[0][1],positions[0][2]))
    # add stretching force
    if force!=0.0:
        system.addForce(opf.external_force(N,"z",force*unit.piconewton))
    # add torques at both extremities
    if torque._value!=0.0:
        system.addForce(opf.add_torque_to_bond(0,N,-torque,linear,PBC))
        system.addForce(opf.add_torque_to_bond(N-1,N,torque,linear,PBC))
    # creating the platform
    platform,properties=opf.create_platform(platform_name,P,precision)
    # creating the integrator
    integrator=opf.create_integrator(thermostat,Temp,0.01*tau,B,system.getNumConstraints(),m,4.0*256.0*tau,seed,False,True)
    # creating the context
    context=opf.create_context(system,integrator,platform,properties,positions,opf.v_gaussian(B,system.getNumConstraints(),Temp,m,seed))
    # get info about the system you just created
    opf.get_info_system(context)

    # variables for the contact matrix
    contact_ij=[0]
    ind_contact_ij=[0]
    # sample sizes for the contact matrix average over time
    sample=0
    ind_sample=0
    # variables for the contact probability P(s)
    Ps=[0.0]
    ind_Ps=[0.0]
    # variables for the internal distances R(s)
    cRs2=[0.0]*B
    sample_Rs2=0
    # variables for R_g^2
    Rs2_linear=(N*resolution/k_bp)*100.0*100.0
    Rg2_linear=(N*resolution/k_bp)*100.0*100.0/(12.0-6.0*int(linear))
    # variables for the cumulative of the twist^2
    cum_Tw2=[0.0]*(N-int(linear))
    sample_Tw2=0

    # do we have an OpenMM checkpoint to start from ?
    opf.read_write_checkpoint(context,path_to+"/restart/restart.s"+str(start)+".chk","read")
    iterations=start
    # number of iterations before OpenMM handing back to you
    dSTEP=100000
    # running the model
    while iterations<(start+40000000):
        # step the context
        context.getIntegrator().step(dSTEP)
        iterations+=dSTEP
        # get the positions (in nanometer) for further analysis
        positions=opf.get_positions_from_context(context)
        # write and draw the conformation
        opf.write_draw_conformation(positions,0.5*B*sigma/unit.nanometer,[0.7,1.3,0.7,1.3,0.7,1.3],path_to+"/out/xyz."+str(iterations)+".out",path_to+"/png/xyz."+str(iterations)+".png","write_draw")
        # gyration radius
        Rg2,asphericity,acylindricity,kappa2,P=opf.Rg2_and_shape(positions,0,B-1,linear)
        opf.draw_Rg2(iterations,Rg2,Rg2_linear,linear,path_to+"/Rg2/Rg2")
        opf.draw_kappa2(iterations,kappa2,path_to+"/Rg2/kappa2")
        # # get the plectonemes
        # plectonemes=opf.get_plectonemes(positions,1.25*sigma/unit.nanometer,int(100.0*unit.nanometer/sigma)+1,linear)
        # opf.draw_plectonemes(plectonemes,positions,0.5*B*sigma,path_to+"/plectonemes/plectonemes"+str(iterations)+".png")
        # opf.draw_plectonemes_stats(plectonemes,positions,iterations,path_to+"/plectonemes")
        # print information
        opf.print_variables(context,linear,iterations)
        if (iterations%dSTEP)==0 and iterations>=10000000:
            # internal distances
            Rs2=opf.Rs2(positions,0,B-1,linear)
            cRs2=[cRs2[i]+r for i,r in enumerate(Rs2)]
            sample_Rs2+=1
            opf.draw_Rs2([c/sample_Rs2 for c in cRs2],50.0,sigma/unit.nanometer,path_to+"/png/Rs2")
            # contact matrix with indicative function to define +1 contact
            contact_ij,Ps,sample=opf.ind_contact_matrix(contact_ij,Ps,sample,2,positions,0.0,100.0,linear)
            ind_contact_ij,ind_Ps,ind_sample=opf.ind_contact_matrix(ind_contact_ij,ind_Ps,ind_sample,2,positions,40.0,60.0,linear)
            opf.draw_symmetric_matrix(contact_ij,path_to+"/png/contact_matrix_s"+str(iterations),True)
            opf.draw_symmetric_matrix(ind_contact_ij,path_to+"/png/ind_contact_matrix_s"+str(iterations),True)
            # draw the contact probability
            opf.draw_Ps(Ps,path_to+"/png/Ps")
            opf.draw_Ps(ind_Ps,path_to+"/png/ind_Ps")
            # get the twist along the chain
            u,v,t=opf.get_uvt_from_positions(opf.get_vpositions_from_context(context))
            Tw_i=opf.chain_twist(u,v,t,linear,True,path_to+"/png")
            # square of the twist
            cum_Tw2=[sum(e) for e in zip(cum_Tw2,[t*t for t in Tw_i])]# cum_Tw2=list(map(add,cum_Tw2,[t*t for t in Tw_i]))
            sample_Tw2+=1
    # write a checkpoint to restart the simulation
    opf.read_write_checkpoint(context,path_to+"/restart/restart.s"+str(iterations)+".chk","write")
    # print the average of Tw^2
    print(sum(cum_Tw2)/(sample_Tw2*len(cum_Tw2)),sigma/(95.0*unit.nanometer))

if __name__=="__main__":
    main(sys.argv[1:])
